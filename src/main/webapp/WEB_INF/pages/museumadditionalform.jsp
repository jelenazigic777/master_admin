<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.museum.form.additional"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>

    <body>
        <spring:message code="placeholder.type" var="typePlaceholder"/>
        <spring:message code="placeholder.longitude" var="xPlaceholder"/>
        <spring:message code="placeholder.latitude" var="yPlaceholder"/>
        <spring:message code="placeholder.z" var="zPlaceholder"/>
        <spring:message code="placeholder.distance" var="distancePlaceholder"/>
        <spring:message code="placeholder.address" var="addressPlaceholder"/>
        <spring:message code="placeholder.zipcode" var="zipcodePlaceholder"/>
        <spring:message code="placeholder.town" var="townPlaceholder"/>
        <spring:message code="placeholder.township" var="townshipPlaceholder"/>
        <spring:message code="placeholder.district" var="districtPlaceholder"/>
        <spring:message code="placeholder.openyear" var="openyearPlaceholder"/>
        <spring:message code="placeholder.buildyear" var="buildyearPlaceholder"/>
        <spring:message code="placeholder.aims" var="aimsPlaceholder"/>
        <spring:message code="placeholder.contactphones" var="contactphonesPlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <section>
            <div class="container">
                <h3 class="center-headline">${title}</h3>
                <form:form action="museums/museum/additionalform" class="form-signin"
                  method="POST"
                  role="form"
                  commandName="museumadditional">

                    <h6 class="success-message">${successMessage}</h6>

                    <label class="section-title"><spring:message code="basic.insert"/></label>

                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <input type="hidden" name="idinstitution" value="${idinstitution}">

                    <form:hidden path="idmuseumadditional" name="idmuseumadditional"/>
                    <div class="form-group row">
                        <label for="type" class="col-sm-4 col-form-label">${typePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="int" path="type" name="type" placeholder="${museumadditional.type}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="type" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="buildyear" class="col-sm-4 col-form-label">${buildyearPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="int" path="buildyear" name="buildyear" placeholder="${museumadditional.buildyear}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="buildyear" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="openyear" class="col-sm-4 col-form-label">${openyearPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="int" path="openyear" name="openyear" placeholder="${museumadditional.openyear}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="openyear" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="aims" class="col-sm-4 col-form-label">${aimsPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="aims" name="aims" placeholder="${museumadditional.aims}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="aims" cssClass="form-error-field"/></label>


                    <label class="section-title"><spring:message code="location.insert"/></label>

                    <div class="form-group row">
                        <label for="xcoordinate" class="col-sm-4 col-form-label">${xPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="double" path="xcoordinate" name="xcoordinate" placeholder="${museumadditional.xcoordinate}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="xcoordinate" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="ycoordinate" class="col-sm-4 col-form-label">${yPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="double" path="ycoordinate" name="yxcoordinate"
                              placeholder="${museumadditional.ycoordinate}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="ycoordinate" cssClass="form-error-field"/></label>

                    <%--<div class="form-group row">--%>
                        <%--<label for="zcoordinate" class="col-sm-4 col-form-label">${zPlaceholder}</label>--%>
                        <%--<div class="col-sm-8">--%>
                            <%--<form:input type="double" path="zcoordinate" name="zcoordinate" placeholder="${museumadditional.zcoordinate}"--%>
                              <%--class="form-control"></form:input>--%>
                        <%--</div>--%>
                    <%--</div>--%>
                    <%--<label><form:errors path="zcoordinate" cssClass="form-error-field"/></label>--%>

                    <div class="form-group row">
                        <label for="distance" class="col-sm-4 col-form-label">${distancePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="double" path="distance" name="distance" placeholder="${museumadditional.distance}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="distance" cssClass="form-error-field"/></label>

                    <label class="section-title"><spring:message code="address.info"/></label>

                    <div class="form-group row">
                        <label for="address" class="col-sm-4 col-form-label">${addressPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="address" name="address" placeholder="${museumadditional.address}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="address" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="town" class="col-sm-4 col-form-label">${townPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="town" name="town" placeholder="${museumadditional.town}" rows="4"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="town" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="town" class="col-sm-4 col-form-label">${zipcodePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="zipcode" name="zipcode" placeholder="${museumadditional.zipcode}" rows="4"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="town" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="township" class="col-sm-4 col-form-label">${townshipPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="township" name="township" placeholder="${museumadditional.township}"
                              rows="4"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="township" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="district" class="col-sm-4 col-form-label">${districtPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="district" name="district" placeholder="${museumadditional.district}"
                              rows="4"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="district" cssClass="form-error-field"/></label>

                    <label class="section-title"><spring:message code="contact.info"/></label>

                    <div class="form-group row">
                        <label for="contactphones" class="col-sm-4 col-form-label">${contactphonesPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:textarea type="text" path="contactphones" name="contactphones"
                              placeholder="${museumadditional.contactphones}" rows="4"
                              class="form-control"></form:textarea>
                        </div>
                    </div>
                    <label><form:errors path="contactphones" cssClass="form-error-field"/></label>

                    <div>
                        <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
                    </div>

                </form:form>

            </div>
        </section>
    </body>
</html>

