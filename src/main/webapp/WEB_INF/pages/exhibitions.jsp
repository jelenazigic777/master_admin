<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.exhibitions"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>

        <spring:message code="delete" var="deletePlaceholder"/>
        <spring:message code="copy" var="copyPlaceholder"/>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.authors" var="authorPlaceholder"/>
        <spring:message code="placeholder.item.name" var="itemNamePlaceholder"/>
        <spring:message code="placeholder.item.code" var="itemCodePlaceholder"/>
        <spring:message code="placeholder.search" var="searchPlaceholder"/>
        <spring:message code="placeholder.room.name" var="roomNamePlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <section>
            <form class="btn-position-float-right-2">

                <input type="hidden" name="idmuseum" value="${idmuseum}">
                <c:if test="${isActive == false}">
                    <input class="btn btn-primary" type="submit"
                      value="${activeInactiveButtonText}"
                      formaction="exhibitions" formmethod="get"/>
                </c:if>
                <c:if test="${isActive == true}">
                    <input class="btn btn-primary" type="submit"
                      value="${activeInactiveButtonText}"
                      formaction="exhibitions/inactive" formmethod="get"/>
                </c:if>
            </form>
            <div class="container">

                <form style="border-bottom-style: inset">
                    <label class="section-title-main"><spring:message code="search"/></label>
                    <div class="btn-position-float-right">
                        <i class="glyphicon glyphicon-search form-control-feedback"></i>
                        <input class="btn btn-primary" type="submit" value="${searchPlaceholder}"
                          formaction="exhibitions/search" formmethod="get"/>
                    </div>

                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="name"
                              class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="author" class="col-sm-4 col-form-label">${authorPlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="author"
                              class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="item.name" class="col-sm-4 col-form-label">${itemNamePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="itemName"
                              class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="item.code" class="col-sm-4 col-form-label">${itemCodePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="itemCode"
                              class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="museummap.name" class="col-sm-4 col-form-label">${roomNamePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="roomName"
                              class="form-control">
                        </div>
                    </div>
                </form>
                <br>
                <br>
                <br>

                <label class="section-title-main"><spring:message code="exhibitions.list"/></label>

                <ul class="list-group">
                    <c:forEach var="exhibition" items="${exhibitions}">
                        <form action="exhibitions/exhibitionform/update" method="get">
                            <input type="hidden" name="idmuseum" value="${idmuseum}">
                            <input type="hidden" name="idexhibition" value="${exhibition.idexhibition}">
                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <button type="submit"
                                      class="list-group-item list-group-item-action center-txt">
                                            ${exhibition.name} </button>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-danger list-button" type="submit" value="${deletePlaceholder}"
                                      formaction="exhibitions/exhibition/delete" formmethod="get"/>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-danger list-button blue-bg" type="submit" value="${copyPlaceholder}"
                                      formaction="exhibitions/exhibition/copy" formmethod="get"/>
                                </div>
                            </div>
                        </form>
                    </c:forEach>
                </ul>

                <form action="exhibitions/exhibitionform" method="get">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <button class="btn-link  btn-position" type="Submit">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                    </button>
                </form>
            </div>
        </section>
    </body>
</html>

