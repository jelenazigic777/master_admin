<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="title.museum.form.details"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
</head>

<body>
<spring:message code="placeholder.name" var="namePlaceholder"/>
<spring:message code="placeholder.institution" var="institutionPlaceholder"/>
<spring:message code="placeholder.text" var="textPlaceholder"/>
<spring:message code="placeholder.texturl" var="texturlPlaceholder"/>
<spring:message code="placeholder.texturllocal" var="texturllocalPlaceholder"/>
<spring:message code="placeholder.imageurl" var="imageurlPlaceholder"/>
<spring:message code="placeholder.imageurllocal" var="imageurllocalPlaceholder"/>
<spring:message code="placeholder.videourl" var="videourlPlaceholder"/>
<spring:message code="placeholder.videourllocal" var="videourllocalPlaceholder"/>
<spring:message code="placeholder.x" var="xPlaceholder"/>
<spring:message code="placeholder.y" var="yPlaceholder"/>
<spring:message code="placeholder.z" var="zPlaceholder"/>
<spring:message code="placeholder.radius" var="radiusPlaceholder"/>

<jsp:include page="museumform.jsp"/>


<section>
    <div class="buttons-museum">
        <div class="row justify-content-center">
            <div class="col-sm-3">
                <form action="items" method="get">
                    <input type="hidden" name="idmuseum" value="${museum.idmuseum}">
                    <button type="submit" class="btn btn-info btn-design btn-lg btn-block">
                        <spring:message code="items.museum"/></button>
                </form>
            </div>
            <div class="col-sm-3">
                <form action="exhibitions" method="get">
                    <input type="hidden" name="idmuseum" value="${museum.idmuseum}">
                    <button type="submit" class="btn btn-info btn-design btn-lg btn-block">
                        <spring:message code="exhibition.museum"/></button>
                </form>
            </div>
            <div class="col-sm-3">
                <form action="floors" method="get">
                    <input type="hidden" name="idmuseum" value="${museum.idmuseum}">
                    <button type="submit" class="btn btn-info btn-design btn-lg btn-block">
                        <spring:message code="museum.floors"/></button>
                </form>
            </div>
            <div class="col-sm-3">
                <form action="beacons" method="get">
                    <input type="hidden" name="idmuseum" value="${museum.idmuseum}">
                    <button type="submit" class="btn btn-info btn-design btn-lg btn-block">
                        <spring:message code="museum.beacons"/></button>
                </form>
            </div>
        </div>
    </div>
</section>

</body>
</html>

