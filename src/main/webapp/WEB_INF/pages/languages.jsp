<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.languages"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>
        <jsp:include page="header.jsp"/>

        <spring:message code="delete" var="deletePlaceholder"/>
        <spring:message code="language.placeholder" var="languagePlaceholder"/>
        <spring:message code="language.abbreviation.placeholder" var="languageAbbreviationPlaceholder"/>

        <section>
            <div class="container">

                <form action="languages/add" method="post">
                    <h6 class="success-message">${errorMessage}</h6>
                    <label class="section-title-main"><spring:message code="languages.list"/></label>

                    <ul class="list-group">
                        <c:forEach var="language" items="${languages}">
                            <input type="hidden" name="idlanguage" value="${language.idlanguages}">
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <p class="list-group-item list-group-item-action center-txt">${language.languagename}</p>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-danger list-button" type="submit" value="${deletePlaceholder}"
                                      formaction="languages/delete" formmethod="get"/>
                                </div>
                            </div>
                        </c:forEach>
                    </ul>
                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">${languagePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" path="language" name="language"
                              class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">${languageAbbreviationPlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" path="languageAbbreviation" name="languageAbbreviation"
                              class="form-control"/>
                        </div>
                    </div>

                    <button class="btn-link  btn-position" type="Submit">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                    </button>
                </form>
            </div>
        </section>
    </body>
</html>

