<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.floor.form.additional"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>

    <body>
        <spring:message code="placeholder.imageurl" var="imageurlPlaceholder"/>
        <spring:message code="placeholder.videourl" var="videourlPlaceholder"/>
        <spring:message code="placeholder.videourl.add.manually" var="additionalVideourlPlaceholder"/>
        <spring:message code="placeholder.audiourl" var="audiourlPlaceholder"/>

        <spring:message code="set.additional.on.item" var="setAll"/>
        <spring:message code="add.new.item.video.url" var="addnewItem"/>
        <spring:message code="ambient.audio" var="ambientAudioPlaceholder"/>
        <spring:message code="placeholder.video.urls" var="videoUrlsPlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <section>

            <div class="container">

                <form action="floors/floor/set" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="idmuseummap" value="${idmuseummap}"/>
                    <input type="hidden" name="idmuseum" value="${idmuseum}"/>

                    <%--AMBIENT AUDIO--%>

                    <label class="col-sm-4 col-form-label">${ambientAudioPlaceholder}</label>
                    <c:if test="${museummap.ambientaudio!=null}">
                        <div class="row">
                            <div class="change-margin uploadItem">
                                <audio controls>
                                    <source src="${museummap.ambientaudio.ambientaudiourl}" type="audio/mpeg">
                                </audio>
                                <form>
                                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                                    <input type="hidden" name="idmuseummap" value="${idmuseummap}">
                                    <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                      formaction="floors/ambientaudio/delete/${museummap.ambientaudio.idambientaudio}"
                                      formmethod="get"/>
                                </form>
                            </div>
                        </div>
                    </c:if>

                    <div class="form-group row">
                        <c:if test="${museummap.ambientaudio==null}">
                            <div class="form-group row">
                                <div class="change-margin">
                                    <input type="file" name="ambientaudio">
                                </div>
                            </div>
                        </c:if>
                    </div>

                    <br>
                    <br>
                    <div class="form-group row">
                        <label for="images" class="col-sm-4 col-form-label"> ${imageurlPlaceholder} </label>
                        <input type="file" name="images" multiple="multiple"/>
                    </div>

                    <div class="row">
                        <c:forEach var="img" items="${mapimages}">
                            <div class="col-lg-3 change-margin images-custom">
                                <div class="card">
                                    <input type="hidden" name="iditemimage" value="${img.iditemimage}">
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="maps/image/delete/${img.iditemimage}" formmethod="get"/>
                                    <div>
                                        <img class="design-image-columns" src="${img.imageurl}"/>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>

                    <div class="form-group row">
                        <label for="audios" class="col-sm-4 col-form-label"> ${audiourlPlaceholder} </label>
                        <input type="file" name="audios" multiple="multiple"/>
                    </div>

                    <div class="row">
                        <c:forEach var="aud" items="${mapaudios}">
                            <div class="change-margin uploadItem">
                                <audio controls>
                                    <source src="${aud.audiourl}" type="audio/mpeg">
                                </audio>
                                <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                  formaction="maps/audio/delete/${aud.iditemaudio}" formmethod="get"/>
                            </div>
                        </c:forEach>
                    </div>

                    <div class="form-group row">
                        <label for="videos" class="col-sm-4 col-form-label"> ${videourlPlaceholder} </label>
                        <input type="file" name="videos" multiple="multiple"/>
                    </div>

                    <div class="row">
                        <c:forEach var="vid" items="${mapvideos}">
                            <div class="change-margin uploadItem col-md-6">
                                <video controls="" name="media" style="width:calc(100% + 16px)">
                                    <source src="${vid.videourl}" type="video/mp4">
                                </video>
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                  formaction="maps/video/delete/${vid.iditemvideo}" formmethod="get"/>
                            </div>
                        </c:forEach>
                    </div>

                    <label> ${videoUrlsPlaceholder} </label>
                    <br>
                    <br>

                    <c:forEach var="video" items="${videoUrls}">
                        <div>
                            <a href="${video.videourl}">${video.videourl}</a>
                            <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                              formaction="maps/video/delete/${vid.iditemvideo}" formmethod="get"/>
                        </div>
                    </c:forEach>

                    <br>

                    <label class="col-sm-4"> ${additionalVideourlPlaceholder}</label>
                    <div id="videourlitems" class="display-block">
                        <input type="text" name="urlvideo" class="form-control">
                    </div>

                    <div class="row  btn-position-float-right">
                        <button type="button" name="addnewItem" class="btn btn-primary back-button btn-bg"
                          onclick="addNewInputField()">
                            ${addnewItem}
                        </button>
                    </div>

                    <button type="submit" class="btn btn-primary btn-block change-margin-top">${setAll}</button>
                </form>

            </div>
        </section>
    </body>
</html>

