/* Set the width of the side navigation to 250px */
function openNav() {
	document.getElementById("mySidenav").style.width = "250px";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
}

function gotoMuseums(){
	var idinstitution = Cookies.get('idInstitution');
	var element = document.getElementById("museums");
	element.setAttribute('href', $('#baseUrl').val() + '/museums?idinstitution=' + idinstitution);
}

function gotoItems(){
	var query_string = window.location.search;
	var search_params = new URLSearchParams(query_string);
	var idmuseum = search_params.get('idmuseum');
	var element = document.getElementById("items");
	element.setAttribute('href', $('#baseUrl').val() + '/items?idmuseum=' + idmuseum);
}
function gotoExhibitions(){
	var query_string = window.location.search;
	var search_params = new URLSearchParams(query_string);
	var idmuseum = search_params.get('idmuseum');

	var element = document.getElementById("exhibitions");
	element.setAttribute('href', $('#baseUrl').val()  + '/exhibitions?idmuseum=' + idmuseum);
}

function getoFloors(){
	var query_string = window.location.search;
	var search_params = new URLSearchParams(query_string);
	var idmuseum = search_params.get('idmuseum');

	var element = document.getElementById("floors");
	element.setAttribute('href', $('#baseUrl').val()  + '/floors?idmuseum=' + idmuseum);
}

function gotoBeacons(){
	var query_string = window.location.search;
	var search_params = new URLSearchParams(query_string);
	var idmuseum = search_params.get('idmuseum');

	var element = document.getElementById("beacons");
	element.setAttribute('href', $('#baseUrl').val()  + '/beacons?idmuseum=' + idmuseum);
}


