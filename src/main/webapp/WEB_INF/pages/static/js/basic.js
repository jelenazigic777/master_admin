window.onmousedown = function (e) {
	var el = e.target;
	if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
		e.preventDefault();

		// toggle selection
		if (el.hasAttribute('selected')) el.removeAttribute('selected');
		else el.setAttribute('selected', '');

		// hack to correct buggy behavior
		var select = el.parentNode.cloneNode(true);
		el.parentNode.parentNode.replaceChild(select, el.parentNode);
	}
}

function addInstitutionToCookie(idInstitution) {
	if (Cookies.get('idInstitution')) {
		Cookies.remove('idInstitution');
	}
	Cookies.set('idInstitution', idInstitution, { expires: 1 });
}

function goBack () {
	var previousPagePath = document.referrer.substring(window.location.origin.length, document.referrer.indexOf("?"));
	if (previousPagePath =="/museums/museumform" ||
	  previousPagePath == "/institutions/intitutionform" ||
	  previousPagePath == "/items/itemform" ||
	  previousPagePath == "/items/item/set" ||
	  previousPagePath == "/exhibitions/exhibitionform" ||
	  previousPagePath == "/exhibitions/exhibition/set" ||
	  previousPagePath == "/maps/mapform" ||
	  previousPagePath == "/floors/floorform" ||
	  previousPagePath == "/exhibitions/search"||
	  previousPagePath == "/items/search" ||
	  previousPagePath == "/beacons/beaconform") {
		event.preventDefault();
		window.history.go(-1);
	}
	event.preventDefault();
	window.history.back();

}

function addNewInputField () {
//Append a new row of code to the "#items" div
	$("#videourlitems").append('<input type="text" name="urlvideo" class="form-control">');
}

function addNewInputFieldText () {
//Append a new row of code to the "#items" div
	$("#textitems").append(' <textarea type="text" name="text" class="form-control"></textarea>');
}

function changeAudio (sourceUrl) {
	var audio = document.getElementById("player");
	var source = document.getElementById("mp3_src");

	audio.pause();

	if (sourceUrl) {
		source.src = sourceUrl;
		audio.load();
		audio.play();
	}
	if(url != "") {
		jQuery("#player").css("display", "block");
	} else {
		jQuery("#player").css("display", "none");
	}
}

function changeVideo (url) {
	var iframe = document.getElementById("video_player");
	iframe.src = url;
	if(url != "") {
		jQuery("#video_player").css("display", "block");
	}else {
		jQuery("#video_player").css("display", "none");
	}
}

function getMuseumLanguage(idlanguage, idmuseum) {
	$.post(window.location.origin + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) + "/languages/museum/language/" + idlanguage + '/' + idmuseum, function(data) {
		$('#idmuseumlanguages').val(data.idmuseumlanguages)
		$('#name').val(data.name);
		$('#text').val(data.text);
		$('#texturl').val(data.texturl);
	});
}

function getMuseummapLanguage(idlanguage, idmuseummap) {
	$.post(window.location.origin + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) + "/languages/museummap/language/" + idlanguage + '/' + idmuseummap, function(data) {
		$('#idmuseummaplanguages').val(data.idmuseummaplanguages)
		$('#name').val(data.name);
		$('#text').val(data.text);
		$('#texturl').val(data.texturl);
	});
}

function getExhibitionLanguage(idlanguage, idexhibition) {
	$.post(window.location.origin + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) + "/languages/exhibition/language/" + idlanguage + '/' + idexhibition, function(data) {
		$('#idmuseumlanguages').val(data.idexhibitionlanguages)
		$('#name').val(data.name);
		$('#author').val(data.author);
		$('#text').val(data.text);
		$('#texturl').val(data.texturl);
	});
}

function getItemLanguage(idlanguage, iditem) {
	$.post(window.location.origin + window.location.pathname.substring(0, window.location.pathname.indexOf("/",2)) + "/languages/item/language/" + idlanguage + '/' + iditem, function(data) {
		$('#idmuseumlanguages').val(data.idmuseumlanguages)
		$('#name').val(data.name);
		$('#author').val(data.author);
		$('#text').val(data.text);
		$('#texturl').val(data.texturl);
	});
}

$(function () {
	$('[data-toggle="tooltip"]').tooltip()
})


