window.onload = function() {
	var img = document.getElementById('beacon-coordinates-img');
	var data = {}
	data.beacons = jQuery.parseJSON($('#otherBeacons').val())
	var beacons = data.beacons;
	var imgWidth = img.clientWidth;
	var imgHeight = img.clientHeight;
	var canvas = document.getElementById('map-canvas');
	canvas.offsetLeft= img.offsetLeft;
	canvas.offsetTop = img.offsetTop;
	canvas.width = imgWidth;
	canvas.height = imgHeight;

	canvas.style.position = "absolute";
	var context = canvas.getContext('2d');
	//draw all beacons
	var xEnd = $("#xEndMuseummap").attr('value');
	var yEnd = $("#yEndMuseummap").attr('value');
	for (var i = 0; i < beacons.length; i++) {
		context.beginPath();
		var X = (imgWidth * beacons[i].xcoordinate) / xEnd;
		var Y = (imgHeight * (yEnd - beacons[i].ycoordinate)) / yEnd;
		context.arc(X, Y, 5, 0, 2 * Math.PI, false);
		context.fillStyle = 'red';
		context.fill();
		context.stroke();
		var drawingRadius = (imgWidth * beacons[i].radius) / xEnd;
		context.beginPath();
		context.arc(X, Y, 5 + drawingRadius, 0, 2 * Math.PI, false);
		context.lineWidth = 2;
		context.stroke();
		context.closePath();
	}
	//draw that beacon
	context.beginPath();
	var x = $("#xcoordinate").val();
	var y = $("#ycoordinate").val();
	var X = (imgWidth * x) / xEnd;
	var Y = (imgHeight * (yEnd-y)) / yEnd;
	context.arc(X, Y, 5, 0, 2 * Math.PI, false);
	context.fillStyle = 'green';
	context.fill();
	context.stroke();
	//radius
	var radius = $("#radius").attr('value');
	var drawingRadius = (imgWidth * radius) / xEnd;
	context.beginPath();
	context.arc(X, Y, 5 + drawingRadius, 0, 2 * Math.PI, false);
	context.lineWidth = 2;
	context.stroke();
}
$(document).ready(function () {
	$("#map-canvas").on("click", function (event) {
		//real map data
		var xEnd = $("#xEndMuseummap").attr('value');
		var yEnd = $("#yEndMuseummap").attr('value');
		//image data
		var img = document.getElementById('beacon-coordinates-img');
		var imgWidth = img.clientWidth;
		var imgHeight = img.clientHeight;
		var clickedX = event.pageX - this.offsetLeft;
		var clickedY = event.pageY - this.offsetTop;
		//calculate and set

		var X = (xEnd * clickedX) / imgWidth;
		var Y = (yEnd * (imgHeight + this.offsetTop - event.pageY)) / imgHeight;
		$("#xcoordinate").val(X.toFixed(2));
		$("#ycoordinate").val(Y.toFixed(2));
		//draw
		var canvas = document.getElementById('map-canvas');
		var context = canvas.getContext('2d');
		//clear
		context.clearRect(0, 0, canvas.width, canvas.height);
		//draw all beacons
		var data = {}
		data.beacons = jQuery.parseJSON($('#otherBeacons').val())
		var beacons = data.beacons;
		for (i = 0; i < beacons.length; i++) {
			context.beginPath();
			var X = (imgWidth * beacons[i].xcoordinate) / xEnd;
			var Y = (imgHeight * (yEnd-beacons[i].ycoordinate)) / yEnd;
			context.arc(X, Y, 5, 0, 2 * Math.PI, false);
			context.fillStyle = 'red';
			context.fill();
			context.stroke();
			var drawingRadius = (imgWidth * beacons[i].radius) / xEnd;
			context.beginPath();
			context.arc(X, Y, 5 + drawingRadius, 0, 2 * Math.PI, false);
			context.lineWidth = 2;
			context.stroke();
			context.closePath();
		}

		context.beginPath();
		context.arc(clickedX, clickedY, 5, 0, 2 * Math.PI, false);
		context.fillStyle = 'green';
		context.fill();
		context.stroke();
		context.closePath();

		//radius
		var radius = $("#radius").attr('value');
		var drawingRadius = (imgWidth * radius) / xEnd;
		context.beginPath();
		context.arc(clickedX, clickedY, 5 + drawingRadius, 0, 2 * Math.PI, false);
		context.lineWidth = 2;
		context.stroke();
	});
});