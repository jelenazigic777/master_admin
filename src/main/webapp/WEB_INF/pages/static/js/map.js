function wantToDelete() {
	//maybe change this to custom modal
	var message = document.getElementById("messageChange").value;
	return confirm(message, 'title');
}

window.onload = function() {
	var img = document.getElementById('map-coordinates-img');
	var data = {}
	data.items = jQuery.parseJSON($('#jsonItems').val())
	var items = data.items;
	var imgWidth = img.clientWidth;
	var imgHeight = img.clientHeight;
	var canvas = document.getElementById('map-canvas');
	//canvas.offsetLeft = img.offsetLeft;
	//canvas.offsetTop = img.offsetTop;
	canvas.width = imgWidth;
	canvas.height = imgHeight;

	canvas.style.position = "absolute";
	var context = canvas.getContext('2d');
	//draw all items
	var xEnd = $("#xEndMuseummap").attr('value');
	var yEnd = $("#yEndMuseummap").attr('value');
	for (var i = 0; i < items.length; i++) {
		context.beginPath();
		var X = (imgWidth * items[i].xcoordinate) / xEnd;
		var Y = (imgHeight * (yEnd - items[i].ycoordinate)) / yEnd;
		if (items[i].imgUrl == null) {
			context.arc(X, Y, 5, 0, 2 * Math.PI, false);
			context.fillStyle = 'green';
			context.fill();
			context.stroke();
		} else {
			var img = new Image;
			img.src = items[i].imgUrl;
			context.drawImage(img, X-15, Y-15, 30, 30);
		}
		var drawingRadius = (imgWidth * items[i].radius) / xEnd;
		context.beginPath();
		context.arc(X, Y, 5 + drawingRadius, 0, 2 * Math.PI, false);
		context.lineWidth = 2;
		context.stroke();
		context.closePath();
	}
}