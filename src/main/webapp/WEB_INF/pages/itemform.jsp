<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.item.form"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>

    <body>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.author" var="authorPlaceholder"/>
        <spring:message code="placeholder.identificationcode" var="identificationcodePlaceholder"/>
        <spring:message code="placeholder.text" var="textPlaceholder"/>
        <spring:message code="placeholder.texturl" var="texturlPlaceholder"/>
        <spring:message code="placeholder.texturllocal" var="texturllocalPlaceholder"/>
        <spring:message code="placeholder.imageurl" var="imageurlPlaceholder"/>
        <spring:message code="placeholder.imageurllocal" var="imageurllocalPlaceholder"/>
        <spring:message code="placeholder.videourl" var="videourlPlaceholder"/>
        <spring:message code="placeholder.videourllocal" var="videourllocalPlaceholder"/>
        <spring:message code="placeholder.x" var="xPlaceholder"/>
        <spring:message code="placeholder.y" var="yPlaceholder"/>
        <spring:message code="placeholder.z" var="zPlaceholder"/>
        <spring:message code="placeholder.radius" var="radiusPlaceholder"/>
        <spring:message code="button.additional.text" var="additionalText"/>
        <spring:message code="placeholder.set.other.language" var="setItemLanguagePlaceholder"/>

        <spring:message code="choose.map.text" var="choosePlaceholder"/>
        <spring:message code="map.text" var="mapsPlaceholder"/>
        <spring:message code="set.placeholder" var="setPlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>
        <script>
			$(function () {
				$("select").imagepicker();
			});
        </script>
        <section>

            <div class="container">

                <h3 class="center-headline"><spring:message code="add.item.form.title"/></h3>
                <form:form action="items/itemform" class="form-signin"
                  method="POST"
                  role="form"
                  commandName="item">

                    <img class="design-image" src="${item.imageurl}"/>

                    <h6 class="success-message">${successMessage}</h6>

                    <c:if test="${item.iditem != 0}">
                        <input class="btn btn-primary remove-button btn-position-float-right" type="submit"
                          value="${setItemLanguagePlaceholder}"
                          formaction="languages/item" formmethod="get"/>
                    </c:if>

                    <label class="section-title"><spring:message code="basic.insert"/></label>

                    <input type="hidden" name="idmuseum" value="${idmuseum}"/>

                    <input type="hidden" id="otherItems" value='${otherItems}'/>

                    <form:hidden path="museum" name="museum"/>
                    <form:hidden path="institution" name="institution"/>
                    <form:hidden path="iditem" name="iditem"/>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="name" name="name" placeholder="${item.name}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="name" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="author" class="col-sm-4 col-form-label">${authorPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="author" name="author" placeholder="${item.author}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="author" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${identificationcodePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="identificationcode" name="identificationcode" placeholder="${item.identificationcode}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="identificationcode" cssClass="form-error-field"/></label>

                    <label class="section-title"><spring:message code="rest.insert"/></label></br>

                    <div class="form-group row">
                        <label for="text" class="col-sm-4 col-form-label">${textPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:textarea type="text" path="text" name="text" placeholder="${item.text}" rows="4"
                              class="form-control"></form:textarea>
                        </div>
                    </div>
                    <label><form:errors path="text" cssClass="form-error-field"/></label>

                    <div>
                        <button type="submit" class="btn btn-secondary btn-block" formaction="items/item/set/additional"
                          formmethod="post">${additionalText}</button>
                    </div>


                    <label class="section-title"><spring:message code="location.insert"/></label>

                    <div class="row">
                        <div class="change-margin">
                            <canvas id="map-canvas"></canvas>
                            <img id="item-coordinates-img" style="height: 400px; max-width: 100%;" src="${museummap.floormap.imageurl}" ismap/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="xcoordinate" class="col-sm-4 col-form-label">${xPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="xcoordinate" type="double" path="xcoordinate" name="xcoordinate" placeholder="${item.xcoordinate}"
                              class="form-control" readonly="true"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="xcoordinate" cssClass="form-error-field"/></label>


                    <div class="form-group row">
                        <label for="ycoordinate" class="col-sm-4 col-form-label">${yPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="ycoordinate" type="double" path="ycoordinate" name="yxcoordinate"
                              placeholder="${item.ycoordinate}"
                              class="form-control" readonly="true"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="ycoordinate" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="zcoordinate" class="col-sm-4 col-form-label">${zPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="double" path="zcoordinate" name="zcoordinate" placeholder="${item.zcoordinate}"
                              class="form-control" readonly="true"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="zcoordinate" cssClass="form-error-field"/></label>

                    <input type="hidden" id="xStartMuseummap" value="${floormap.xstart}"/>
                    <input type="hidden" id="yStartMuseummap" value="${floormap.ystart}"/>
                    <input type="hidden" id="xEndMuseummap" value="${floormap.xend}"/>
                    <input type="hidden" id="yEndMuseummap" value="${floormap.yend}"/>

                    <div class="form-group row">
                        <label for="radius" class="col-sm-4 col-form-label">${radiusPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="radius" type="double" path="radius" name="radius" placeholder="${item.radius}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="radius" cssClass="form-error-field"/></label>

                    <label class="section-title">${mapsPlaceholder}</label>

                    <label class="col-form-label"> ${choosePlaceholder} </label>
                    <select name="selectedFloormap" class="custom-select">
                        <c:forEach var="floormap" items="${floormaps}">
                            <option data-img-src="${floormap.imageurl}" value="${floormap.idfloormap}"></option>
                        </c:forEach>
                    </select>
                    <input type="hidden" name="iditem" value="${iditem}"/>
                    <input class="btn btn-primary btn-position-float-right" style="margin-bottom:10px" type="submit" value="${setPlaceholder}"
                      formaction="items/set/map" formmethod="post"/>

                    <div>
                        <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
                    </div>

                </form:form>

            </div>
        </section>
        <script>
			<%@include file="static/js/item.js" %>
        </script>
        <script>
			if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {
				if (document.URL.substring(window.location.origin.length, document.URL.indexOf("?")) == "/items/itemform") {
					history.go(-1);
				}
			}
        </script>
    </body>
</html>

