<head>
    <style type="text/css">
     <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
     <%@include file="static/css/basic.css" %>
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <style type="text/css">
        <%@include file="static/css/image-picker.css" %>
    </style>
    <%--<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script>
        <%@include file="static/js/basic.js" %>
        <%@include file="static/js/cookie.js" %>
		<%@include file="static/js/image-picker.js" %>
		<%@include file="static/js/image-picker.min.js" %>
    </script>
    <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
</head>

<div class="header-style sticky" id="header">
    <div class="row">
        <div class="col-sm-6">
            <%--<button type="button" name="back" class="btn btn-primary back-button btn-bg" onclick="goBack()">--%>
                <%--<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"/>--%>
            <%--</button>--%>
        </div>
        <div class="col-sm-6">
            <form action="logout" method="get">
                <button class="btn btn-danger logout-button btn-bg" type="Submit">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </button>
            </form>
        </div>
    </div>
</div>
