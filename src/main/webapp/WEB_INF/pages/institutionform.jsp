<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="title.institution.form"/></title>
    <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
</head>

<body>
<spring:message code="placeholder.name" var="namePlaceholder"/>
<spring:message code="placeholder.address" var="addressPlaceholder"/>
<spring:message code="placeholder.town" var="townPlaceholder"/>
<spring:message code="placeholder.township" var="townshipPlaceholder"/>
<spring:message code="placeholder.district" var="districtPlaceholder"/>
<spring:message code="placeholder.founder" var="founderPlaceholder"/>
<spring:message code="placeholder.foundationdate" var="foundationdatePlaceholder"/>
<spring:message code="placeholder.startdate" var="startdatePlaceholder"/>
<spring:message code="placeholder.idnumber" var="idnumberPlaceholder"/>
<spring:message code="placeholder.registrationnumber" var="registrationnumberPlaceholder"/>
<spring:message code="placeholder.taxid" var="taxidPlaceholder"/>
<spring:message code="placeholder.contactphones" var="contactphonesPlaceholder"/>
<spring:message code="placeholder.emails" var="emailsPlaceholder"/>
<spring:message code="placeholder.webaddresses" var="webaddressesPlaceholder"/>

<jsp:include page="header.jsp"/>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <script>
		$(document).ready(function () {
			$('#foundationdate').datepicker({
				uiLibrary: 'bootstrap'
			});
			$('#startdate').datepicker({
				uiLibrary: 'bootstrap'
			});
		});
    </script>

<section>
    <div class="container">
        <h3 class="center-headline"><spring:message code="add.institution.form.title"/></h3>
        <form:form action="institutions/institutionform" class="form-signin"
                   method="POST"
                   role="form"
                   commandName="institution">

            <h6 class="success-message">${successMessage}</h6>

            <label class="section-title"><spring:message code="basic.insert"/></label>

            <form:hidden path="idinstitution" name="idinstitution"/>

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="text" path="name" name="name" placeholder="${institution.name}"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="name" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="idnumber" class="col-sm-4 col-form-label">${idnumberPlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="int" path="idnumber" name="xcoordinate" placeholder="${institution.idnumber}"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="idnumber" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="registrationnumber" class="col-sm-4 col-form-label">${registrationnumberPlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="int" path="registrationnumber" name="registrationnumber"
                                placeholder="${institution.registrationnumber}"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="registrationnumber" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="taxid" class="col-sm-4 col-form-label">${taxidPlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="int" path="taxid" name="taxid" placeholder="${institution.taxid}"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="taxid" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="address" class="col-sm-4 col-form-label">${addressPlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="text" path="address" name="address" placeholder="${institution.address}"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="address" cssClass="form-error-field"/></label>

            <label class="section-title"><spring:message code="address.info"/></label>

            <div class="form-group row">
                <label for="town" class="col-sm-4 col-form-label">${townPlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="text" path="town" name="town" placeholder="${institution.town}" rows="4"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="town" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="township" class="col-sm-4 col-form-label">${townshipPlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="text" path="township" name="township" placeholder="${institution.township}"
                                rows="4"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="township" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="district" class="col-sm-4 col-form-label">${districtPlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="text" path="district" name="district" placeholder="${institution.district}"
                                rows="4"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="district" cssClass="form-error-field"/></label>

            <label class="section-title"><spring:message code="foundation.info"/></label>

            <div class="form-group row">
                <label for="founder" class="col-sm-4 col-form-label">${founderPlaceholder}</label>
                <div class="col-sm-8">
                    <form:input type="text" path="founder" name="founder" placeholder="${institution.founder}"
                                class="form-control"></form:input>
                </div>
            </div>
            <label><form:errors path="founder" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="foundationdate" class="col-sm-4 col-form-label">${foundationdatePlaceholder}</label>
                <div class='col-sm-8'>
                    <input path="foundationdate" name="foundationdate" class="form-control" id='foundationdate' value="${foundationdate}"/>
                </div>
            </div>
            <label><form:errors path="foundationdate" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="startdate" class="col-sm-4 col-form-label">${startdatePlaceholder}</label>
                <div class='col-sm-8'>
                    <input path="startdate" name="startdate" class="form-control" id='startdate' value="${startdate}"/>
                </div>
            </div>
            <label><form:errors path="startdate" cssClass="form-error-field"/></label>

            <label class="section-title"><spring:message code="contact.info"/></label>

            <div class="form-group row">
                <label for="contactphones" class="col-sm-4 col-form-label">${contactphonesPlaceholder}</label>
                <div class="col-sm-8">
                    <form:textarea type="text" path="contactphones" name="contactphones"
                                   placeholder="${institution.contactphones}" rows="4"
                                   class="form-control"></form:textarea>
                </div>
            </div>
            <label><form:errors path="contactphones" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="emails" class="col-sm-4 col-form-label">${emailsPlaceholder}</label>
                <div class="col-sm-8">
                    <form:textarea type="text" path="emails" name="emails" placeholder="${institution.emails}" rows="4"
                                   class="form-control"></form:textarea>
                </div>
            </div>
            <label><form:errors path="emails" cssClass="form-error-field"/></label>

            <div class="form-group row">
                <label for="webaddresses" class="col-sm-4 col-form-label">${webaddressesPlaceholder}</label>
                <div class="col-sm-8">
                    <form:textarea type="text" path="webaddresses" name="webaddresses"
                                   placeholder="${institution.webaddresses}"
                                   rows="4"
                                   class="form-control"></form:textarea>
                </div>
            </div>
            <label><form:errors path="webaddresses" cssClass="form-error-field"/></label>

            <div>
                <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
            </div>

        </form:form>

    </div>
</section>
</body>
</html>

