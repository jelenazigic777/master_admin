<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.map.form.additional"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>

        <spring:message code="delete" var="deletePlaceholder"/>
        <spring:message code="add.item.new" var="addItemPlaceholder"/>
        <spring:message code="add.room.new" var="addRoomsPlaceholder"/>
        <spring:message code="set.additional.on.item" var="setAll"/>
        <spring:message code="placeholder.imageurl" var="imageurlPlaceholder"/>
        <spring:message code="placeholder.videourl" var="videourlPlaceholder"/>
        <spring:message code="placeholder.audiourl" var="audiourlPlaceholder"/>
        <spring:message code="ambient.audio" var="ambientAudioPlaceholder"/>
        <spring:message code="set.placeholder" var="setPlaceholder"/>
        <spring:message code="active.map.placeholder" var="activeMapPlaceholder"/>
        <spring:message code="active.map.choose.placeholder" var="activeMapChoosePlaceholder"/>
        <spring:message code="set.additional.on.item" var="setAll"/>
        <spring:message code="add.new.item.video.url" var="addnewItem"/>
        <spring:message code="placeholder.videourl.add.manually" var="videourladdmanuallyPlaceholder"/>
        <spring:message code="want.to.change" var="sureWantToChange"/>
        <spring:message code="placeholder.video.urls" var="videoUrlsPlaceholder"/>
        <spring:message code="placeholder.connect.rooms" var="insertConnectedPlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <script>
			$(function () {
				$("select").imagepicker();
			});
        </script>

        <section>
            <div class="container">

                <label class="section-title"><spring:message code="items.insert.map"/></label>

                <input type="hidden" id="xEndMuseummap" value="${floormap.xend}"/>
                <input type="hidden" id="yEndMuseummap" value="${floormap.yend}"/>

                <input type="hidden" name="idmuseum" value="${idmuseum}">
                <input type="hidden" id="jsonItems" value='${jsonItems}'/>
                <input type="hidden" id="messageChange" value='${sureWantToChange}'/>
                <spring:message code="placeholder.name" var="namePlaceholder"/>
                <spring:message code="placeholder.floornumber" var="floornumberPlaceholder"/>

                <form>
                    <div class="row">
                        <div class="change-margin">
                            <canvas id="map-canvas"></canvas>
                            <img id="map-coordinates-img" style="height: 400px; max-width: 100%;" src="${museummap.floormap.imageurl}" ismap/>
                        </div>
                    </div>
                </form>

                <%--ITEMS ON MAP--%>

                <div class="row">
                    <c:forEach var="item" items="${mapitems}">
                        <div class="col-lg-3 change-margin images-custom">
                            <form action="items/itemform/update" method="get">
                                <input type="hidden" name="idmuseum" value="${idmuseum}">
                                <input type="hidden" name="idmuseummap" value="${idmuseummap}">
                                <input type="hidden" name="idroom" value="${idroom}">
                                <input type="hidden" name="iditem" value="${item.iditem}">
                                <div class="card">
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="maps/delete/item" formmethod="get"/>
                                    <button type="submit"
                                      class="center-txt">
                                        <div class="thumbnail">
                                            <img class="design-image-columns" src="${item.images[0].imageurl}"/>
                                        </div>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </c:forEach>
                </div>

                <form action="items/itemfromanothersource" method="get">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <button class="btn btn-secondary btn-position" type="Submit">
                        ${addItemPlaceholder}
                    </button>
                </form>

                <%--ADD ROOMS--%>
                <label class="section-title"><spring:message code="rooms.insert"/></label>

                <div class="row">
                    <c:forEach var="room" items="${maprooms}">
                        <div class="col-lg-3 change-margin images-custom">
                            <form>
                                <input type="hidden" name="idmuseum" value="${idmuseum}">
                                <input type="hidden" name="idmuseummap" value="${idmuseummap}">
                                <input type="hidden" name="idroom1" value="${idroom}">
                                <input type="hidden" name="idroom2" value="${room.idroom}">
                                <div class="card">
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="maps/delete/room" formmethod="get"/>
                                    <div class="center-txt">
                                        <div class="thumbnail">
                                            <img class="design-image-columns" src="${room.floormap.imageurl}"/>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="col-sm-12">
                                            <p class="card-text card-text-extra"><b>${namePlaceholder}:</b> ${room.floormap.name}</p>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="card-text card-text-extra"><b>${floornumberPlaceholder}:</b> ${room.floormap.floornumber}</p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </c:forEach>
                </div>

                <label class="col-form-label"> ${insertConnectedPlaceholder} </label>

                <form action="maps/map/set/additional" method="post">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <input type="hidden" name="idmuseummap" value="${idmuseummap}">
                    <input type="hidden" name="idroom" value="${idroom}">
                    <select name="selectedRooms" class="custom-select" id="availablerooms" multiple="multiple">
                        <c:forEach var="room" items="${roomstoset}">
                            <option data-img-src="${room.floormap.imageurl}" value="${room.idroom}"></option>
                        </c:forEach>
                    </select>
                    <button type="submit" class="btn btn-primary btn-position-float-right" style="margin-left: 10px">
                        ${setPlaceholder}
                    </button>
                </form>

                <form action="maps/roomsfromanothersource" method="get">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <button class="btn btn-secondary btn-position" type="Submit">
                        ${addRoomsPlaceholder}
                    </button>
                </form>
                <br>

                <%--SET FILES--%>
                <label class="section-title"><spring:message code="set.files"/></label>
                <c:if test="${room.ambientaudio.exhibition == null && room.ambientaudio.room != null}">
                    <div class="container">
                        <form>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"> ${ambientAudioPlaceholder} </label>
                                <div class="col-md-8 change-margin uploadItem">
                                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                                    <input type="hidden" name="idmuseummap" value="${idmuseummap}">
                                    <input type="hidden" name="idroom" value="${idroom}">
                                    <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                      formaction="maps/ambientaudio/delete/${room.ambientaudio.idambientaudio}"
                                      formmethod="get"/>
                                    <audio controls class="col-sm-8">
                                        <source src="${room.ambientaudio.ambientaudiourl}" type="audio/mpeg">
                                    </audio>
                                </div>
                            </div>
                        </form>
                    </div>
                </c:if>
                <br>
                <div class="container">
                    <form action="maps/map/set" method="post" enctype="multipart/form-data">
                        <c:if test="${room.ambientaudio==null}">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label"> ${ambientAudioPlaceholder} </label>
                                <input type="hidden" name="idmuseum" value="${idmuseum}">
                                <input type="hidden" name="idroom" value="${idroom}">
                                <input type="hidden" name="idmuseummap" value="${idmuseummap}">
                                <div class="change-margin">
                                    <input type="file" name="ambientaudio">
                                </div>
                            </div>
                        </c:if>


                        <input type="hidden" name="idmuseummap" value="${idmuseummap}"/>
                        <input type="hidden" name="idmuseum" value="${idmuseum}"/>
                        <input type="hidden" name="idroom" value="${idroom}"/>

                        <div class="form-group row">
                            <label for="images" class="col-sm-4 col-form-label"> ${imageurlPlaceholder} </label>
                            <input type="file" name="images" multiple="multiple"/>
                        </div>

                        <div class="row">
                            <c:forEach var="img" items="${mapimages}">
                                <div class="col-lg-3 change-margin images-custom">
                                    <div class="card">
                                            <%--TODO:ADD ISLOCAL--%>
                                        <input type="hidden" name="iditemimage" value="${img.iditemimage}">
                                        <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                          formaction="maps/image/delete/${img.iditemimage}" formmethod="get"/>
                                        <div class="thumbnail">
                                            <img class="design-image-columns" src="${img.imageurl}"/>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>

                        <div class="form-group row">
                            <label for="audios" class="col-sm-4 col-form-label"> ${audiourlPlaceholder} </label>
                            <input type="file" name="audios" multiple="multiple"/>
                        </div>

                        <div class="row">
                            <c:forEach var="aud" items="${mapaudios}">
                                <div class="uploadItem change-margin">
                                    <audio controls>
                                        <source src="${aud.audiourl}" type="audio/mpeg">
                                    </audio>
                                    <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                      formaction="maps/audio/delete/${aud.iditemaudio}" formmethod="get"/>
                                </div>
                            </c:forEach>
                        </div>

                        <div class="form-group row">
                            <label for="videos" class="col-sm-4 col-form-label"> ${videourlPlaceholder} </label>
                            <input type="file" name="videos" multiple="multiple"/>
                        </div>

                        <div class="row">
                            <c:forEach var="vid" items="${mapvideos}">
                                <div class="change-margin uploadItem col-md-6">
                                    <video controls="" name="media" style="width:calc(100% + 16px)">
                                        <source src="${vid.videourl}" type="video/mp4">
                                    </video>
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="maps/video/delete/${vid.iditemvideo}" formmethod="get"/>
                                </div>
                            </c:forEach>
                        </div>

                        <label> ${videoUrlsPlaceholder} </label>
                        <br>
                        <br>

                        <c:forEach var="video" items="${videoUrls}">
                            <div>
                                <a href="${video.videourl}">${video.videourl}</a>
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                  formaction="maps/video/delete/${vid.iditemvideo}" formmethod="get"/>
                            </div>
                        </c:forEach>

                        <br>
                        <label class="col-sm-4"> ${videourladdmanuallyPlaceholder} </label>
                        <div id="videourlitems" class="display-block">
                            <input type="text" name="urlvideo" class="form-control">
                        </div>

                        <div class="row btn-position-float-right">
                            <button type="button" name="addnewItem" class="btn btn-primary back-button btn-bg"
                              onclick="addNewInputField()">
                                ${addnewItem}
                            </button>
                        </div>

                        <%--SET ACTIVE MAP--%>
                        <label class="section-title"><spring:message code="set.active.map"/></label>

                        <%--<label class="col-form-label"> ${activeMapPlaceholder} </label>--%>
                        <%--<div class="row">--%>
                        <%--<div class="col-lg-3 change-margin images-custom">--%>
                        <%--<div class="card">--%>
                        <%--<div class="thumbnail">--%>
                        <%--<img class="design-image-columns" src="${museummap.floormap.imageurl}"/>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>

                        <input type="hidden" name="idmuseum" value="${idmuseum}">
                        <input type="hidden" name="idmuseummap" value="${idmuseummap}">
                        <input type="hidden" name="idroom" value="${idroom}">
                        <select name="selectedMap" class="image-picker show-html" id="availablemaps" multiple="multiple">
                            <c:forEach var="map" items="${mapimages}">
                                <option data-img-src="${map.imageurl}" value="${map.iditemimage}"></option>
                            </c:forEach>
                        </select>
                        <input onclick="return wantToDelete();" class="btn btn-primary btn-position-float-right" style="margin-bottom:10px" type="submit"
                          value="${setPlaceholder}"
                          formaction="maps/map/set/active" formmethod="post"/>

                        <button type="submit" class="btn btn-primary btn-block change-margin-top">${setAll}</button>
                    </form>
                </div>
            </div>
        </section>
        <script>
			<%@include file="static/js/map.js" %>
        </script>
    </body>
</html>


