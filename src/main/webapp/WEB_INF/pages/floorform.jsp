<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.floor.form"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>

    <body>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.floornumber" var="floornumberPlaceholder"/>
        <spring:message code="placeholder.text" var="textPlaceholder"/>
        <spring:message code="placeholder.texturl" var="texturlPlaceholder"/>
        <spring:message code="placeholder.imageurl" var="imageurlPlaceholder"/>
        <spring:message code="placeholder.videourl" var="videourlPlaceholder"/>
        <spring:message code="placeholder.audiourl" var="audiourlPlaceholder"/>
        <spring:message code="placeholder.x" var="xPlaceholder"/>
        <spring:message code="placeholder.y" var="yPlaceholder"/>
        <spring:message code="placeholder.z" var="zPlaceholder"/>
        <spring:message code="placeholder.radius" var="radiusPlaceholder"/>
        <spring:message code="floors.rooms" var="getmapsonfloorPlaceholder"/>
        <spring:message code="set.files.on.map" var="additionalText"/>
        <spring:message code="placeholder.set.other.language" var="setFloorLanguagePlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <section>
            <div class="container">

                <h3 class="center-headline"><spring:message code="add.floor.form.title"/></h3>
                <form:form action="floors/floorform" class="form-signin"
                  method="POST"
                  role="form"
                  commandName="museummap">

                    <img class="design-image" src="${museummap.imageurl}"/>

                    <h6 class="success-message">${successMessage}</h6>

                    <form:hidden path="museum" name="museum"/>
                    <form:hidden path="institution" name="institution"/>
                    <form:hidden path="idmuseummap" name="idmuseummap"/>


                    <input class="btn btn-primary remove-button btn-position-float-right" type="submit"
                      value="${getmapsonfloorPlaceholder}"
                      formaction="maps" formmethod="get"/>
                    <c:if test="${museummap.idmuseummap != 0}">
                        <br>
                        <br>
                        <input class="btn btn-primary remove-button btn-position-float-right" type="submit"
                          value="${setFloorLanguagePlaceholder}"
                          formaction="languages/museummap" formmethod="get"/>
                    </c:if>

                    <label class="section-title"><spring:message code="basic.insert"/></label>

                    <input type="hidden" name="idmuseum" value="${idmuseum}"/>
                    <input type="hidden" name="idfloor" value="${museummap.idmuseummap}"/>
                    <input type="hidden" name="isfloor" value="${museummap.isfloor}"/>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="name" name="name" placeholder="${museummap.name}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="name" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${floornumberPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="floormap.floornumber" name="floormap.floornumber"
                              placeholder="${musemummap.floormap.floornumber}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="institution" cssClass="form-error-field"/></label>

                    <label class="section-title"><spring:message code="rest.insert"/></label></br>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${textPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:textarea type="text" path="text" name="text" placeholder="${museummap.text}" rows="4"
                              class="form-control"></form:textarea>
                        </div>
                    </div>
                    <label><form:errors path="text" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${texturlPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="texturl" name="texturl" placeholder="${museuummap.texturl}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="texturl" cssClass="form-error-field"/></label>

                    <div>
                        <button type="submit" class="btn btn-secondary btn-block" formaction="floors/floor/set/additional" formmethod="post">${additionalText}</button>
                    </div>

                    <%--LOCATION INSERTING--%>

                    <%--START LOCATION--%>

                    <label class="section-title"><spring:message code="location.insert"/></label>

                    <div class="row form-group justify-content-center">
                        <div class="col-sm-4">
                            <label class="section-title"><spring:message code="location.insert.start"/></label>

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">${xPlaceholder}</label>
                                <div class="col-sm-8">
                                    <form:input type="double" path="floormap.xstart" name="floormap.xstart"
                                      placeholder="${museummap.floormap.xstart}"
                                      class="form-control"></form:input>
                                </div>
                            </div>
                            <label><form:errors path="floormap.xstart" cssClass="form-error-field"/></label>


                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">${yPlaceholder}</label>
                                <div class="col-sm-8">
                                    <form:input type="double" path="floormap.ystart" name="floormap.ystart"
                                      placeholder="${museummap.floormap.ystart}"
                                      class="form-control"></form:input>
                                </div>
                            </div>
                            <label><form:errors path="floormap.ystart" cssClass="form-error-field"/></label>

                        </div>

                            <%--END LOCATION--%>

                        <div class="col-sm-4">
                            <label class="section-title"><spring:message code="location.insert.end"/></label>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">${xPlaceholder}</label>
                                <div class="col-sm-8">
                                    <form:input type="double" path="floormap.xend" name="floormap.xend"
                                      placeholder="${museummap.floormap.xend}"
                                      class="form-control"></form:input>
                                </div>
                            </div>
                            <label><form:errors path="floormap.xend" cssClass="form-error-field"/></label>


                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">${yPlaceholder}</label>
                                <div class="col-sm-8">
                                    <form:input type="double" path="floormap.yend" name="floormap.yend"
                                      placeholder="${museummap.floormap.yend}"
                                      class="form-control"></form:input>
                                </div>
                            </div>
                            <label><form:errors path="floormap.yend" cssClass="form-error-field"/></label>

                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">${zPlaceholder}</label>
                        <div class="col-sm-10">
                            <form:input type="double" path="floormap.zcoordinate" name="floormap.zcoordinate"
                              placeholder="${museummap.floormap.zcoordinate}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="floormap.zcoordinate" cssClass="form-error-field"/></label>

                    <div>
                        <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
                    </div>

                </form:form>
            </div>
        </section>
        <script>
			if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {
				if (document.URL.substring(window.location.origin.length, document.URL.indexOf("?")) == "/floors/floorform") {
					history.go(-1);
				}
			}
        </script>
    </body>
</html>

