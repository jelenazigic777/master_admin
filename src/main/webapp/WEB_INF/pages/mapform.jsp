<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.map.form"/></title>
        <style type="text/css">
            <%@include file="static/css/basic.css" %>
        </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>

    <body>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.floornumber" var="floornumberPlaceholder"/>
        <spring:message code="placeholder.text" var="textPlaceholder"/>
        <spring:message code="placeholder.texturl" var="texturlPlaceholder"/>
        <spring:message code="placeholder.texturllocal" var="texturllocalPlaceholder"/>
        <spring:message code="placeholder.imageurl" var="imageurlPlaceholder"/>
        <spring:message code="placeholder.imageurllocal" var="imageurllocalPlaceholder"/>
        <spring:message code="placeholder.videourl" var="videourlPlaceholder"/>
        <spring:message code="placeholder.videourllocal" var="videourllocalPlaceholder"/>
        <spring:message code="placeholder.x" var="xPlaceholder"/>
        <spring:message code="placeholder.y" var="yPlaceholder"/>
        <spring:message code="placeholder.z" var="zPlaceholder"/>
        <spring:message code="placeholder.radius" var="radiusPlaceholder"/>
        <spring:message code="add.item.museum" var="addItemPlaceholder"/>
        <spring:message code="add.room.museum" var="addRoomsPlaceholder"/>
        <spring:message code="set.items.and.rooms.on.map" var="setImagesAndRoomsOnMapPlaceholder"/>
        <spring:message code="placeholder.isstaircase" var="isStaircasePlaceholder"/>
        <spring:message code="placeholder.set.other.language" var="setMapLanguagePlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <section>
            <div class="container">

                <h3 class="center-headline"><spring:message code="add.map.form.title"/></h3>
                <form:form action="maps/mapform" class="form-signin"
                  method="POST"
                  role="form"
                  commandName="museummap">

                    <img class="design-image" src="${museummap.imageurl}"/>

                    <h6 class="success-message">${successMessage}</h6>

                    <c:if test="${museummap.idmuseummap != 0}">
                        <input class="btn btn-primary remove-button btn-position-float-right" type="submit"
                          value="${setMapLanguagePlaceholder}"
                          formaction="languages/museummap" formmethod="get"/>
                    </c:if>
                    <br>
                    <br>
                    <input class="btn btn-secondary btn-position-float-right" type="submit"
                      value="${setImagesAndRoomsOnMapPlaceholder}"
                      formaction="maps/map/set" formmethod="get"/>

                    <label class="section-title"><spring:message code="basic.insert"/></label>

                    <input type="hidden" name="idmuseum" value="${idmuseum}"/>
                    <input type="hidden" name="idroom" value="${room.idroom}"/>

                    <form:hidden path="museum" name="museum"/>
                    <form:hidden path="institution" name="institution"/>
                    <form:hidden path="idmuseummap" name="idmuseummap"/>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="name" name="name" placeholder="${museummap.name}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="name" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="floormap.floornumber" class="col-sm-4 col-form-label">${floornumberPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input readonly="true" type="text" path="floormap.floornumber" name="floormap.floornumber"
                              placeholder="${musemummap.floormap.floornumber}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="floormap.floornumber" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">${isStaircasePlaceholder}</label>
                        <div class="col-sm-10">
                            <form:checkbox value="${museummap.isstaircase}" path="isstaircase" name="isstaircase"/>
                        </div>
                    </div>
                    <label><form:errors path="isstaircase" cssClass="form-error-field"/></label>

                    <label class="section-title"><spring:message code="rest.insert"/></label></br>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${textPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:textarea type="text" path="text" name="text" placeholder="${museummap.text}" rows="4"
                              class="form-control"></form:textarea>
                        </div>
                    </div>
                    <label><form:errors path="text" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${texturlPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="texturl" name="texturl" placeholder="${museuummap.texturl}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="texturl" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${texturllocalPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="texturllocal" name="texturllocal"
                              placeholder="${museuummap.texturllocal}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="texturllocal" cssClass="form-error-field"/></label>

                    <%--LOCATION INSERTING--%>

                    <%--START LOCATION--%>

                    <label class="section-title"><spring:message code="location.insert"/></label>

                    <div class="row form-group justify-content-center">
                        <div class="col-sm-4">
                            <label class="section-title"><spring:message code="location.insert.start"/></label>

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">${xPlaceholder}</label>
                                <div class="col-sm-8">
                                    <form:input type="double" path="floormap.xstart" name="floormap.xstart"
                                      placeholder="${museummap.floormap.xstart}"
                                      class="form-control"></form:input>
                                </div>
                            </div>
                            <label><form:errors path="floormap.xstart" cssClass="form-error-field"/></label>


                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">${yPlaceholder}</label>
                                <div class="col-sm-8">
                                    <form:input type="double" path="floormap.ystart" name="floormap.ystart"
                                      placeholder="${museummap.floormap.ystart}"
                                      class="form-control"></form:input>
                                </div>
                            </div>
                            <label><form:errors path="floormap.ystart" cssClass="form-error-field"/></label>

                        </div>

                            <%--END LOCATION--%>

                        <div class="col-sm-4">
                            <label class="section-title"><spring:message code="location.insert.end"/></label>
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">${xPlaceholder}</label>
                                <div class="col-sm-8">
                                    <form:input type="double" path="floormap.xend" name="floormap.xend"
                                      placeholder="${museummap.floormap.xend}"
                                      class="form-control"></form:input>
                                </div>
                            </div>
                            <label><form:errors path="floormap.xend" cssClass="form-error-field"/></label>


                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">${yPlaceholder}</label>
                                <div class="col-sm-8">
                                    <form:input type="double" path="floormap.yend" name="floormap.yend"
                                      placeholder="${museummap.floormap.yend}"
                                      class="form-control"></form:input>
                                </div>
                            </div>
                            <label><form:errors path="floormap.yend" cssClass="form-error-field"/></label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">${zPlaceholder}</label>
                        <div class="col-sm-10">
                            <form:input readonly="true" type="double" path="floormap.zcoordinate" name="floormap.zcoordinate"
                              placeholder="${museummap.floormap.zcoordinate}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="floormap.zcoordinate" cssClass="form-error-field"/></label>

                    <div>
                        <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
                    </div>

                </form:form>
            </div>
        </section>
        <script>
			if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {
				if (document.URL.substring(window.location.origin.length, document.URL.indexOf("?")) == "/maps/mapform") {
					history.go(-1);
				}
			}
        </script>
    </body>
</html>

