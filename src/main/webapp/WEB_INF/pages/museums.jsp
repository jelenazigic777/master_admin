<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.museums"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>
        <jsp:include page="header.jsp"/>

        <spring:message code="delete" var="deletePlaceholder"/>

        <section>
            <div class="container">

                <h6 class="success-message">${errorMessage}</h6>
                <label class="section-title-main"><spring:message code="museums.list"/></label>

                <ul class="list-group">
                    <c:forEach var="museum" items="${museums}">
                        <form action="museums/museumform/update" method="get">
                            <input type="hidden" name="idmuseum" value="${museum.idmuseum}">
                            <input type="hidden" name="idinstitution" value="${idinstitution}">
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit"
                                      class="list-group-item list-group-item-action center-txt">${museum.name}</button>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-danger list-button" type="submit" value="${deletePlaceholder}"
                                      formaction="museums/museum/delete" formmethod="get"/>
                                </div>
                            </div>
                        </form>
                    </c:forEach>
                </ul>

                <form action="museums/museumform" method="get">
                    <input type="hidden" name="idinstitution" value="${idinstitution}">
                    <button class="btn-link  btn-position" type="Submit">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                    </button>
                </form>
            </div>
        </section>
    </body>
</html>

