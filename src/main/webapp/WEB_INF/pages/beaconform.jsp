<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.beacon.form"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>

    <body>
        <spring:message code="placeholder.id" var="namePlaceholder"/>
        <spring:message code="placeholder.power" var="powerPlaceholder"/>
        <spring:message code="placeholder.minor" var="minorPlaceholder"/>
        <spring:message code="placeholder.major" var="majorPlaceholder"/>
        <spring:message code="placeholder.x" var="xPlaceholder"/>
        <spring:message code="placeholder.y" var="yPlaceholder"/>
        <spring:message code="placeholder.z" var="zPlaceholder"/>
        <spring:message code="placeholder.radius" var="radiusPlaceholder"/>
        <spring:message code="button.additional.text" var="additionalText"/>
        <spring:message code="choose.map.text" var="choosePlaceholder"/>
        <spring:message code="map.text" var="mapsPlaceholder"/>
        <spring:message code="set.placeholder" var="setPlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <script>
			$(function () {
				$("select").imagepicker();
			});
        </script>
        <section>

            <div class="container">

                <h3 class="center-headline"><spring:message code="add.beacon.form.title"/></h3>
                <form:form action="beacons/beaconform" class="form-signin"
                  method="POST"
                  role="form"
                  commandName="beacon">

                    <h6 class="success-message">${successMessage}</h6>

                    <label class="section-title"><spring:message code="basic.insert"/></label>

                    <input type="hidden" name="idmuseum" value="${idmuseum}"/>

                    <input type="hidden" id="otherBeacons" value='${otherBeacons}'/>

                    <form:hidden path="idbeacon" name="idbeacon"/>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="name" name="name" placeholder="${beacon.name}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="name" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="power" class="col-sm-4 col-form-label">${powerPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="integer" path="power" name="power" placeholder="${beacon.power}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="power" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="minor" class="col-sm-4 col-form-label">${minorPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="integer" path="minor" name="minor" placeholder="${beacon.minor}" rows="4"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="minor" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="major" class="col-sm-4 col-form-label">${majorPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="integer" path="major" name="major" placeholder="${beacon.major}" rows="4"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="major" cssClass="form-error-field"/></label>


                    <label class="section-title"><spring:message code="location.insert"/></label>


                    <div class="row">
                        <div class="change-margin">
                            <canvas id="map-canvas"></canvas>
                                <img id="beacon-coordinates-img" style="height: 400px; max-width: 100%;" src="${museummap.floormap.imageurl}" ismap/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="xcoordinate" class="col-sm-4 col-form-label">${xPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="xcoordinate" type="double" path="xcoordinate" name="xcoordinate" placeholder="${beacon.xcoordinate}"
                              class="form-control" readonly="true"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="xcoordinate" cssClass="form-error-field"/></label>


                    <div class="form-group row">
                        <label for="ycoordinate" class="col-sm-4 col-form-label">${yPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="ycoordinate" type="double" path="ycoordinate" name="yxcoordinate"
                              placeholder="${beacon.ycoordinate}"
                              class="form-control" readonly="true"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="ycoordinate" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="zcoordinate" class="col-sm-4 col-form-label">${zPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="double" path="zcoordinate" name="zcoordinate" placeholder="${beacon.zcoordinate}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="zcoordinate" cssClass="form-error-field"/></label>

                    <input type="hidden" id="xStartMuseummap" value="${floormap.xstart}"/>
                    <input type="hidden" id="yStartMuseummap" value="${floormap.ystart}"/>
                    <input type="hidden" id="xEndMuseummap" value="${floormap.xend}"/>
                    <input type="hidden" id="yEndMuseummap" value="${floormap.yend}"/>

                    <div class="form-group row">
                        <label for="radius" class="col-sm-4 col-form-label">${radiusPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="radius" type="double" path="radius" name="radius" placeholder="${beacon.radius}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="radius" cssClass="form-error-field"/></label>

                    <label class="section-title">${mapsPlaceholder}</label>
                    <%--<div class="row">--%>
                        <%--<div class="col-lg-3 change-margin images-custom">--%>
                            <%--<div class="card">--%>
                                <%--<div class="thumbnail">--%>
                                    <%--<img class="design-image-columns" src="${museummap.floormap.imageurl}"/>--%>
                                <%--</div>--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    <%--</div>--%>

                    <label class="col-form-label"> ${choosePlaceholder} </label>
                    <select name="selectedFloormap" class="custom-select">
                        <option value="0"></option>
                        <c:forEach var="floormap" items="${floormaps}">
                            <option data-img-src="${floormap.imageurl}" value="${floormap.idfloormap}"></option>
                        </c:forEach>
                    </select>
                    <input type="hidden" name="idbeacon" value="${idbeacon}"/>
                    <input class="btn btn-primary btn-position-float-right" style="margin-bottom:10px" type="submit" value="${setPlaceholder}"
                      formaction="beacons/set/map" formmethod="post"/>

                    <div>
                        <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
                    </div>

                </form:form>
            </div>
        </section>
        <script>
        <%@include file="static/js/beacon.js" %>
        </script>
    </body>
</html>

