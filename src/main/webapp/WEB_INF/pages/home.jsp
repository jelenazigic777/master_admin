<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.home"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>
        <spring:message code="delete" var="deletePlaceholder"/>
        <spring:message code="languages.open.editor" var="languagesPlaceholder"/>
        <spring:message code="admin.page" var="adminPlaceholder"/>

        <jsp:include page="header.jsp"/>

        <section>
            <form>
                <input class="btn btn-primary languages-open-editor btn-position-float-right-2" type="submit" value="${languagesPlaceholder}"
                  formaction="languages/editor" formmethod="get"/>
            </form>
            <br>
            <br>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <form>
                    <input class="btn btn-primary remove-button btn-position-float-right-2" type="submit" value="${adminPlaceholder}"
                      formaction="admin" formmethod="get"/>
                </form>
            </sec:authorize>
            <div class="container">
                <h6 class="success-message">${errorMessage}</h6>
                <label class="section-title-main"><spring:message code="institutions.list"/></label>

                <ul class="list-group">
                    <c:forEach var="institution" items="${institutions}">
                        <form action="institutions/institutionform/update" method="get">
                            <input type="hidden" name="idinstitution" value="${institution.idinstitution}">
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit"
                                      class="list-group-item list-group-item-action center-txt">${institution.name}</button>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-danger list-button" type="submit" value="${deletePlaceholder}"
                                      formaction="institutions/institution/delete" formmethod="get">
                                </div>
                            </div>
                        </form>
                    </c:forEach>
                </ul>

                <form action="institutions/institutionform" method="get">
                    <button class="btn-link  btn-position" type="Submit">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                    </button>
                </form>
            </div>
        </section>
    </body>
</html>

