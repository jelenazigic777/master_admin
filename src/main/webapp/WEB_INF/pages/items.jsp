<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.items"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>

        <spring:message code="delete" var="deletePlaceholder"/>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.author" var="authorPlaceholder"/>
        <spring:message code="placeholder.code" var="codePlaceholder"/>
        <spring:message code="placeholder.exhibition.name" var="exhibitionNamePlaceholder"/>
        <spring:message code="placeholder.room.name" var="roomNamePlaceholder"/>
        <spring:message code="placeholder.x" var="xPlaceholder"/>
        <spring:message code="placeholder.y" var="yPlaceholder"/>
        <spring:message code="placeholder.z" var="zPlaceholder"/>
        <spring:message code="placeholder.deviation" var="deviationPlaceholder"/>
        <spring:message code="placeholder.search" var="searchPlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>
        <section>
            <form class="btn-position-float-right-2">
                <input type="hidden" name="idmuseum" value="${idmuseum}">
                <c:if test="${isActive == false}">
                    <input class="btn btn-primary" type="submit"
                      value="${activeInactiveButtonText}"
                      formaction="items" formmethod="get"/>
                </c:if>
                <c:if test="${isActive == true}">
                    <input class="btn btn-primary" type="submit"
                      value="${activeInactiveButtonText}"
                      formaction="items/inactive" formmethod="get"/>
                </c:if>
            </form>
            <div class="container">

                <form style="border-bottom-style: inset">
                    <label class="section-title-main"><spring:message code="search"/></label>
                    <div class="btn-position-float-right">
                        <i class="glyphicon glyphicon-search form-control-feedback"></i>
                        <input class="btn btn-primary" type="submit" value="${searchPlaceholder}"
                          formaction="items/search" formmethod="get"/>
                    </div>

                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="name"
                              class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="author" class="col-sm-4 col-form-label">${authorPlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="author"
                              class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="code" class="col-sm-4 col-form-label">${codePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="code"
                              class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="exhibition.name" class="col-sm-4 col-form-label">${exhibitionNamePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="exhibitionName"
                              class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="museummap.name" class="col-sm-4 col-form-label">${roomNamePlaceholder}</label>
                        <div class="col-sm-8">
                            <input type="text" name="roomName"
                              class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-3">
                            <label for="x" class="col-sm-6 col-form-label">${xPlaceholder}</label>
                            <div class="col-sm-6">
                                <input type="text" name="xCoordinate"
                                  class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="y" class="col-sm-6 col-form-label">${yPlaceholder}</label>
                            <div class="col-sm-6">
                                <input type="text" name="yCoordinate"
                                  class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="z" class="col-sm-6 col-form-label">${zPlaceholder}</label>
                            <div class="col-sm-6">
                                <input type="text" name="zCoordinate"
                                  class="form-control">
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="deviation" class="col-sm-6 col-form-label">${deviationPlaceholder}</label>
                            <div class="col-sm-6">
                                <input type="text" name="deviation"
                                  class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
                <br>
                <br>
                <br>

                <label class="section-title-main"><spring:message code="items.list"/></label>

                <div class="row">
                    <c:forEach var="item" items="${items}">
                        <div class="col-lg-3 change-margin images-custom">
                            <form action="items/itemform/update" method="get">
                                <input type="hidden" name="idmuseum" value="${idmuseum}">
                                <input type="hidden" name="iditem" value="${item.iditem}">
                                <div class="card">
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="/items/item/delete" formmethod="get"/>
                                    <button type="submit"
                                      class="center-txt">
                                        <div class="thumbnail">
                                            <img class="design-image-columns" src="${item.images[0].imageurl}"/>
                                        </div>
                                    </button>
                                    <div class="card-body">
                                        <div class="col-sm-12">
                                            <p class="card-text card-text-extra"><b>${namePlaceholder}:</b> ${item.name}</p>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="card-text card-text-extra"><b>${authorPlaceholder}:</b> ${item.author}</p>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </c:forEach>
                </div>

                <form action="items/itemform" method="get">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <button class="btn-link  btn-position" type="Submit">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                    </button>
                </form>
            </div>
        </section>
    </body>
</html>

