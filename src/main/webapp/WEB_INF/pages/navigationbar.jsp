<head>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <style type="text/css">
        <%@include file="static/css/navigation.css" %>
    </style>
    <script>
		<%@include file="static/js/navigation.js" %>
    </script>
    <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
</head>

<spring:message code="placeholder.home" var="home"/>
<spring:message code="placeholder.museums" var="musuems"/>
<spring:message code="placeholder.items" var="items"/>
<spring:message code="placeholder.exhibitions" var="exhibitions"/>
<spring:message code="placeholder.floors" var="floors"/>
<spring:message code="placeholder.beacons" var="beacons"/>

<span class="glyphicon glyphicon-indent-left glyphicon-custom-size" onclick="openNav()"></span>
<input type="hidden" id="baseUrl" value="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}">
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="home">${home}</a>
    <a id="museums" href="" onclick="gotoMuseums()">${musuems}</a>
    <a id="items" href="" onclick="gotoItems()">${items}</a>
    <a id="exhibitions" href="" onclick="gotoExhibitions()">${exhibitions}</a>
    <a id="floors" href="" onclick="getoFloors()">${floors}</a>
    <a id="beacons" href="" onclick="gotoBeacons()">${beacons}</a>
</div>
