<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.item.form.additional"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>

    <body>
        <spring:message code="placeholder.texturl" var="texturlPlaceholder"/>
        <spring:message code="placeholder.imageurl" var="imageurlPlaceholder"/>
        <spring:message code="placeholder.barcode" var="barcodePlaceholder"/>
        <spring:message code="placeholder.videourl" var="videourlPlaceholder"/>
        <spring:message code="placeholder.audiourl" var="audiourlPlaceholder"/>
        <spring:message code="placeholder.videourl.add.manually" var="videourladdmanuallyPlaceholder"/>
        <spring:message code="placeholder.video.urls" var="videoUrlsPlaceholder"/>

        <spring:message code="set.additional.on.item" var="setAll"/>
        <spring:message code="add.new.item.video.url" var="addnewItem"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>
        <section>

            <div class="container">

                <%--Here images should be inserted--%>
                <label class="section-title"><spring:message code="images"/></label>

                <form action="items/item/set" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="iditem" value="${iditem}"/>
                    <input type="hidden" name="idmuseum" value="${idmuseum}"/>
                    <div class="form-group row">
                        <label for="images" class="col-sm-4"> ${imageurlPlaceholder} </label>
                        <input class="col-sm-8" type="file" name="images" multiple="multiple"/>
                    </div>

                    <div class="row">
                        <c:forEach var="img" items="${itemimages}">
                            <div class="col-lg-3 change-margin images-custom">
                                <div class="card">
                                    <input type="hidden" name="iditemimage" value="${img.iditemimage}">
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="items/image/delete/${img.iditemimage}" formmethod="get"/>
                                    <div class="thumbnail">
                                        <img class="design-image-columns" src="${img.imageurl}"/>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>

                    <label class="section-title"><spring:message code="audios"/></label>

                    <div class="form-group row">
                        <label for="audios" class="col-sm-4"> ${audiourlPlaceholder} </label>
                        <input class="col-sm-8" type="file" name="audios" multiple="multiple"/>
                    </div>

                    <div class="row">
                        <c:forEach var="aud" items="${itemaudios}">
                            <div class="change-margin uploadItem col-md-6">
                                <audio controls class="col-md-10">
                                    <source src="${aud.audiourl}" type="audio/mpeg">
                                </audio>
                                <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                  formaction="items/audio/delete/${aud.iditemaudio}" formmethod="get"/>
                            </div>
                        </c:forEach>
                    </div>

                    <label class="section-title"><spring:message code="videos"/></label>

                    <div class="form-group row">
                        <label for="videos" class="col-sm-4"> ${videourlPlaceholder} </label>
                        <input class="col-sm-8" type="file" name="videos" multiple="multiple"/>
                    </div>

                    <div class="row">
                        <c:forEach var="vid" items="${itemvideos}">
                            <div class="change-margin uploadItem col-md-6">
                                <video controls="" name="media" style="width:calc(100% + 16px)">
                                    <source src="${vid.videourl}" type="video/mp4">
                                </video>
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                  formaction="items/video/delete/${vid.iditemvideo}" formmethod="get"/>
                            </div>
                        </c:forEach>
                    </div>

                    <label> ${videoUrlsPlaceholder} </label>
                    <br>
                    <br>

                    <c:forEach var="video" items="${videoUrls}">
                        <div>
                            <a href="${video.videourl}">${video.videourl}</a>
                            <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                              formaction="items/video/delete/${video.iditemvideo}"
                              formmethod="get"/>
                        </div>
                    </c:forEach>

                    <br>
                    <label class="col-sm-4"> ${videourladdmanuallyPlaceholder} </label>

                    <div id="videourlitems" class="display-block">
                        <input type="text" name="urlvideo" class="form-control">
                    </div>

                    <div class="btn-position-float-right">
                        <button type="button" name="addnewItem" class="btn btn-primary back-button btn-bg"
                          onclick="addNewInputField()">
                            ${addnewItem}
                        </button>
                    </div>

                    <br>
                    <label class="section-title"><spring:message code="texts"/></label>

                    <c:forEach var="txt" items="${itemtexts}">
                        <ul class="list-group">
                            <li class="list-group-item uploadItem" data-toggle="tooltip" data-placement="top" title="${txt.text}">
                                    ${txt.text}
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                  formaction="items/text/delete/${txt.iditemtext}" style="border-radius: 0 3px 3px 0;" formmethod="get"/>
                            </li>
                        </ul>
                    </c:forEach>


                    <div id="textitems" class="display-block">
                        <textarea type="text" name="text" class="form-control"></textarea>
                    </div>

                    <div class="btn-position-float-right">
                        <button type="button" name="addnewItem" class="btn btn-primary back-button btn-bg"
                          onclick="addNewInputFieldText()">
                            ${addnewItem}
                        </button>
                    </div>

                    <label class="section-title"><spring:message code="barcode"/></label>

                    <c:if test="${barcode == null}">
                        <div class="form-group row">
                            <label for="barcode" class="col-sm-4"> ${barcodePlaceholder} </label>
                            <input class="col-sm-8" type="file" name="barcode"/>
                        </div>
                    </c:if>

                    <c:if test="${barcode != null}">
                        <div class="col-lg-3 change-margin uploadItem">
                            <div>
                                <img class="design-image" src="${barcode.imageurl}">
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                  formaction="items/barcode/delete/${barcode.idbarcodeimage}" formmethod="get"/>
                            </div>
                        </div>
                    </c:if>

                    <button type="submit" class="btn btn-secondary btn-block change-margin-top">${setAll}</button>
                </form>

            </div>
        </section>
    </body>
</html>

