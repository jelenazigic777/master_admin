<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.exhibition.form.additional"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>

        <spring:message code="delete" var="deletePlaceholder"/>
        <spring:message code="add.item.new" var="addItemPlaceholder"/>
        <spring:message code="add.room.new" var="addMapPlaceholder"/>
        <spring:message code="set.additional.on.item" var="setAll"/>
        <spring:message code="ambient.audio" var="ambientAudioPlaceholder"/>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.author" var="authorPlaceholder"/>
        <spring:message code="set.placeholder" var="setPlaceholder"/>
        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <script>
			$(function () {
				$("select").imagepicker();
			});
        </script>
        <section>
            <div class="container">
                <label class="section-title"><spring:message code="museum.items"/></label>

                <label class="col-form-label"><spring:message code="items.manage"/></label>

                <div class="row">
                    <c:forEach var="item" items="${exhibitionitems}">
                        <div class="col-lg-3 change-margin images-custom">
                            <form action="exhibitions/set/item/additional" method="get">
                                <input type="hidden" name="idmuseum" value="${idmuseum}">
                                <input type="hidden" name="idexhibition" value="${idexhibition}">
                                <input type="hidden" name="iditem" value="${item.iditem}">
                                <div class="card">
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="exhibitions/delete/item" formmethod="get"/>
                                    <button type="submit"
                                      class="center-txt">
                                        <div class="thumbnail">
                                            <img class="design-image-columns" src="${item.images[0].imageurl}"/>
                                        </div>
                                    </button>
                                    <div class="card-body">
                                        <div class="col-sm-12">
                                            <p class="card-text card-text-extra"><b>${namePlaceholder}:</b> ${item.name}</p>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="card-text card-text-extra"><b>${authorPlaceholder}:</b> ${item.author}</p>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </c:forEach>
                </div>

                <%--ITEMS SETTING--%>
                <label class="col-form-label"><spring:message code="items.insert"/></label>

                <form action="exhibitions/exhibition/set/additional" method="post">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <input type="hidden" name="idexhibition" value="${idexhibition}">
                    <select name="selectedItems" class="image-picker show-html" id="availableitems" multiple="multiple">
                        <c:forEach var="pict" items="${itemstoselect}">
                            <option data-img-src="${pict.images[0].imageurl}" value="${pict.iditem}">${pict.name}</option>
                        </c:forEach>
                    </select>
                    <button type="submit" class="btn btn-primary btn-position-float-right" style="margin-left:10px">
                        ${setPlaceholder}
                    </button>
                </form>

                <form action="items/itemfromanothersource" method="get">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <button class="btn btn-secondary btn-position" type="Submit">
                        ${addItemPlaceholder}
                    </button>
                </form>
                <br>
                <br>

                <%--MAPS SETTING--%>
                <label class="section-title"><spring:message code="museummaps.insert"/></label>

                <c:forEach var="response" items="${responseList}" varStatus="loop">
                    <div class="row">
                        <div class="change-margin">
                            <canvas id="map-canvas_${loop.count}"></canvas>
                            <img id="map-coordinates-img_${loop.count}" style="height: 400px; max-width: 100%;" src="${response.floor.floormap.imageurl}" ismap/>
                            <input type="hidden" id="xEndMuseummap_${loop.count}" value="${response.xEndCoordinate}"/>
                            <input type="hidden" id="yEndMuseummap_${loop.count}" value="${response.yEndCoordinate}"/>
                            <input type="hidden" id="jsonItems_${loop.count}" value='${response.jsonItems}'/>
                        </div>
                    </div>
                </c:forEach>
                <input type="hidden" id="mapListSize" value="${responseListSize}"/>

                <form enctype="multipart/form-data">
                    <input type="hidden" name="idexhibition" value="${idexhibition}">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <label class="section-title"><spring:message code="ambientaudio.insert"/></label>
                    <div class="row">
                        <c:if test="${exhibition.ambientaudio.exhibition != null && exhibition.ambientaudio.room == null}">
                            <div class="col-md-6 change-margin">
                                <audio controls class="col-sm-10">
                                    <source src="${exhibition.ambientaudio.ambientaudiourl}" type="audio/mpeg">
                                </audio>
                                <form>
                                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                                    <input type="hidden" name="idexhibition" value="${idexhibition}">
                                    <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                      formaction="exhibitions/ambientaudio/delete/${exhibition.ambientaudio.idambientaudio}"
                                      formmethod="get"/>
                                </form>
                            </div>
                        </c:if>
                    </div>
                    <c:if test="${exhibition.ambientaudio==null}">
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">${ambientAudioPlaceholder}</label>
                            <div class="col-sm-6 change-margin">
                                <input type="file" name="ambientaudio">
                            </div>
                        </div>
                    </c:if>

                    <br>
                    <div class="justify-content-center">
                        <input class="btn btn-primary btn-block" type="submit" value="${setAll}"
                          formaction="exhibitions/goto/exhibitionform/additional" formmethod="post"/>
                    </div>
                </form>
            </div>
        </section>
        <script>
			<%@include file="static/js/exhibition.js" %>
        </script>
    </body>
</html>


