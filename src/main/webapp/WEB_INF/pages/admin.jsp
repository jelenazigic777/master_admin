<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.users"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>
        <jsp:include page="header.jsp"/>

        <spring:message code="approve" var="approvePlaceholder"/>
        <spring:message code="delete.user" var="deletePlaceholder"/>
        <spring:message code="user.name" var="namePlaceholder"/>
        <spring:message code="user.surname" var="surnamePlaceholder"/>
        <spring:message code="user.username" var="usernamePlaceholder"/>

        <section>
            <div class="container">

                <label class="section-title-main"><spring:message code="users.list"/></label>

                <ul class="list-group">
                    <c:forEach var="user" items="${users}">
                        <form>
                            <input type="hidden" name="iduser" value="${user.iduser}">
                            <div class="form-group row">
                                <div class="col-sm-8 form-row center-txt">
                                    <div class="col-sm-4"><b>${namePlaceholder}:</b></div>
                                    <div class="col-sm-4"><b>${surnamePlaceholder}:</b></div>
                                    <div class="col-sm-4"><b>${usernamePlaceholder}:</b></div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-8 form-row list-group-item center-txt">
                                    <div class="col-sm-4"><b>${user.name}</b></div>
                                    <div class="col-sm-4"><b>${user.surname}</b></div>
                                    <div class="col-sm-4"><b>${user.username}</b></div>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-danger list-button blue-bg" type="submit" value="${approvePlaceholder}"
                                      formaction="admin/user/setActive" formmethod="get"/>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-danger list-button" type="submit" value="${deletePlaceholder}"
                                      formaction="admin/user/delete" formmethod="get"/>
                                </div>
                            </div>
                        </form>
                    </c:forEach>
                </ul>

            </div>
        </section>
    </body>
</html>

