<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.beacons"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>
        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <spring:message code="delete" var="deletePlaceholder"/>

        <section>
            <div class="container">

                <label class="section-title-main"><spring:message code="beacons.list"/></label>

                <ul class="list-group">
                    <c:forEach var="beacon" items="${beacons}">
                        <form action="beacons/beaconform/update" method="get">
                            <input type="hidden" name="idbeacon" value="${beacon.idbeacon}">
                            <input type="hidden" name="idmuseum" value="${idmuseum}">
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit"
                                      class="list-group-item list-group-item-action center-txt">${beacon.name}</button>
                                </div>
                                <div class="col-sm-2">
                                    <input class="btn btn-danger list-button" type="submit" value="${deletePlaceholder}"
                                      formaction="beacons/beacon/delete" formmethod="get"/>
                                </div>
                            </div>
                        </form>
                    </c:forEach>
                </ul>

                <form action="beacons/beaconform" method="get">
                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <button class="btn-link  btn-position" type="Submit">
                        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                    </button>
                </form>
            </div>
        </section>
    </body>
</html>

