<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.item.form.languages"/></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script>
			$(document).ready(function () {
				$('#selectedLanguage').on('change', function () {
					getItemLanguage($('#selectedLanguage option:selected').attr('value'), $('#iditem').attr('value'));
				});
			});
        </script>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>
    </head>

    <body>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.text" var="textPlaceholder"/>
        <spring:message code="placeholder.texturl" var="texturlPlaceholder"/>
        <spring:message code="placeholder.language" var="languagePlaceholder"/>
        <spring:message code="placeholder.authors" var="authorPlaceholder"/>
        <spring:message code="placeholder.video" var="videоPickPlaceholder"/>
        <spring:message code="placeholder.video.title" var="videoPlaceholder"/>
        <spring:message code="placeholder.audio" var="audioPickPlaceholder"/>
        <spring:message code="placeholder.audio.title" var="audioPlaceholder"/>
        <jsp:include page="header.jsp"/>

        <section>
            <div class="container">
                <h3 class="center-headline"><spring:message code="add.item.form.title"/></h3>
                <form:form action="languages/item" class="form-signin"
                  method="POST"
                  enctype="multipart/form-data"
                  role="form"
                  commandName="itemlanguages">

                    <h6 class="success-message">${successMessage}</h6>

                    <input type="hidden" name="iditem" id="iditem" value="${iditem}">


                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${languagePlaceholder}</label>
                        <div class="col-sm-8">

                            <select id="selectedLanguage"
                              name="selectedLanguage" class="form-control" onchange="">
                                <c:forEach items="${languages}" var="language">
                                    <option value="${language.idlanguages}">
                                            ${language.languagename}
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <form:input id="iditemlanguages" type="hidden" path="iditemlanguages" name="iditemlanguages" value="${itemlanguages.iditemlanguages}"
                      class="form-control"></form:input>
                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="name" type="text" path="name" name="name" value="${itemlanguages.name}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="name" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${authorPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="author" type="text" path="author" name="author" value="${itemlanguages.author}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="name" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="text" class="col-sm-4 col-form-label">${textPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:textarea id="text" type="text" path="text" name="text" value="${itemlanguages.text}" rows="4"
                              class="form-control"></form:textarea>
                        </div>
                    </div>
                    <label><form:errors path="text" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="texturl" class="col-sm-4 col-form-label">${texturlPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input id="texturl" type="text" path="texturl" name="texturl" value="${itemlanguages.texturl}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="texturl" cssClass="form-error-field"/></label>

                    <label class="section-title-2">${audioPlaceholder}</label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${audioPickPlaceholder}</label>
                        <input class="col-md-8" type="file" name="pickedaudios" multiple="multiple"/>
                    </div>
                    <c:forEach var="lang" items="${languages}">
                        <label class="section-title-3">${lang.languagename}</label>
                        <c:forEach var="audio" items="${item.itemaudiolanguages}">
                            <c:if test="${audio.language.idlanguages == lang.idlanguages}">
                                <div class="col-md-6 change-margin uploadItem">
                                    <audio controls="controls" class="col-sm-10">
                                        <source src="${audio.audiourl}" type="audio/mp3"/>
                                    </audio>
                                    <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                      formaction="languages/item/audio/delete/${audio.iditemaudiolanguages}"
                                      formmethod="get"/>
                                </div>
                            </c:if>
                        </c:forEach>
                    </c:forEach>

                    <label class="section-title-2">${videoPlaceholder}</label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${videоPickPlaceholder}</label>
                        <input class="col-md-8" type="file" name="pickedvideos"/>
                    </div>
                    <c:forEach var="lang" items="${languages}">
                        <label class="section-title-3">${lang.languagename}</label>

                        <c:forEach var="video" items="${item.itemvideolanguages}">
                            <c:if test="${video.language.idlanguages == lang.idlanguages}">
                                <div class="change-margin uploadItem col-md-6">
                                    <video controls="" name="media" style="width:calc(100% + 16px)">
                                        <source src="${video.videourl}" type="video/mp4">
                                    </video>
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="languages/item/video/delete/${video.iditemvideolanguages}"
                                      formmethod="get"/>
                                </div>
                            </c:if>
                        </c:forEach>
                    </c:forEach>

                    <div class="change-margin">
                        <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
                    </div>

                </form:form>

            </div>
        </section>
    </body>
</html>

