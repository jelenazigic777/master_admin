<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.item.for.exhibition"/></title>
        <script>
			$(document).ready(function () {
				$('#selection_audio').on('change', function () {
					changeAudio($('#selection_audio option:selected').attr('title'));

				});
				$('#selection_video').on('change', function () {
					changeVideo($('#selection_video option:selected').attr('title'));
				});
			});
        </script>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>

        <spring:message code="delete" var="deletePlaceholder"/>
        <spring:message code="add.item.museum" var="addItemPlaceholder"/>
        <spring:message code="add.map.museum" var="addMapPlaceholder"/>
        <spring:message code="set.additional.on.item" var="setAll"/>
        <spring:message code="choose.text" var="choosePlaceholder"/>
        <spring:message code="go.to.item" var="itemformPlaceholder"/>
        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <script>
			$(function () {
				$(".image-picker").imagepicker();
			});
        </script>
        <section>
            <div class="container">
                <form action="exhibitions/set/item/additional" method="post">

                    <input class="btn btn-danger remove-button btn-position-float-right" type="submit"
                      value="${itemformPlaceholder}"
                      formaction="items/itemform/update" formmethod="get"/>

                    <label class="section-title"><spring:message code="images"/></label>
                    <br>
                    <div>
                        <div class="col-md-6 change-margin uploadItem">
                            <img class="design-image" src="${image.imageurl}"/>
                            <c:if test="${image != null}">
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                  formaction="exhibitions/image/remove/${image.iditemimage}"
                                  formmethod="get"/>
                            </c:if>
                        </div>
                        <div class="col-md-12">
                            <select class="image-picker show-html" name="itemimage">
                                <option value="0"></option>
                                <c:forEach var="img" items="${item.images}">
                                    <option data-img-src="${img.imageurl}" value="${img.iditemimage}"></option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <label class="section-title"><spring:message code="audios"/></label>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <select id="selection_audio" class="selectpicker" name="itemaudio" style="width:100%">
                                <option value="0" title="">${choosePlaceholder}</option>
                                <c:forEach var="aud" items="${item.audios}">
                                    <option title="${aud.audiourl}" value="${aud.iditemaudio}">${aud.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6 change-margin">

                            <c:if test="${audio != null}">
                                <audio controls="controls" class="col-md-10">
                                    <source src="${audio.audiourl}" type="audio/mp3"/>
                                </audio>
                                <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                  formaction="exhibitions/audio/remove/${audio.iditemaudio}"
                                  formmethod="get"/>
                            </c:if>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <audio id="player" controls="controls" class="col-md-10" style="display:none;">
                            <source id="mp3_src" src="" type="audio/mp3"/>
                        </audio>
                    </div>
                    <br>
                    <br>

                    <label class="section-title"><spring:message code="videos"/></label>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <select id="selection_video" name="itemvideo" style="width: 100%">
                                <option value="0" title="">${choosePlaceholder}</option>
                                <c:forEach var="vid" items="${item.videos}">
                                    <option title="${vid.videourl}" value="${vid.iditemvideo}">${vid.name}</option>
                                </c:forEach>
                            </select>
                            <br>
                            <iframe id="video_player" width="320" height="240"
                              src="" style="display: none">
                            </iframe>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="change-margin uploadItem col-md-6">
                            <c:if test="${video != null}">
                                <video controls="" name="media" style="width:calc(100% + 16px)">
                                    <source src="${video.videourl}" type="video/mp4">
                                </video>
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                  formaction="exhibitions/video/remove/${video.iditemvideo}"
                                  formmethod="get"/>
                            </c:if>
                        </div>
                    </div>
                    <br>
                    <br>

                    <label class="section-title"><spring:message code="texts"/></label>
                    <br>

                    <c:if test="${text != null}">
                        <ul class="list-group">
                            <li class="list-group-item uploadItem">
                                    ${text.text}
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x" style="height: 100%; border-radius: 0 3px 3px 0;"
                                  formaction="exhibitions/text/remove/${text.iditemtext}"
                                  formmethod="get"/>
                            </li>
                        </ul>
                    </c:if>

                    <select id="text_selection" name="itemtext" style="width: 100%">
                        <option value="0">${choosePlaceholder}</option>
                        <c:forEach var="txt" items="${item.texts}">
                            <option value="${txt.iditemtext}">${txt.text}</option>
                        </c:forEach>
                    </select>
                    <br>


                    <input type="hidden" name="idmuseum" value="${idmuseum}">
                    <input type="hidden" name="iditem" value="${iditem}">
                    <input type="hidden" name="idexhibition" value="${idexhibition}">
                    <div>
                        <button type="submit" class="btn btn-secondary btn-block change-margin-top">${setAll}</button>
                    </div>
                </form>

            </div>
        </section>
    </body>
</html>


