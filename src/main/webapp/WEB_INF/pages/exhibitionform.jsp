<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.exhibition.form"/></title>
        <script>
			<%@include file="static/js/basic.js" %>
        </script>
        <style type="text/css">
            <%@include file="static/css/basic.css" %>
        </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>

    <body>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.authors" var="authorPlaceholder"/>
        <spring:message code="placeholder.text" var="textPlaceholder"/>
        <spring:message code="placeholder.image" var="imagePlaceholder"/>
        <spring:message code="placeholder.video" var="videoPlaceholder"/>
        <spring:message code="placeholder.audio" var="audioPlaceholder"/>
        <spring:message code="placeholder.x" var="xPlaceholder"/>
        <spring:message code="placeholder.y" var="yPlaceholder"/>
        <spring:message code="placeholder.z" var="zPlaceholder"/>
        <spring:message code="placeholder.radius" var="radiusPlaceholder"/>
        <spring:message code="add.item.museum" var="addItemPlaceholder"/>
        <spring:message code="add.map.museum" var="addMapPlaceholder"/>
        <spring:message code="is.active" var="activePlaceholder"/>
        <spring:message code="set.images.and.maps.on.exhibition" var="setimagesanmapsPlaceholder"/>
        <spring:message code="copy" var="copyPlaceholder"/>
        <spring:message code="authors.from.items" var="authorsFromItemsPlaceholder"/>
        <spring:message code="placeholder.set.other.language" var="setExhibitionLanguagePlaceholder"/>

        <jsp:include page="header.jsp"/>
        <jsp:include page="navigationbar.jsp"/>

        <section>
            <div class="container">
                <h3 class="center-headline"><spring:message code="add.exhibition.form.title"/></h3>
                <form:form action="exhibitions/exhibitionform" class="form-signin"
                  method="POST"
                  enctype="multipart/form-data"
                  role="form"
                  commandName="exhibition">

                    <h6 class="success-message">${successMessage}</h6>

                    <c:if test="${showCopyButton}">
                        <input class="btn btn-primary remove-button btn-position-float-right" type="submit"
                          value="${copyPlaceholder}"
                          formaction="exhibitions/exhibition/copy" formmethod="get"/>
                    </c:if>
                    <c:if test="${exhibition.idexhibition != 0}">
                        <br>
                        <br>
                        <input class="btn btn-primary remove-button btn-position-float-right" type="submit"
                          value="${setExhibitionLanguagePlaceholder}"
                          formaction="languages/exhibition" formmethod="get"/>
                    </c:if>
                    <br>
                    <br>
                    <input class="btn btn-secondary btn-position-float-right" type="submit"
                      value="${setimagesanmapsPlaceholder}"
                      formaction="exhibitions/exhibition/set" formmethod="post"/>


                    <label class="section-title"><spring:message code="basic.insert"/></label>

                    <input type="hidden" name="idmuseum" value="${idmuseum}"/>

                    <form:hidden path="museum" name="museum"/>
                    <form:hidden path="institution" name="institution"/>
                    <form:hidden path="idexhibition" name="idexibition"/>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="name" name="name" placeholder="${exhibition.name}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="name" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="author" class="col-sm-4 col-form-label">${authorPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="author" name="author" placeholder="${exhibition.author}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="author" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">${authorsFromItemsPlaceholder}</label>
                        <div class="col-sm-8">
                            <ul class="list-group">
                                <c:forEach var="author" items="${authors}">
                                    <li class="list-group-item list-group-item-secondary">${author}</li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">${activePlaceholder}</label>
                        <div class="col-sm-10">
                            <form:checkbox value="${exhibition.active}" path="active" name="active"/>
                        </div>
                    </div>
                    <label><form:errors path="active" cssClass="form-error-field"/></label>

                    <label class="section-title"><spring:message code="rest.insert"/></label></br>

                    <div class="form-group row">
                        <label for="text" class="col-sm-4 col-form-label">${textPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:textarea type="text" path="text" name="text" placeholder="${exhibition.text}" rows="4"
                              class="form-control"></form:textarea>
                        </div>
                    </div>
                    <label><form:errors path="text" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                    <c:if test="${exhibition.image != null}">
                        <div class="col-md-6 change-margin uploadItem">
                            <img class="design-image" src="${exhibition.image.imageurl}">
                            <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                              formaction="exhibitions/image/delete/${exhibition.image.iditemimage}" formmethod="post"/>
                        </div>
                    </c:if>
                    <br>

                    <c:if test="${exhibition.image == null}">

                            <label for="name" class="col-sm-4 col-form-label">${imagePlaceholder}</label>
                            <input class="col-sm-8" type="file" name="pickedimage"/>

                    </c:if>
                    </div>
                    <div class="form-group row">
                    <c:if test="${exhibition.audio != null}">
                        <div class="uploadItem col-md-6 change-margin">
                            <audio controls="controls" class="col-md-10">
                                <source src="${exhibition.audio.audiourl}" type="audio/mp3"/>
                            </audio>
                            <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                              formaction="exhibitions/audio/delete/${exhibition.audio.iditemaudio}"
                              formmethod="post"/>
                        </div>
                    </c:if>

                    <c:if test="${exhibition.audio == null}">

                            <label class="col-sm-4 col-form-label">${audioPlaceholder}</label>
                            <input class="col-sm-8" type="file" name="pickedaudio"/>

                    </c:if>
                    </div>
                    <div class="form-group row">
                    <c:if test="${exhibition.video != null}">
                        <div class="uploadItem col-md-6 change-margin ">
                            <video controls="" name="media" style="width:calc(100% + 16px)">
                                <source src="${exhibition.video.videourl}" type="video/mp4">
                            </video>
                            <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                              formaction="exhibitions/video/delete/${exhibition.video.iditemvideo}"
                              formmethod="post"/>
                        </div>
                    </c:if>

                    <c:if test="${exhibition.video == null}">
                            <label class="col-sm-4 col-form-label">${videoPlaceholder}</label>
                            <input class="col-sm-8" type="file" name="pickedvideo"/>
                    </c:if>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
                    </div>

                </form:form>
            </div>
        </section>
        <script>
			if (window.performance && window.performance.navigation.type == window.performance.navigation.TYPE_BACK_FORWARD) {
				if (document.URL.substring(window.location.origin.length, document.URL.indexOf("?")) == "/exhibitions/exhibitionform") {
					history.go(-1);
				}
			}
        </script>
    </body>
</html>

