<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="title.login"/></title>
    <style type="text/css">
        <%@include file="static/css/login.css" %>
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
</head>

<body>
<spring:message code="placeholder.username" var="usernamePlaceholder"/>
<spring:message code="placeholder.password" var="passwordPlaceholder"/>

<section class="login-block">
    <div class="container">
        <div class="row">
            <div class="login-sec">
                <form action="login" method="POST" class="form-signin" id="userLogin">
                    <h3 class="form-signin-heading"><spring:message code="wellcome"></spring:message></h3>
                    <br/>

                    <input type="text" id="username" name="username" placeholder="${usernamePlaceholder}"
                           class="form-control" path="username"/> <br/>

                    <input type="password" placeholder="${passwordPlaceholder}"
                           id="password" name="password" class="form-control" path="password"/> <br/>

                    <h6 class="form-error-credential">${errorMessage}</h6>

                    <button id="btn-login-register" class="btn btn-primary btn-login-register btn-block" name="Submit" value="Login"
                            type="Submit"><spring:message code="login.button"/>
                    </button>
                    <div class="center-button">
                        <a href="registration" class="text-login-register"><spring:message code="register"/></a>
                    </div>
                </form>
            </div>

        </div>
    </div>
</section>
    <script>
		<%@include file="static/js/login.js" %>
    </script>
</body>
</html>

