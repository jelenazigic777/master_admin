<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml"
  xmlns:th="http://www.thymeleaf.org">
    <%@page contentType="text/html" pageEncoding="UTF-8" %>
    <head>
        <title><spring:message code="title.registration"/></title>
        <style type="text/css">
            <%@include file="static/css/login.css" %>
        </style>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/" />
    </head>
    <body>
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.surname" var="surnamePlaceholder"/>
        <spring:message code="placeholder.username" var="usernamePlaceholder"/>
        <spring:message code="placeholder.password" var="passwordPlaceholder"/>


        <section class="login-block">
            <div class="container">
                <div class="row">
                    <form:form action="registration" class="form-signin"
                      method="POST"
                      role="form"
                      commandName="user">

                        <h3 class="form-signin-heading"><spring:message code="registration.form"/></h3>

                        <form:input type="text" path="name" name="name" placeholder="${namePlaceholder}" class="form-control"></form:input>
                        <label><form:errors path="name" cssClass="form-error-credential"/></label>

                        <form:input type="text" path="surname" name="surname" placeholder="${surnamePlaceholder}"
                          class="form-control"></form:input>
                        <label><form:errors path="surname" cssClass="form-error-credential"/></label>

                        <form:input type="text" path="username" name="username" placeholder="${usernamePlaceholder}"
                          class="form-control"></form:input>
                        <label><form:errors path="username" cssClass="form-error-credential"/></label>

                        <form:input type="password" path="password" name="password" placeholder="${passwordPlaceholder}"
                          class="form-control"></form:input>
                        <label><form:errors path="password" cssClass="form-error-credential"/></label>

                        <h6 class="success-message">${successMessage}</h6>

                        <button type="submit" class="btn btn-primary btn-login-register btn-block"><spring:message code="register.button"/></button>
                        <div class="center-button">
                            <a href="" class="text-login-register"><spring:message code="go.login"/></a>
                        </div>
                    </form:form>
                </div>
            </div>

        </section>
    </body>
</html>