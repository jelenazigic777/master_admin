<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title><spring:message code="title.museum.form"/></title>
        <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}/"/>
    </head>

    <body onload="addInstitutionToCookie(${idinstitution})">
        <spring:message code="placeholder.name" var="namePlaceholder"/>
        <spring:message code="placeholder.institution" var="institutionPlaceholder"/>
        <spring:message code="placeholder.text" var="textPlaceholder"/>
        <spring:message code="placeholder.texturl" var="texturlPlaceholder"/>
        <spring:message code="placeholder.texturllocal" var="texturllocalPlaceholder"/>
        <spring:message code="placeholder.imageurl" var="imageurlPlaceholder"/>
        <spring:message code="placeholder.imageurllocal" var="imageurllocalPlaceholder"/>
        <spring:message code="placeholder.videourl" var="videourlPlaceholder"/>
        <spring:message code="placeholder.videourllocal" var="videourllocalPlaceholder"/>
        <spring:message code="placeholder.x" var="xPlaceholder"/>
        <spring:message code="placeholder.y" var="yPlaceholder"/>
        <spring:message code="placeholder.z" var="zPlaceholder"/>
        <spring:message code="placeholder.radius" var="radiusPlaceholder"/>
        <spring:message code="ambient.audio.museum" var="ambientAudioPlaceholder"/>
        <spring:message code="ambient.audio.museum.pick" var="ambientAudioPickPlaceholder"/>
        <spring:message code="placeholder.image" var="imagePickPlaceholder"/>
        <spring:message code="placeholder.image.title" var="imagePlaceholder"/>
        <spring:message code="placeholder.image.client.title" var="clientImagesPlaceholder"/>
        <spring:message code="placeholder.image.client.primary" var="imagePrimaryPickPlaceholder"/>
        <spring:message code="placeholder.image.client.secondary" var="imageSecondaryPickPlaceholder"/>
        <spring:message code="placeholder.video" var="videоPickPlaceholder"/>
        <spring:message code="placeholder.video.title" var="videoPlaceholder"/>
        <spring:message code="placeholder.audio" var="audioPickPlaceholder"/>
        <spring:message code="placeholder.audio.title" var="audioPlaceholder"/>
        <spring:message code="placeholder.set.additional.museum" var="setmuseumadditionalPlaceholder"/>
        <spring:message code="placeholder.set.other.language" var="setMuseumLanguagePlaceholder"/>

        <jsp:include page="header.jsp"/>
        <c:if test="${museum.idmuseum != 0}">
            <jsp:include page="navigationbar.jsp"/>
        </c:if>
        <section>
            <div class="container">
                <h3 class="center-headline"><spring:message code="add.museum.form.title"/></h3>
                <form:form action="museums/museumform" class="form-signin"
                  method="POST"
                  enctype="multipart/form-data"
                  role="form"
                  commandName="museum">

                    <h6 class="success-message">${successMessage}</h6>

                    <input type="hidden" name="idmuseum" value="${museum.idmuseum}">

                    <input class="btn btn-primary remove-button btn-position-float-right" type="submit"
                      value="${setmuseumadditionalPlaceholder}"
                      formaction="museums/museum/additionalform" formmethod="get"/>

                    <c:if test="${museum.idmuseum != 0}">
                        <br>
                        <br>
                        <input class="btn btn-primary remove-button btn-position-float-right" type="submit"
                          value="${setMuseumLanguagePlaceholder}"
                          formaction="languages/museum" formmethod="get"/>
                    </c:if>

                    <label class="section-title"><spring:message code="basic.insert"/></label>

                    <input type="hidden" name="idinstitution" value="${idinstitution}">

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${namePlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="name" name="name" placeholder="${museum.name}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="name" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="institution" class="col-sm-4 col-form-label">${institutionPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input readonly="true" type="text" path="institution" name="institution" placeholder="${museum.institution}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="institution" cssClass="form-error-field"/></label>

                    <label class="section-title"><spring:message code="rest.insert"/></label></br>

                    <div class="form-group row">
                        <label for="text" class="col-sm-4 col-form-label">${textPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:textarea type="text" path="text" name="text" placeholder="${museum.text}" rows="4"
                              class="form-control"></form:textarea>
                        </div>
                    </div>
                    <label><form:errors path="text" cssClass="form-error-field"/></label>

                    <div class="form-group row">
                        <label for="texturl" class="col-sm-4 col-form-label">${texturlPlaceholder}</label>
                        <div class="col-sm-8">
                            <form:input type="text" path="texturl" name="texturl" placeholder="${museum.texturl}"
                              class="form-control"></form:input>
                        </div>
                    </div>
                    <label><form:errors path="texturl" cssClass="form-error-field"/></label>

                    <label class="section-title-2">${ambientAudioPlaceholder}</label>

                    <div class="form-group row">
                        <label class="col-sm-4 col-form-label">${ambientAudioPickPlaceholder}</label>
                        <div class="col-md-8">
                            <input type="file" name="ambientaudio">
                        </div>
                    </div>
                    <c:if test="${not empty museum.ambientaudios }">
                        <div class="form-group row">
                            <c:forEach var="aud" items="${museum.ambientaudios}">

                                <c:if test="${aud.exhibition == null && aud.room == null}">
                                    <div class="col-md-6 change-margin uploadItem">
                                        <audio controls class="col-sm-10">
                                            <source src="${aud.ambientaudiourl}" type="audio/mpeg">
                                        </audio>
                                        <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                          formaction="museums/ambientaudio/delete/${aud.idambientaudio}" formmethod="get"/>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </c:if>

                    <label class="section-title-2">${imagePlaceholder}</label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${imagePickPlaceholder}</label>
                        <input class="col-md-8" type="file" name="pickedimages" multiple="multiple"/>
                    </div>
                    <div class="row">
                        <c:forEach var="image" items="${museum.images}">
                            <c:if test="${image.clientImagePriority == null}">
                                <div class="col-lg-3 change-margin uploadItem">
                                    <div>
                                        <img class="design-image" src="${image.imageurl}">
                                        <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                          formaction="museums/image/delete/${image.iditemimage}" formmethod="post"/>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>

                    <label class="section-title-2">${clientImagesPlaceholder}</label>

                    <c:if test="${primaryClientImage == null}">
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label">${imagePrimaryPickPlaceholder}</label>
                            <input class="col-md-8" type="file" name="pickedPrimaryImage"/>
                        </div>
                    </c:if>
                    <c:if test="${primaryClientImage != null}">
                        <div class="row">
                            <div class="col-lg-3 change-margin uploadItem">
                                <div>
                                    <img class="design-image" src="${primaryClientImage.imageurl}">
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="museums/image/delete/${primaryClientImage.iditemimage}" formmethod="post"/>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <c:if test="${secondaryClientImage == null}">
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-form-label">${imageSecondaryPickPlaceholder}</label>
                            <input class="col-md-8" type="file" name="pickedSecondaryImage"/>
                        </div>
                    </c:if>
                    <c:if test="${secondaryClientImage != null}">
                        <div class="row">
                            <div class="col-lg-3 change-margin uploadItem">
                                <div>
                                    <img class="design-image" src="${secondaryClientImage.imageurl}">
                                    <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                      formaction="museums/image/delete/${secondaryClientImage.iditemimage}" formmethod="post"/>
                                </div>
                            </div>
                        </div>
                    </c:if>

                    <label class="section-title-2">${audioPlaceholder}</label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${audioPickPlaceholder}</label>
                        <input class="col-md-8" type="file" name="pickedaudios" multiple="multiple"/>
                    </div>
                    <div class="row">
                        <c:forEach var="audio" items="${museum.audios}">
                            <div class="col-md-6 change-margin uploadItem">
                                <audio controls="controls" class="col-sm-10">
                                    <source src="${audio.audiourl}" type="audio/mp3"/>
                                </audio>
                                <input class="btn-link text-danger deleteAudio" type="submit" value="x"
                                  formaction="museums/audio/delete/${audio.iditemaudio}"
                                  formmethod="get"/>
                            </div>
                        </c:forEach>
                    </div>

                    <label class="section-title-2">${videoPlaceholder}</label>

                    <div class="form-group row">
                        <label for="name" class="col-sm-4 col-form-label">${videоPickPlaceholder}</label>
                        <input class="col-md-8" type="file" name="pickedvideos"/>
                    </div>
                    <div class="row">
                        <c:forEach var="video" items="${museum.videos}">
                            <div class="change-margin uploadItem col-md-6">
                                <video controls="" name="media" style="width:calc(100% + 16px)">
                                    <source src="${video.videourl}" type="video/mp4">
                                </video>
                                <input class="btn-link text-danger deleteIcon" type="submit" value="x"
                                  formaction="museums/video/delete/${video.iditemvideo}"
                                  formmethod="get"/>
                            </div>
                        </c:forEach>
                    </div>

                    <div>
                        <button type="submit" class="btn btn-primary btn-block">${buttontext}</button>
                    </div>

                </form:form>
            </div>
        </section>
    </body>
</html>

