package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.ItemlanguagesDAO;
import com.etf.master.models.Itemlanguages;

@Service
public class ItemlanguagePersistance {

    @Autowired
    ItemlanguagesDAO itemlanguagesDAO;

    public Itemlanguages findItemLanguage(int idItemLanguage) {
        return itemlanguagesDAO.findOne(idItemLanguage);
    }

    public void saveItemLanguage(Itemlanguages itemlanguages) {
        itemlanguagesDAO.save(itemlanguages);
    }

    public Itemlanguages findItemLanguageByLanguageAndItem(int idLanguage, int idItem) {
        return itemlanguagesDAO.findItemLanguageByLanguageAndItem(idLanguage, idItem);
    }
}
