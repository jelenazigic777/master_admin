package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.AudioLanguageDAO;
import com.etf.master.models.Itemaudiolanguages;

@Service
public class AudioLanguagePersistence {

    @Autowired
    AudioLanguageDAO audioLanguageDAO;

    public Itemaudiolanguages findAudio(final int iditemaudiolanguage) {
        return audioLanguageDAO.findOne(iditemaudiolanguage);
    }

    public void deleteAudio(final int iditemaudiolanguage) {
        audioLanguageDAO.delete(iditemaudiolanguage);
    }
}
