package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.VideoLanguageDAO;
import com.etf.master.models.Itemvideolanguages;

@Service
public class VideoLanguagePersistence {

    @Autowired
    VideoLanguageDAO videoLanguageDAO;

    public Itemvideolanguages findVideo(final int iditemvideolanguage) {
        return videoLanguageDAO.findOne(iditemvideolanguage);
    }

    public void deleteVideo(final int iditemvideolanguage) {
        videoLanguageDAO.delete(iditemvideolanguage);
    }
}
