package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.MuseumlanguagesDAO;
import com.etf.master.models.Museumlanguages;

@Service
public class MuseumlanguagePersistance {

    @Autowired
    MuseumlanguagesDAO museumlanguagesDAO;

    public Museumlanguages findMuseumLanguage(int idMuseumLanguage) {
        return museumlanguagesDAO.findOne(idMuseumLanguage);
    }

    public void saveMuseumLanguage(Museumlanguages museumlanguages) {
        museumlanguagesDAO.save(museumlanguages);
    }

    public Museumlanguages findMuseumLanguageByLanguageAndMuseum(int idLanguage, int idMuseum) {
        return museumlanguagesDAO.findMuseumLanguageByLanguageAndMuseum(idLanguage, idMuseum);
    }
}
