package com.etf.master.persistance;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.FloormapDAO;
import com.etf.master.dao.ItemaudioDAO;
import com.etf.master.dao.ItemimageDAO;
import com.etf.master.dao.MuseummapDAO;
import com.etf.master.dao.ItemvideoDAO;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemvideo;
import com.etf.master.models.Museummap;
import com.etf.master.models.Itemimage;

@Service
public class MuseummapPersistence {

    @Autowired
    MuseummapDAO museummapDAO;

    @Autowired
    FloormapDAO floormapDAO;

    @Autowired
    ItemimageDAO itemimageDAO;

    @Autowired
    ItemaudioDAO itemaudioDAO;

    @Autowired
    ItemvideoDAO itemvideoDAO;

    public List<Museummap> findAllFromMuseum(int idmuseum) {
    return museummapDAO.findAllFromMuseum(idmuseum);
    }

    public List<Museummap> findAllFloorsFromMuseum(int idmuseum) {return museummapDAO.findAllFloorsFromMuseum(idmuseum);}

    public List<Museummap> findAllRoomsMapsFromMuseum(int idmuseum) {return museummapDAO.findAllRoomsMapsFromMuseum(idmuseum);}

    public Museummap findMuseummap(int idmuseummap) {
        return museummapDAO.findOne(idmuseummap);
    }

    public Museummap save(Museummap museummap) {
       return museummapDAO.save(museummap);
    }

    public void delete(int id) {
        museummapDAO.delete(id);
        floormapDAO.deleteAll();
    }

    public List<Museummap> findMuseummapsByExhibitionId(int id) {
        return museummapDAO.findMuseummapsByExhibitionId(id);
    }

    public List<Museummap> findAllFromFloor(int idmuseum, int floorNumber) {
       return museummapDAO.findAllFromFloor(idmuseum, floorNumber);
    }

    public Museummap findFloorByRoomFloorNumber(int idmuseum, int floorNumber) {
        return museummapDAO.findFloorByRoomFloorNumber(idmuseum, floorNumber);
    }

    public List<Itemimage> getallImagesFromMuseummap(int idmuseummap) {
        return itemimageDAO.getallImagesFromMuseummap(idmuseummap);
    }
    public List<Itemaudio> getallAudiosFromMuseummap(int idmuseummap) {
        return itemaudioDAO.getallAudiosFromMuseummap(idmuseummap);
    }
    public List<Itemvideo> getallVideosFromMuseummap(int idmuseummap) {
        return itemvideoDAO.getallVideosFromMuseummap(idmuseummap);
    }
}
