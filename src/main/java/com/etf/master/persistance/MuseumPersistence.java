package com.etf.master.persistance;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.MuseumDAO;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Museum;

@Service
public class MuseumPersistence {

    @Autowired
    MuseumDAO museumDAO;

    public List<Museum> findAll() {
       return museumDAO.findAll();
    }

    public Museum saveMuseum(Museum museum) {
        return museumDAO.save(museum);
    }

    public Museum findMuseum(Integer id) {
        return museumDAO.findOne(id);
    }

    public void delete(int id) {museumDAO.delete(id);}

    public List<Museum> findAllFromInstitution(int idinstitution) { return museumDAO.findAllFromInstitution(idinstitution);}

    public Museum findMuseumByName(String name) {
        return museumDAO.findMuseumByName(name);
    }

    public Itemimage findPrimaryClientImageForMuseumId(final int idmuseum) {
        return museumDAO.findPrimaryClientImageForMuseumId(idmuseum);
    }

    public Itemimage findSecondaryClientImageForMuseumId(final int idmuseum) {
        return  museumDAO.findSecondaryClientImageForMuseumId(idmuseum);
    }
}
