package com.etf.master.persistance;

import com.etf.master.dao.AmbientaudioDAO;
import com.etf.master.models.Ambientaudio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AmbientaudioPersistence {
    
    @Autowired
    AmbientaudioDAO ambientaudioDAO;

    public void delete(int idambientaudio) { ambientaudioDAO.delete(idambientaudio);}

    public Ambientaudio findAmbientaudio(int idambientaudio) { return ambientaudioDAO.findOne(idambientaudio);}

    public void save(Ambientaudio ambientaudio) {ambientaudioDAO.save(ambientaudio);}
}
