package com.etf.master.persistance;

import com.etf.master.dao.InstitutionDAO;
import com.etf.master.models.Institution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class InstitutionPersistence {

    @Autowired
    InstitutionDAO institutionDAO;

    public List<Institution> findAll() {
        return institutionDAO.findAll();
    }

    public Institution findInstitution(Integer id) {
       return institutionDAO.findOne(id);
    }

    public void delete(Integer idinstitution) {
        institutionDAO.delete(idinstitution);
    }

    public Institution saveInstitution(Institution institution) {
        return institutionDAO.save(institution);
    }
}
