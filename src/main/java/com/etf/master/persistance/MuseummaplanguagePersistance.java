package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.MuseumaplanguagesDAO;
import com.etf.master.models.Museummaplanguages;

@Service
public class MuseummaplanguagePersistance {

    @Autowired
    MuseumaplanguagesDAO museummaplanguagesDAO;

    public Museummaplanguages findMuseummapLanguage(int idMuseummapLanguage) {
        return museummaplanguagesDAO.findOne(idMuseummapLanguage);
    }

    public void saveMuseummapLanguage(Museummaplanguages museuemmaplanguages) {
        museummaplanguagesDAO.save(museuemmaplanguages);
    }

    public Museummaplanguages findExhibitionLanguageByLanguageAndExhibition(int idLanguage, int idMuseummap) {
        return museummaplanguagesDAO.findMuseummapLanguageByLanguageAndMuseummap(idLanguage, idMuseummap);
    }
}
