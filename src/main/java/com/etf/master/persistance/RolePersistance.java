package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.RoleDAO;
import com.etf.master.models.Role;

@Service
public class RolePersistance {

    @Autowired
    RoleDAO roleDAO;

    public Role findRoleByName(String name) {
        return roleDAO.findRoleByName(name);
    }

}
