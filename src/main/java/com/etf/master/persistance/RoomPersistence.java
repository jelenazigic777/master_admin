package com.etf.master.persistance;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.RoomDAO;
import com.etf.master.models.Room;

@Service
public class RoomPersistence {

    @Autowired
    RoomDAO roomDAO;

    public Room findRoom(int id) {
        return roomDAO.findOne(id);
    }

    public List<Room> findAllFromMuseum(int idmuseum) {
        return roomDAO.findAllFromMuseum(idmuseum);
    }

    public Room save(Room room) {
       return roomDAO.save(room);
    }

    public void delete(int id) {
        roomDAO.delete(id);
    }

    public List<Room> findConnectedRooms(int idroom) {
        return roomDAO.findConnectedRooms(idroom);
    }

}
