package com.etf.master.persistance;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.AudioLanguageDAO;
import com.etf.master.dao.LanguageDAO;
import com.etf.master.dao.VideoLanguageDAO;
import com.etf.master.models.Itemaudiolanguages;
import com.etf.master.models.Itemvideolanguages;
import com.etf.master.models.Languages;

@Service
public class LanguagePersistence {

    @Autowired
    LanguageDAO languageDAO;

    @Autowired
    AudioLanguageDAO audioLanguageDAO;

    @Autowired
    VideoLanguageDAO videoLanguageDAO;

    public List<Languages> loadAllLanguges() {
        return languageDAO.findAll();
    }

    public void saveLanguage(String languageName, String languageAbbreviation) {
        Languages language = new Languages(languageName, languageAbbreviation);
        languageDAO.save(language);
    }

    public void deleteLanguage(int idLanguages) {
        languageDAO.delete(idLanguages);
    }

    public Languages findLanguage(Integer idlanguage) {
        return languageDAO.findOne(idlanguage);
    }

    public void saveAudioLanguage(final Itemaudiolanguages itemaudio) {
       audioLanguageDAO.save(itemaudio);
    }

    public void saveVideoLanguage(final Itemvideolanguages itemvideo) {
        videoLanguageDAO.save(itemvideo);
    }
}
