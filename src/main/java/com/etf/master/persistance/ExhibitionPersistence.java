package com.etf.master.persistance;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.ExhibitionDAO;
import com.etf.master.models.Exhibition;

@Service
public class ExhibitionPersistence {

    @Autowired
    ExhibitionDAO exhibitionDAO;

    public List<Exhibition> findAllFromMuseum(int idmuseum) {
        return exhibitionDAO.findAllFromMuseum(idmuseum);
    }

    public Exhibition findExhibition(int idexhibition) {
        return exhibitionDAO.findOne(idexhibition);
    }

    public Exhibition saveExhibition(Exhibition exhibition) {
        return exhibitionDAO.save(exhibition);
    }

    public void deleteExhibition(int idexhibition) {
        exhibitionDAO.delete(idexhibition);
    }

    public void removeItemExhibitionRelation(int idexhibition, int iditem) {
        exhibitionDAO.removeItemExhibitionRelation(idexhibition, iditem);
    }

    public void removeMuseummapExhibitionRelation(int idexhibition, int idmuseummap) {
        exhibitionDAO.removeMuseummapExhibitionRelation(idexhibition, idmuseummap);
    }

    public Integer findExhibitionImage(int idexhibition, int iditem) {
        return exhibitionDAO.findExhibitionImage(idexhibition, iditem);
    }

    public Integer findExhibitionVideo(int idexhibition, int iditem) {
        return exhibitionDAO.findExhibitionVideo(idexhibition, iditem);
    }

    public Integer findExhibitionAudio(int idexhibition, int iditem) {
        return exhibitionDAO.findExhibitionAudio(idexhibition, iditem);
    }

    public Integer findExhibitionText(int idexhibition, int iditem) {
        return exhibitionDAO.findExhibitionText(idexhibition, iditem);
    }

    public void removeImageExhibitionRelation(int idexhibition, int iditemimage) {
        exhibitionDAO.removeImageExhibitionRelation(idexhibition, iditemimage);
    }
    public void removeAudioExhibitionRelation(int idexhibition, int iditemaudio) {
        exhibitionDAO.removeAudioExhibitionRelation(idexhibition, iditemaudio);
    }
    public void removeVideoExhibitionRelation(int idexhibition, int iditemvideo) {
        exhibitionDAO.removeVideoExhibitionRelation(idexhibition, iditemvideo);
    }
    public void removeTextExhibitionRelation(int idexhibition, int iditemtext) {
        exhibitionDAO.removeTextExhibitionRelation(idexhibition, iditemtext);
    }

    public List<Exhibition> findAllActiveFromMuseum(final int idmuseum) {
       return exhibitionDAO.findAllActiveFromMuseum(idmuseum);
    }

    public List<Exhibition> findAllInactiveItemsFromMuseum(final Integer idmuseum) {
        return exhibitionDAO.findAllInactiveItemsFromMuseum(idmuseum);
    }

    public List<Exhibition> findExhibitionsByParams(final String name, final String author, final String itemName, final String itemCode, final String roomName, final String convertedName, final String convertedAuthor, final String convertedItemName, final String convertedItemCode, final String convertedRoomName) {
        List<Exhibition> exhibitions = null;
        if (itemName.isEmpty() && itemCode.isEmpty() && roomName.isEmpty() && convertedItemName.isEmpty() && convertedItemCode.isEmpty() && convertedRoomName.isEmpty()) {
            exhibitions = exhibitionDAO.findExhibitionsByNameAndAuthor(name, author, convertedName, convertedAuthor);
        } else {
            exhibitions = exhibitionDAO.findExhibitionsByParams(name, author, itemName, itemCode, roomName, convertedName, convertedAuthor, convertedItemName, convertedItemCode, convertedRoomName);
        }
        return exhibitions;
    }
}

