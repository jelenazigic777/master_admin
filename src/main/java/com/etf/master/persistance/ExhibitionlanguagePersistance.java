package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.ExhibitionlanguagesDAO;
import com.etf.master.models.Exhibitionlanguages;

@Service
public class ExhibitionlanguagePersistance {

    @Autowired
    ExhibitionlanguagesDAO exhibitionlanguagesDAO;

    public Exhibitionlanguages findExhibitionLanguage(int idExhibitionLanguage) {
        return exhibitionlanguagesDAO.findOne(idExhibitionLanguage);
    }

    public void saveExhibitionLanguage(Exhibitionlanguages exhibitionlanguages) {
        exhibitionlanguagesDAO.save(exhibitionlanguages);
    }

    public Exhibitionlanguages findExhibitionLanguageByLanguageAndExhibition(int idLanguage, int idExhibition) {
        return exhibitionlanguagesDAO.findExhibitionLanguageByLanguageAndExhibition(idLanguage, idExhibition);
    }
}
