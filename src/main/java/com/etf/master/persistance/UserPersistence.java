package com.etf.master.persistance;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.etf.master.dao.UserDAO;
import com.etf.master.models.User;

@Service("userService")
public class UserPersistence {

    private UserDAO userDAO;

    @Autowired
    public UserPersistence(UserDAO userDAO, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDAO = userDAO;
    }

    public User findByUsername(String username) { return userDAO.findByUsername(username);}

    public void saveUser(User user) {
        userDAO.save(user);
    }

    public List<User> findAllUsersNotActive() {
        return userDAO.findAllUsersNotActive();
    }

    public User findUserById(final Integer idUser) {
        return userDAO.findOne(idUser);
    }

    public void deleteUserById(final Integer iduser) {
        userDAO.delete(iduser);
    }
}
