package com.etf.master.persistance;

import java.beans.Transient;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.ItemaudioDAO;
import com.etf.master.dao.ItemDAO;
import com.etf.master.dao.ItemimageDAO;
import com.etf.master.dao.ItemtextDAO;
import com.etf.master.dao.ItemvideoDAO;
import com.etf.master.models.Item;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Itemtext;
import com.etf.master.models.Itemvideo;

@Service
public class ItemPersistence {

    @Autowired
    ItemDAO itemDAO;

    @Autowired
    ItemimageDAO itemimageDAO;

    @Autowired
    ItemtextDAO itemtextDAO;

    @Autowired
    ItemaudioDAO itemaudioDAO;

    @Autowired
    ItemvideoDAO itemvideoDAO;

    public List<Item> findAllFromMuseum(Integer idmuseum) {
        return itemDAO.findAllFromMuseum(idmuseum);
    }

    public List<Item> findAllFromMuseummap(Integer idmuseummap) {
        return itemDAO.findAllFromMuseummap(idmuseummap);
    }

    public Item findItem(Integer id) {return itemDAO.findOne(id);}

    @Transient
    public Item saveItem(Item item) { return itemDAO.saveAndFlush(item);}

    public void deleteItem(Integer id) {
        itemDAO.delete(id);
    }

    public List<Item> findItemsByExhibitionId(int idexhibition) {
        return itemDAO.findItemsByExhibitionId(idexhibition);
    }

    public List<Item> findItemsByMuseummapId(int idmuseummap) {
        return itemDAO.findItemsByMuseummapId(idmuseummap);
    }

    public void removeItemMapRelation(int idmuseummap, int iditem) {
        itemDAO.removeItemMapRelation(idmuseummap, iditem);
    }

    public List<Itemimage> getallImagesFromItem(int iditem) {
        return itemimageDAO.getallImagesFromItem(iditem);
    }

    public List<Itemaudio> getallAudiosFromItem(int iditem) {
        return itemaudioDAO.getallAudiosFromItem(iditem);
    }

    public List<Itemvideo> getallVideosFromItem(int iditem) {
        return itemvideoDAO.getallVideosFromItem(iditem);
    }
    public List<Itemtext> getallTextsFromItem(int iditem) {
        return itemtextDAO.getallTextsFromItem(iditem);
    }

    public Itemimage findImage(int iditemimage) {
        return itemimageDAO.getOne(iditemimage);
    }

    public void deleteImage(int iditemimage) {
        itemimageDAO.delete(iditemimage);
    }

    public Itemaudio findAudio(int iditemaudio) {
        return itemaudioDAO.getOne(iditemaudio);
    }

    public void deleteAudio(int iditemaudio) {
        itemaudioDAO.delete(iditemaudio);
    }

    public Itemvideo findVideo(int iditemvideo) {
        return itemvideoDAO.getOne(iditemvideo);
    }

    public void deleteVideo(int iditemvideo) {
        itemvideoDAO.delete(iditemvideo);
    }

    public Itemtext findText(int iditemtext) {
        return itemtextDAO.getOne(iditemtext);
    }

    public void deleteText(int iditemtext) {itemtextDAO.delete(iditemtext);}

    public void saveItemvideo(Itemvideo itemvideo) {
        itemvideoDAO.save(itemvideo);
    }

    public void saveItemaudio(Itemaudio itemaudio) {
        itemaudioDAO.save(itemaudio);
    }

    public void saveItemtext(Itemtext itemtext) {
        itemtextDAO.save(itemtext);
    }

    public void saveItemimage(Itemimage itemimage) {
        itemimageDAO.save(itemimage);
    }

    public List<Item> findItemsByExhibitionIdAndMapId(final int idexhibition, final int idmuseummap) {
        return itemDAO.findItemsByExhibitionIdAndMapId(idexhibition, idmuseummap);
    }

    public List<Item> findItemsByParams(final String name, final String author, final String code, final String exhibitionName, final Double xCoordinate, final Double yCoordinate, final Double zCoordinate, Double deviation, final String roomName, final String convertedName, final String convertedAuthor, final String convertedCode, final String convertedExhibitionName, final String convertedRoomName) {
     List<Item> items = null;
        if (deviation == null) {
            deviation = 0.0d;
        }
     if (exhibitionName.isEmpty() && roomName.isEmpty() && convertedExhibitionName.isEmpty() && convertedRoomName.isEmpty()) {
         items = itemDAO.findItemsByItemParams(name, author, convertedName, convertedAuthor, xCoordinate, yCoordinate, zCoordinate, deviation);
     } else {
         items = itemDAO.findItemsByParams(name, author, code, exhibitionName, xCoordinate, yCoordinate, zCoordinate, deviation, roomName, convertedName, convertedAuthor, convertedCode, convertedExhibitionName, convertedRoomName);
     }
     return items;
    }
}
