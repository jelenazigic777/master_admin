package com.etf.master.persistance;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etf.master.dao.BeaconDAO;
import com.etf.master.models.Beacon;

@Service
public class BeaconPersistence {
    @Autowired
    BeaconDAO beaconDAO;

    public List<Beacon> findAllFromMuseum(int idmuseum) {
        return beaconDAO.findAllFromMuseum(idmuseum);
    }

    public List<Beacon> findAllFromMuseummap(int idMuseummap) {
        return beaconDAO.findAllFromMuseummap(idMuseummap);
    }

    public Beacon findBeacon(int idbeacon) {
        return beaconDAO.findOne(idbeacon);
    }

    @Transactional
    public Beacon save(Beacon beacon) {
        return beaconDAO.saveAndFlush(beacon);
    }

    public void delete(int idBeacon) {
        beaconDAO.delete(idBeacon);
    }
}
