package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.etf.master.dao.FloormapDAO;
import com.etf.master.models.Floormap;

@Service
public class FloormapPersistence {

    @Autowired
    private FloormapDAO floormapDAO;

    @Transactional
    public Floormap save(Floormap floormap) {
      return floormapDAO.saveAndFlush(floormap);
    }

    public void delete(int id) {
        floormapDAO.delete(id);
    }

    public Floormap getFloormapByMuseummapId(int idmuseummap) {return floormapDAO.getFloormapByMuseummapId(idmuseummap);}

    public Floormap findFloormap(int idFloormap) {
        return floormapDAO.findOne(idFloormap);
    }
}
