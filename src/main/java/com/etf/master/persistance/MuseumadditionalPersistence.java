package com.etf.master.persistance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dao.MuseumadditionalDAO;
import com.etf.master.models.Museumadditional;

@Service
public class MuseumadditionalPersistence {

    @Autowired
    MuseumadditionalDAO museumadditionalDAO;

    public Museumadditional findMuseumadditionalWithMuseumId(int idmuseum) {
        return museumadditionalDAO.findMuseumadditionalWithMuseumId(idmuseum);
    }

    public void saveMuseumadditional(Museumadditional museumadditional) {
        museumadditionalDAO.save(museumadditional);
    }
}
