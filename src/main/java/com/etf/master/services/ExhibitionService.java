package com.etf.master.services;

import java.util.List;
import java.util.Locale;

import org.springframework.web.multipart.MultipartFile;

import com.etf.master.dtos.MapResponseDTO;
import com.etf.master.models.Exhibition;
import com.etf.master.models.Museummap;

public interface ExhibitionService {

    List<Exhibition> findAllFromMuseum(int idmuseum);

    Exhibition findExhibition(int idexhibition);

    List<String> findAuthorsFromExhibitionItems(int idexhibition);

    Exhibition setItems(String[] selectedItems, Exhibition exhibition);

    Exhibition setMaps(String[] selectedMaps, Exhibition exhibition);

    void deleteExhibition(int idexhibition);

    Exhibition saveExhibition(Exhibition exhibition);

    void removeItemExhibitionRelation(int idexhibition, int iditem);

    void removeMuseummapExhibitionRelation(int idexhibition, int idmuseummap);

    void setAmbientAudioOnExhibition(MultipartFile ambientaudio, int idmuseum, int idexhibition);

    Integer findExhibitionImage(int idexhibition, int iditem);

    Integer findExhibitionVideo(int idexhibition, int iditem);

    Integer findExhibitionAudio(int idexhibition, int iditem);

    Integer findExhibitionText(int idexhibition, int iditem);

    void basicSave(Exhibition exhibition, int idmuseum, Locale locale);

    void removeImageExhibitionRelation(int idexhibition, int iditem);

    void removeAudioExhibitionRelation(int idexhibition, int iditem);

    void removeVideoExhibitionRelation(int idexhibition, int iditem);

    void removeTextExhibitionRelation(int idexhibition, int iditem);

    Exhibition cloneExhibitionAndReturnSaved(Exhibition exhibition);

    Exhibition saveMultipartFiles(MultipartFile image, MultipartFile audio, MultipartFile video, Exhibition exhibition);

    List<MapResponseDTO> findMapResponseListByExhibitionId(int idexhibition);

    List<Museummap> findMuseummapsByExhibitionId(int idexhibition);

    List<Exhibition> findAllActiveFromMuseum(int idmuseum);

    List<Exhibition> findAllInactiveItemsFromMuseum(Integer idmuseum);

    List<Exhibition> findExhibitions(String name, String author, final String itemName, final String itemCode, final String exhibitionName);
}
