package com.etf.master.services;

import java.util.List;

import com.etf.master.models.User;

public interface UserService {

    User findByUsername(String username);

    void saveUser(User user);

    List<User> findAllUsersNotActive();

    User findUserById(Integer idUser);

    void deleteUserById(Integer iduser);
}
