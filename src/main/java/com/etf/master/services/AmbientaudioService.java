package com.etf.master.services;

import com.etf.master.models.Ambientaudio;

public interface AmbientaudioService {

    void deleteAmbientAudio(int idambientaudio);

    void saveAmbientAudio(Ambientaudio ambientaudio);
}
