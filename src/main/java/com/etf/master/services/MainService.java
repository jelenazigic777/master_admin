package com.etf.master.services;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.etf.master.models.Institution;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Museum;
import com.etf.master.models.Museumadditional;

public interface MainService {

    List<Institution> findAllInstitutions();

    List<Museum> findAllMuseumsFromInstitution(int idInstitution);

    Institution findInstitution(int idInstitution);

    void saveInstitution(Institution institution);

    void deleteInstitution(int idInstitution);

    Museum findMuseum(int idMuseum);

    Museum saveMuseum(Museum museum);

    void deleteMuseum(int idMuseum);

    void saveAdditionalMuseumInfo(Museumadditional museumadditional);

    Museum saveAmbientAudioOnMuseum(MultipartFile ambientaudio, Museum museum);

    void deleteMuseumImage(int idImage);

    void deleteMuseumAudio(int idAudio);

    void deleteMuseumVideo(int idVideo);

    Museum saveMultipartFiles(List<MultipartFile> images, List<MultipartFile> audios, List<MultipartFile> videos, Museum museum);

    Museum saveClientImages(MultipartFile primaryImage, MultipartFile secondaryImage, Museum museum);

    String transformDateFormat(Date date);

    Museumadditional findMuseumAdditionalForMuseum(int idMuseum);

    Museum findMuseumByName(String name);

    Itemimage findPrimaryClientImageForMuseumId(int idmuseum);

    Itemimage findSecondaryClientImageForMuseumId(int idmuseum);
}
