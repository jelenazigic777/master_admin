package com.etf.master.services;

import java.util.List;
import java.util.Locale;

import org.springframework.web.multipart.MultipartFile;

import com.etf.master.dtos.ItemResponseDTO;
import com.etf.master.models.Item;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Itemtext;
import com.etf.master.models.Itemvideo;

public interface ItemService {

    List<Item> findAllFromMuseum(Integer idmuseum);

    List<Item> findAllActiveItemsFromMuseum(int idmuseum);

    List<Item> findAllInactiveItemsFromMuseum(int idmuseum);

    List<Item> findAllFromMuseummap(int idMuseummap);

    Item findItem(Integer id);

    void deleteItem(Integer id);

    Item basicSave(Item item, int idmuseum, Locale locale);

    Item saveItem(Item item);

    void deleteImage(int iditemimage);

    void deleteVideo(int iditemvideo);

    void deleteAudio(int iditemaudio);

    void deleteText(int idtext);

    void saveImage(Itemimage image);

    void saveVideo(Itemvideo video);

    void saveAudio(Itemaudio audio);

    void saveText(Itemtext text);

    List<Itemimage> getallImagesFromItem(int iditem);

    List<Itemaudio> getallAudiosFromItem(int iditem);

    List<Itemvideo> getallVideosFromItem(int iditem);

    List<Itemtext> getallTextsFromItem(int iditem);

    List<Item> findItemsByExhibitionId(int idexhibition);

    Itemimage findImage(int iditemimage);

    Itemaudio findAudio(int iditemaudio);

    Itemvideo findVideo(int iditemvideo);

    Itemtext findText(int iditemtext);

    List<Item> findItemsByMuseummapId(int idMap);

    void removeItemMapRelation(int idMap, int idItem);

    Item saveMultipartFiles(MultipartFile[] images, MultipartFile[] audios, MultipartFile[] videos, String[] texts, Item item, String[] additionalVideoUrls, MultipartFile barcode);

    List<ItemResponseDTO> convertItems(List<Item> items);

    List<ItemResponseDTO> convertMapItems(List<Item> items);

    List<Item> findItemsByExhibitionIdAndMapId(int idexhibition, int idmuseummap);

    List<Itemvideo> findVideosWithOnlyUrlByItemId(int iditem);

    List<Itemvideo> findVideosWithoutOnlyUrlByItemId(int iditem);

    void deleteBarcode(int idbarcode, int iditem);

    List<Item> findItems(String name, String author, String code, String exhibitionName, Double xCoordinate, Double yCoordinate, Double zCoordinate, Double deviation, final String roomName);

    void updateExistingItem(Item item);
}
