package com.etf.master.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.etf.master.models.Exhibitionlanguages;
import com.etf.master.models.Itemlanguages;
import com.etf.master.models.Languages;
import com.etf.master.models.Museumlanguages;
import com.etf.master.models.Museummaplanguages;

public interface LanguageService {

    void saveLanguage(String name, String bbreviation);

    Languages findLanguage(int idLanguage);

    List<Languages> loadAllLanguges();

    void deleteLanguage(int idLanguage);

    Museumlanguages findMuseumLanguage(int idMuseumLanguage);

    void saveMuseumLanguage(Museumlanguages museumlanguages, int idmuseum, int idlanguage, final List<MultipartFile> audios, final List<MultipartFile> videos);

    Itemlanguages findItemLanguage(int idItemLanguage);

    void saveItemLanguage(Itemlanguages itemlanguages, int iditem, int idlanguage, final List<MultipartFile> audios, final List<MultipartFile> videos);

    Exhibitionlanguages findExhibitionLanguage(int idExhibitionLanguage);

    void saveExhibitionLanguage(Exhibitionlanguages exhibitionlanguages, int idexhibition, int idlanguage, final List<MultipartFile> audios, final List<MultipartFile> videos);

    Museummaplanguages findMuseummapLanguage(int idMuseummapLanguage);

    void saveMuseummapLanguage(Museummaplanguages museummaplanguages, int idmuseummap, int idlanguage, final List<MultipartFile> audios, final List<MultipartFile> videos);

    Museumlanguages findMuseumLanguageByLanguageAndMuseum(int idLanguage, int idMuseum);

    Itemlanguages findItemLanguageByLanguageAndItem(int idLanguage, int idItem);

    Exhibitionlanguages findExhibitionLanguageByLanguageAndExhibition(int idLanguage, int idExhibition);

    Museummaplanguages findMuseummapLanguageByLanguageAndMuseummap(int idLanguage, int idMuseummap);

    void deleteAudioLanguage(int iditemaudio);

    void deleteVideoLanguage(int iditemvideo);
}
