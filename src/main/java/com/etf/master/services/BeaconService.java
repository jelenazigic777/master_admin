package com.etf.master.services;

import java.util.List;

import com.etf.master.dtos.BeaconResponseDTO;
import com.etf.master.models.Beacon;

public interface BeaconService {

    List<Beacon> findAllFromMuseum(int idMuseum);

    List<Beacon> findAllFromMuseummap(int idMuseummap);

    Beacon findBeacon(int idBeacon);

    Beacon saveBeacon(Beacon beacon);

    void deleteBeacon(int idBeacon);

    List<BeaconResponseDTO> convertBeacons(List<Beacon> beacons);
}

