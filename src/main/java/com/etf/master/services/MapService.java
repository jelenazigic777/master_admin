package com.etf.master.services;

import java.util.List;
import java.util.Locale;

import org.springframework.web.multipart.MultipartFile;

import com.etf.master.models.Beacon;
import com.etf.master.models.Floormap;
import com.etf.master.models.Item;
import com.etf.master.models.Museum;
import com.etf.master.models.Museummap;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Itemvideo;
import com.etf.master.models.Room;

public interface MapService {

    Museummap saveMultipartFiles(MultipartFile[] images, MultipartFile[] audios, MultipartFile[] videos, Museummap map, String[] additionalVideoUrls);

    List<Museummap> findMuseummapsByExhibitionId(int idexhibition);

    List<Museummap> findAllFromMuseum(int idmuseum);

    List<Museummap> findAllFloorsFromMuseum(int idmuseum);

    List<Museummap> findAllRoomsMapsFromMuseum(int idmuseum);

    List<Floormap> findAllActiveFloormapsFromMuseum(int idmuseum);

    Museummap findMuseummap(int idMuseummap);

    Floormap findFloormap(int idFloormap);

    Room findRoom(int idRoom);

    List<Room> findConnectedRooms(int idRoom);

    void saveMuseummap(Museummap museummap);

    void saveFloorMap(Floormap floormap);

    List<Item> findItemsByMuseummapId(int idMuseummap);

    List<Beacon> findAllBeaconsByMuseummapId(int idMuseummap);

    void removeItemMapRelation(int idmuseummap, int iditem);

    void removeBeaconMapRelation(int idmuseummap, int idbeacon);

    void removeRoomRelation(int idroom1, int idroom2);

    void deleteImage(int idImage);

    void deleteAudio(int idAudio);

    void deleteVideo(int idVideo);

    void deleteMuseummap(int idMuseummap);

    void deleteFloormap(int idFloormap);

    void deleteRoom(int idRoom);

    void saveAmbientAudio(MultipartFile ambientaudio, int idMuseum, int idRoom);

    void saveAmbientAudioOnFloor(MultipartFile ambientaudio, int idMuseum, int idMuseummap);

    List<Itemimage> findallImagesFromMuseummap(int idmuseummap);

    List<Itemaudio> findallAudiosFromMuseummap(int idmuseummap);

    List<Itemvideo> findallVideosFromMuseummap(int idmuseummap);

    List<Museummap> findAllFromFloor(int idMuseum, int idFloor);

    List<Room> findAllRoomsFromMuseum(int idMuseum);

    Museummap findFloorByRoomFloorNumber(int idMuseum, int floorNumber);

    void setRooms(String[] selectedRooms, Room room);

    void setItems(String[] selectedItems, Museummap museummap, Room room);

    void setAndSave(Museummap museummap, Room room, Museum museum, Locale locale, boolean isold, boolean isfloor);

    List<Itemvideo> findVideosWithoutOnlyUrlByMuseummapId(int idmuseummap);

    List<Itemvideo> findVideosWithOnlyUrlByMuseummapId(int idmuseummap);
}
