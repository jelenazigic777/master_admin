package com.etf.master.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.models.User;
import com.etf.master.persistance.UserPersistence;
import com.etf.master.services.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserPersistence userPersistence;

    @Override
    public User findByUsername(String username) {
       return userPersistence.findByUsername(username);
    }

    @Override
    public void saveUser(User user) {
        userPersistence.saveUser(user);
    }

    @Override
    public List<User> findAllUsersNotActive() {
        return userPersistence.findAllUsersNotActive();
    }

    @Override
    public User findUserById(Integer idUser) {
        return userPersistence.findUserById(idUser);
    }

    @Override
    public void deleteUserById(final Integer iduser) {
        userPersistence.deleteUserById(iduser);
    }
}
