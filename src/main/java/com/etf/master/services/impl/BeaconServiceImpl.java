package com.etf.master.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dtos.BeaconResponseDTO;
import com.etf.master.models.Beacon;
import com.etf.master.persistance.BeaconPersistence;
import com.etf.master.services.BeaconService;

@Service
public class BeaconServiceImpl implements BeaconService {

    @Autowired
    BeaconPersistence beaconPersistence;

    @Override
    public List<Beacon> findAllFromMuseum(int idMuseum) {
        return beaconPersistence.findAllFromMuseum(idMuseum);
    }

    @Override
    public List<Beacon> findAllFromMuseummap(final int idMuseummap) {
        return beaconPersistence.findAllFromMuseummap(idMuseummap);
    }

    @Override
    public Beacon findBeacon(int idBeacon) {
        return beaconPersistence.findBeacon(idBeacon);
    }

    @Override
    public Beacon saveBeacon(Beacon beacon) {
        if (beacon.getXcoordinate() == null) {
            beacon.setXcoordinate(0.0);
        }
        if (beacon.getYcoordinate() == null) {
            beacon.setYcoordinate(0.0);
        }
        if (beacon.getZcoordinate() == null) {
            beacon.setZcoordinate(0.0);
        }
       return beaconPersistence.save(beacon);
    }

    @Override
    public void deleteBeacon(final int idBeacon) {
        beaconPersistence.delete(idBeacon);
    }

    @Override
    public List<BeaconResponseDTO> convertBeacons(List<Beacon> beacons) {
        List<BeaconResponseDTO> responseList = new ArrayList<>();
        for (Beacon b : beacons) {
            BeaconResponseDTO response = new BeaconResponseDTO();
            response.setIdbeacon(b.getIdbeacon());
            response.setXcoordinate(b.getXcoordinate());
            response.setYcoordinate(b.getYcoordinate());
            response.setRadius(b.getRadius());
            responseList.add(response);
        }
        return responseList;
    }
}

