package com.etf.master.services.impl;

import com.etf.master.services.AmbientaudioService;
import com.etf.master.filesmanipulation.FilesManipulator;
import com.etf.master.models.Ambientaudio;
import com.etf.master.persistance.AmbientaudioPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AmbientaudioServiceImpl implements AmbientaudioService {

    @Autowired
    FilesManipulator cloudc;

    @Autowired
    AmbientaudioPersistence ambientaudioPersistence;

    @Override
    public void deleteAmbientAudio(int idambientaudio) {
        cloudc.delete(ambientaudioPersistence.findAmbientaudio(idambientaudio).getServerid());
        ambientaudioPersistence.delete(idambientaudio);
    }

    @Override
    public void saveAmbientAudio(Ambientaudio ambientaudio) {
        ambientaudioPersistence.save(ambientaudio);
    }
}
