package com.etf.master.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.utils.ObjectUtils;
import com.etf.master.convertors.CyrillicLatinConverter;
import com.etf.master.dtos.ItemResponseDTO;
import com.etf.master.filesmanipulation.FilesManipulator;
import com.etf.master.models.Barcodeimage;
import com.etf.master.models.Item;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Itemtext;
import com.etf.master.models.Museum;
import com.etf.master.models.Itemvideo;
import com.etf.master.persistance.MuseumPersistence;
import com.etf.master.persistance.ItemPersistence;
import com.etf.master.services.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    FilesManipulator cloudc;

    @Autowired
    private MuseumPersistence museumPersistence;

    @Autowired
    private ItemPersistence itemPersistence;

    @Autowired
    private CyrillicLatinConverter cyrillicLatinConverter;

    @Override
    public Item basicSave(Item item, int idmuseum, Locale locale) {
        Museum museum = museumPersistence.findMuseum(idmuseum);
        item.setMuseum(museum);
        item.setInstitution(museum.getInstitution());
        item.setLanguage(locale.getLanguage());
        if (item.getXcoordinate() == null) {
            item.setXcoordinate(0.0);
        }
        if (item.getYcoordinate() == null) {
            item.setYcoordinate(0.0);
        }
        if (item.getZcoordinate() == null) {
            item.setZcoordinate(0.0);
        }
        return itemPersistence.saveItem(item);
    }

    @Override
    public List<Item> findAllFromMuseum(Integer idmuseum) {
        return itemPersistence.findAllFromMuseum(idmuseum);
    }

    @Override
    public List<Item> findAllActiveItemsFromMuseum(final int idmuseum) {
        List<Item> active = new ArrayList<>();
        for (Item p : findAllFromMuseum(idmuseum)) {
            if (p.getXcoordinate() != null && p.getYcoordinate() != null) {
                active.add(p);
            }
        }
        return active;
    }

    @Override
    public List<Item> findAllInactiveItemsFromMuseum(final int idmuseum) {
        List<Item> inactive = new ArrayList<>();
        for (Item p : findAllFromMuseum(idmuseum)) {
            if (p.getXcoordinate() == null || p.getYcoordinate() == null) {
                inactive.add(p);
            }
        }
        return inactive;
    }

    @Override
    public List<Item> findAllFromMuseummap(final int idMuseummap) {
        return itemPersistence.findAllFromMuseummap(idMuseummap);
    }

    @Override
    public Item findItem(Integer id) {
        return itemPersistence.findItem(id);
    }

    @Override
    public void deleteItem(Integer id) {
        itemPersistence.deleteItem(id);
    }

    @Override
    public Item saveItem(Item item) {
        return itemPersistence.saveItem(item);
    }

    @Override
    public void deleteImage(int iditemimage) {
        cloudc.delete(itemPersistence.findImage(iditemimage).getServerid());
        itemPersistence.deleteImage(iditemimage);
    }

    @Override
    public void deleteBarcode(final int idbarcode, int iditem) {
        Item item = itemPersistence.findItem(iditem);
        cloudc.delete(item.getBarcodeimage().getServerid());
        item.setBarcodeimage(null);
        itemPersistence.saveItem(item);
    }

    @Override
    public void deleteVideo(int iditemvideo) {
        if (itemPersistence.findVideo(iditemvideo).getServerid() != null) {
            cloudc.delete(itemPersistence.findVideo(iditemvideo).getServerid());
        }
        itemPersistence.deleteVideo(iditemvideo);
    }

    @Override
    public void deleteText(int idtext) {
        itemPersistence.deleteText(idtext);
    }

    @Override
    public void deleteAudio(int iditemaudio) {
        cloudc.delete(itemPersistence.findAudio(iditemaudio).getServerid());
        itemPersistence.deleteAudio(iditemaudio);
    }

    @Override
    public List<Item> findItemsByMuseummapId(int idMap) {
        return itemPersistence.findItemsByMuseummapId(idMap);
    }

    @Override
    public void removeItemMapRelation(int idMap, int idItem) {
        itemPersistence.removeItemMapRelation(idMap, idItem);
    }

    @Override
    public Itemimage findImage(int iditemimage) {
        return itemPersistence.findImage(iditemimage);
    }

    @Override
    public Itemaudio findAudio(int iditemaudio) {
        return itemPersistence.findAudio(iditemaudio);
    }

    @Override
    public Itemvideo findVideo(int iditemvideo) {
        return itemPersistence.findVideo(iditemvideo);
    }

    @Override
    public Itemtext findText(int iditemtext) {
        return itemPersistence.findText(iditemtext);
    }

    @Override
    public List<Itemimage> getallImagesFromItem(int iditem) {
        return itemPersistence.getallImagesFromItem(iditem);
    }

    @Override
    public List<Itemaudio> getallAudiosFromItem(int iditem) {
        return itemPersistence.getallAudiosFromItem(iditem);
    }

    @Override
    public List<Itemvideo> getallVideosFromItem(int iditem) {
        return itemPersistence.getallVideosFromItem(iditem);
    }

    @Override
    public List<Itemtext> getallTextsFromItem(int iditem) {
        return itemPersistence.getallTextsFromItem(iditem);
    }

    @Override
    public List<Item> findItemsByExhibitionId(int idexhibition) {
        return itemPersistence.findItemsByExhibitionId(idexhibition);
    }

    @Override
    public void saveImage(final Itemimage image) {
        itemPersistence.saveItemimage(image);
    }

    @Override
    public void saveVideo(final Itemvideo video) {
        itemPersistence.saveItemvideo(video);
    }

    @Override
    public void saveAudio(final Itemaudio audio) {
        itemPersistence.saveItemaudio(audio);
    }

    @Override
    public void saveText(final Itemtext text) {
        itemPersistence.saveItemtext(text);
    }

    @Override
    public List<Item> findItems(String name, String author, String code, String exhibitionName, Double xCoordinate, Double yCoordinate, Double zCoordinate, Double deviation, final String roomName) {
        String convertedName = cyrillicLatinConverter.convertWord(name);
        String convertedAuthor = cyrillicLatinConverter.convertWord(author);
        String convertedCode = cyrillicLatinConverter.convertWord(code);
        String convertedExhibitionName = cyrillicLatinConverter.convertWord(exhibitionName);
        String convertedRoomName = cyrillicLatinConverter.convertWord(roomName);
        return itemPersistence.findItemsByParams(name, author, code, exhibitionName, xCoordinate, yCoordinate, zCoordinate, deviation, roomName,
                                                 convertedName, convertedAuthor, convertedCode, convertedExhibitionName, convertedRoomName);
    }

    @Override
    public void updateExistingItem(final Item item) {
        Item foundedItem = findItem(item.getIditem());
        foundedItem.setName(item.getName());
        foundedItem.setAuthor(item.getAuthor());
        foundedItem.setText(item.getText());
        foundedItem.setXcoordinate(item.getXcoordinate());
        foundedItem.setYcoordinate(item.getYcoordinate());
        foundedItem.setZcoordinate(item.getZcoordinate());
        foundedItem.setRadius(item.getRadius());
        saveItem(foundedItem);
    }

    @Override
    public Item saveMultipartFiles(MultipartFile[] images, MultipartFile[] audios, MultipartFile[] videos, String[] texts, Item item, String[] additionalVideoUrls, MultipartFile barcode) {
        if (barcode != null && !barcode.isEmpty()) {
            try {
                Map uploadResult = cloudc.upload(barcode.getBytes(),
                                                 ObjectUtils.asMap("resourcetype", "auto"));
                Barcodeimage barcodeImage = new Barcodeimage();
                barcodeImage.setImageurl(uploadResult.get("url").toString());
                barcodeImage.setServerid(uploadResult.get("public_id").toString());
                barcodeImage.setItem(item);
                item.setBarcodeimage(barcodeImage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for (MultipartFile image : images) {
            if (!image.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(image.getBytes(),
                                                     ObjectUtils.asMap("resourcetype", "auto"));
                    Itemimage itemimage = new Itemimage();
                    itemimage.setImageurl(uploadResult.get("url").toString());
                    itemimage.setServerid(uploadResult.get("public_id").toString());
                    itemimage.setItem(item);
                    item.getImages().add(itemimage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for (MultipartFile audio : audios) {
            if (!audio.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(audio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                    Itemaudio itemaudio = new Itemaudio();
                    itemaudio.setAudiourl(uploadResult.get("url").toString());
                    itemaudio.setServerid(uploadResult.get("public_id").toString());
                    itemaudio.setName(audio.getOriginalFilename());
                    itemaudio.setItem(item);
                    item.getAudios().add(itemaudio);
                    //TODO: set type
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for (MultipartFile video : videos) {
            if (!video.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(video.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                    Itemvideo itemvideo = new Itemvideo();
                    itemvideo.setVideourl(uploadResult.get("url").toString());
                    itemvideo.setServerid(uploadResult.get("public_id").toString());
                    itemvideo.setName(video.getOriginalFilename().substring(0, 20));
                    itemvideo.setItem(item);
                    itemPersistence.saveItemvideo(itemvideo);
                    item.getVideos().add(itemvideo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (!additionalVideoUrls[0].isEmpty()) {
            for (int i = 0; i < additionalVideoUrls.length; i++) {
                Itemvideo itemvideo = new Itemvideo();
                itemvideo.setVideourl(additionalVideoUrls[i]);
                itemvideo.setItem(item);
                item.getVideos().add(itemvideo);

            }
        }

        if (!texts[0].isEmpty()) {
            for (int i = 0; i < texts.length; i++) {
                if (!texts[i].isEmpty()) {
                    Itemtext itemtext = new Itemtext();
                    itemtext.setText(texts[i]);
                    itemtext.setItem(item);
                    item.getTexts().add(itemtext);
                }
            }
        }
        return item;
    }

    @Override
    public List<ItemResponseDTO> convertItems(final List<Item> items) {
        List<ItemResponseDTO> responseList = new ArrayList<>();
        for (Item p : items) {
            ItemResponseDTO response = new ItemResponseDTO();
            response.setIditem(p.getIditem());
            response.setXcoordinate(p.getXcoordinate());
            response.setYcoordinate(p.getYcoordinate());
            response.setRadius(p.getRadius());
            responseList.add(response);
        }
        return responseList;
    }

    @Override
    public List<ItemResponseDTO> convertMapItems(final List<Item> items) {
        List<ItemResponseDTO> responseList = new ArrayList<>();
        for (Item p : items) {
            ItemResponseDTO response = new ItemResponseDTO();
            response.setIditem(p.getIditem());
            response.setXcoordinate(p.getXcoordinate());
            response.setYcoordinate(p.getYcoordinate());
            response.setRadius(p.getRadius());
            if (!p.getImages().isEmpty()) {
                response.setImgUrl(p.getImages().get(0).getImageurl());
            }
            responseList.add(response);
        }
        return responseList;
    }

    @Override
    public List<Item> findItemsByExhibitionIdAndMapId(final int idexhibition, final int idmuseummap) {
        return itemPersistence.findItemsByExhibitionIdAndMapId(idexhibition, idmuseummap);
    }

    @Override
    public List<Itemvideo> findVideosWithOnlyUrlByItemId(final int iditem) {
        List<Itemvideo> videoWithOnlyUrl = new ArrayList<>();
        List<Itemvideo> videos = getallVideosFromItem(iditem);
        for (Itemvideo pv : videos) {
            if (pv.getServerid() == null) {
                videoWithOnlyUrl.add(pv);
            }
        }
        return videoWithOnlyUrl;
    }

    @Override
    public List<Itemvideo> findVideosWithoutOnlyUrlByItemId(final int iditem) {
        List<Itemvideo> videoWithOnlyUrl = new ArrayList<>();
        List<Itemvideo> videos = getallVideosFromItem(iditem);
        for (Itemvideo pv : videos) {
            if (pv.getServerid() != null) {
                videoWithOnlyUrl.add(pv);
            }
        }
        return videoWithOnlyUrl;
    }
}
