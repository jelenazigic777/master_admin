package com.etf.master.services.impl;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.utils.ObjectUtils;
import com.etf.master.filesmanipulation.FilesManipulator;
import com.etf.master.models.Ambientaudio;
import com.etf.master.models.Institution;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemvideo;
import com.etf.master.models.Museum;
import com.etf.master.models.Museumadditional;
import com.etf.master.models.Itemimage;
import com.etf.master.persistance.InstitutionPersistence;
import com.etf.master.persistance.MuseumPersistence;
import com.etf.master.persistance.MuseumadditionalPersistence;
import com.etf.master.services.MainService;
import com.etf.master.services.ItemService;

@Service
public class MainServiceImpl implements MainService {

    @Autowired
    private MuseumPersistence museumPersistence;

    @Autowired
    private InstitutionPersistence institutionPersistence;

    @Autowired
    private MuseumadditionalPersistence museumadditionalPersistence;

    @Autowired
    private ItemService itemService;

    @Autowired
    FilesManipulator cloudc;

    @Override
    public List<Institution> findAllInstitutions() {
        return institutionPersistence.findAll();
    }

    @Override
    public List<Museum> findAllMuseumsFromInstitution(int idInstitution) {
        return museumPersistence.findAllFromInstitution(idInstitution);
    }

    @Override
    public Museum findMuseum(int idMuseum) {
        return museumPersistence.findMuseum(idMuseum);
    }

    @Override
    public Museum saveMuseum(Museum museum) {
        if (museum.getXcoordinate() == null) {
            museum.setXcoordinate(0.0);
        }
        if (museum.getYcoordinate() == null) {
            museum.setYcoordinate(0.0);
        }
        if (museum.getZcoordinate() == null) {
            museum.setZcoordinate(0.0);
        }
        return museumPersistence.saveMuseum(museum);
    }

    @Override
    public void saveAdditionalMuseumInfo(Museumadditional museumadditional) {
        museumadditionalPersistence.saveMuseumadditional(museumadditional);
    }

    @Override
    public Museum saveAmbientAudioOnMuseum(MultipartFile ambientaudio, Museum museum) {
        if (ambientaudio != null)
        if (!ambientaudio.isEmpty()) {
            try {
                Map uploadResult = cloudc.upload(ambientaudio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                Ambientaudio ambientaudio1 = new Ambientaudio();
                ambientaudio1.setAmbientaudiourl(uploadResult.get("url").toString());
                ambientaudio1.setServerid(uploadResult.get("public_id").toString());
                ambientaudio1.setMuseum(museum);
                museum.getAmbientaudios().add(ambientaudio1);
                saveMuseum(museum);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return museum;
    }

    @Override
    public void deleteMuseum(int idMuseum) {
        museumPersistence.delete(idMuseum);
    }

    @Override
    public Institution findInstitution(int idInstitution) {
        return institutionPersistence.findInstitution(idInstitution);
    }

    @Override
    public void deleteInstitution(int idInstitution) {
        institutionPersistence.delete(idInstitution);
    }

    @Override
    public void saveInstitution(Institution institution) {
        institutionPersistence.saveInstitution(institution);
    }

    @Override
    public void deleteMuseumImage(int idImage) {
        itemService.deleteImage(idImage);
    }

    @Override
    public void deleteMuseumAudio(int idAudio) {
        itemService.deleteAudio(idAudio);
    }

    @Override
    public void deleteMuseumVideo(int idVideo) {
        itemService.deleteVideo(idVideo);
    }

    @Override
    public String transformDateFormat(final Date date) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        String dateStr = format.format(date);
        return dateStr;
    }

    @Override
    public Museum saveMultipartFiles(List<MultipartFile> images, List<MultipartFile> audios, List<MultipartFile> videos, Museum museum) {
        //TODO: check if already is set
        for (MultipartFile image : images) {
            if (image != null)
                if (!image.isEmpty()) {
                    try {
                        Map uploadResult = cloudc.upload(image.getBytes(),
                                                         ObjectUtils.asMap("resourcetype", "auto"));
                        Itemimage itemimage = new Itemimage();
                        itemimage.setImageurl(uploadResult.get("url").toString());
                        itemimage.setServerid(uploadResult.get("public_id").toString());
                        itemimage.setMuseum(museum);
                        itemService.saveImage(itemimage);
                        museum.getImages().add(itemimage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
        for (MultipartFile audio : audios) {
            if (audio != null)
                if (!audio.isEmpty()) {
                    try {
                        Map uploadResult = cloudc.upload(audio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                        Itemaudio itemaudio = new Itemaudio();
                        itemaudio.setAudiourl(uploadResult.get("url").toString());
                        itemaudio.setServerid(uploadResult.get("public_id").toString());
                        itemaudio.setName(audio.getOriginalFilename());
                        itemaudio.setMuseum(museum);
                        itemService.saveAudio(itemaudio);
                        museum.getAudios().add(itemaudio);
                        //TODO: set type
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
        for (MultipartFile video : videos) {
            if (video != null)
                if (!video.isEmpty()) {
                    try {
                        Map uploadResult = cloudc.upload(video.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                        Itemvideo itemvideo = new Itemvideo();
                        itemvideo.setVideourl(uploadResult.get("url").toString());
                        itemvideo.setServerid(uploadResult.get("public_id").toString());
                        itemvideo.setName(video.getOriginalFilename().substring(0, 20));
                        itemvideo.setMuseum(museum);
                        itemService.saveVideo(itemvideo);
                        museum.getVideos().add(itemvideo);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
        museumPersistence.saveMuseum(museum);
        return museum;
    }

    @Override
    public Museum saveClientImages(final MultipartFile primaryImage, final MultipartFile secondaryImage, Museum museum) {
        if (primaryImage != null)
            if (!primaryImage.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(primaryImage.getBytes(),
                                                     ObjectUtils.asMap("resourcetype", "auto"));
                    Itemimage itemimage = new Itemimage();
                    itemimage.setImageurl(uploadResult.get("url").toString());
                    itemimage.setServerid(uploadResult.get("public_id").toString());
                    itemimage.setMuseum(museum);
                    itemimage.setClientImagePriority(2);
                    itemService.saveImage(itemimage);
                    museum.getImages().add(itemimage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        if (secondaryImage != null)
            if (!secondaryImage.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(secondaryImage.getBytes(),
                                                     ObjectUtils.asMap("resourcetype", "auto"));
                    Itemimage itemimage = new Itemimage();
                    itemimage.setImageurl(uploadResult.get("url").toString());
                    itemimage.setServerid(uploadResult.get("public_id").toString());
                    itemimage.setMuseum(museum);
                    itemimage.setClientImagePriority(1);
                    itemService.saveImage(itemimage);
                    museum.getImages().add(itemimage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        return museum;
    }

    @Override
    public Museumadditional findMuseumAdditionalForMuseum(final int idMuseum) {
        return museumadditionalPersistence.findMuseumadditionalWithMuseumId(idMuseum);
    }

    @Override
    public Museum findMuseumByName(final String name) {
        return museumPersistence.findMuseumByName(name);
    }

    @Override
    public Itemimage findPrimaryClientImageForMuseumId(final int idmuseum) {
        return museumPersistence.findPrimaryClientImageForMuseumId(idmuseum);
    }

    @Override
    public Itemimage findSecondaryClientImageForMuseumId(final int idmuseum) {
        return museumPersistence.findSecondaryClientImageForMuseumId(idmuseum);
    }
}
