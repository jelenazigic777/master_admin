package com.etf.master.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.utils.ObjectUtils;
import com.etf.master.filesmanipulation.FilesManipulator;
import com.etf.master.models.Ambientaudio;
import com.etf.master.models.Beacon;
import com.etf.master.models.Floormap;
import com.etf.master.models.Item;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Itemvideo;
import com.etf.master.models.Museum;
import com.etf.master.models.Museummap;
import com.etf.master.models.Room;
import com.etf.master.persistance.FloormapPersistence;
import com.etf.master.persistance.MuseummapPersistence;
import com.etf.master.persistance.RoomPersistence;
import com.etf.master.services.AmbientaudioService;
import com.etf.master.services.BeaconService;
import com.etf.master.services.MainService;
import com.etf.master.services.MapService;
import com.etf.master.services.ItemService;

@Service
public class MapServiceImpl implements MapService {

    @Autowired
    FilesManipulator cloudc;

    @Autowired
    ItemService itemService;

    @Autowired
    MainService mainService;

    @Autowired
    BeaconService beaconService;

    @Autowired
    AmbientaudioService ambientaudioService;

    @Autowired
    MuseummapPersistence museummapPersistence;

    @Autowired
    RoomPersistence roomPersistence;

    @Autowired
    FloormapPersistence floormapPersistence;

    @Override
    public List<Museummap> findMuseummapsByExhibitionId(int idexhibition) {
        return museummapPersistence.findMuseummapsByExhibitionId(idexhibition);
    }

    @Override
    public List<Museummap> findAllFromMuseum(int idmuseum) {
        return museummapPersistence.findAllFromMuseum(idmuseum);
    }

    @Override
    public List<Museummap> findAllFloorsFromMuseum(final int idmuseum) {
        return museummapPersistence.findAllFloorsFromMuseum(idmuseum);
    }

    @Override
    public List<Museummap> findAllRoomsMapsFromMuseum(final int idmuseum) {
        return museummapPersistence.findAllRoomsMapsFromMuseum(idmuseum);
    }

    @Override
    public List<Floormap> findAllActiveFloormapsFromMuseum(final int idmuseum) {
        List<Floormap> floormaps = new ArrayList<>();
        List<Museummap> maps = museummapPersistence.findAllRoomsMapsFromMuseum(idmuseum);
        for (Museummap map : maps) {
            if (map.getFloormap().getImageurl() != null) {
                floormaps.add(map.getFloormap());
            }
        }
        return floormaps;
    }

    @Override
    public Museummap findMuseummap(int idMuseummap) {
        return museummapPersistence.findMuseummap(idMuseummap);
    }

    @Override
    public Floormap findFloormap(final int idFloormap) {
        return floormapPersistence.findFloormap(idFloormap);
    }

    @Override
    public void saveMuseummap(Museummap museummap) {
        if (museummap.getXstart() == null) {
            museummap.setXstart(0.0);
        }
        if (museummap.getYstart() == null) {
            museummap.setYstart(0.0);
        }
        if (museummap.getXend() == null) {
            museummap.setXend(0.0);
        }
        if (museummap.getYend() == null) {
            museummap.setYend(0.0);
        }
        if (museummap.getZcoordinate() == null) {
            museummap.setZcoordinate(0.0);
        }
        museummapPersistence.save(museummap);
    }

    @Override
    public void saveFloorMap(final Floormap floormap) {
        if (floormap.getXstart() == null) {
            floormap.setXstart(0.0);
        }
        if (floormap.getYstart() == null) {
            floormap.setYstart(0.0);
        }
        if (floormap.getXend() == null) {
            floormap.setXend(0.0);
        }
        if (floormap.getYend() == null) {
            floormap.setYend(0.0);
        }
        if (floormap.getZcoordinate() == null) {
            floormap.setZcoordinate(0.0);
        }
        floormapPersistence.save(floormap);
    }

    @Override
    public void deleteMuseummap(int idMuseummap) {
        museummapPersistence.delete(idMuseummap);
    }

    @Override
    public void deleteFloormap(int idFloormap) {
        floormapPersistence.delete(idFloormap);
    }

    @Override
    public void deleteRoom(int idRoom) {
        roomPersistence.delete(idRoom);
    }

    @Override
    public void deleteImage(int idImage) {
        itemService.deleteImage(idImage);
    }

    @Override
    public void deleteAudio(int idAudio) {
        itemService.deleteAudio(idAudio);
    }

    @Override
    public void deleteVideo(int idVideo) {
        itemService.deleteVideo(idVideo);
    }

    @Override
    public List<Museummap> findAllFromFloor(int idMuseum, int idFloor) {
        return museummapPersistence.findAllFromFloor(idMuseum, idFloor);
    }

    @Override
    public Museummap findFloorByRoomFloorNumber(int idMuseum, int floorNumber) {
        return museummapPersistence.findFloorByRoomFloorNumber(idMuseum, floorNumber);
    }

    @Override
    public List<Room> findAllRoomsFromMuseum(int idMuseum) {
        return roomPersistence.findAllFromMuseum(idMuseum);
    }

    @Override
    public Room findRoom(int idRoom) {
        return roomPersistence.findRoom(idRoom);
    }

    @Override
    public List<Room> findConnectedRooms(int idRoom) {
        return roomPersistence.findConnectedRooms(idRoom);
    }

    @Override
    public List<Item> findItemsByMuseummapId(int idMuseummap) {
        return itemService.findItemsByMuseummapId(idMuseummap);
    }

    @Override
    public List<Beacon> findAllBeaconsByMuseummapId(int idMuseummap) {
        return beaconService.findAllFromMuseummap(idMuseummap);
    }

    @Override
    public void removeItemMapRelation(int idmuseummap, int iditem) {
        itemService.removeItemMapRelation(idmuseummap, iditem);
        Item item = itemService.findItem(iditem);
        item.setXcoordinate(null);
        item.setYcoordinate(null);
        item.setMuseummap(null);
        itemService.saveItem(item);
    }

    @Override
    public void removeBeaconMapRelation(final int idmuseummap, final int idbeacon) {
        //beaconService.removeBeaconMapRelation(idmuseummap, idbeacon);
        Beacon beacon = beaconService.findBeacon(idbeacon);
        beacon.setXcoordinate(null);
        beacon.setYcoordinate(null);
        beaconService.saveBeacon(beacon);
    }

    @Override
    public void removeRoomRelation(int idroom1, int idroom2) {
        Room room1 = findRoom(idroom1);
        Room room2 = findRoom(idroom2);
        room1.getRooms().remove(room2);
        room2.getRooms().remove(room1);
        roomPersistence.save(room1);
        roomPersistence.save(room2);
    }

    @Override
    public List<Itemimage> findallImagesFromMuseummap(int idmuseummap) {
        return museummapPersistence.getallImagesFromMuseummap(idmuseummap);
    }

    @Override
    public List<Itemaudio> findallAudiosFromMuseummap(int idmuseummap) {
        return museummapPersistence.getallAudiosFromMuseummap(idmuseummap);
    }

    @Override
    public List<Itemvideo> findallVideosFromMuseummap(int idmuseummap) {
        return museummapPersistence.getallVideosFromMuseummap(idmuseummap);
    }

    @Override
    public void saveAmbientAudio(MultipartFile ambientaudio, int idmuseum, int idroom) {
        Map uploadResult = null;
        try {
            uploadResult = cloudc.upload(ambientaudio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
            Ambientaudio ambientaudio1 = new Ambientaudio();
            ambientaudio1.setAmbientaudiourl(uploadResult.get("url").toString());
            ambientaudio1.setServerid(uploadResult.get("public_id").toString());
            Room room = roomPersistence.findRoom(idroom);
            ambientaudio1.setRoom(room);
            ambientaudioService.saveAmbientAudio(ambientaudio1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveAmbientAudioOnFloor(final MultipartFile ambientaudio, final int idMuseum, final int idMuseummap) {
        Map uploadResult = null;
        try {
            uploadResult = cloudc.upload(ambientaudio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
            Ambientaudio ambientaudio1 = new Ambientaudio();
            ambientaudio1.setAmbientaudiourl(uploadResult.get("url").toString());
            ambientaudio1.setServerid(uploadResult.get("public_id").toString());
            Museummap museummap = museummapPersistence.findMuseummap(idMuseummap);
            ambientaudio1.setFloor(museummap);
            ambientaudioService.saveAmbientAudio(ambientaudio1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setRooms(String[] selectedRooms, Room room) {
        if (selectedRooms != null) {
            List<Room> roomsToSet = new ArrayList<Room>();
            for (int i = 0; i < selectedRooms.length; i++) {
                Room r = roomPersistence.findRoom(Integer.parseInt(selectedRooms[i]));
                List<Room> rooms = new ArrayList<>();
                rooms.add(room);
                r.setRooms(rooms);
                roomsToSet.add(r);
            }
            //TODO--test if many to many set - its not
            List<Room> rooms = roomPersistence.findConnectedRooms(room.getIdroom());
            for (Room p : rooms) {
                if (!roomsToSet.contains(p)) {
                    List<Room> roomss = new ArrayList<>();
                    roomss.add(room);
                    p.setRooms(roomss);
                    roomsToSet.add(p);
                }
            }
            room.setRooms(roomsToSet);
        }
    }

    @Override
    public void setItems(String[] selectedItems, Museummap museummap, Room room) {
        if (selectedItems != null) {
            List<Item> itemsToSet = new ArrayList<Item>();
            for (int i = 0; i < selectedItems.length; i++) {
                Item p = itemService.findItem(Integer.parseInt(selectedItems[i]));
                p.setMuseummap(museummap);
                itemsToSet.add(p);
            }
            List<Item> items = itemService.findItemsByMuseummapId(museummap.getIdmuseummap());
            for (Item p : items) {
                if (!itemsToSet.contains(p)) {
                    p.setMuseummap(museummap);
                    itemsToSet.add(p);
                }
            }
            museummap.getItems().clear();
            museummap.getItems().addAll(itemsToSet);
            if (room != null) {
                room.getItems().clear();
                room.setItems(room.getItems());
            }
        }
    }

    @Override
    public void setAndSave(Museummap museummap, Room room, Museum museum, Locale locale, boolean isold, boolean isfloor) {

        museummap.setMuseum(museum);
        museummap.setInstitution(museum.getInstitution());
        museummap.setLanguage(locale.getLanguage());
        museummap.setIsfloor(isfloor);

        if (room != null) {
            room.setMuseum(museum);
            if (room.getIdroom() != 0) {
                room.setRooms(findConnectedRooms(room.getIdroom()));
            }
            room.setInstitutuion(museum.getInstitution());
            room.setLanguage(locale.getLanguage());
            room.setName(museummap.getName());
            room.setText(museummap.getText());
            room.setTexturl(museummap.getTexturl());
            room.setTexturllocal(museummap.getTexturllocal());
            room.setImageurl(museummap.getImageurl());
            room.setImageurllocal(museummap.getImageurllocal());
            room.setAudiourl(museummap.getAudiourl());
            room.setAudiourllocal(museummap.getAudiourllocal());
            room.setVideourl(museummap.getVideourl());
            room.setVideourllocal(museummap.getVideourllocal());
            room.setRadius(museummap.getRadius());
        }
        Floormap floormapFromForm = museummap.getFloormap();
        Floormap floormap = museummap.getFloormap();
        if (isold) {
            floormap = floormapPersistence.getFloormapByMuseummapId(museummap.getIdmuseummap());
            floormap.setXstart(floormapFromForm.getXstart());
            floormap.setYstart(floormapFromForm.getYstart());
            floormap.setXend(floormapFromForm.getXend());
            floormap.setYend(floormapFromForm.getYend());
            floormap.setZcoordinate(floormapFromForm.getZcoordinate());
            floormap.setFloornumber(floormapFromForm.getFloornumber());
        } else {
            //set base data only
            floormap.setMuseum(museum);
            floormap.setLanguage(locale.getLanguage());
            floormap.setName(museummap.getName());
            floormap.setText(museummap.getText());
            floormap.setTexturl(museummap.getTexturl());
            floormap.setTexturllocal(museummap.getTexturllocal());
        }
        museummap.setXstart(floormap.getXstart());
        museummap.setXend(floormap.getXend());
        museummap.setYstart(floormap.getYstart());
        museummap.setYend(floormap.getYend());
        museummap.setZcoordinate(floormap.getZcoordinate());
        floormap.setMuseummap(museummap);
        museummap.setFloormap(floormap);
        museummapPersistence.save(museummap);
        if (room != null) {
            room.setXcoordinate(floormap.getXstart());
            room.setYcoordinate(floormap.getYstart());
            room.setZcoordinate(floormap.getZcoordinate());

            floormap.setRoom(room);
            room.setFloormap(floormap);
            roomPersistence.save(room);
        }
    }

    @Override
    public List<Itemvideo> findVideosWithoutOnlyUrlByMuseummapId(final int idmuseummap) {
        List<Itemvideo> videoWithOnlyUrl = new ArrayList<>();
        List<Itemvideo> videos = findallVideosFromMuseummap(idmuseummap);
        for (Itemvideo pv : videos) {
            if (pv.getServerid() != null) {
                videoWithOnlyUrl.add(pv);
            }
        }
        return videoWithOnlyUrl;
    }

    @Override
    public List<Itemvideo> findVideosWithOnlyUrlByMuseummapId(final int idmuseummap) {
        List<Itemvideo> videoWithOnlyUrl = new ArrayList<>();
        List<Itemvideo> videos = findallVideosFromMuseummap(idmuseummap);
        for (Itemvideo pv : videos) {
            if (pv.getServerid() == null) {
                videoWithOnlyUrl.add(pv);
            }
        }
        return videoWithOnlyUrl;
    }

    @Override
    public Museummap saveMultipartFiles(MultipartFile[] images, MultipartFile[] audios, MultipartFile[] videos, Museummap map, String[] additionalVideoUrls) {

        for (MultipartFile image : images) {
            if (!image.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(image.getBytes(),
                                                     ObjectUtils.asMap("resourcetype", "auto"));
                    Itemimage itemimage = new Itemimage();
                    itemimage.setImageurl(uploadResult.get("url").toString());
                    itemimage.setServerid(uploadResult.get("public_id").toString());
                    itemimage.setMuseummap(map);
                    map.getImages().add(itemimage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for (MultipartFile audio : audios) {
            if (!audio.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(audio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                    Itemaudio itemaudio = new Itemaudio();
                    itemaudio.setAudiourl(uploadResult.get("url").toString());
                    itemaudio.setServerid(uploadResult.get("public_id").toString());
                    itemaudio.setName(audio.getOriginalFilename());
                    itemaudio.setMuseuumap(map);
                    map.getAudios().add(itemaudio);
                    //TODO: set type
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for (MultipartFile video : videos) {
            if (!video.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(video.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                    Itemvideo itemvideo = new Itemvideo();
                    itemvideo.setVideourl(uploadResult.get("url").toString());
                    itemvideo.setServerid(uploadResult.get("public_id").toString());
                    itemvideo.setName(video.getOriginalFilename().substring(0, 20));
                    itemvideo.setMuseummap(map);
                    itemService.saveVideo(itemvideo);
                    map.getVideos().add(itemvideo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (!additionalVideoUrls[0].isEmpty()) {
            for (int i = 0; i < additionalVideoUrls.length; i++) {
                Itemvideo itemvideo = new Itemvideo();
                itemvideo.setVideourl(additionalVideoUrls[i]);
                itemvideo.setMuseummap(map);
                map.getVideos().add(itemvideo);
            }
        }
        saveMuseummap(map);
        return map;
    }
}
