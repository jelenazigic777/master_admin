package com.etf.master.services.impl;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.utils.ObjectUtils;
import com.etf.master.filesmanipulation.FilesManipulator;
import com.etf.master.models.Exhibition;
import com.etf.master.models.Exhibitionlanguages;
import com.etf.master.models.Item;
import com.etf.master.models.Itemaudiolanguages;
import com.etf.master.models.Itemlanguages;
import com.etf.master.models.Itemvideolanguages;
import com.etf.master.models.Languages;
import com.etf.master.models.Museum;
import com.etf.master.models.Museumlanguages;
import com.etf.master.models.Museummap;
import com.etf.master.models.Museummaplanguages;
import com.etf.master.persistance.AudioLanguagePersistence;
import com.etf.master.persistance.ExhibitionlanguagePersistance;
import com.etf.master.persistance.ItemlanguagePersistance;
import com.etf.master.persistance.LanguagePersistence;
import com.etf.master.persistance.MuseumlanguagePersistance;
import com.etf.master.persistance.MuseummaplanguagePersistance;
import com.etf.master.persistance.VideoLanguagePersistence;
import com.etf.master.services.ExhibitionService;
import com.etf.master.services.ItemService;
import com.etf.master.services.LanguageService;
import com.etf.master.services.MainService;
import com.etf.master.services.MapService;

@Service
public class LanguageServiceImpl implements LanguageService {

    @Autowired
    MainService mainService;

    @Autowired
    ItemService itemService;

    @Autowired
    ExhibitionService exhibitionService;

    @Autowired
    MapService mapService;

    @Autowired
    LanguagePersistence languagePersistence;

    @Autowired
    MuseumlanguagePersistance museumlanguagePersistance;

    @Autowired
    ItemlanguagePersistance itemlanguagePersistance;

    @Autowired
    ExhibitionlanguagePersistance exhibitonlanguagePersistance;

    @Autowired
    MuseummaplanguagePersistance museummaplanguagePersistance;

    @Autowired
    FilesManipulator cloudc;

    @Autowired
    AudioLanguagePersistence audioLanguagePersistence;

    @Autowired
    VideoLanguagePersistence videoLanguagePersistence;

    @Override
    public void saveLanguage(String name, String abbreviation) {
        languagePersistence.saveLanguage(name, abbreviation);
    }

    @Override
    public Languages findLanguage(final int idLanguage) {
        return languagePersistence.findLanguage(idLanguage);
    }

    @Override
    public List<Languages> loadAllLanguges() {
        return languagePersistence.loadAllLanguges();
    }

    @Override
    public void deleteLanguage(int idLanguage) {
        languagePersistence.deleteLanguage(idLanguage);
    }

    @Override
    public Museumlanguages findMuseumLanguage(final int idMuseumLanguage) {
        return museumlanguagePersistance.findMuseumLanguage(idMuseumLanguage);
    }

    @Override
    public void saveMuseumLanguage(final Museumlanguages museumlanguages, final int idmuseum, final int idlanguage, final List<MultipartFile> audios, final List<MultipartFile> videos) {
        Languages language = findLanguage(idlanguage);
        Museum museum = mainService.findMuseum(idmuseum);
        museumlanguages.setLanguage(language);
        museumlanguages.setMuseum(museum);
        if (audios != null && !audios.isEmpty()) {
            for (MultipartFile audio : audios) {
                if (audio != null) {
                    if (!audio.isEmpty()) {
                        try {
                            Map uploadResult = cloudc.upload(audio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                            Itemaudiolanguages itemaudio = new Itemaudiolanguages();
                            itemaudio.setAudiourl(uploadResult.get("url").toString());
                            itemaudio.setServerid(uploadResult.get("public_id").toString());
                            itemaudio.setName(audio.getOriginalFilename());
                            itemaudio.setLanguage(language);
                            itemaudio.setMuseum(museum);
                            languagePersistence.saveAudioLanguage(itemaudio);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        if (videos != null && !videos.isEmpty()) {
            for (MultipartFile video : videos) {
                if (video != null) {
                    if (!video.isEmpty()) {
                        try {
                            Map uploadResult = cloudc.upload(video.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                            Itemvideolanguages itemvideo = new Itemvideolanguages();
                            itemvideo.setVideourl(uploadResult.get("url").toString());
                            itemvideo.setServerid(uploadResult.get("public_id").toString());
                            itemvideo.setName(video.getOriginalFilename().substring(0, 20));
                            itemvideo.setLanguage(language);
                            itemvideo.setMuseum(museum);
                            languagePersistence.saveVideoLanguage(itemvideo);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        museumlanguagePersistance.saveMuseumLanguage(museumlanguages);
    }

    @Override
    public Itemlanguages findItemLanguage(final int idItemLanguage) {
        return itemlanguagePersistance.findItemLanguage(idItemLanguage);
    }

    @Override
    public void saveItemLanguage(final Itemlanguages itemlanguages, final int iditem, final int idlanguage, final List<MultipartFile> audios, final List<MultipartFile> videos) {
        Languages language = findLanguage(idlanguage);
        Item item = itemService.findItem(iditem);
        itemlanguages.setLanguage(language);
        itemlanguages.setItem(item);
        if (audios != null && !audios.isEmpty()) {
            for (MultipartFile audio : audios) {
                if (audio != null) {
                    if (!audio.isEmpty()) {
                        try {
                            Map uploadResult = cloudc.upload(audio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                            Itemaudiolanguages itemaudio = new Itemaudiolanguages();
                            itemaudio.setAudiourl(uploadResult.get("url").toString());
                            itemaudio.setServerid(uploadResult.get("public_id").toString());
                            itemaudio.setName(audio.getOriginalFilename());
                            itemaudio.setLanguage(language);
                            itemaudio.setItem(item);
                            languagePersistence.saveAudioLanguage(itemaudio);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        if (videos != null && !videos.isEmpty()) {
            for (MultipartFile video : videos) {
                if (video != null) {
                    if (!video.isEmpty()) {
                        try {
                            Map uploadResult = cloudc.upload(video.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                            Itemvideolanguages itemvideo = new Itemvideolanguages();
                            itemvideo.setVideourl(uploadResult.get("url").toString());
                            itemvideo.setServerid(uploadResult.get("public_id").toString());
                            itemvideo.setName(video.getOriginalFilename().substring(0, 20));
                            itemvideo.setLanguage(language);
                            itemvideo.setItem(item);
                            languagePersistence.saveVideoLanguage(itemvideo);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        itemlanguagePersistance.saveItemLanguage(itemlanguages);
    }

    @Override
    public Exhibitionlanguages findExhibitionLanguage(final int idExhibitionLanguage) {
        return exhibitonlanguagePersistance.findExhibitionLanguage(idExhibitionLanguage);
    }

    @Override
    public void saveExhibitionLanguage(final Exhibitionlanguages exhibitionlanguages, final int idexhibition, final int idlanguage, final List<MultipartFile> audios, final List<MultipartFile> videos) {
        Languages language = findLanguage(idlanguage);
        Exhibition exhibition = exhibitionService.findExhibition(idexhibition);
        exhibitionlanguages.setLanguage(language);
        exhibitionlanguages.setExhibition(exhibition);
        if (audios != null && !audios.isEmpty()) {
            for (MultipartFile audio : audios) {
                if (audio != null) {
                    if (!audio.isEmpty()) {
                        try {
                            Map uploadResult = cloudc.upload(audio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                            Itemaudiolanguages itemaudio = new Itemaudiolanguages();
                            itemaudio.setAudiourl(uploadResult.get("url").toString());
                            itemaudio.setServerid(uploadResult.get("public_id").toString());
                            itemaudio.setName(audio.getOriginalFilename());
                            itemaudio.setLanguage(language);
                            itemaudio.setExhibition(exhibition);
                            languagePersistence.saveAudioLanguage(itemaudio);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        if (videos != null && !videos.isEmpty()) {
            for (MultipartFile video : videos) {
                if (video != null) {
                    if (!video.isEmpty()) {
                        try {
                            Map uploadResult = cloudc.upload(video.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                            Itemvideolanguages itemvideo = new Itemvideolanguages();
                            itemvideo.setVideourl(uploadResult.get("url").toString());
                            itemvideo.setServerid(uploadResult.get("public_id").toString());
                            itemvideo.setName(video.getOriginalFilename().substring(0, 20));
                            itemvideo.setLanguage(language);
                            itemvideo.setExhibition(exhibition);
                            languagePersistence.saveVideoLanguage(itemvideo);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        exhibitonlanguagePersistance.saveExhibitionLanguage(exhibitionlanguages);
    }

    @Override
    public Museummaplanguages findMuseummapLanguage(final int idMuseummapLanguage) {
        return museummaplanguagePersistance.findMuseummapLanguage(idMuseummapLanguage);
    }

    @Override
    public void saveMuseummapLanguage(final Museummaplanguages museummaplanguages, final int idmuseummap, final int idlanguage, final List<MultipartFile> audios, final List<MultipartFile> videos) {
        Languages language = findLanguage(idlanguage);
        Museummap museummap = mapService.findMuseummap(idmuseummap);
        museummaplanguages.setLanguage(language);
        museummaplanguages.setMuseummap(museummap);
        if (audios != null && !audios.isEmpty()) {
            for (MultipartFile audio : audios) {
                if (audio != null) {
                    if (!audio.isEmpty()) {
                        try {
                            Map uploadResult = cloudc.upload(audio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                            Itemaudiolanguages itemaudio = new Itemaudiolanguages();
                            itemaudio.setAudiourl(uploadResult.get("url").toString());
                            itemaudio.setServerid(uploadResult.get("public_id").toString());
                            itemaudio.setName(audio.getOriginalFilename());
                            itemaudio.setLanguage(language);
                            itemaudio.setMuseummap(museummap);
                            languagePersistence.saveAudioLanguage(itemaudio);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        if (videos != null && !videos.isEmpty()) {
            for (MultipartFile video : videos) {
                if (video != null) {
                    if (!video.isEmpty()) {
                        try {
                            Map uploadResult = cloudc.upload(video.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                            Itemvideolanguages itemvideo = new Itemvideolanguages();
                            itemvideo.setVideourl(uploadResult.get("url").toString());
                            itemvideo.setServerid(uploadResult.get("public_id").toString());
                            itemvideo.setName(video.getOriginalFilename().substring(0, 20));
                            itemvideo.setLanguage(language);
                            itemvideo.setMuseummap(museummap);
                            languagePersistence.saveVideoLanguage(itemvideo);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        museummaplanguagePersistance.saveMuseummapLanguage(museummaplanguages);
    }

    @Override
    public Museumlanguages findMuseumLanguageByLanguageAndMuseum(final int idLanguage, final int idMuseum) {
        return museumlanguagePersistance.findMuseumLanguageByLanguageAndMuseum(idLanguage, idMuseum);
    }

    @Override
    public Itemlanguages findItemLanguageByLanguageAndItem(final int idLanguage, final int idItem) {
        return itemlanguagePersistance.findItemLanguageByLanguageAndItem(idLanguage, idItem);
    }

    @Override
    public Exhibitionlanguages findExhibitionLanguageByLanguageAndExhibition(final int idLanguage, final int idExhibition) {
        return exhibitonlanguagePersistance.findExhibitionLanguageByLanguageAndExhibition(idLanguage, idExhibition);
    }

    @Override
    public Museummaplanguages findMuseummapLanguageByLanguageAndMuseummap(final int idLanguage, final int idMuseummap) {
        return museummaplanguagePersistance.findExhibitionLanguageByLanguageAndExhibition(idLanguage, idMuseummap);
    }

    @Override
    public void deleteAudioLanguage(final int iditemaudiolanguage) {
        if (audioLanguagePersistence.findAudio(iditemaudiolanguage).getServerid() != null) {
            cloudc.delete(audioLanguagePersistence.findAudio(iditemaudiolanguage).getServerid());
        }
        audioLanguagePersistence.deleteAudio(iditemaudiolanguage);
    }

    @Override
    public void deleteVideoLanguage(final int iditemvideolanguage) {
        if (videoLanguagePersistence.findVideo(iditemvideolanguage).getServerid() != null) {
            cloudc.delete(videoLanguagePersistence.findVideo(iditemvideolanguage).getServerid());
        }
        videoLanguagePersistence.deleteVideo(iditemvideolanguage);
    }
}
