package com.etf.master.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.models.Role;
import com.etf.master.persistance.RolePersistance;
import com.etf.master.services.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RolePersistance rolePersistance;
    @Override
    public Role findRoleByName(final String name) {
        return rolePersistance.findRoleByName(name);
    }
}
