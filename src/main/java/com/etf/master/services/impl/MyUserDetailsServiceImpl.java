package com.etf.master.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.etf.master.models.User;
import com.etf.master.persistance.UserPersistence;

//todo : move every role in some enum in Role.java
//todo : move default user to properties

@Service
public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserPersistence userPersistence;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userPersistence.findByUsername(username);
        List<GrantedAuthority> authorityList = new ArrayList<>();
        if (user != null) {
            if (user.getUsername().equals("admin") && user.getPassword().equals("admin")) {
                authorityList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            } else {
                authorityList.add(new SimpleGrantedAuthority("ROLE_USER"));
            }
            return new org.springframework.security.core.userdetails.User(username, user.getPassword(), user.getActive(), true, true, true, authorityList);
        }
    return null;
    }
}
