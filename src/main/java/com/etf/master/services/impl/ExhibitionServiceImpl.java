package com.etf.master.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.utils.ObjectUtils;
import com.etf.master.convertors.CyrillicLatinConverter;
import com.etf.master.dtos.MapResponseDTO;
import com.etf.master.filesmanipulation.FilesManipulator;
import com.etf.master.models.Ambientaudio;
import com.etf.master.models.Exhibition;
import com.etf.master.models.Item;
import com.etf.master.models.Itemtext;
import com.etf.master.models.Itemvideo;
import com.etf.master.models.Museum;
import com.etf.master.models.Museummap;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemimage;
import com.etf.master.persistance.ExhibitionPersistence;
import com.etf.master.services.AmbientaudioService;
import com.etf.master.services.ExhibitionService;
import com.etf.master.services.MainService;
import com.etf.master.services.MapService;
import com.etf.master.services.ItemService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class ExhibitionServiceImpl implements ExhibitionService {

    @Autowired
    private MainService mainService;

    @Autowired
    private ExhibitionPersistence exhibitionPersistence;

    @Autowired
    private ItemService itemService;

    @Autowired
    private MapService mapService;

    @Autowired
    AmbientaudioService ambientaudioService;

    @Autowired
    FilesManipulator cloudc;

    @Autowired
    CyrillicLatinConverter cyrillicLatinConverter;

    @Override
    public List<Exhibition> findAllFromMuseum(int idmuseum) {
        return exhibitionPersistence.findAllFromMuseum(idmuseum);
    }

    @Override
    public Exhibition findExhibition(int idexhibition) {
        return exhibitionPersistence.findExhibition(idexhibition);
    }

    @Override
    public List<String> findAuthorsFromExhibitionItems(int idexhibition) {
        List<Item> exhibitionitems = itemService.findItemsByExhibitionId(idexhibition);
        List<String> authors = new ArrayList<>();
        for (Item item : exhibitionitems) {
            if (!authors.contains(item.getAuthor())) {
                authors.add(item.getAuthor());
            }
        }
        return authors;
    }

    @Override
    public Exhibition setItems(String[] selectedItems, Exhibition exhibition) {
        if (selectedItems != null) {
            List<Item> itemsToSet = new ArrayList<Item>();
            for (int i = 0; i < selectedItems.length; i++) {
                itemsToSet.add(itemService.findItem(Integer.parseInt(selectedItems[i])));
            }
            List<Item> items = itemService.findItemsByExhibitionId(exhibition.getIdexhibition());
            for (Item p : items) {
                if (!itemsToSet.contains(p)) {
                    itemsToSet.add(p);
                }
            }
            exhibition.setItems(itemsToSet);
        }
        return exhibition;
    }

    @Override
    public Exhibition setMaps(String[] selectedMaps, Exhibition exhibition) {
        if (selectedMaps != null) {
            List<Museummap> museumMapsToSet = new ArrayList<Museummap>();
            for (int i = 0; i < selectedMaps.length; i++) {
                museumMapsToSet.add(mapService.findMuseummap(Integer.parseInt(selectedMaps[i])));
            }
            List<Museummap> museummaps = mapService.findMuseummapsByExhibitionId(exhibition.getIdexhibition());
            for (Museummap m : museummaps) {
                if (!museumMapsToSet.contains(m)) {
                    museumMapsToSet.add(m);
                }
            }
            exhibition.setMuseummaps(museumMapsToSet);
        }
        return exhibition;
    }

    @Override
    public void deleteExhibition(int idexhibition) {
        exhibitionPersistence.deleteExhibition(idexhibition);
    }

    @Override
    public Exhibition saveExhibition(Exhibition exhibition) {
        return exhibitionPersistence.saveExhibition(exhibition);
    }

    @Override
    public void removeItemExhibitionRelation(int idexhibition, int iditem) {
        exhibitionPersistence.removeItemExhibitionRelation(idexhibition, iditem);
    }

    @Override
    public void removeMuseummapExhibitionRelation(int idexhibition, int idmuseummap) {
        exhibitionPersistence.removeMuseummapExhibitionRelation(idexhibition, idmuseummap);
    }

    @Override
    public void setAmbientAudioOnExhibition(MultipartFile ambientaudio, int idmuseum, int idexhibition) {
        if (ambientaudio != null) {
            if (!ambientaudio.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(ambientaudio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                    ;
                    Ambientaudio ambientaudio1 = new Ambientaudio();
                    ambientaudio1.setAmbientaudiourl(uploadResult.get("url").toString());
                    ambientaudio1.setServerid(uploadResult.get("public_id").toString());
                    Museum museum = mainService.findMuseum(idmuseum);
                    Exhibition exhibition = exhibitionPersistence.findExhibition(idexhibition);
                    ambientaudio1.setExhibition(exhibition);
                    ambientaudioService.saveAmbientAudio(ambientaudio1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public Integer findExhibitionImage(int idexhibition, int iditem) {
        return exhibitionPersistence.findExhibitionImage(idexhibition, iditem);
    }

    @Override
    public Integer findExhibitionVideo(int idexhibition, int iditem) {
        return exhibitionPersistence.findExhibitionVideo(idexhibition, iditem);
    }

    @Override
    public Integer findExhibitionAudio(int idexhibition, int iditem) {
        return exhibitionPersistence.findExhibitionAudio(idexhibition, iditem);
    }

    @Override
    public Integer findExhibitionText(int idexhibition, int iditem) {
        return exhibitionPersistence.findExhibitionText(idexhibition, iditem);
    }

    @Override
    public void removeImageExhibitionRelation(int idexhibition, int iditem) {
        Integer foundedid = findExhibitionImage(idexhibition, iditem);
        if (foundedid != null) {
            exhibitionPersistence.removeImageExhibitionRelation(idexhibition, foundedid);
        }
    }

    @Override
    public void removeAudioExhibitionRelation(int idexhibition, int iditem) {
        Integer foundedid = findExhibitionAudio(idexhibition, iditem);
        if (foundedid != null) {
            exhibitionPersistence.removeAudioExhibitionRelation(idexhibition, foundedid);
        }
    }

    @Override
    public void removeVideoExhibitionRelation(int idexhibition, int iditem) {
        Integer foundedid = findExhibitionVideo(idexhibition, iditem);
        if (foundedid != null) {
            exhibitionPersistence.removeVideoExhibitionRelation(idexhibition, foundedid);
        }
    }

    @Override
    public void removeTextExhibitionRelation(int idexhibition, int iditem) {
        Integer foundedid = findExhibitionText(idexhibition, iditem);
        if (foundedid != null) {
            exhibitionPersistence.removeTextExhibitionRelation(idexhibition, foundedid);
        }
    }

    @Override
    public void basicSave(Exhibition exhibition, int idmuseum, Locale locale) {
        Museum museum = mainService.findMuseum(idmuseum);
        exhibition.setMuseum(museum);
        exhibition.setInstitution(museum.getInstitution());
        exhibition.setLanguage(locale.getLanguage());
        exhibitionPersistence.saveExhibition(exhibition);
    }

    @Override
    public Exhibition cloneExhibitionAndReturnSaved(final Exhibition exhibition) {
        Exhibition newExhibition = new Exhibition();
        newExhibition.setActive(exhibition.getActive());
        newExhibition = saveExhibition(newExhibition);
        newExhibition.setMuseum(exhibition.getMuseum());
        newExhibition.setInstitution(exhibition.getInstitution());
        newExhibition.setLanguage(exhibition.getLanguage());
        List<Museummap> museummaps = exhibition.getMuseummaps();
        List<Museummap> newmuseummaps = new ArrayList<>();
        newmuseummaps.addAll(museummaps);
        newExhibition.setMuseummaps(newmuseummaps);
        List<Item> items = exhibition.getItems();
        List<Item> newitems = new ArrayList<>();
        newitems.addAll(items);
        newExhibition.setItems(newitems);

        //TODO: but what if someone erase on cloud, that is not copied !!!
        Ambientaudio oldAmbientaudio = exhibition.getAmbientaudio();
        if (oldAmbientaudio != null) {
            Ambientaudio newAmbientaudio = new Ambientaudio();
            newAmbientaudio.setMuseum(oldAmbientaudio.getMuseum());
            newAmbientaudio.setServerid(oldAmbientaudio.getServerid());
            newAmbientaudio.setAmbientaudiourl(oldAmbientaudio.getAmbientaudiourl());
            newAmbientaudio.setRoom(oldAmbientaudio.getRoom());
            newAmbientaudio.setExhibition(newExhibition);
            ambientaudioService.saveAmbientAudio(newAmbientaudio);
        }
        List<Itemimage> images = new ArrayList<>();
        images.addAll(exhibition.getImages());
        newExhibition.setImages(images);
        List<Itemvideo> videos = new ArrayList<>();
        videos.addAll(exhibition.getVideos());
        newExhibition.setVideos(videos);
        List<Itemaudio> audios = new ArrayList<>();
        audios.addAll(exhibition.getAudios());
        newExhibition.setAudios(audios);
        List<Itemtext> texts = new ArrayList<>();
        texts.addAll(exhibition.getTexts());
        newExhibition.setTexts(texts);

        newExhibition.setActive(exhibition.getActive());
        newExhibition.setAdditionalimagesurl(exhibition.getAdditionalimagesurl());
        newExhibition.setAuthor(exhibition.getAuthor());
        newExhibition.setImageurl(exhibition.getImageurl());
        newExhibition.setImageurllocal(exhibition.getImageurllocal());
        newExhibition.setName(exhibition.getName());
        newExhibition.setRadius(exhibition.getRadius());
        newExhibition.setRoom(exhibition.getRoom());
        newExhibition.setText(exhibition.getText());
        newExhibition.setTexturl(exhibition.getTexturl());
        newExhibition.setTexturllocal(exhibition.getTexturllocal());
        newExhibition.setVideourl(exhibition.getVideourl());
        newExhibition.setVideourllocal(exhibition.getVideourllocal());
        newExhibition.setXcoordinate(exhibition.getXcoordinate());
        newExhibition.setYcoordinate(exhibition.getYcoordinate());
        newExhibition.setZcoordinate(exhibition.getZcoordinate());
        return saveExhibition(newExhibition);
    }

    @Transactional
    @Override
    public Exhibition saveMultipartFiles(MultipartFile image, MultipartFile audio, MultipartFile video, Exhibition exhibition) {
        if (image != null) {
            if (!image.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(image.getBytes(),
                                                     ObjectUtils.asMap("resourcetype", "auto"));
                    Itemimage itemimage = new Itemimage();
                    itemimage.setImageurl(uploadResult.get("url").toString());
                    itemimage.setServerid(uploadResult.get("public_id").toString());
                    itemimage.setExhibition(exhibition);
                    itemService.saveImage(itemimage);
                    exhibition.setImage(itemimage);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (audio != null) {
            if (!audio.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(audio.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                    ;
                    Itemaudio itemaudio = new Itemaudio();
                    itemaudio.setAudiourl(uploadResult.get("url").toString());
                    itemaudio.setServerid(uploadResult.get("public_id").toString());
                    itemaudio.setName(audio.getOriginalFilename());
                    itemaudio.setExhibition(exhibition);
                    itemService.saveAudio(itemaudio);
                    exhibition.setAudio(itemaudio);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (video != null) {
            if (!video.isEmpty()) {
                try {
                    Map uploadResult = cloudc.upload(video.getBytes(), ObjectUtils.asMap("resource_type", "video"));
                    Itemvideo itemvideo = new Itemvideo();
                    itemvideo.setVideourl(uploadResult.get("url").toString());
                    itemvideo.setServerid(uploadResult.get("public_id").toString());
                    itemvideo.setName(video.getOriginalFilename().substring(0, 20));
                    itemvideo.setExhibition(exhibition);
                    itemService.saveVideo(itemvideo);
                    exhibition.setVideo(itemvideo);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        exhibitionPersistence.saveExhibition(exhibition);
        return exhibition;
    }

    @Override
    public List<MapResponseDTO> findMapResponseListByExhibitionId(final int idexhibition) {
        List<Item> exhibitionitems = itemService.findItemsByExhibitionId(idexhibition);
        List<Museummap> floorMapsFromItems = new ArrayList<>();
        List<MapResponseDTO> responseList = new ArrayList<>();
        for (Item p : exhibitionitems) {
            Museummap floor = mapService.findFloorByRoomFloorNumber(p.getMuseum().getIdmuseum(), p.getMuseummap().getFloormap().getFloornumber());
            if (!floorMapsFromItems.contains(p.getMuseummap()) && p.getMuseummap() != null) {
                MapResponseDTO response = new MapResponseDTO();
                floorMapsFromItems.add(p.getMuseummap());
                response.setFloor(p.getMuseummap());
                response.setxEndCoordinate(floor.getXend());
                response.setyEndCoordinate(floor.getYend());
                List<Item> items = null;
                if (p.getMuseummap() != null) {
                    items = itemService.findItemsByExhibitionIdAndMapId(idexhibition, p.getMuseummap().getIdmuseummap());
                } else {
                    items = new ArrayList<>();
                }
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                response.setJsonItems(gson.toJson(itemService.convertMapItems(items)));
                responseList.add(response);
            }
        }
        return responseList;
    }

    @Override
    public List<Museummap> findMuseummapsByExhibitionId(int idexhibition) {
        List<Item> exhibitionitems = itemService.findItemsByExhibitionId(idexhibition);
        List<Museummap> floorMapsFromItems = new ArrayList<>();
        for (Item p : exhibitionitems) {
            if (!floorMapsFromItems.contains(p.getMuseummap()) && p.getMuseummap() != null) {
                floorMapsFromItems.add(p.getMuseummap());
            }
        }
        return floorMapsFromItems;
    }

    @Override
    public List<Exhibition> findAllActiveFromMuseum(final int idmuseum) {
        return exhibitionPersistence.findAllActiveFromMuseum(idmuseum);
    }

    @Override
    public List<Exhibition> findAllInactiveItemsFromMuseum(final Integer idmuseum) {
        return exhibitionPersistence.findAllInactiveItemsFromMuseum(idmuseum);
    }

    @Override
    public List<Exhibition> findExhibitions(final String name, final String author, final String itemName, final String itemCode, final String roomName) {

        String convertedName = cyrillicLatinConverter.convertWord(name);
        String convertedAuthor = cyrillicLatinConverter.convertWord(author);
        String convertedItemName = cyrillicLatinConverter.convertWord(itemName);
        String convertedItemCode = cyrillicLatinConverter.convertWord(itemCode);
        String convertedRoomName = cyrillicLatinConverter.convertWord(roomName);
        return exhibitionPersistence.findExhibitionsByParams(name, author, itemName, itemCode, roomName,
                                                             convertedName, convertedAuthor, convertedItemName, convertedItemCode,
                                                             convertedRoomName);
    }
}
