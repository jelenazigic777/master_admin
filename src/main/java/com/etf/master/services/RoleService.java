package com.etf.master.services;

import com.etf.master.models.Role;

public interface RoleService {

    public Role findRoleByName (String name);
}
