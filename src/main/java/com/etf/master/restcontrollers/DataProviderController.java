package com.etf.master.restcontrollers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.etf.master.dtos.BeaconProviderResponseDTO;
import com.etf.master.dtos.ExhibitionProviderResponseDTO;
import com.etf.master.dtos.FloorMapProviderResponseDTO;
import com.etf.master.dtos.MuseumMapProviderResponseDTO;
import com.etf.master.dtos.MuseumProviderResponseDTO;
import com.etf.master.dtos.ItemProviderResponseDTO;
import com.etf.master.dtos.RoomProviderResponseDTO;
import com.etf.master.models.Beacon;
import com.etf.master.models.Exhibition;
import com.etf.master.models.Floormap;
import com.etf.master.models.Item;
import com.etf.master.models.Museum;
import com.etf.master.models.Museummap;
import com.etf.master.models.Room;
import com.etf.master.populators.Populator;
import com.etf.master.services.BeaconService;
import com.etf.master.services.ExhibitionService;
import com.etf.master.services.MainService;
import com.etf.master.services.MapService;
import com.etf.master.services.ItemService;
@RestController
public class DataProviderController {

    @Autowired
    MainService mainService;

    @Autowired
    ItemService itemService;

    @Autowired
    MapService mapService;

    @Autowired
    ExhibitionService exhibitionService;

    @Autowired
    BeaconService beaconService;

    @Autowired
    Populator populator;

    @RequestMapping("/provider/museums/{idMuseum}")
    public MuseumProviderResponseDTO getMuseums(@PathVariable(value = "idMuseum") String idMuseum) {
        Museum museum =  mainService.findMuseum(Integer.valueOf(idMuseum));
        MuseumProviderResponseDTO museumResponse = populator.populateMuseum(museum);
        return museumResponse;
    }

    @RequestMapping("/provider/items/{idMuseum}")
    public List<ItemProviderResponseDTO> getItems(@PathVariable(value = "idMuseum") String idMuseum) {
        List<Item> items =  itemService.findAllFromMuseum(Integer.valueOf(idMuseum));
        List<ItemProviderResponseDTO> responseList = populator.populateItems(items);
        return responseList;
    }

    @RequestMapping("/provider/floormaps/{idMuseum}")
    public List<FloorMapProviderResponseDTO> getFloormaps(@PathVariable(value = "idMuseum") String idMuseum) {
        List<Floormap> floormaps =  mapService.findAllActiveFloormapsFromMuseum(Integer.valueOf(idMuseum));
        List<FloorMapProviderResponseDTO> responseList = populator.populateFloormaps(floormaps);
        return responseList;
    }

    @RequestMapping("/provider/museummaps/{idMuseum}")
    public List<MuseumMapProviderResponseDTO> getMuseummaps(@PathVariable(value = "idMuseum") String idMuseum) {
        List<Museummap> museummaps =  mapService.findAllFloorsFromMuseum(Integer.valueOf(idMuseum));
        museummaps.addAll(mapService.findAllRoomsMapsFromMuseum(Integer.valueOf(idMuseum)));
        List<MuseumMapProviderResponseDTO> responseList = populator.populateMuseummaps(museummaps);
        return responseList;
    }

    @RequestMapping("/provider/rooms/{idMuseum}")
    public List<RoomProviderResponseDTO> getRooms(@PathVariable(value = "idMuseum") String idMuseum) {
        List<Room> rooms =  mapService.findAllRoomsFromMuseum(Integer.valueOf(idMuseum));
        List<RoomProviderResponseDTO> responseList = populator.populateRooms(rooms);
        return responseList;
    }

    @RequestMapping("/provider/exhibitions/{idMuseum}")
    public List<ExhibitionProviderResponseDTO> getExhibitions(@PathVariable(value = "idMuseum") String idMuseum) {
        List<Exhibition> exhibitions =  exhibitionService.findAllActiveFromMuseum(Integer.valueOf(idMuseum));
        List<ExhibitionProviderResponseDTO> responseList = populator.populateExhibitions(exhibitions);
        return responseList;
    }

    @RequestMapping("/provider/beacons/{idMuseum}")
    public List<BeaconProviderResponseDTO> getBeacons(@PathVariable(value = "idMuseum") String idMuseum) {
        List<Beacon> beacons = beaconService.findAllFromMuseum(Integer.valueOf(idMuseum));
        List<BeaconProviderResponseDTO> responseList = populator.populateBeacons(beacons);
        return responseList;
    }
}
