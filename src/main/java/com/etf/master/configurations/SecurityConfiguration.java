package com.etf.master.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.etf.master.services.impl.MyUserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyUserDetailsServiceImpl userDetailsService;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
              authorizeRequests()

            .antMatchers( "/").permitAll()
            .antMatchers( "/provider/**").permitAll()
            .antMatchers( "/registration").permitAll()
            .antMatchers( "/admin").access("hasRole('ADMIN')")
            .antMatchers( "/admin/**").access("hasRole('ADMIN')")
            .antMatchers( "/**").access("hasRole('USER') or hasRole('ADMIN')")
            .and().csrf().disable()
            .formLogin()
            .loginPage("/login").permitAll()
            .defaultSuccessUrl("/")
            .failureUrl("/login/error")
            .permitAll()
            .and()
            .logout()
            .logoutUrl("/logout")
            .logoutSuccessUrl("/login");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
          .ignoring()
          .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
    }

}
