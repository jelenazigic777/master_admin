package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.etf.master.models.Itemimage;
import com.etf.master.models.Museum;


public interface MuseumDAO extends JpaRepository<Museum, Integer> {

    @Query(value = "FROM Museum m WHERE m.name = :name")
    Museum findMuseumByName(@Param("name") String name);

    @Query(value = "SELECT * FROM museum r WHERE r.idinstitution=?1", nativeQuery = true)
    List<Museum> findAllFromInstitution(int idinstitution);

    @Query(value = "FROM Itemimage i where i.museum.idmuseum = :idmuseum and i.clientImagePriority = 2")
    Itemimage findPrimaryClientImageForMuseumId(@Param("idmuseum")int idmuseum);

    @Query(value = "FROM Itemimage i where i.museum.idmuseum = :idmuseum and i.clientImagePriority = 1")
    Itemimage findSecondaryClientImageForMuseumId(@Param("idmuseum") int idmuseum);
}
