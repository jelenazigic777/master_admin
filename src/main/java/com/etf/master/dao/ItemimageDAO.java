package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Itemimage;

public interface ItemimageDAO extends JpaRepository<Itemimage, Integer> {

    @Query(value= "DELETE pi FROM itemimage pi WHERE pi.iditem=?1", nativeQuery=true)
    void deleteFromItem(int id);

    @Query(value= "SELECT * FROM itemimage pi WHERE pi.iditem=?1", nativeQuery=true)
    List<Itemimage> getallImagesFromItem(int iditem);

    @Query(value= "SELECT * FROM itemimage pi WHERE pi.idmuseummap=?1", nativeQuery=true)
    List<Itemimage> getallImagesFromMuseummap(int idmuseummap);
}
