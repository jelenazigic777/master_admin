package com.etf.master.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Museummaplanguages;

public interface MuseumaplanguagesDAO extends JpaRepository<Museummaplanguages, Integer> {

    @Query(value= "SELECT * FROM museummaplanguages m WHERE m.idlanguage=?1 AND m.idmuseummap=?2", nativeQuery=true)
    Museummaplanguages findMuseummapLanguageByLanguageAndMuseummap(int idLanguage, int idMuseummap);
}
