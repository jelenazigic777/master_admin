package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Beacon;

public interface BeaconDAO extends JpaRepository<Beacon, Integer> {

    @Query(value= "SELECT * FROM beacon b WHERE b.idmuseum=?1", nativeQuery=true)
    List<Beacon> findAllFromMuseum(Integer idmuseum);

    @Query(value= "SELECT * FROM beacon b WHERE b.idmuseummap=?1", nativeQuery=true)
    List<Beacon> findAllFromMuseummap(Integer idMuseummap);

}
