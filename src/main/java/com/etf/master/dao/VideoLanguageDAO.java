package com.etf.master.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.etf.master.models.Itemvideolanguages;

public interface VideoLanguageDAO extends JpaRepository<Itemvideolanguages, Integer> {
}
