package com.etf.master.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Exhibitionlanguages;

public interface ExhibitionlanguagesDAO extends JpaRepository<Exhibitionlanguages, Integer> {

    @Query(value= "SELECT * FROM exhibitionlanguages m WHERE m.idlanguage=?1 AND m.idexhibition=?2", nativeQuery=true)
    Exhibitionlanguages findExhibitionLanguageByLanguageAndExhibition(int idLanguage, int idExhibition);
}
