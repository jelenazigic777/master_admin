package com.etf.master.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Role;

public interface RoleDAO extends JpaRepository<Role, Integer> {

    @Query("SELECT r FROM Role r WHERE r.name=?1")
    Role findRoleByName(String name);

}
