package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Itemaudio;

public interface ItemaudioDAO extends JpaRepository<Itemaudio, Integer> {

    @Query(value= "DELETE pa FROM itemaudio pa WHERE pa.iditem=?1", nativeQuery=true)
    void deleteFromItem(int id);

    @Query(value= "SELECT * FROM itemaudio pa WHERE pa.iditem=?1", nativeQuery=true)
    List<Itemaudio> getallAudiosFromItem(int iditem);

    @Query(value= "SELECT * FROM itemaudio pa WHERE pa.idmuseummap=?1", nativeQuery=true)
    List<Itemaudio> getallAudiosFromMuseummap(int idmuseuumap);


}
