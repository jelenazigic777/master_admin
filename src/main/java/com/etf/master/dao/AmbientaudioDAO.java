package com.etf.master.dao;

import com.etf.master.models.Ambientaudio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AmbientaudioDAO extends JpaRepository<Ambientaudio, Integer> {

}
