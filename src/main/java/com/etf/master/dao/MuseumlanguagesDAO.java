package com.etf.master.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Museumlanguages;

public interface MuseumlanguagesDAO extends JpaRepository<Museumlanguages, Integer> {

    @Query(value= "SELECT * FROM museumlanguages m WHERE m.idlanguage=?1 AND m.idmuseum=?2", nativeQuery=true)
    Museumlanguages findMuseumLanguageByLanguageAndMuseum(int idLanguage, int idMuseum);
}
