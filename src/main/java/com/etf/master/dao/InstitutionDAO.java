package com.etf.master.dao;

import com.etf.master.models.Institution;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstitutionDAO extends JpaRepository<Institution, Integer> {
}
