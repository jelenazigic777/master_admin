package com.etf.master.dao;


import com.etf.master.models.Languages;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageDAO  extends JpaRepository<Languages, Integer> {
}
