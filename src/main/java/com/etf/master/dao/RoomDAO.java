package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.etf.master.models.Room;

public interface RoomDAO extends JpaRepository<Room, Integer> {

    @Query(value = "SELECT * FROM room r WHERE r.idmuseum=?1", nativeQuery = true)
    List<Room> findAllFromMuseum(Integer idmuseum);

    @Query(value = "FROM Room r where r.idroom in (SELECT rr.idroom2 from Roomtoroom rr where rr.idroom1 = :idroom)")
    List<Room> findConnectedRooms(@Param("idroom") Integer idroom);

}
