package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.etf.master.models.Item;

public interface ItemDAO extends JpaRepository<Item, Integer> {

    @Query(value = "SELECT * FROM item i WHERE i.idmuseum=?1", nativeQuery = true)
    List<Item> findAllFromMuseum(Integer idmuseum);

    @Query(value = "SELECT * FROM item i WHERE i.idmuseummap=?1", nativeQuery = true)
    List<Item> findAllFromMuseummap(Integer idmuseummap);

    @Query(value = "SELECT * FROM item p JOIN itemtoexhibition pe ON p.iditem= pe.iditem JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 ORDER BY p.author ASC", nativeQuery = true)
    List<Item> findItemsByExhibitionId(int idexhibition);

    @Query(value = "SELECT * FROM item i WHERE i.idmuseummap=?1", nativeQuery = true)
    List<Item> findItemsByMuseummapId(int idmuseummap);

    @Modifying
    @Transactional
    @Query(value = "UPDATE item AS p SET idmuseummap = NULL WHERE  p.idmuseummap =?1 AND p.iditem=?2", nativeQuery = true)
    void removeItemMapRelation(int idmuseummap, int iditem);

    @Query(value = "SELECT * FROM item p JOIN itemtoexhibition pe ON p.iditem= pe.iditem JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 and p.idmuseummap=?2", nativeQuery = true)
    List<Item> findItemsByExhibitionIdAndMapId(int idexhibition, int idmuseummap);

    @Query(value = "FROM Item p " +
                   "WHERE (p.name like %:name% or p.name like %:convertedName%) " +
                   "and (p.author like %:author% or p.author like %:convertedAuthor%) " +
                   "and (p.xcoordinate >= :xCoordinate - :deviation " +
                   "and p.xcoordinate <= :xCoordinate + :deviation " +
                   "or :xCoordinate is null) " +
                   "and (p.ycoordinate >= :yCoordinate - :deviation " +
                   "and p.ycoordinate <= :yCoordinate + :deviation " +
                   "or :yCoordinate is null) " +
                   "and (p.zcoordinate >= :zCoordinate - :deviation " +
                   "and p.zcoordinate <= :zCoordinate + :deviation or :zCoordinate is null) " +
                   "and (p.identificationcode like %:code% " +
                   "or p.identificationcode like %:convertedCode%) " +
                   "and p.museummap.idmuseummap in " +
                   "(SELECT mm.idmuseummap FROM Museummap mm " +
                   "WHERE (mm.name like %:roomName% " +
                   "or mm.name like %:convertedRoomName%)) " +
                   "and p.iditem in " +
                   "(SELECT pe.iditem FROM Itemtoexhibition pe " +
                   "WHERE pe.idexhibition " +
                   "in (SELECT e.idexhibition FROM Exhibition e " +
                   "WHERE (e.name like %:exhibitionName% " +
                   "or e.name like %:convertedExhibitionName%)))")
    List<Item> findItemsByParams(@Param("name") String name, @Param("author") String author, @Param("code") String code, @Param("exhibitionName") String exhibitionName, @Param("xCoordinate") Double xCoordinate, @Param("yCoordinate") Double yCoordinate, @Param("zCoordinate") Double zCoordinate, @Param("deviation") Double deviation, @Param("roomName") String roomName, @Param("convertedName") String convertedName, @Param("convertedAuthor") String convertedAuthor, @Param("convertedCode") String convertedCode, @Param("convertedExhibitionName") String convertedExhibitionName, @Param("convertedRoomName") String convertedRoomName);

    @Query(value = "FROM Item p WHERE (p.name like %:name% or p.name like %:convertedName%) and (p.author like %:author% or p.author like %:convertedAuthor%) " +
                   "and (p.xcoordinate >= :xCoordinate - :deviation " +
                   "and p.xcoordinate <= :xCoordinate + :deviation or :xCoordinate is null) " +
                   "and (p.ycoordinate >= :yCoordinate - :deviation " +
                   "and p.ycoordinate <= :yCoordinate + :deviation or :yCoordinate is null) " +
                   "and (p.zcoordinate >= :zCoordinate - :deviation " +
                   "and p.zcoordinate <= :zCoordinate + :deviation or :zCoordinate is null)")
    List<Item> findItemsByItemParams(@Param("name") String name, @Param("author") String author, @Param("convertedName") String convertedName, @Param("convertedAuthor") String convertedAuthor, @Param("xCoordinate") Double xCoordinate, @Param("yCoordinate") Double yCoordinate, @Param("zCoordinate") Double zCoordinate, @Param("deviation") Double deviation);
}
