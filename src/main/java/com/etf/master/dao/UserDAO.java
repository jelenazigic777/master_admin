package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.etf.master.models.User;

@Repository
public interface UserDAO extends JpaRepository<User, Integer> {
    @Query("SELECT u FROM User u WHERE u.name=?1 AND u.surname=?2")
    User findByNameSurname(String name, String surname);

    @Query("SELECT u FROM User u WHERE u.username=?1")
    User findByUsername(String username);

    @Query("SELECT u FROM User u WHERE u.username=?1 AND u.password=?2")
    User findByUsernamePassword(String username, String password);

    @Query(value= "SELECT * FROM user U WHERE U.active=FALSE", nativeQuery=true)
    List<User> findAllUsersNotActive();
}

