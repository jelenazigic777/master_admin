package com.etf.master.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.etf.master.models.Museumadditional;

@Component
public interface MuseumadditionalDAO extends JpaRepository<Museumadditional, Integer> {

    @Query(value = "SELECT * FROM museumadditional r WHERE r.idmuseum=?1", nativeQuery = true)
    Museumadditional findMuseumadditionalWithMuseumId(int idmuseum);

}
