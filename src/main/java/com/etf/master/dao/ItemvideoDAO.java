package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Itemvideo;

public interface ItemvideoDAO extends JpaRepository<Itemvideo, Integer> {

    @Query(value= "DELETE pv FROM itemvideo pv WHERE pv.iditem=?1", nativeQuery=true)
    void deleteFromItem(int id);

    @Query(value= "SELECT * FROM itemvideo pv WHERE pv.iditem=?1", nativeQuery=true)
    List<Itemvideo> getallVideosFromItem(int iditem);

    @Query(value= "SELECT * FROM itemvideo pv WHERE pv.idmuseummap=?1", nativeQuery=true)
    List<Itemvideo> getallVideosFromMuseummap(int idmuseummap);
}
