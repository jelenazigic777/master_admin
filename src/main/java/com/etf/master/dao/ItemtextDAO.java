package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Itemtext;

public interface ItemtextDAO extends JpaRepository<Itemtext, Integer> {

    @Query(value= "DELETE pt FROM itemtext pt WHERE pt.iditem=?1", nativeQuery=true)
    void deleteFromItem(int id);

    @Query(value= "SELECT * FROM itemtext pt WHERE pt.iditem=?1", nativeQuery=true)
    List<Itemtext> getallTextsFromItem(int iditem);
}
