package com.etf.master.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.etf.master.models.Itemlanguages;

public interface ItemlanguagesDAO extends JpaRepository<Itemlanguages, Integer> {

    @Query(value= "SELECT * FROM itemlanguages m WHERE m.idlanguage=?1 AND m.iditem=?2", nativeQuery=true)
    Itemlanguages findItemLanguageByLanguageAndItem(int idLanguage, int idItem);
}
