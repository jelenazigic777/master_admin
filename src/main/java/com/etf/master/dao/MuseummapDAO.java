package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import com.etf.master.models.Museummap;

@Component
public interface MuseummapDAO extends JpaRepository<Museummap, Integer> {

    @Query(value= "SELECT * FROM museummap m WHERE m.idmuseum=?1", nativeQuery=true)
    List<Museummap> findAllFromMuseum(Integer idmuseum);

    @Query(value= "SELECT * FROM museummap p JOIN exhibitiontomuseummap pe ON p.idmuseummap= pe.idmuseummap JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 AND p.isfloor=false", nativeQuery=true)
    List<Museummap> findMuseummapsByExhibitionId(int idexhibition);

    @Query(value= "SELECT * FROM museummap m WHERE m.idmuseum=?1 AND m.isfloor=true", nativeQuery=true)
    List<Museummap> findAllFloorsFromMuseum(int idmuseum);

    @Query(value= "SELECT * FROM museummap m WHERE m.idmuseum=?1 AND m.isfloor=false", nativeQuery=true)
    List<Museummap> findAllRoomsMapsFromMuseum(int idmuseum);

    @Query(value= "SELECT * FROM museummap m JOIN floormap f on m.idmuseummap = f.idmuseummap WHERE m.idmuseum=?1 AND f.floornumber=?2 AND m.isfloor=false", nativeQuery = true)
    List<Museummap> findAllFromFloor(int idmuseum, int floorNumber);

    @Query(value= "SELECT * FROM museummap m JOIN floormap f on m.idmuseummap = f.idmuseummap WHERE f.floornumber=?2 AND m.idmuseum=?1 AND m.isfloor=true", nativeQuery=true)
    Museummap findFloorByRoomFloorNumber(int idmuseum, int floorNumber);
}
