package com.etf.master.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.etf.master.models.Itemaudiolanguages;

public interface AudioLanguageDAO extends JpaRepository<Itemaudiolanguages, Integer> {
}
