package com.etf.master.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.etf.master.models.Exhibition;

public interface ExhibitionDAO extends JpaRepository<Exhibition, Integer> {
    @Query(value = "SELECT * FROM exhibition e WHERE e.idmuseum=?1", nativeQuery = true)
    List<Exhibition> findAllFromMuseum(Integer idmuseum);

    @Transactional
    @Modifying
    @Query(value="DELETE pe FROM itemtoexhibition pe JOIN item p ON p.iditem= pe.iditem JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 AND p.iditem=?2", nativeQuery = true)
    void removeItemExhibitionRelation(int idexhibition, int iditem);

    @Transactional
    @Modifying
    @Query(value="DELETE pe FROM exhibitiontomuseummap pe JOIN museummap p ON p.idmuseummap= pe.idmuseummap JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 AND p.idmuseummap=?2", nativeQuery = true)
    void removeMuseummapExhibitionRelation(int idexhibition, int idmuseummap);

    @Query(value="SELECT pi.iditemimage FROM itemimage pi JOIN exhibitionimage ei on pi.iditemimage = ei.iditemimage WHERE ei.idexhibition=?1 AND pi.iditem=?2", nativeQuery = true)
    Integer findExhibitionImage(int idexhibition, int iditem);

    @Query(value="SELECT pv.iditemvideo FROM itemvideo pv JOIN exhibitionvideo ev on pv.iditemvideo = ev.iditemvideo WHERE ev.idexhibition=?1 AND pv.iditem=?2", nativeQuery = true)
    Integer findExhibitionVideo(int idexhibition, int iditem);

    @Query(value="SELECT pa.iditemaudio FROM itemaudio pa JOIN exhibitionaudio ea on pa.iditemaudio = ea.iditemaudio WHERE ea.idexhibition=?1 AND pa.iditem=?2", nativeQuery = true)
    Integer findExhibitionAudio(int idexhibition, int iditem);

    @Query(value="SELECT pt.iditemtext FROM itemtext pt JOIN exhibitiontext et on pt.iditemtext = et.iditemtext WHERE et.idexhibition=?1 AND pt.iditem=?2", nativeQuery = true)
    Integer findExhibitionText(int idexhibition, int iditem);

    @Transactional
    @Modifying
    @Query(value="DELETE pe FROM exhibitionimage pe JOIN itemimage p ON p.iditemimage= pe.iditemimage JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 AND p.iditemimage=?2", nativeQuery = true)
    void removeImageExhibitionRelation(int idexhibition, int iditemimage);

    @Transactional
    @Modifying
    @Query(value="DELETE pe FROM exhibitionvideo pe JOIN itemvideo p ON p.iditemvideo= pe.iditemvideo JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 AND p.iditemvideo=?2", nativeQuery = true)
    void removeVideoExhibitionRelation(int idexhibition, int iditemvideo);

    @Transactional
    @Modifying
    @Query(value="DELETE pe FROM exhibitionaudio pe JOIN itemaudio p ON p.iditemaudio= pe.iditemaudio JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 AND p.iditemaudio=?2", nativeQuery = true)
    void removeAudioExhibitionRelation(int idexhibition, int iditemaudio);

    @Transactional
    @Modifying
    @Query(value="DELETE pe FROM exhibitiontext pe JOIN itemtext p ON p.iditemtext= pe.iditemtext JOIN exhibition e ON pe.idexhibition=e.idexhibition WHERE pe.idexhibition=?1 AND p.iditemtext=?2", nativeQuery = true)
    void removeTextExhibitionRelation(int idexhibition, int iditemtext);

    @Query(value = "SELECT * FROM exhibition e WHERE e.idmuseum=?1 and e.active=true", nativeQuery = true)
    List<Exhibition> findAllActiveFromMuseum(int idmuseum);

    @Query(value = "SELECT * FROM exhibition e WHERE e.idmuseum=?1 and e.active=false", nativeQuery = true)
    List<Exhibition> findAllInactiveItemsFromMuseum(Integer idmuseum);

    @Query(value = "FROM Exhibition e WHERE " +
                   "(e.name like %:name% or e.name like %:convertedName%) " +
                   "and (e.author like %:author% or e.author like %:convertedAuthor%)")
    List<Exhibition> findExhibitionsByNameAndAuthor(@Param("name") String name, @Param("author") String author, @Param("convertedName") String convertedName, @Param("convertedAuthor") String convertedAuthor);

    @Query(value = "FROM Exhibition e WHERE (e.name like %:name% or e.name like %:convertedName%) and (e.author like %:author% or e.author like %:convertedAuthor%) and e.idexhibition in (SELECT pe.idexhibition FROM Itemtoexhibition pe WHERE pe.iditem in (SELECT p.iditem FROM Item p WHERE (p.name like %:itemName% or p.name like %:convertedItemName%) and (p.identificationcode like %:itemCode% or p.identificationcode like %:convertedItemCode%))) and e.idexhibition in (SELECT pe.idexhibition FROM Itemtoexhibition pe WHERE pe.iditem in (SELECT p.iditem FROM Item p WHERE p.museummap.idmuseummap in (SELECT mm.idmuseummap FROM Museummap mm WHERE (mm.name like %:roomName% or mm.name like %:convertedRoomName%))))")
    List<Exhibition> findExhibitionsByParams(@Param("name") String name, @Param("author") String author, @Param("itemName") String itemName, @Param("itemCode") String itemCode, @Param("roomName") String roomName, @Param("convertedName") String convertedName, @Param("convertedAuthor") String convertedAuthor, @Param("convertedItemName") String convertedItemName, @Param("convertedItemCode") String convertedItemCode, @Param("convertedRoomName") String convertedRoomName);
}
