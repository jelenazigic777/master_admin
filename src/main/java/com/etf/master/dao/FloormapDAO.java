package com.etf.master.dao;

import com.etf.master.models.Floormap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FloormapDAO extends JpaRepository<Floormap, Integer> {

    @Query(value = "SELECT * FROM floormap f WHERE f.idmuseummap=?1", nativeQuery = true)
    Floormap getFloormapByMuseummapId(Integer idmuseummap);
}
