package com.etf.master.dtos;

public class BeaconResponseDTO {

    private int idbeacon;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double radius;

    public int getIdbeacon() {
        return idbeacon;
    }

    public void setIdbeacon(final int idbeacon) {
        this.idbeacon = idbeacon;
    }

    public Double getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(final Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public Double getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(final Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(final Double radius) {
        this.radius = radius;
    }
}
