package com.etf.master.dtos;

import com.etf.master.models.Museummap;

public class MapResponseDTO {

    private Museummap floor;

    private Double xEndCoordinate;

    private Double yEndCoordinate;

    private String jsonItems;

    public Museummap getFloor() {
        return floor;
    }

    public void setFloor(final Museummap floor) {
        this.floor = floor;
    }

    public String getJsonItems() {
        return jsonItems;
    }

    public void setJsonItems(final String jsonItems) {
        this.jsonItems = jsonItems;
    }

    public Double getxEndCoordinate() {
        return xEndCoordinate;
    }

    public void setxEndCoordinate(final Double xEndCoordinate) {
        this.xEndCoordinate = xEndCoordinate;
    }

    public Double getyEndCoordinate() {
        return yEndCoordinate;
    }

    public void setyEndCoordinate(final Double yEndCoordinate) {
        this.yEndCoordinate = yEndCoordinate;
    }
}
