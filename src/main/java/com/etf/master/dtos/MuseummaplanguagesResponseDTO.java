package com.etf.master.dtos;

public class MuseummaplanguagesResponseDTO {

    private int idmuseummaplanguages;
    private String name;
    private String text;
    private String texturl;

    public int getIdmuseummaplanguages() {
        return idmuseummaplanguages;
    }

    public void setIdmuseummaplanguages(final int idmuseummaplanguages) {
        this.idmuseummaplanguages = idmuseummaplanguages;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getTexturl() {
        return texturl;
    }

    public void setTexturl(final String texturl) {
        this.texturl = texturl;
    }
}
