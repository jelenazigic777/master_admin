package com.etf.master.dtos;

public class MuseumlanguagesResponseDTO {

    private int idmuseumlanguages;
    private String name;
    private String text;
    private String texturl;

    public int getIdmuseumlanguages() {
        return idmuseumlanguages;
    }

    public void setIdmuseumlanguages(final int idmuseumlanguages) {
        this.idmuseumlanguages = idmuseumlanguages;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getTexturl() {
        return texturl;
    }

    public void setTexturl(final String texturl) {
        this.texturl = texturl;
    }
}
