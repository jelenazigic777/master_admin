package com.etf.master.dtos;

public class BeaconProviderResponseDTO {

    private int id;
    private String name;
    private String institution;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double zcoordinate;
    private Double radius;
    private Integer major;
    private Integer minor;
    private Integer power;
    //maybe add
    private Integer UUID;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Double getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(final Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public Double getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(final Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    public Double getZcoordinate() {
        return zcoordinate;
    }

    public void setZcoordinate(final Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(final Double radius) {
        this.radius = radius;
    }

    public Integer getMajor() {
        return major;
    }

    public void setMajor(final Integer major) {
        this.major = major;
    }

    public Integer getMinor() {
        return minor;
    }

    public void setMinor(final Integer minor) {
        this.minor = minor;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(final Integer power) {
        this.power = power;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(final String institution) {
        this.institution = institution;
    }
}
