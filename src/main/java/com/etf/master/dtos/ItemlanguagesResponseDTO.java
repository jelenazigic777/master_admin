package com.etf.master.dtos;

public class ItemlanguagesResponseDTO {

    private int iditemlanguages;
    private String name;
    private String author;
    private String text;
    private String texturl;

    public int getIditemlanguages() {
        return iditemlanguages;
    }

    public void setIditemlanguages(final int iditemlanguages) {
        this.iditemlanguages = iditemlanguages;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getTexturl() {
        return texturl;
    }

    public void setTexturl(final String texturl) {
        this.texturl = texturl;
    }
}
