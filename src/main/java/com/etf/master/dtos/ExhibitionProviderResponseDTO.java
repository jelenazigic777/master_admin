package com.etf.master.dtos;

import java.util.List;

public class ExhibitionProviderResponseDTO {

    private int id;
    private String name;
    private String author;
    private String institution;
    private String language;
    private List<String> texts;
    private List<String> imageUrls;
    private List<String> audioUrls;
    private List<String> videoUrls;
    private String imageUrl;
    private String audioUrl;
    private String videoUrl;
    private String text;
    private List<Integer> itemsIds;
    private List<Integer> museummapsIds;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(final String institution) {
        this.institution = institution;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public List<String> getTexts() {
        return texts;
    }

    public void setTexts(final List<String> texts) {
        this.texts = texts;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(final List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<String> getAudioUrls() {
        return audioUrls;
    }

    public void setAudioUrls(final List<String> audioUrls) {
        this.audioUrls = audioUrls;
    }

    public List<String> getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrls(final List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }

    public List<Integer> getItemsIds() {
        return itemsIds;
    }

    public void setItemsIds(final List<Integer> itemsIds) {
        this.itemsIds = itemsIds;
    }

    public List<Integer> getMuseummapsIds() {
        return museummapsIds;
    }

    public void setMuseummapsIds(final List<Integer> museummapsIds) {
        this.museummapsIds = museummapsIds;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(final String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(final String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }
}
