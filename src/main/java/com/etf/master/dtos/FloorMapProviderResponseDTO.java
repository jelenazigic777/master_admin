package com.etf.master.dtos;

public class FloorMapProviderResponseDTO {

    private int id;
    private String institution;
    private String language;
    private String name;
    private int floorNumber;
    private String text;
    private String textUrl;
    private Double xStart;
    private Double yStart;
    private Double xEnd;
    private Double yEnd;
    private Double zCoordinate;
    private String imageUrl;
    private String audioUrl;
    private String videoUrl;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(final String institution) {
        this.institution = institution;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public int getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(final int floorNumber) {
        this.floorNumber = floorNumber;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getTextUrl() {
        return textUrl;
    }

    public void setTextUrl(final String textUrl) {
        this.textUrl = textUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Double getxStart() {
        return xStart;
    }

    public void setxStart(final Double xStart) {
        this.xStart = xStart;
    }

    public Double getyStart() {
        return yStart;
    }

    public void setyStart(final Double yStart) {
        this.yStart = yStart;
    }

    public Double getxEnd() {
        return xEnd;
    }

    public void setxEnd(final Double xEnd) {
        this.xEnd = xEnd;
    }

    public Double getyEnd() {
        return yEnd;
    }

    public void setyEnd(final Double yEnd) {
        this.yEnd = yEnd;
    }

    public Double getzCoordinate() {
        return zCoordinate;
    }

    public void setzCoordinate(final Double zCoordinate) {
        this.zCoordinate = zCoordinate;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public void setAudioUrl(final String audioUrl) {
        this.audioUrl = audioUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(final String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
