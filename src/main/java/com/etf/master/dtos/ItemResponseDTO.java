package com.etf.master.dtos;

public class ItemResponseDTO {

    private int iditem;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double radius;
    private String imgUrl;

    public int getIditem() {
        return iditem;
    }

    public void setIditem(final int iditem) {
        this.iditem = iditem;
    }

    public Double getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(final Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public Double getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(final Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(final Double radius) {
        this.radius = radius;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(final String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
