package com.etf.master.dtos;

import java.util.List;

public class ItemProviderResponseDTO {

    private int id;
    private String name;
    private String institution;
    private String language;
    private String author;
    private List<String> texts;
    private List<String> imageUrls;
    private List<String> audioUrls;
    private List<String> videoUrls;
    private String barcodeUrl;
    private Double xCoordinate;
    private Double yCoordinate;
    private Double zCoordinate;
    private Double radius;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(final String institution) {
        this.institution = institution;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public List<String> getTexts() {
        return texts;
    }

    public void setTexts(final List<String> texts) {
        this.texts = texts;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(final List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<String> getAudioUrls() {
        return audioUrls;
    }

    public void setAudioUrls(final List<String> audioUrls) {
        this.audioUrls = audioUrls;
    }

    public List<String> getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrls(final List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }

    public String getBarcodeUrl() {
        return barcodeUrl;
    }

    public void setBarcodeUrl(final String barcodeUrl) {
        this.barcodeUrl = barcodeUrl;
    }

    public Double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(final Double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public Double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(final Double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public Double getzCoordinate() {
        return zCoordinate;
    }

    public void setzCoordinate(final Double zCoordinate) {
        this.zCoordinate = zCoordinate;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(final Double radius) {
        this.radius = radius;
    }
}
