package com.etf.master.dtos;

import java.util.List;

public class RoomProviderResponseDTO {

    private int id;
    private String name;
    private String institution;
    private String language;
    private String text;
    private String textUrl;
    private Double xStart;
    private Double yStart;
    private Double xEnd;
    private Double yEnd;
    private Double zCoordinate;
    private Integer floormapId;
    private List<Integer> itemsIds;
    private List<Integer> roomsIds;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(final String institution) {
        this.institution = institution;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getTextUrl() {
        return textUrl;
    }

    public void setTextUrl(final String textUrl) {
        this.textUrl = textUrl;
    }

    public Double getxStart() {
        return xStart;
    }

    public void setxStart(final Double xStart) {
        this.xStart = xStart;
    }

    public Double getyStart() {
        return yStart;
    }

    public void setyStart(final Double yStart) {
        this.yStart = yStart;
    }

    public Double getxEnd() {
        return xEnd;
    }

    public void setxEnd(final Double xEnd) {
        this.xEnd = xEnd;
    }

    public Double getyEnd() {
        return yEnd;
    }

    public void setyEnd(final Double yEnd) {
        this.yEnd = yEnd;
    }

    public Double getzCoordinate() {
        return zCoordinate;
    }

    public void setzCoordinate(final Double zCoordinate) {
        this.zCoordinate = zCoordinate;
    }

    public Integer getFloormapId() {
        return floormapId;
    }

    public void setFloormapId(final Integer floormapId) {
        this.floormapId = floormapId;
    }

    public List<Integer> getItemsIds() {
        return itemsIds;
    }

    public void setItemsIds(final List<Integer> itemsIds) {
        this.itemsIds = itemsIds;
    }

    public List<Integer> getRoomsIds() {
        return roomsIds;
    }

    public void setRoomsIds(final List<Integer> roomsIds) {
        this.roomsIds = roomsIds;
    }
}
