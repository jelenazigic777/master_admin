package com.etf.master.dtos;

import java.util.List;

public class MuseumProviderResponseDTO {

    private int id;
    private String name;
    private String institution;
    private String language;
    private String text;
    private String textUrl;
    private List<String> imageUrls;
    private List<String> audioUrls;
    private List<String> videoUrls;
    private Double xCoordinate;
    private Double yCoordinate;
    private Double zCoordinate;
    private String primaryClientImageUrl;
    private String secondaryClientImageUrl;

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(final String institution) {
        this.institution = institution;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public String getText() {
        return text;
    }

    public void setText(final String text) {
        this.text = text;
    }

    public String getTextUrl() {
        return textUrl;
    }

    public void setTextUrl(final String textUrl) {
        this.textUrl = textUrl;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(final List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public List<String> getAudioUrls() {
        return audioUrls;
    }

    public void setAudioUrls(final List<String> audioUrls) {
        this.audioUrls = audioUrls;
    }

    public List<String> getVideoUrls() {
        return videoUrls;
    }

    public void setVideoUrls(final List<String> videoUrls) {
        this.videoUrls = videoUrls;
    }

    public Double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(final Double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public Double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(final Double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public Double getzCoordinate() {
        return zCoordinate;
    }

    public void setzCoordinate(final Double zCoordinate) {
        this.zCoordinate = zCoordinate;
    }

    public String getPrimaryClientImageUrl() {
        return primaryClientImageUrl;
    }

    public void setPrimaryClientImageUrl(final String primaryClientImageUrl) {
        this.primaryClientImageUrl = primaryClientImageUrl;
    }

    public String getSecondaryClientImageUrl() {
        return secondaryClientImageUrl;
    }

    public void setSecondaryClientImageUrl(final String secondaryClientImageUrl) {
        this.secondaryClientImageUrl = secondaryClientImageUrl;
    }
}
