package com.etf.master.validators;

import com.etf.master.models.Museum;
import com.etf.master.persistance.MuseumPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.List;

@Component
public class InstitutionValidator implements Validator {

    @Autowired
    MuseumPersistence museumPersistence;

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "founder", "NotEmpty");
    }

    public boolean haveMuseums(int idinstitution) {
        List<Museum> museums = museumPersistence.findAllFromInstitution(idinstitution);
        return !museums.isEmpty();
    }
}
