package com.etf.master.validators;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.etf.master.models.Beacon;
import com.etf.master.models.Exhibition;
import com.etf.master.models.Item;
import com.etf.master.models.Museummap;
import com.etf.master.persistance.BeaconPersistence;
import com.etf.master.persistance.ExhibitionPersistence;
import com.etf.master.persistance.MuseumPersistence;
import com.etf.master.persistance.MuseummapPersistence;
import com.etf.master.persistance.ItemPersistence;

@Component
public class MuseumValidator implements Validator {

    @Autowired
    MuseumPersistence museumPersistence;

    @Autowired
    BeaconPersistence beaconPersistence;

    @Autowired
    ItemPersistence itemPersistence;

    @Autowired
    ExhibitionPersistence exhibitionPersistence;

    @Autowired
    MuseummapPersistence museummapPersistence;

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "institution", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "text", "NotEmpty");
    }

    public void validateLanguage(Object o, Errors errors) {

    }

    public boolean canBeErased(int idmuseum) {
        List<Beacon> beacons = beaconPersistence.findAllFromMuseum(idmuseum);
        List<Item> items = itemPersistence.findAllFromMuseum(idmuseum);
        List<Exhibition> exhibitions = exhibitionPersistence.findAllFromMuseum(idmuseum);
        List<Museummap> maps = museummapPersistence.findAllFromMuseum(idmuseum);
        boolean canBeErased = beacons.isEmpty() && items.isEmpty() && exhibitions.isEmpty() && maps.isEmpty();
        return canBeErased;
    }
}
