package com.etf.master.convertors;

public interface CyrillicLatinConverter {

    String convertWord(String word);
}
