package com.etf.master.convertors.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.etf.master.convertors.CyrillicLatinConverter;

@Service
public class CyrillicLatinConverterImpl implements CyrillicLatinConverter {

    private static Map<String, String> dictionary;

    static {
        Map<String, String> map = new HashMap<>();
        map.put("а", "a");
        map.put("б", "b");
        map.put("в", "v");
        map.put("г", "g");
        map.put("д", "d");
        map.put("ђ", "đ");
        map.put("е", "e");
        map.put("ж", "ž");
        map.put("з", "z");
        map.put("и", "i");
        map.put("ј", "ј");
        map.put("к", "k");
        map.put("л", "l");
        map.put("љ", "lј");
        map.put("м", "m");
        map.put("н", "n");
        map.put("њ", "nj");
        map.put("o", "о");
        map.put("о", "o");
        map.put("п", "p");
        map.put("р", "r");
        map.put("с", "s");
        map.put("т", "t");
        map.put("ћ", "ć");
        map.put("у", "u");
        map.put("ф", "f");
        map.put("х", "h");
        map.put("ц", "c");
        map.put("ч", "č");
        map.put("џ", "dž");
        map.put("ш", "š");
        map.put("А", "А");
        map.put("Б", "B");
        map.put("В", "V");
        map.put("Г", "G");
        map.put("Д", "D");
        map.put("Ђ", "Đ");
        map.put("Е", "Е");
        map.put("Ж", "Ž");
        map.put("З", "Z");
        map.put("И", "I");
        map.put("Ј", "Ј");
        map.put("К", "К");
        map.put("Л", "L");
        map.put("Љ", "Lј");
        map.put("М", "М");
        map.put("Н", "N");
        map.put("Њ", "Nj");
        map.put("О", "О");
        map.put("П", "P");
        map.put("Р", "R");
        map.put("С", "S");
        map.put("Т", "T");
        map.put("Ћ", "Ć");
        map.put("У", "U");
        map.put("Ф", "F");
        map.put("Х", "H");
        map.put("Ц", "C");
        map.put("Ч", "Č");
        map.put("Џ", "Dž");
        map.put("Ш", "Š");
        map.put(" ", " ");
        map.put(".", ".");
        map.put(",", ",");
        map.put("0", "0");
        map.put("1", "1");
        map.put("2", "2");
        map.put("3", "3");
        map.put("4", "4");
        map.put("5", "5");
        map.put("6", "6");
        map.put("7", "7");
        map.put("8", "8");
        map.put("9", "9");
        map.put("a", "а");
        map.put("b", "б");
        map.put("v", "в");
        map.put("g", "г");
        map.put("d", "д");
        map.put("đ", "ђ");
        map.put("e", "е");
        map.put("ž", "ж");
        map.put("z", "з");
        map.put("i", "и");
        map.put("j", "ј");
        map.put("k", "к");
        map.put("l", "л");
        map.put("lј", "љ");
        map.put("m", "м");
        map.put("n", "н");
        map.put("nj", "њ");
        map.put("о", "о");
        map.put("p", "п");
        map.put("r", "р");
        map.put("s", "с");
        map.put("t", "т");
        map.put("ć", "ћ");
        map.put("u", "у");
        map.put("f", "ф");
        map.put("h", "х");
        map.put("c", "ц");
        map.put("č", "ч");
        map.put("dž", "џ");
        map.put("š", "ш");
        map.put("А", "А");
        map.put("B", "Б");
        map.put("V", "В");
        map.put("G", "Г");
        map.put("D", "Д");
        map.put("Đ", "Ђ");
        map.put("Ž", "Ж");
        map.put("Z", "З");
        map.put("I", "И");
        map.put("L", "Л");
        map.put("Lj", "Љ");
        map.put("N", "Н");
        map.put("Nј", "Њ");
        map.put("P", "П");
        map.put("R", "Р");
        map.put("S", "С");
        map.put("T", "Т");
        map.put("Ć", "Ћ");
        map.put("U", "У");
        map.put("F", "Ф");
        map.put("H", "Х");
        map.put("C", "Ц");
        map.put("Č", "Ч");
        map.put("Dž", "Џ");
        map.put("Š", "Ш");
        dictionary = Collections.unmodifiableMap(map);
    }

    @Override
    public String convertWord(final String word) {
        if (!word.isEmpty()) {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < word.length(); i++) {
                if (dictionary.containsKey(String.valueOf(word.charAt(i)))) {
                    result.append(dictionary.get(String.valueOf(word.charAt(i))));
                }
            }
            return result.toString();
        }
        return "";
    }
}
