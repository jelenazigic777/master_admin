package com.etf.master.populators.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.etf.master.dtos.BeaconProviderResponseDTO;
import com.etf.master.dtos.ExhibitionProviderResponseDTO;
import com.etf.master.dtos.FloorMapProviderResponseDTO;
import com.etf.master.dtos.MuseumMapProviderResponseDTO;
import com.etf.master.dtos.MuseumProviderResponseDTO;
import com.etf.master.dtos.ItemProviderResponseDTO;
import com.etf.master.dtos.RoomProviderResponseDTO;
import com.etf.master.models.Beacon;
import com.etf.master.models.Exhibition;
import com.etf.master.models.Floormap;
import com.etf.master.models.Item;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Itemtext;
import com.etf.master.models.Museum;
import com.etf.master.models.Museummap;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemvideo;
import com.etf.master.models.Room;
import com.etf.master.populators.Populator;
import com.etf.master.services.ExhibitionService;
import com.etf.master.services.MainService;
import com.etf.master.services.MapService;

@Service
public class PopulatorImpl implements Populator {

    @Autowired
    ExhibitionService exhibitionService;

    @Autowired
    MapService mapService;

    @Autowired
    MainService mainService;

    @Override
    public MuseumProviderResponseDTO populateMuseum(final Museum museum) {
        MuseumProviderResponseDTO museumDTO = new MuseumProviderResponseDTO();
        museumDTO.setId(museum.getIdmuseum());
        museumDTO.setName(museum.getName());
        museumDTO.setLanguage(museum.getLanguage());
        museumDTO.setText(museum.getText());
        museumDTO.setTextUrl(museum.getTexturl());
        museumDTO.setInstitution(museum.getInstitutionObj().getName());
        List<String> imageUrls = findImageUrls(museum.getImages());
        List<String> audioUrls = findAudioUrls(museum.getAudios());
        List<String> videoUrls = findVideoUrls(museum.getVideos());
        museumDTO.setImageUrls(imageUrls);
        museumDTO.setAudioUrls(audioUrls);
        museumDTO.setVideoUrls(videoUrls);
        Itemimage primaryImage = mainService.findPrimaryClientImageForMuseumId(museum.getIdmuseum());
        if (primaryImage != null) {
            museumDTO.setPrimaryClientImageUrl(primaryImage.getImageurl());
        }
        Itemimage secondaryImage = mainService.findSecondaryClientImageForMuseumId(museum.getIdmuseum());
        if (secondaryImage != null) {
            museumDTO.setSecondaryClientImageUrl(secondaryImage.getImageurl());
        }
        return museumDTO;
    }

    @Override
    public List<ItemProviderResponseDTO> populateItems(final List<Item> items) {
        List<ItemProviderResponseDTO> responseList = new ArrayList<>();
        for (Item p : items) {
            responseList.add(populateItem(p));
        }
        return responseList;
    }

    @Override
    public List<FloorMapProviderResponseDTO> populateFloormaps(final List<Floormap> floormaps) {
        List<FloorMapProviderResponseDTO> responseList = new ArrayList<>();
        for (Floormap f : floormaps) {
            responseList.add(populateFloormap(f));
        }
        return responseList;
    }

    @Override
    public List<MuseumMapProviderResponseDTO> populateMuseummaps(final List<Museummap> museummaps) {
        List<MuseumMapProviderResponseDTO> responseList = new ArrayList<>();
        for (Museummap m : museummaps) {
            responseList.add(populateMuseummap(m));
        }
        return responseList;
    }

    @Override
    public List<RoomProviderResponseDTO> populateRooms(final List<Room> rooms) {
        List<RoomProviderResponseDTO> responseList = new ArrayList<>();
        for (Room r : rooms) {
            responseList.add(populateRoom(r));
        }
        return responseList;
    }

    @Override
    public List<ExhibitionProviderResponseDTO> populateExhibitions(final List<Exhibition> exhibitions) {
        List<ExhibitionProviderResponseDTO> responseList = new ArrayList<>();
        for (Exhibition e : exhibitions) {
            responseList.add(populateExhibition(e));
        }
        return responseList;
    }

    @Override
    public List<BeaconProviderResponseDTO> populateBeacons(final List<Beacon> beacons) {
        List<BeaconProviderResponseDTO> responseList = new ArrayList<>();
        for (Beacon b : beacons) {
            responseList.add(populateBeacon(b));
        }
        return responseList;
    }

    private ItemProviderResponseDTO populateItem(Item p) {
        ItemProviderResponseDTO response = new ItemProviderResponseDTO();
        response.setId(p.getIditem());
        response.setName(p.getName());
        response.setInstitution(p.getMuseum().getInstitutionObj().getName());
        response.setLanguage(p.getLanguage());
        response.setAuthor(p.getAuthor());
        if (p.getBarcodeimage() != null) {
            response.setBarcodeUrl(p.getBarcodeimage().getImageurl());
        }
        Museummap map = mapService.findFloorByRoomFloorNumber(p.getMuseum().getIdmuseum(), p.getMuseummap().getFloormap().getFloornumber());
        response.setxCoordinate(p.getXcoordinate());
        response.setyCoordinate(map.getYend() - p.getYcoordinate());
        response.setzCoordinate(p.getZcoordinate());
        response.setRadius(p.getRadius());
        response.setImageUrls(findImageUrls(p.getImages()));
        response.setAudioUrls(findAudioUrls(p.getAudios()));
        response.setVideoUrls(findVideoUrls(p.getVideos()));
        response.setTexts(findTexts(p.getTexts()));
        return response;
    }

    private BeaconProviderResponseDTO populateBeacon(Beacon b) {
        BeaconProviderResponseDTO response = new BeaconProviderResponseDTO();
        response.setId(b.getIdbeacon());
        response.setMajor(b.getMajor());
        response.setInstitution(b.getMuseum().getInstitutionObj().getName());
        response.setMinor(b.getMinor());
        response.setName(b.getName());
        response.setPower(b.getPower());
        response.setRadius(b.getRadius());
        response.setXcoordinate(b.getXcoordinate());
        response.setYcoordinate(b.getYcoordinate());
        response.setZcoordinate(b.getZcoordinate());
        return response;
    }

    private FloorMapProviderResponseDTO populateFloormap(Floormap f) {
        FloorMapProviderResponseDTO response = new FloorMapProviderResponseDTO();
        response.setId(f.getIdfloormap());
        response.setFloorNumber(f.getFloornumber());
        response.setInstitution(f.getMuseum().getInstitutionObj().getName());
        response.setLanguage(f.getMuseum().getLanguage());
        response.setName(f.getName());
        //check how to set this!
        response.setImageUrl(f.getImageurl());
        //response.setAudioUrl(f.getAudiourl());
        //response.setVideoUrl(f.getVideourl());

        Museummap map = mapService.findFloorByRoomFloorNumber(f.getMuseum().getIdmuseum(), f.getFloornumber());
        response.setxStart(map.getXstart());
        response.setyStart(map.getYstart());
        response.setxEnd(map.getXend());
        response.setyEnd(map.getYend());
        response.setzCoordinate(f.getZcoordinate());
        response.setText(f.getText());
        response.setTextUrl(f.getTexturl());
        return response;
    }

    private MuseumMapProviderResponseDTO populateMuseummap(Museummap m) {
        MuseumMapProviderResponseDTO response = new MuseumMapProviderResponseDTO();
        response.setId(m.getIdmuseummap());
        response.setInstitution(m.getInstitution());
        response.setLanguage(m.getLanguage());
        response.setFloor(m.getIsfloor());
        if (m.getIsstaircase() != null) {
            response.setStaircase(m.getIsstaircase());
        } else {
            response.setStaircase(false);
        }
        Museummap map = mapService.findFloorByRoomFloorNumber(m.getMuseum().getIdmuseum(), m.getFloormap().getFloornumber());
        response.setxStart(map.getXstart());
        response.setyStart(map.getYstart());
        response.setxEnd(map.getXend());
        response.setyEnd(map.getYend());
        response.setzCoordinate(m.getZcoordinate());
        response.setImageUrls(findImageUrls(m.getImages()));
        response.setAudioUrls(findAudioUrls(m.getAudios()));
        response.setVideoUrls(findVideoUrls(m.getVideos()));
        response.setText(m.getText());
        response.setTextUrl(m.getTexturl());
        //check this
        response.setItemsIds(populateItemIds(m.getItems()));
        response.setBeaconIds(populateBeaconIds(m.getBeacons()));
        response.setFloormapId(m.getFloormap().getIdfloormap());
        return response;
    }

    private List<Integer> populateItemIds(List<Item> items) {
        List<Integer> ids = new ArrayList<>();
        for (Item p : items) {
            ids.add(p.getIditem());
        }
        return ids;
    }

    private List<Integer> populateBeaconIds(List<Beacon> beacons) {
        List<Integer> ids = new ArrayList<>();
        for (Beacon b : beacons) {
            ids.add(b.getIdbeacon());
        }
        return ids;
    }

    private RoomProviderResponseDTO populateRoom(Room r) {
        RoomProviderResponseDTO response = new RoomProviderResponseDTO();
        response.setId(r.getIdroom());
        response.setInstitution(r.getMuseum().getInstitutionObj().getName());
        response.setName(r.getName());
        response.setLanguage(r.getMuseum().getLanguage());
        response.setText(r.getText());
        response.setTextUrl(r.getTexturl());
        response.setxStart(r.getFloormap().getXstart());
        response.setyStart(r.getFloormap().getYstart());
        response.setxEnd(r.getFloormap().getXend());
        response.setyEnd(r.getFloormap().getYend());
        response.setzCoordinate(r.getFloormap().getZcoordinate());
        //maybe images videos and audios - check
        response.setItemsIds(populateItemIds(r.getFloormap().getMuseummap().getItems()));
        response.setRoomsIds(populateRoomsIds(r.getRooms()));
        response.setFloormapId(r.getFloormap().getIdfloormap());
        return response;
    }

    private List<Integer> populateRoomsIds(List<Room> rooms) {
        List<Integer> ids = new ArrayList<>();
        for (Room r : rooms) {
            ids.add(r.getIdroom());
        }
        return ids;
    }

    private ExhibitionProviderResponseDTO populateExhibition(Exhibition e) {
        ExhibitionProviderResponseDTO response = new ExhibitionProviderResponseDTO();
        response.setId(e.getIdexhibition());
        response.setInstitution(e.getMuseum().getInstitutionObj().getName());
        response.setLanguage(e.getLanguage());
        response.setAuthor(e.getAuthor());
        response.setName(e.getName());
        response.setImageUrls(findImageUrls(e.getImages()));
        response.setAudioUrls(findAudioUrls(e.getAudios()));
        response.setVideoUrls(findVideoUrls(e.getVideos()));
        response.setTexts(findTexts(e.getTexts()));
        if (e.getImage() != null) {
            response.setImageUrl(e.getImage().getImageurl());
        }
        if (e.getAudio() != null) {
            response.setAudioUrl(e.getAudio().getAudiourl());
        }
        if (e.getVideo() != null) {
            response.setVideoUrl(e.getVideo().getVideourl());
        }
        response.setText(e.getText());
        response.setItemsIds(populateItemIds(e.getItems()));
        response.setMuseummapsIds(populateMuseummapIds(exhibitionService.findMuseummapsByExhibitionId(e.getIdexhibition())));
        return response;
    }

    private List<Integer> populateMuseummapIds(List<Museummap> museummaps) {
        List<Integer> ids = new ArrayList<>();
        for (Museummap m : museummaps) {
            ids.add(m.getIdmuseummap());
        }
        return ids;
    }

    private List<String> findImageUrls(List<Itemimage> images) {
        List<String> imageUrls = new ArrayList<>();
        for (Itemimage i : images) {
            imageUrls.add(i.getImageurl());
        }
        return imageUrls;
    }

    private List<String> findAudioUrls(List<Itemaudio> audios) {
        List<String> audioUrls = new ArrayList<>();
        for (Itemaudio a : audios) {
            audioUrls.add(a.getAudiourl());
        }
        return audioUrls;
    }

    private List<String> findVideoUrls(List<Itemvideo> videos) {
        List<String> videoUrls = new ArrayList<>();
        for (Itemvideo v : videos) {
            videoUrls.add(v.getVideourl());
        }
        return videoUrls;
    }

    private List<String> findTexts(List<Itemtext> texts) {
        List<String> responseTexts = new ArrayList<>();
        for (Itemtext t : texts) {
            responseTexts.add(t.getText());
        }
        return responseTexts;
    }
}
