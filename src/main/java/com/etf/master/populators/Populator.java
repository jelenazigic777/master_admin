package com.etf.master.populators;

import java.util.List;

import com.etf.master.dtos.BeaconProviderResponseDTO;
import com.etf.master.dtos.ExhibitionProviderResponseDTO;
import com.etf.master.dtos.FloorMapProviderResponseDTO;
import com.etf.master.dtos.MuseumMapProviderResponseDTO;
import com.etf.master.dtos.MuseumProviderResponseDTO;
import com.etf.master.dtos.ItemProviderResponseDTO;
import com.etf.master.dtos.RoomProviderResponseDTO;
import com.etf.master.models.Beacon;
import com.etf.master.models.Exhibition;
import com.etf.master.models.Floormap;
import com.etf.master.models.Museum;
import com.etf.master.models.Museummap;
import com.etf.master.models.Item;
import com.etf.master.models.Room;

public interface Populator {

    MuseumProviderResponseDTO populateMuseum(Museum museum);

    List<ItemProviderResponseDTO> populateItems(List<Item> items);

    List<FloorMapProviderResponseDTO> populateFloormaps(List<Floormap> floormaps);

    List<MuseumMapProviderResponseDTO> populateMuseummaps(List<Museummap> museummaps);

    List<RoomProviderResponseDTO> populateRooms(List<Room> rooms);

    List<ExhibitionProviderResponseDTO> populateExhibitions(List<Exhibition> exhibitions);

    List<BeaconProviderResponseDTO> populateBeacons(List<Beacon> beacons);
}
