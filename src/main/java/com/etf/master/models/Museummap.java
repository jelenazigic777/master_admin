package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Museummap {
    private int idmuseummap;
    private String institution;
    private String language;
    private String name;
    private String text;
    private String texturl;
    private String texturllocal;
    private String room;
    private Double radius;
    private Museum museum;
    private Floormap floormap;
    private List<Item> items;
    private List<Exhibition> exhibitions;
    private String imageurl;
    private String imageurllocal;
    private List<Beacon> beacons;
    private Boolean isfloor;
    private List<Itemimage> images;
    private List<Itemaudio> audios;
    private List<Itemvideo> videos;
    private String audiourl;
    private String audiourllocal;
    private String videourl;
    private String videourllocal;
    private Boolean isstaircase;
    private Double xstart;
    private Double ystart;
    private Double xend;
    private Double yend;
    private Double zcoordinate;
    private Ambientaudio ambientaudio;
    private List<Itemvideolanguages> itemvideolanguages;
    private List<Itemaudiolanguages> itemaudiolanguages;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Museummap museummap = (Museummap) o;
        return idmuseummap == museummap.idmuseummap &&
                Objects.equals(institution, museummap.institution) &&
                Objects.equals(language, museummap.language) &&
                Objects.equals(name, museummap.name) &&
                Objects.equals(text, museummap.text) &&
                Objects.equals(texturl, museummap.texturl) &&
                Objects.equals(texturllocal, museummap.texturllocal) &&
                Objects.equals(room, museummap.room) &&
                Objects.equals(zcoordinate, museummap.zcoordinate) &&
                Objects.equals(xstart, museummap.xstart) &&
                Objects.equals(ystart, museummap.ystart) &&
                Objects.equals(xend, museummap.xend) &&
                Objects.equals(yend, museummap.yend) &&
                Objects.equals(radius, museummap.radius);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idmuseummap, institution, language, name, text, texturl, texturllocal, room, xstart, ystart, xend, yend, zcoordinate, radius);
    }

    @Id
    @Column(name = "idmuseummap")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int getIdmuseummap() {
        return idmuseummap;
    }

    @Basic
    @Column(name = "institution")
    public String getInstitution() {
        return institution;
    }

    @Basic
    @Column(name = "language")
    public String getLanguage() {
        return language;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    @Basic
    @Column(name = "texturllocal")
    public String getTexturllocal() {
        return texturllocal;
    }

    @Basic
    @Column(name = "room")
    public String getRoom() {
        return room;
    }

    @Basic
    @Column(name = "zcoordinate")
    public Double getZcoordinate() {
        return zcoordinate;
    }

    @Basic
    @Column(name = "radius")
    public Double getRadius() {
        return radius;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    @OneToOne(mappedBy = "museummap", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap", nullable = false)
    public Floormap getFloormap() {
        return floormap;
    }

    @OneToMany(mappedBy = "museummap", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Item> getItems() {
        return items;
    }

    @ManyToMany(mappedBy = "museummaps", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Exhibition> getExhibitions() {
        return exhibitions;
    }

    @Basic
    @Column(name = "imageurl")
    public String getImageurl() {
        return imageurl;
    }

    @Basic
    @Column(name = "imageurllocal")
    public String getImageurllocal() {
        return imageurllocal;
    }

    @OneToMany(mappedBy = "museummap")
    public List<Beacon> getBeacons() {
        return beacons;
    }

    @Basic
    @Column(name = "isfloor")
    public Boolean getIsfloor() {
        return isfloor;
    }

    @OneToMany(mappedBy = "museummap", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemimage> getImages() {
        return images;
    }

    @OneToMany(mappedBy = "museuumap", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemaudio> getAudios() {
        return audios;
    }

    @OneToMany(mappedBy = "museummap", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemvideo> getVideos() {
        return videos;
    }

    @Basic
    @Column(name = "audiourl")
    public String getAudiourl() {
        return audiourl;
    }

    @Basic
    @Column(name = "audiourllocal")
    public String getAudiourllocal() {
        return audiourllocal;
    }

    @Basic
    @Column(name = "videourl")
    public String getVideourl() {
        return videourl;
    }

    @Basic
    @Column(name = "videourllocal")
    public String getVideourllocal() {
        return videourllocal;
    }

    @Basic
    @Column(name = "isstaircase")
    public Boolean getIsstaircase() {
        return isstaircase;
    }

    @Basic
    @Column(name = "xstart")
    public Double getXstart() {
        return xstart;
    }

    @Basic
    @Column(name = "ystart")
    public Double getYstart() {
        return ystart;
    }

    @Basic
    @Column(name = "xend")
    public Double getXend() {
        return xend;
    }

    @Basic
    @Column(name = "yend")
    public Double getYend() {
        return yend;
    }

    @OneToOne(mappedBy = "floor")
    public Ambientaudio getAmbientaudio() {
        return ambientaudio;
    }

    @OneToMany(mappedBy = "museummap")
    public List<Itemvideolanguages> getItemvideolanguages() {
        return itemvideolanguages;
    }

    @OneToMany(mappedBy = "museummap")
    public List<Itemaudiolanguages> getItemaudiolanguages() {
        return itemaudiolanguages;
    }

    public void setIdmuseummap(int idmuseummap) {
        this.idmuseummap = idmuseummap;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    public void setTexturllocal(String texturllocal) {
        this.texturllocal = texturllocal;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setZcoordinate(Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    public void setFloormap(Floormap floormap) {
        this.floormap = floormap;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void setExhibitions(List<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public void setImageurllocal(String imageurllocal) {
        this.imageurllocal = imageurllocal;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    public void setIsfloor(Boolean isfloor) {
        this.isfloor = isfloor;
    }

    public void setImages(List<Itemimage> images) {
        this.images = images;
    }

    public void setAudios(List<Itemaudio> audios) {
        this.audios = audios;
    }

    public void setVideos(List<Itemvideo> videos) {
        this.videos = videos;
    }

    public void setAudiourl(String audiourl) {
        this.audiourl = audiourl;
    }

    public void setAudiourllocal(String audiourllocal) {
        this.audiourllocal = audiourllocal;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    public void setVideourllocal(String videourllocal) {
        this.videourllocal = videourllocal;
    }

    public void setIsstaircase(Boolean isstaircase) {
        this.isstaircase = isstaircase;
    }

    public void setXstart(final Double xstart) {
        this.xstart = xstart;
    }

    public void setYstart(final Double ystart) {
        this.ystart = ystart;
    }

    public void setXend(final Double xend) {
        this.xend = xend;
    }

    public void setYend(final Double yend) {
        this.yend = yend;
    }

    public void setAmbientaudio(final Ambientaudio ambientaudio) {
        this.ambientaudio = ambientaudio;
    }

    public void setItemvideolanguages(final List<Itemvideolanguages> itemvideolanguages) {
        this.itemvideolanguages = itemvideolanguages;
    }

    public void setItemaudiolanguages(final List<Itemaudiolanguages> itemaudiolanguages) {
        this.itemaudiolanguages = itemaudiolanguages;
    }
}
