package com.etf.master.models;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Barcodeimage {
    private Item item;
    private int idbarcodeimage;
    private String imageurl;
    private String serverid;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Barcodeimage that = (Barcodeimage)o;
        return idbarcodeimage == that.idbarcodeimage &&
               Objects.equals(imageurl, that.imageurl) &&
               Objects.equals(serverid, that.serverid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idbarcodeimage, imageurl, serverid);
    }

    @OneToOne
    @JoinColumn(name = "iditem", referencedColumnName = "iditem")
    public Item getItem() {
        return item;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "idbarcodeimage")
    public int getIdbarcodeimage() {
        return idbarcodeimage;
    }

    @Basic
    @Column(name = "imageurl")
    public String getImageurl() {
        return imageurl;
    }

    @Basic
    @Column(name = "serverid")
    public String getServerid() {
        return serverid;
    }

    public void setItem(final Item item) {
        this.item = item;
    }

    public void setIdbarcodeimage(final int idbarcodeimage) {
        this.idbarcodeimage = idbarcodeimage;
    }

    public void setImageurl(final String imageurl) {
        this.imageurl = imageurl;
    }

    public void setServerid(final String serverid) {
        this.serverid = serverid;
    }
}
