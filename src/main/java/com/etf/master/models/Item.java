package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "item")
public class Item {
    private int iditem;
    private String institution;
    private String language;
    private String name;
    private String author;
    private String text;
    private String texturl;
    private String texturllocal;
    private String imageurl;
    private String imageurllocal;
    private String audiourl;
    private String audiourllocal;
    private String videourl;
    private String videourllocal;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double zcoordinate;
    private Double radius;
    private String qr;
    private String additionalimagesurl;
    private String additionalimagesurllocal;
    private Museum museum;
    private Museummap museummap;
    private Room room;
    private List<Exhibition> exhibitions;
    private Integer audiotype;
    private List<Itemimage> images;
    private List<Itemaudio> audios;
    private List<Itemtext> texts;
    private List<Itemvideo> videos;
    private String identificationcode;
    private Barcodeimage barcodeimage;
    private List<Itemaudiolanguages> itemaudiolanguages;
    private List<Itemvideolanguages> itemvideolanguages;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return iditem == item.iditem &&
               Objects.equals(institution, item.institution) &&
               Objects.equals(language, item.language) &&
               Objects.equals(name, item.name) &&
               Objects.equals(author, item.author) &&
               Objects.equals(text, item.text) &&
               Objects.equals(texturl, item.texturl) &&
               Objects.equals(texturllocal, item.texturllocal) &&
               Objects.equals(imageurl, item.imageurl) &&
               Objects.equals(imageurllocal, item.imageurllocal) &&
               Objects.equals(audiourl, item.audiourl) &&
               Objects.equals(audiourllocal, item.audiourllocal) &&
               Objects.equals(videourl, item.videourl) &&
               Objects.equals(videourllocal, item.videourllocal) &&
               Objects.equals(xcoordinate, item.xcoordinate) &&
               Objects.equals(ycoordinate, item.ycoordinate) &&
               Objects.equals(zcoordinate, item.zcoordinate) &&
               Objects.equals(radius, item.radius) &&
               Objects.equals(qr, item.qr) &&
               Objects.equals(additionalimagesurl, item.additionalimagesurl) &&
               Objects.equals(additionalimagesurllocal, item.additionalimagesurllocal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iditem, institution, language, name, author, text, texturl, texturllocal, imageurl, imageurllocal, audiourl, audiourllocal, videourl, videourllocal, xcoordinate, ycoordinate, zcoordinate, radius, qr, additionalimagesurl, additionalimagesurllocal);
    }

    @Id
    @Column(name= "iditem")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getIditem() {
        return iditem;
    }

    @Basic
    @Column(name = "institution")
    public String getInstitution() {
        return institution;
    }

    @Basic
    @Column(name = "language")
    public String getLanguage() {
        return language;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    @Basic
    @Column(name = "texturllocal")
    public String getTexturllocal() {
        return texturllocal;
    }

    @Basic
    @Column(name= "imageurl")
    public String getImageurl() {
        return imageurl;
    }

    @Basic
    @Column(name= "imageurllocal")
    public String getImageurllocal() {
        return imageurllocal;
    }

    @Basic
    @Column(name = "audiourl")
    public String getAudiourl() {
        return audiourl;
    }

    @Basic
    @Column(name = "audiourllocal")
    public String getAudiourllocal() {
        return audiourllocal;
    }

    @Basic
    @Column(name = "videourl")
    public String getVideourl() {
        return videourl;
    }

    @Basic
    @Column(name = "videourllocal")
    public String getVideourllocal() {
        return videourllocal;
    }

    @Basic
    @Column(name = "xcoordinate")
    public Double getXcoordinate() {
        return xcoordinate;
    }

    @Basic
    @Column(name = "ycoordinate")
    public Double getYcoordinate() {
        return ycoordinate;
    }

    @Basic
    @Column(name = "zcoordinate")
    public Double getZcoordinate() {
        return zcoordinate;
    }

    @Basic
    @Column(name = "radius")
    public Double getRadius() {
        return radius;
    }

    @Basic
    @Column(name = "qr")
    public String getQr() {
        return qr;
    }

    @Basic
    @Column(name = "additionalimagesurl")
    public String getAdditionalimagesurl() {
        return additionalimagesurl;
    }

    @Basic
    @Column(name = "additionalimagesurllocal")
    public String getAdditionalimagesurllocal() {
        return additionalimagesurllocal;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap")
    public Museummap getMuseummap() {
        return museummap;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "idroom", referencedColumnName = "idroom")
    public Room getRoom() {
        return room;
    }

    @ManyToMany(mappedBy = "items", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Exhibition> getExhibitions() {
        return exhibitions;
    }

    @Basic
    @Column(name = "audiotype")
    public Integer getAudiotype() {
        return audiotype;
    }

    @OneToMany(mappedBy = "item", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemimage> getImages() {
        return images;
    }

    @OneToMany(mappedBy = "item", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemaudio> getAudios() {
        return audios;
    }

    @OneToMany(mappedBy = "item", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemtext> getTexts() {
        return texts;
    }

    @OneToMany(mappedBy = "item", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    public List<Itemvideo> getVideos() {
        return videos;
    }

    @Basic
    @Column(name = "identificationcode")
    public String getIdentificationcode() {
        return identificationcode;
    }

    @OneToOne(mappedBy = "item", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public Barcodeimage getBarcodeimage() {
        return barcodeimage;
    }

    @OneToMany(mappedBy = "item")
    public List<Itemaudiolanguages> getItemaudiolanguages() {
        return itemaudiolanguages;
    }

    @OneToMany(mappedBy = "item")
    public List<Itemvideolanguages> getItemvideolanguages() {
        return itemvideolanguages;
    }

    public void setIditem(int iditem) {
        this.iditem = iditem;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    public void setTexturllocal(String texturllocal) {
        this.texturllocal = texturllocal;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public void setImageurllocal(String imageurllocal) {
        this.imageurllocal = imageurllocal;
    }

    public void setAudiourl(String audiourl) {
        this.audiourl = audiourl;
    }

    public void setAudiourllocal(String audiourllocal) {
        this.audiourllocal = audiourllocal;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    public void setVideourllocal(String videourllocal) {
        this.videourllocal = videourllocal;
    }

    public void setXcoordinate(Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public void setYcoordinate(Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    public void setZcoordinate(Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public void setAdditionalimagesurl(String additionalimagesurl) {
        this.additionalimagesurl = additionalimagesurl;
    }

    public void setAdditionalimagesurllocal(String additionalimagesurllocal) {
        this.additionalimagesurllocal = additionalimagesurllocal;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    public void setMuseummap(Museummap museummap) {
        this.museummap = museummap;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setExhibitions(List<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }

    public void setAudiotype(Integer audiotype) {
        this.audiotype = audiotype;
    }

    public void setImages(List<Itemimage> images) {
        this.images = images;
    }

    public void setAudios(List<Itemaudio> audios) {
        this.audios = audios;
    }

    public void setTexts(List<Itemtext> texts) {
        this.texts = texts;
    }

    public void setVideos(List<Itemvideo> videos) {
        this.videos = videos;
    }

    public void setIdentificationcode(final String identificationcode) {
        this.identificationcode = identificationcode;
    }

    public void setBarcodeimage(final Barcodeimage barcodeimage) {
        this.barcodeimage = barcodeimage;
    }

    public void setItemaudiolanguages(final List<Itemaudiolanguages> itemaudiolanguages) {
        this.itemaudiolanguages = itemaudiolanguages;
    }

    public void setItemvideolanguages(final List<Itemvideolanguages> itemvideolanguages) {
        this.itemvideolanguages = itemvideolanguages;
    }
}
