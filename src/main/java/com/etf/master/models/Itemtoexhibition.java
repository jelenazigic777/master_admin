package com.etf.master.models;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Itemtoexhibition {
    private int iditemtoexhibition;
    private int iditem;
    private int idexhibition;

    @Id
    @Column(name = "iditemtoexhibition")
    public int getIditemtoexhibition() {
        return iditemtoexhibition;
    }

    public void setIditemtoexhibition(int iditemtoexhibition) {
        this.iditemtoexhibition = iditemtoexhibition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Itemtoexhibition that = (Itemtoexhibition) o;
        return iditemtoexhibition == that.iditemtoexhibition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(iditemtoexhibition);
    }

    @Basic
    @Column(name = "iditem")
    public int getIditem() {
        return iditem;
    }

    public void setIditem(int iditem) {
        this.iditem = iditem;
    }

    @Basic
    @Column(name = "idexhibition")
    public int getIdexhibition() {
        return idexhibition;
    }

    public void setIdexhibition(int idexhibition) {
        this.idexhibition = idexhibition;
    }

}
