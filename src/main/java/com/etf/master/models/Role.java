package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Role {
    private List<User> users;
    private int idrole;
    private String name;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Role role = (Role)o;
        return idrole == role.idrole &&
               Objects.equals(name, role.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idrole, name);
    }

    @ManyToMany(mappedBy = "roles")
    public List<User> getUsers() {
        return users;
    }

    @Id
    @Column(name = "idrole")
    public int getIdrole() {
        return idrole;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setUsers(final List<User> users) {
        this.users = users;
    }

    public void setIdrole(final int idrole) {
        this.idrole = idrole;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
