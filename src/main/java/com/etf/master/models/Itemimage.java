package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Itemimage {
    private int iditemimage;
    private String imageurl;
    private Boolean islocal;
    private Item item;
    private String serverid;
    private List<Exhibition> exhibitions;
    private Exhibition exhibition;
    private Museum museum;
    private Museummap museummap;
    private Integer clientImagePriority;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Itemimage that = (Itemimage) o;
        return iditemimage == that.iditemimage &&
               Objects.equals(imageurl, that.imageurl) &&
               Objects.equals(islocal, that.islocal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iditemimage, imageurl, islocal);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "iditemimage")
    public int getIditemimage() {
        return iditemimage;
    }

    @Basic
    @Column(name = "imageurl")
    public String getImageurl() {
        return imageurl;
    }

    @Basic
    @Column(name = "islocal")
    public Boolean getIslocal() {
        return islocal;
    }

    @ManyToOne
    @JoinColumn(name = "iditem", referencedColumnName = "iditem")
    public Item getItem() {
        return item;
    }

    @Basic
    @Column(name = "serverid")
    public String getServerid() {
        return serverid;
    }

    @ManyToMany(mappedBy = "images", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Exhibition> getExhibitions() {
        return exhibitions;
    }

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition")
    public Exhibition getExhibition() {
        return exhibition;
    }

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap")
    public Museummap getMuseummap() {
        return museummap;
    }

    @Basic
    @Column(name = "clientImagePriority")
    public Integer getClientImagePriority() {
        return clientImagePriority;
    }

    public void setIditemimage(int iditemimage) {
        this.iditemimage = iditemimage;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public void setIslocal(Boolean islocal) {
        this.islocal = islocal;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public void setExhibitions(List<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }

    public void setExhibition(Exhibition exhibition) {
        this.exhibition = exhibition;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    public void setMuseummap(Museummap museummap) {
        this.museummap = museummap;
    }

    public void setClientImagePriority(final Integer clientImagePriority) {
        this.clientImagePriority = clientImagePriority;
    }
}
