package com.etf.master.models;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Institution {
    private int idinstitution;
    private String name;
    private String address;
    private String township;
    private String town;
    private String district;
    private String founder;
    private Date foundationdate;
    private Date startdate;
    private Integer idnumber;
    private Integer registrationnumber;
    private Integer taxid;
    private String contactphones;
    private String emails;
    private String webaddresses;
    private List<Museum> museums;

    @Id
    @Column(name = "idinstitution")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getIdinstitution() {
        return idinstitution;
    }

    public void setIdinstitution(int idinstitution) {
        this.idinstitution = idinstitution;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "township")
    public String getTownship() {
        return township;
    }

    public void setTownship(String township) {
        this.township = township;
    }

    @Basic
    @Column(name = "town")
    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    @Basic
    @Column(name = "district")
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Basic
    @Column(name = "founder")
    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    @Basic
    @Column(name = "foundationdate")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getFoundationdate() {
        return foundationdate;
    }

    public void setFoundationdate(Date foundationdate) {
        this.foundationdate = foundationdate;
    }

    @Basic
    @Column(name = "startdate")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    @Basic
    @Column(name = "idnumber")
    public Integer getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(Integer idnumber) {
        this.idnumber = idnumber;
    }

    @Basic
    @Column(name = "registrationnumber")
    public Integer getRegistrationnumber() {
        return registrationnumber;
    }

    public void setRegistrationnumber(Integer registrationnumber) {
        this.registrationnumber = registrationnumber;
    }

    @Basic
    @Column(name = "taxid")
    public Integer getTaxid() {
        return taxid;
    }

    public void setTaxid(Integer taxid) {
        this.taxid = taxid;
    }

    @Basic
    @Column(name = "contactphones")
    public String getContactphones() {
        return contactphones;
    }

    public void setContactphones(String contactphones) {
        this.contactphones = contactphones;
    }

    @Basic
    @Column(name = "emails")
    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    @Basic
    @Column(name = "webaddresses")
    public String getWebaddresses() {
        return webaddresses;
    }

    public void setWebaddresses(String webaddresses) {
        this.webaddresses = webaddresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Institution that = (Institution) o;
        return idinstitution == that.idinstitution &&
                Objects.equals(name, that.name) &&
                Objects.equals(address, that.address) &&
                Objects.equals(township, that.township) &&
                Objects.equals(town, that.town) &&
                Objects.equals(district, that.district) &&
                Objects.equals(founder, that.founder) &&
                Objects.equals(foundationdate, that.foundationdate) &&
                Objects.equals(startdate, that.startdate) &&
                Objects.equals(idnumber, that.idnumber) &&
                Objects.equals(registrationnumber, that.registrationnumber) &&
                Objects.equals(taxid, that.taxid) &&
                Objects.equals(contactphones, that.contactphones) &&
                Objects.equals(emails, that.emails) &&
                Objects.equals(webaddresses, that.webaddresses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idinstitution, name, address, township, town, district, founder, foundationdate, startdate, idnumber, registrationnumber, taxid, contactphones, emails, webaddresses);
    }

    @OneToMany(mappedBy = "institutionObj")
    public List<Museum> getMuseums() {
        return museums;
    }

    public void setMuseums(List<Museum> museums) {
        this.museums = museums;
    }
}
