package com.etf.master.models;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Itemaudiolanguages {
    private int iditemaudiolanguages;
    private String audiourl;
    private String serverid;
    private String name;
    private Item item;
    private Museum museum;
    private Museummap museummap;
    private Exhibition exhibition;
    private Languages language;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Itemaudiolanguages that = (Itemaudiolanguages)o;
        return iditemaudiolanguages == that.iditemaudiolanguages &&
               Objects.equals(audiourl, that.audiourl) &&
               Objects.equals(serverid, that.serverid) &&
               Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iditemaudiolanguages, audiourl, serverid, name);
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "iditemaudiolanguages")
    public int getIditemaudiolanguages() {
        return iditemaudiolanguages;
    }

    @Basic
    @Column(name = "audiourl")
    public String getAudiourl() {
        return audiourl;
    }

    @Basic
    @Column(name = "serverid")
    public String getServerid() {
        return serverid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @ManyToOne
    @JoinColumn(name = "iditem", referencedColumnName = "iditem")
    public Item getItem() {
        return item;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap")
    public Museummap getMuseummap() {
        return museummap;
    }

    @ManyToOne
    @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition")
    public Exhibition getExhibition() {
        return exhibition;
    }

    @ManyToOne
    @JoinColumn(name = "idlanguage", referencedColumnName = "idlanguages")
    public Languages getLanguage() {
        return language;
    }

    public void setIditemaudiolanguages(final int iditemaudiolanguages) {
        this.iditemaudiolanguages = iditemaudiolanguages;
    }

    public void setAudiourl(final String audiourl) {
        this.audiourl = audiourl;
    }

    public void setServerid(final String serverid) {
        this.serverid = serverid;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setItem(final Item item) {
        this.item = item;
    }

    public void setMuseum(final Museum museum) {
        this.museum = museum;
    }

    public void setMuseummap(final Museummap museummap) {
        this.museummap = museummap;
    }

    public void setExhibition(final Exhibition exhibition) {
        this.exhibition = exhibition;
    }

    public void setLanguage(final Languages language) {
        this.language = language;
    }
}
