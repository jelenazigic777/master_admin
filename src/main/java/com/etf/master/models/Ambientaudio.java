package com.etf.master.models;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Ambientaudio {
    private int idambientaudio;
    private String ambientaudiourl;
    private Museum museum;
    private Room room;
    private Exhibition exhibition;
    private String serverid;
    private Museummap floor;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ambientaudio that = (Ambientaudio) o;
        return idambientaudio == that.idambientaudio &&
                Objects.equals(ambientaudiourl, that.ambientaudiourl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idambientaudio, ambientaudiourl);
    }

    @Id
    @Column(name = "idambientaudio")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getIdambientaudio() {
        return idambientaudio;
    }

    @Basic
    @Column(name = "ambientaudiourl")
    public String getAmbientaudiourl() {
        return ambientaudiourl;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "idroom", referencedColumnName = "idroom")
    public Room getRoom() {
        return room;
    }

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition")
    public Exhibition getExhibition() {
        return exhibition;
    }

    @Basic
    @Column(name = "serverid")
    public String getServerid() {
        return serverid;
    }

    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "idfloor", referencedColumnName = "idmuseummap")
    public Museummap getFloor() {
        return floor;
    }

    public void setIdambientaudio(int idambientaudio) {
        this.idambientaudio = idambientaudio;
    }

    public void setAmbientaudiourl(String ambientaudiourl) {
        this.ambientaudiourl = ambientaudiourl;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setExhibition(Exhibition exhibition) {
        this.exhibition = exhibition;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public void setFloor(final Museummap floor) {
        this.floor = floor;
    }
}
