package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Room {
    private int idroom;
    private String institutuion;
    private String language;
    private String name;
    private String text;
    private String texturl;
    private String texturllocal;
    private String imageurl;
    private String imageurllocal;
    private String videourl;
    private String videourllocal;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double zcoordinate;
    private Double radius;
    private Floormap floormap;
    private List<Item> items;
    private List<Room> rooms;
    private Museum museum;
    private Ambientaudio ambientaudio;
    private String audiourl;
    private String audiourllocal;

    @Id
    @Column(name = "idroom")
    @GeneratedValue
    public int getIdroom() {
        return idroom;
    }

    public void setIdroom(int idroom) {
        this.idroom = idroom;
    }

    @Basic
    @Column(name = "institutuion")
    public String getInstitutuion() {
        return institutuion;
    }

    public void setInstitutuion(String institutuion) {
        this.institutuion = institutuion;
    }

    @Basic
    @Column(name = "language")
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    @Basic
    @Column(name = "texturllocal")
    public String getTexturllocal() {
        return texturllocal;
    }

    public void setTexturllocal(String texturllocal) {
        this.texturllocal = texturllocal;
    }

    @Basic
    @Column(name = "imageurl")
    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    @Basic
    @Column(name = "imageurllocal")
    public String getImageurllocal() {
        return imageurllocal;
    }

    public void setImageurllocal(String imageurllocal) {
        this.imageurllocal = imageurllocal;
    }

    @Basic
    @Column(name = "videourl")
    public String getVideourl() {
        return videourl;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    @Basic
    @Column(name = "videourllocal")
    public String getVideourllocal() {
        return videourllocal;
    }

    public void setVideourllocal(String videourllocal) {
        this.videourllocal = videourllocal;
    }

    @Basic
    @Column(name = "xcoordinate")
    public Double getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    @Basic
    @Column(name = "ycoordinate")
    public Double getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    @Basic
    @Column(name = "zcoordinate")
    public Double getZcoordinate() {
        return zcoordinate;
    }

    public void setZcoordinate(Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }

    @Basic
    @Column(name = "radius")
    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return idroom == room.idroom &&
                Objects.equals(institutuion, room.institutuion) &&
                Objects.equals(language, room.language) &&
                Objects.equals(name, room.name) &&
                Objects.equals(text, room.text) &&
                Objects.equals(texturl, room.texturl) &&
                Objects.equals(texturllocal, room.texturllocal) &&
                Objects.equals(imageurl, room.imageurl) &&
                Objects.equals(imageurllocal, room.imageurllocal) &&
                Objects.equals(videourl, room.videourl) &&
                Objects.equals(videourllocal, room.videourllocal) &&
                Objects.equals(xcoordinate, room.xcoordinate) &&
                Objects.equals(ycoordinate, room.ycoordinate) &&
                Objects.equals(zcoordinate, room.zcoordinate) &&
                Objects.equals(radius, room.radius);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idroom, institutuion, language, name, text, texturl, texturllocal, imageurl, imageurllocal, videourl, videourllocal, xcoordinate, ycoordinate, zcoordinate, radius);
    }

    @OneToOne(mappedBy = "room", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "idroom", referencedColumnName = "idroom", nullable = false)
    public Floormap getFloormap() {
        return floormap;
    }

    public void setFloormap(Floormap floormap) {
        this.floormap = floormap;
    }

    @OneToMany(mappedBy = "room", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @ManyToMany( cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinTable(name = "roomtoroom", schema = "master_app", joinColumns = @JoinColumn(name = "idroom1", referencedColumnName = "idroom", nullable = false), inverseJoinColumns = @JoinColumn(name = "idroom2", referencedColumnName = "idroom", nullable = false))
    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    @OneToOne(mappedBy = "room")
    public Ambientaudio getAmbientaudio() {
        return ambientaudio;
    }

    public void setAmbientaudio(Ambientaudio ambientaudio) {
        this.ambientaudio = ambientaudio;
    }

    @Basic
    @Column(name = "audiourl")
    public String getAudiourl() {
        return audiourl;
    }

    public void setAudiourl(String audiourl) {
        this.audiourl = audiourl;
    }

    @Basic
    @Column(name = "audiourllocal")
    public String getAudiourllocal() {
        return audiourllocal;
    }

    public void setAudiourllocal(String audiourllocal) {
        this.audiourllocal = audiourllocal;
    }
}
