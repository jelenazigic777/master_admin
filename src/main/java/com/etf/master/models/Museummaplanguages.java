package com.etf.master.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Museummaplanguages {
    private int idmuseummaplanguages;
    private String name;
    private String text;
    private String texturl;
    private Languages language;
    private Museummap museummap;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idmuseummaplanguages")
    public int getIdmuseummaplanguages() {
        return idmuseummaplanguages;
    }

    public void setIdmuseummaplanguages(int idmuseummaplanguages) {
        this.idmuseummaplanguages = idmuseummaplanguages;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Museummaplanguages that = (Museummaplanguages) o;
        return idmuseummaplanguages == that.idmuseummaplanguages &&
                Objects.equals(name, that.name) &&
                Objects.equals(text, that.text) &&
                Objects.equals(texturl, that.texturl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idmuseummaplanguages, name, text, texturl);
    }

    @OneToOne
    @JoinColumn(name = "idlanguage", referencedColumnName = "idlanguages")
    public Languages getLanguage() {
        return language;
    }

    public void setLanguage(Languages language) {
        this.language = language;
    }

    @OneToOne
    @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap")
    public Museummap getMuseummap() {
        return museummap;
    }

    public void setMuseummap(Museummap museummap) {
        this.museummap = museummap;
    }
}
