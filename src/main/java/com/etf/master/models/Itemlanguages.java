package com.etf.master.models;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Itemlanguages {
    private int iditemlanguages;
    private String name;
    private String author;
    private String text;
    private String texturl;
    private Languages language;
    private Item item;

    @Id
    @Column(name = "iditemlanguages")
    public int getIditemlanguages() {
        return iditemlanguages;
    }

    public void setIditemlanguages(int iditemlanguages) {
        this.iditemlanguages = iditemlanguages;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Itemlanguages that = (Itemlanguages) o;
        return iditemlanguages == that.iditemlanguages &&
               Objects.equals(name, that.name) &&
               Objects.equals(author, that.author) &&
               Objects.equals(text, that.text) &&
               Objects.equals(texturl, that.texturl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iditemlanguages, name, author, text, texturl);
    }

    @OneToOne
    @JoinColumn(name = "idlanguage", referencedColumnName = "idlanguages")
    public Languages getLanguage() {
        return language;
    }

    public void setLanguage(Languages language) {
        this.language = language;
    }

    @OneToOne
    @JoinColumn(name = "iditem", referencedColumnName = "iditem")
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
