package com.etf.master.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Museumlanguages {
    private int idmuseumlanguages;
    private String name;
    private String text;
    private String texturl;
    private Languages language;
    private Museum museum;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idmuseumlanguages")
    public int getIdmuseumlanguages() {
        return idmuseumlanguages;
    }

    public void setIdmuseumlanguages(int idmuseumlanguages) {
        this.idmuseumlanguages = idmuseumlanguages;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Museumlanguages that = (Museumlanguages) o;
        return idmuseumlanguages == that.idmuseumlanguages &&
                Objects.equals(name, that.name) &&
                Objects.equals(text, that.text) &&
                Objects.equals(texturl, that.texturl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idmuseumlanguages, name, text, texturl);
    }

    @OneToOne
    @JoinColumn(name = "idlanguage", referencedColumnName = "idlanguages")
    public Languages getLanguage() {
        return language;
    }

    public void setLanguage(Languages language) {
        this.language = language;
    }

    @OneToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }
}
