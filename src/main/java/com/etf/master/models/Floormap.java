package com.etf.master.models;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Floormap {
    private int idfloormap;
    private int floornumber;
    private Double xstart;
    private Double ystart;
    private Double xend;
    private Double yend;
    private Double zcoordinate;
    private String institution;
    private String name;
    private String text;
    private String texturl;
    private String texturllocal;
    private String imageurl;
    private String imageurllocal;
    private String videourl;
    private String videourllocal;
    private Museum museum;
    private Museummap museummap;
    private Room room;
    private String language;
    private Double radius;
    private String audiourl;
    private String audiourllocal;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Floormap floormap = (Floormap) o;
        return idfloormap == floormap.idfloormap &&
                Objects.equals(floornumber, floormap.floornumber) &&
                Objects.equals(xstart, floormap.xstart) &&
                Objects.equals(ystart, floormap.ystart) &&
                Objects.equals(zcoordinate, floormap.zcoordinate) &&
                Objects.equals(xend, floormap.xend) &&
                Objects.equals(yend, floormap.yend) &&
                Objects.equals(institution, floormap.institution) &&
                Objects.equals(name, floormap.name) &&
                Objects.equals(text, floormap.text) &&
                Objects.equals(texturl, floormap.texturl) &&
                Objects.equals(texturllocal, floormap.texturllocal) &&
                Objects.equals(imageurl, floormap.imageurl) &&
                Objects.equals(imageurllocal, floormap.imageurllocal) &&
                Objects.equals(videourl, floormap.videourl) &&
                Objects.equals(videourllocal, floormap.videourllocal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idfloormap, floornumber, xstart, ystart, zcoordinate, xend, yend, institution, name, text, texturl, texturllocal, imageurl, imageurllocal, videourl, videourllocal);
    }

    @Id
    @Column(name = "idfloormap")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getIdfloormap() {
        return idfloormap;
    }

    @Basic
    @Column(name = "floornumber")
    public int getFloornumber() {
        return floornumber;
    }

    @Basic
    @Column(name = "xstart")
    public Double getXstart() {
        return xstart;
    }

    @Basic
    @Column(name = "ystart")
    public Double getYstart() {
        return ystart;
    }

    @Basic
    @Column(name = "xend")
    public Double getXend() {
        return xend;
    }

    @Basic
    @Column(name = "yend")
    public Double getYend() {
        return yend;
    }


    @Basic
    @Column(name = "institution")
    public String getInstitution() {
        return institution;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    @Basic
    @Column(name = "texturllocal")
    public String getTexturllocal() {
        return texturllocal;
    }

    @Basic
    @Column(name = "imageurl")
    public String getImageurl() {
        return imageurl;
    }

    @Basic
    @Column(name = "imageurllocal")
    public String getImageurllocal() {
        return imageurllocal;
    }

    @Basic
    @Column(name = "videourl")
    public String getVideourl() {
        return videourl;
    }

    @Basic
    @Column(name = "videourllocal")
    public String getVideourllocal() {
        return videourllocal;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    @OneToOne
    @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap")
    public Museummap getMuseummap() {
        return museummap;
    }

    @OneToOne
    @JoinColumn(name = "idroom", referencedColumnName = "idroom")
    public Room getRoom() {
        return room;
    }

    @Basic
    @Column(name = "language")
    public String getLanguage() {
        return language;
    }

    @Basic
    @Column(name = "radius")
    public Double getRadius() {
        return radius;
    }

    @Basic
    @Column(name = "audiourl")
    public String getAudiourl() {
        return audiourl;
    }

    @Basic
    @Column(name = "audiourllocal")
    public String getAudiourllocal() {
        return audiourllocal;
    }

    @Basic
    @Column(name = "zcoordinate")
    public Double getZcoordinate() {
        return zcoordinate;
    }

    public void setIdfloormap(int idfloormap) {
        this.idfloormap = idfloormap;
    }

    public void setFloornumber(int floornumber) {
        this.floornumber = floornumber;
    }

    public void setXstart(Double xstart) {
        this.xstart = xstart;
    }

    public void setYstart(Double ystart) {
        this.ystart = ystart;
    }

    public void setXend(Double xend) {
        this.xend = xend;
    }

    public void setYend(Double yend) {
        this.yend = yend;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    public void setTexturllocal(String texturllocal) {
        this.texturllocal = texturllocal;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public void setImageurllocal(String imageurllocal) {
        this.imageurllocal = imageurllocal;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    public void setVideourllocal(String videourllocal) {
        this.videourllocal = videourllocal;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    public void setMuseummap(Museummap museummap) {
        this.museummap = museummap;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setAudiourl(String audiourl) {
        this.audiourl = audiourl;
    }

    public void setAudiourllocal(String audiourllocal) {
        this.audiourllocal = audiourllocal;
    }

    public void setZcoordinate(final Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }
}
