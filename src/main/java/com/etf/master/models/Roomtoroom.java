package com.etf.master.models;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Roomtoroom {

    private int idroomtoroom;
    private int idroom1;
    private int idroom2;

    @Id
    @Column(name = "idroomtoroom")
    public int getIdroomtoroom() {
        return idroomtoroom;
    }

    public void setIdroomtoroom(int idroomtoroom) {
        this.idroomtoroom = idroomtoroom;
    }

    public int getIdroom1() {
        return idroom1;
    }

    public void setIdroom1(final int idroom1) {
        this.idroom1 = idroom1;
    }

    public int getIdroom2() {
        return idroom2;
    }

    public void setIdroom2(final int idroom2) {
        this.idroom2 = idroom2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Roomtoroom that = (Roomtoroom) o;
        return idroomtoroom == that.idroomtoroom;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idroomtoroom);
    }
}
