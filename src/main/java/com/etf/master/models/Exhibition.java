package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Exhibition {
    private int idexhibition;
    private String institution;
    private String language;
    private String name;
    private String author;
    private String text;
    private String texturl;
    private String texturllocal;
    private String imageurl;
    private String imageurllocal;
    private String videourl;
    private String videourllocal;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double zcoordinate;
    private Double radius;
    private String room;
    private String additionalimagesurl;
    private String additionalimagesurllocal;
    private Boolean active;
    private List<Item> items;
    private List<Museummap> museummaps;
    private Museum museum;
    private Ambientaudio ambientaudio;
    private List<Itemimage> images;
    private List<Itemaudio> audios;
    private List<Itemvideo> videos;
    private List<Itemtext> texts;
    private Itemimage image;
    private Itemaudio audio;
    private Itemvideo video;
    private List<Itemvideolanguages> itemvideolanguages;
    private List<Itemaudiolanguages> itemaudiolanguages;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exhibition that = (Exhibition) o;
        return idexhibition == that.idexhibition &&
                Objects.equals(institution, that.institution) &&
                Objects.equals(language, that.language) &&
                Objects.equals(name, that.name) &&
                Objects.equals(author, that.author) &&
                Objects.equals(text, that.text) &&
                Objects.equals(texturl, that.texturl) &&
                Objects.equals(texturllocal, that.texturllocal) &&
                Objects.equals(imageurl, that.imageurl) &&
                Objects.equals(imageurllocal, that.imageurllocal) &&
                Objects.equals(videourl, that.videourl) &&
                Objects.equals(videourllocal, that.videourllocal) &&
                Objects.equals(xcoordinate, that.xcoordinate) &&
                Objects.equals(ycoordinate, that.ycoordinate) &&
                Objects.equals(zcoordinate, that.zcoordinate) &&
                Objects.equals(radius, that.radius) &&
                Objects.equals(room, that.room) &&
                Objects.equals(additionalimagesurl, that.additionalimagesurl) &&
                Objects.equals(additionalimagesurllocal, that.additionalimagesurllocal) &&
                Objects.equals(active, that.active);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idexhibition, institution, language, name, author, text, texturl, texturllocal, imageurl, imageurllocal, videourl, videourllocal, xcoordinate, ycoordinate, zcoordinate, radius, room, additionalimagesurl, additionalimagesurllocal, active);
    }

    @Id
    @Column(name = "idexhibition")
    @GeneratedValue
    public int getIdexhibition() {
        return idexhibition;
    }

    @Basic
    @Column(name = "institution")
    public String getInstitution() {
        return institution;
    }

    @Basic
    @Column(name = "language")
    public String getLanguage() {
        return language;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    @Basic
    @Column(name = "texturllocal")
    public String getTexturllocal() {
        return texturllocal;
    }

    @Basic
    @Column(name = "imageurl")
    public String getImageurl() {
        return imageurl;
    }

    @Basic
    @Column(name = "imageurllocal")
    public String getImageurllocal() {
        return imageurllocal;
    }

    @Basic
    @Column(name = "videourl")
    public String getVideourl() {
        return videourl;
    }

    @Basic
    @Column(name = "videourllocal")
    public String getVideourllocal() {
        return videourllocal;
    }

    @Basic
    @Column(name = "xcoordinate")
    public Double getXcoordinate() {
        return xcoordinate;
    }

    @Basic
    @Column(name = "ycoordinate")
    public Double getYcoordinate() {
        return ycoordinate;
    }

    @Basic
    @Column(name = "zcoordinate")
    public Double getZcoordinate() {
        return zcoordinate;
    }

    @Basic
    @Column(name = "radius")
    public Double getRadius() {
        return radius;
    }

    @Basic
    @Column(name = "room")
    public String getRoom() {
        return room;
    }

    @Basic
    @Column(name = "additionalimagesurl")
    public String getAdditionalimagesurl() {
        return additionalimagesurl;
    }

    @Basic
    @Column(name = "additionalimagesurllocal")
    public String getAdditionalimagesurllocal() {
        return additionalimagesurllocal;
    }

    @Basic
    @Column(name = "active")
    public boolean getActive() {
        return active;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinTable(name = "itemtoexhibition", schema = "master_app", joinColumns = @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition", nullable = false), inverseJoinColumns = @JoinColumn(name = "iditem", referencedColumnName = "iditem", nullable = false))
    public List<Item> getItems() {
        return items;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinTable(name = "exhibitiontomuseummap", schema = "master_app", joinColumns = @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition", nullable = false), inverseJoinColumns = @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap", nullable = false))
    public List<Museummap> getMuseummaps() {
        return museummaps;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    @OneToOne(mappedBy = "exhibition")
    public Ambientaudio getAmbientaudio() {
        return ambientaudio;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinTable(name = "exhibitionimage", schema = "master_app", joinColumns = @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition"), inverseJoinColumns = @JoinColumn(name = "iditemimage", referencedColumnName = "iditemimage"))
    public List<Itemimage> getImages() {
        return images;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinTable(name = "exhibitionaudio", schema = "master_app", joinColumns = @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition"), inverseJoinColumns = @JoinColumn(name = "iditemaudio", referencedColumnName = "iditemaudio"))
    public List<Itemaudio> getAudios() {
        return audios;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinTable(name = "exhibitionvideo", schema = "master_app", joinColumns = @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition"), inverseJoinColumns = @JoinColumn(name = "iditemvideo", referencedColumnName = "iditemvideo"))
    public List<Itemvideo> getVideos() {
        return videos;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinTable(name = "exhibitiontext", schema = "master_app", joinColumns = @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition"), inverseJoinColumns = @JoinColumn(name = "iditemtext", referencedColumnName = "iditemtext"))
    public List<Itemtext> getTexts() {
        return texts;
    }

    @OneToOne(mappedBy = "exhibition")
    public Itemimage getImage() {
        return image;
    }

    @OneToOne(mappedBy = "exhibition")
    public Itemaudio getAudio() {
        return audio;
    }

    @OneToOne(mappedBy = "exhibition")
    public Itemvideo getVideo() {
        return video;
    }

    @OneToMany(mappedBy = "exhibition")
    public List<Itemvideolanguages> getItemvideolanguages() {
        return itemvideolanguages;
    }

    @OneToMany(mappedBy = "exhibition")
    public List<Itemaudiolanguages> getItemaudiolanguages() {
        return itemaudiolanguages;
    }

    public void setIdexhibition(int idexhibition) {
        this.idexhibition = idexhibition;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    public void setTexturllocal(String texturllocal) {
        this.texturllocal = texturllocal;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public void setImageurllocal(String imageurllocal) {
        this.imageurllocal = imageurllocal;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    public void setVideourllocal(String videourllocal) {
        this.videourllocal = videourllocal;
    }

    public void setXcoordinate(Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public void setYcoordinate(Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    public void setZcoordinate(Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setAdditionalimagesurl(String additionalimagesurl) {
        this.additionalimagesurl = additionalimagesurl;
    }

    public void setAdditionalimagesurllocal(String additionalimagesurllocal) {
        this.additionalimagesurllocal = additionalimagesurllocal;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void setMuseummaps(List<Museummap> museummaps) {
        this.museummaps = museummaps;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    public void setAmbientaudio(Ambientaudio ambientaudio) {
        this.ambientaudio = ambientaudio;
    }

    public void setImages(List<Itemimage> images) {
        this.images = images;
    }

    public void setAudios(List<Itemaudio> audios) {
        this.audios = audios;
    }

    public void setVideos(List<Itemvideo> videos) {
        this.videos = videos;
    }

    public void setTexts(List<Itemtext> texts) {
        this.texts = texts;
    }

    public void setImage(Itemimage image) {
        this.image = image;
    }

    public void setAudio(Itemaudio audio) {
        this.audio = audio;
    }

    public void setVideo(Itemvideo video) {
        this.video = video;
    }

    public void setItemvideolanguages(final List<Itemvideolanguages> itemvideolanguages) {
        this.itemvideolanguages = itemvideolanguages;
    }

    public void setItemaudiolanguages(final List<Itemaudiolanguages> itemaudiolanguages) {
        this.itemaudiolanguages = itemaudiolanguages;
    }
}
