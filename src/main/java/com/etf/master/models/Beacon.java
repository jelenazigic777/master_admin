package com.etf.master.models;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Beacon {
    private int idbeacon;
    private String name;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double zcoordinate;
    private Double radius;
    private Integer major;
    private Integer minor;
    private Integer power;
    private Museum museum;
    private Museummap museummap;

    @Id
    @Column(name = "idbeacon")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getIdbeacon() {
        return idbeacon;
    }

    public void setIdbeacon(int idbeacon) {
        this.idbeacon = idbeacon;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "xcoordinate")
    public Double getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    @Basic
    @Column(name = "ycoordinate")
    public Double getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    @Basic
    @Column(name = "zcoordinate")
    public Double getZcoordinate() {
        return zcoordinate;
    }

    public void setZcoordinate(Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }

    @Basic
    @Column(name = "radius")
    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    @Basic
    @Column(name = "major")
    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    @Basic
    @Column(name = "minor")
    public Integer getMinor() {
        return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    @Basic
    @Column(name = "power")
    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beacon beacon = (Beacon) o;
        return idbeacon == beacon.idbeacon &&
                Objects.equals(name, beacon.name) &&
                Objects.equals(xcoordinate, beacon.xcoordinate) &&
                Objects.equals(ycoordinate, beacon.ycoordinate) &&
                Objects.equals(zcoordinate, beacon.zcoordinate) &&
                Objects.equals(radius, beacon.radius) &&
                Objects.equals(major, beacon.major) &&
                Objects.equals(minor, beacon.minor) &&
                Objects.equals(power, beacon.power);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idbeacon, name, xcoordinate, ycoordinate, zcoordinate, radius, major, minor, power);
    }

    @ManyToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap")
    public Museummap getMuseummap() {
        return museummap;
    }

    public void setMuseummap(Museummap museummap) {
        this.museummap = museummap;
    }
}
