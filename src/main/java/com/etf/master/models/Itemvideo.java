package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Itemvideo {
    private int iditemvideo;
    private String videourl;
    private Byte islocal;
    private Item item;
    private String serverid;
    private String name;
    private List<Exhibition> exhibitions;
    private Exhibition exhibition;
    private Museum museum;
    private Museummap museummap;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Itemvideo that = (Itemvideo) o;
        return iditemvideo == that.iditemvideo &&
               Objects.equals(videourl, that.videourl) &&
               Objects.equals(islocal, that.islocal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iditemvideo, videourl, islocal);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "iditemvideo")
    public int getIditemvideo() {
        return iditemvideo;
    }

    @Basic
    @Column(name = "videourl")
    public String getVideourl() {
        return videourl;
    }

    @Basic
    @Column(name = "islocal")
    public Byte getIslocal() {
        return islocal;
    }

    @ManyToOne
    @JoinColumn(name = "iditem", referencedColumnName = "iditem")
    public Item getItem() {
        return item;
    }

    @Basic
    @Column(name = "serverid")
    public String getServerid() {
        return serverid;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @ManyToMany(mappedBy = "videos", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Exhibition> getExhibitions() {
        return exhibitions;
    }

    @OneToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition")
    public Exhibition getExhibition() {
        return exhibition;
    }

    @ManyToOne(cascade = {CascadeType.MERGE})
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    @ManyToOne
    @JoinColumn(name = "idmuseummap", referencedColumnName = "idmuseummap")
    public Museummap getMuseummap() {
        return museummap;
    }

    public void setIditemvideo(int iditemvideo) {
        this.iditemvideo = iditemvideo;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    public void setIslocal(Byte islocal) {
        this.islocal = islocal;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setExhibitions(List<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }

    public void setExhibition(Exhibition exhibition) {
        this.exhibition = exhibition;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    public void setMuseummap(Museummap museummap) {
        this.museummap = museummap;
    }
}
