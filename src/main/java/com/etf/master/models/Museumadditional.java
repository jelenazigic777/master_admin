package com.etf.master.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Museumadditional {
    private Museum museum;
    private int idmuseumadditional;
    private String type;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double zcoordinate;
    private Double distance;
    private String address;
    private String zipcode;
    private String town;
    private String township;
    private String district;
    private Integer buildyear;
    private Integer openyear;
    private String aims;
    private String contactphones;

    @OneToOne
    @JoinColumn(name = "idmuseum", referencedColumnName = "idmuseum")
    public Museum getMuseum() {
        return museum;
    }

    public void setMuseum(Museum museum) {
        this.museum = museum;
    }

    @Id
    @Column(name = "idmuseumadditional")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getIdmuseumadditional() {
        return idmuseumadditional;
    }

    public void setIdmuseumadditional(int idmuseumadditional) {
        this.idmuseumadditional = idmuseumadditional;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "xcoordinate")
    public Double getXcoordinate() {
        return xcoordinate;
    }

    public void setXcoordinate(Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    @Basic
    @Column(name = "ycoordinate")
    public Double getYcoordinate() {
        return ycoordinate;
    }

    public void setYcoordinate(Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    @Basic
    @Column(name = "zcoordinate")
    public Double getZcoordinate() {
        return zcoordinate;
    }

    public void setZcoordinate(Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }

    @Basic
    @Column(name = "distance")
    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Basic
    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "zipcode")
    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Basic
    @Column(name = "town")
    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    @Basic
    @Column(name = "township")
    public String getTownship() {
        return township;
    }

    public void setTownship(String township) {
        this.township = township;
    }

    @Basic
    @Column(name = "district")
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    @Basic
    @Column(name = "buildyear")
    public Integer getBuildyear() {
        return buildyear;
    }

    public void setBuildyear(Integer buildyear) {
        this.buildyear = buildyear;
    }

    @Basic
    @Column(name = "openyear")
    public Integer getOpenyear() {
        return openyear;
    }

    public void setOpenyear(Integer openyear) {
        this.openyear = openyear;
    }

    @Basic
    @Column(name = "aims")
    public String getAims() {
        return aims;
    }

    public void setAims(String aims) {
        this.aims = aims;
    }

    @Basic
    @Column(name = "contactphones")
    public String getContactphones() {
        return contactphones;
    }

    public void setContactphones(String contactphones) {
        this.contactphones = contactphones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Museumadditional that = (Museumadditional) o;
        return idmuseumadditional == that.idmuseumadditional &&
                Objects.equals(type, that.type) &&
                Objects.equals(xcoordinate, that.xcoordinate) &&
                Objects.equals(ycoordinate, that.ycoordinate) &&
                Objects.equals(zcoordinate, that.zcoordinate) &&
                Objects.equals(distance, that.distance) &&
                Objects.equals(address, that.address) &&
                Objects.equals(zipcode, that.zipcode) &&
                Objects.equals(town, that.town) &&
                Objects.equals(township, that.township) &&
                Objects.equals(district, that.district) &&
                Objects.equals(buildyear, that.buildyear) &&
                Objects.equals(openyear, that.openyear) &&
                Objects.equals(aims, that.aims) &&
                Objects.equals(contactphones, that.contactphones);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idmuseumadditional, type, xcoordinate, ycoordinate, zcoordinate, distance, address, zipcode, town, township, district, buildyear, openyear, aims, contactphones);
    }
}
