package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Languages {

    private int idlanguages;
    private String languagename;
    private String languageabbreviation;
    private List<Itemaudiolanguages> itemaudios;
    private List<Itemvideolanguages> itemvideos;

    public Languages(String languageName, String languageAbbreviation) {
        this.languagename=languageName;
        this.languageabbreviation=languageAbbreviation;
    }

    public Languages() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Languages languages = (Languages) o;
        return idlanguages == languages.idlanguages &&
                Objects.equals(languagename, languages.languagename) &&
                Objects.equals(languageabbreviation, languages.languageabbreviation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idlanguages, languagename, languageabbreviation);
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idlanguages")
    public int getIdlanguages() {
        return idlanguages;
    }

    @Basic
    @Column(name = "languagename")
    public String getLanguagename() {
        return languagename;
    }

    @Basic
    @Column(name = "languageabbreviation")
    public String getLanguageabbreviation() {
        return languageabbreviation;
    }

    @OneToMany(mappedBy = "language")
    public List<Itemaudiolanguages> getItemaudios() {
        return itemaudios;
    }

    @OneToMany(mappedBy = "language")
    public List<Itemvideolanguages> getItemvideos() {
        return itemvideos;
    }

    public void setIdlanguages(int idlanguages) {
        this.idlanguages = idlanguages;
    }

    public void setLanguagename(String languagename) {
        this.languagename = languagename;
    }

    public void setLanguageabbreviation(String languageabbreviation) {
        this.languageabbreviation = languageabbreviation;
    }

    public void setItemaudios(final List<Itemaudiolanguages> itemaudios) {
        this.itemaudios = itemaudios;
    }

    public void setItemvideos(final List<Itemvideolanguages> itemvideos) {
        this.itemvideos = itemvideos;
    }
}
