package com.etf.master.models;

import java.util.List;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {
    private int iduser;
    private String username;
    private String password;
    private String name;
    private String surname;
    private List<Role> roles;
    private Boolean active;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return iduser == user.iduser &&
                Objects.equals(username, user.username) &&
                Objects.equals(password, user.password) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iduser, username, password, name, surname);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "iduser")
    public int getIduser() {
        return iduser;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    //TODO : move to messages.properties
    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Basic
    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    @ManyToMany
    @JoinTable(name = "userrole", schema = "master_app", joinColumns = @JoinColumn(name = "iduser", referencedColumnName = "iduser"), inverseJoinColumns = @JoinColumn(name = "idrole", referencedColumnName = "idrole"))
    public List<Role> getRoles() {
        return roles;
    }

    @Basic
    @Column(name = "active")
    public Boolean getActive() {
        return active;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setRoles(final List<Role> roles) {
        this.roles = roles;
    }

    public void setActive(final Boolean active) {
        this.active = active;
    }
}
