package com.etf.master.models;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Exhibitiontomuseummap {
    private int idexhibitiontomuseummap;
    private int idmuseummap;
    private int idexhibition;

    @Id
    @Column(name = "idexhibitiontomuseummap")
    public int getIdexhibitiontomuseummap() {
        return idexhibitiontomuseummap;
    }

    public void setIdexhibitiontomuseummap(int idexhibitiontomuseummap) {
        this.idexhibitiontomuseummap = idexhibitiontomuseummap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exhibitiontomuseummap that = (Exhibitiontomuseummap) o;
        return idexhibitiontomuseummap == that.idexhibitiontomuseummap;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idexhibitiontomuseummap);
    }

    @Basic
    @Column(name = "idmuseummap")
    public int getIdmuseummap() {
        return idmuseummap;
    }

    public void setIdmuseummap(int idmuseummap) {
        this.idmuseummap = idmuseummap;
    }

    @Basic
    @Column(name = "idexhibition")
    public int getIdexhibition() {
        return idexhibition;
    }

    public void setIdexhibition(int idexhibition) {
        this.idexhibition = idexhibition;
    }
}
