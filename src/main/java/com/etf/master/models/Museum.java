package com.etf.master.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Museum {
    private int idmuseum;
    private String name;
    private String language;
    private String text;
    private String texturl;
    private String texturllocal;
    private String imageurl;
    private String imageurllocal;
    private String audiourl;
    private String audiourllocal;
    private String videourl;
    private String videourllocal;
    private Double xcoordinate;
    private Double ycoordinate;
    private Double zcoordinate;
    private Double radius;
    private String qr;
    private List<Item> items;
    private List<Floormap> floormaps;
    private List<Museummap> museummaps;
    private List<Room> rooms;
    private List<Exhibition> exhibitions;
    private List<Beacon> beacons;
    private List<Ambientaudio> ambientaudios = new ArrayList<>();
    private Museumadditional additionalInfo;
    private String institution;
    private Institution institutionObj;
    private List<Itemimage> images = new ArrayList<>();
    private List<Itemaudio> audios = new ArrayList<>();
    private List<Itemvideo> videos = new ArrayList<>();
    private List<Itemaudiolanguages> itemaudiolanguages;
    private List<Itemvideolanguages> itemvideolanguages;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Museum museum = (Museum) o;
        return idmuseum == museum.idmuseum &&
                Objects.equals(name, museum.name) &&
                Objects.equals(language, museum.language) &&
                Objects.equals(text, museum.text) &&
                Objects.equals(texturl, museum.texturl) &&
                Objects.equals(texturllocal, museum.texturllocal) &&
                Objects.equals(imageurl, museum.imageurl) &&
                Objects.equals(imageurllocal, museum.imageurllocal) &&
                Objects.equals(audiourl, museum.audiourl) &&
                Objects.equals(audiourllocal, museum.audiourllocal) &&
                Objects.equals(videourl, museum.videourl) &&
                Objects.equals(videourllocal, museum.videourllocal) &&
                Objects.equals(xcoordinate, museum.xcoordinate) &&
                Objects.equals(ycoordinate, museum.ycoordinate) &&
                Objects.equals(zcoordinate, museum.zcoordinate) &&
                Objects.equals(radius, museum.radius) &&
                Objects.equals(qr, museum.qr);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idmuseum, name, institution, language, text, texturl, texturllocal, imageurl, imageurllocal, audiourl, audiourllocal, videourl, videourllocal, xcoordinate, ycoordinate, zcoordinate, radius, qr);
    }

    @Id
    @Column(name = "idmuseum")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getIdmuseum() {
        return idmuseum;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Basic
    @Column(name = "language")
    public String getLanguage() {
        return language;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    @Basic
    @Column(name = "texturllocal")
    public String getTexturllocal() {
        return texturllocal;
    }

    @Basic
    @Column(name = "imageurl")
    public String getImageurl() {
        return imageurl;
    }

    @Basic
    @Column(name = "imageurllocal")
    public String getImageurllocal() {
        return imageurllocal;
    }

    @Basic
    @Column(name = "audiourl")
    public String getAudiourl() {
        return audiourl;
    }

    @Basic
    @Column(name = "audiourllocal")
    public String getAudiourllocal() {
        return audiourllocal;
    }

    @Basic
    @Column(name = "videourl")
    public String getVideourl() {
        return videourl;
    }

    @Basic
    @Column(name = "videourllocal")
    public String getVideourllocal() {
        return videourllocal;
    }

    @Basic
    @Column(name = "xcoordinate")
    public Double getXcoordinate() {
        return xcoordinate;
    }

    @Basic
    @Column(name = "ycoordinate")
    public Double getYcoordinate() {
        return ycoordinate;
    }

    @Basic
    @Column(name = "zcoordinate")
    public Double getZcoordinate() {
        return zcoordinate;
    }

    @Basic
    @Column(name = "radius")
    public Double getRadius() {
        return radius;
    }

    @Basic
    @Column(name = "qr")
    public String getQr() {
        return qr;
    }

    @OneToMany(mappedBy = "museum")
    public List<Item> getItems() {
        return items;
    }

    @OneToMany(mappedBy = "museum")
    public List<Floormap> getFloormaps() {
        return floormaps;
    }

    @OneToMany(mappedBy = "museum")
    public List<Museummap> getMuseummaps() {
        return museummaps;
    }

    @OneToMany(mappedBy = "museum")
    public List<Room> getRooms() {
        return rooms;
    }

    @OneToMany(mappedBy = "museum")
    public List<Exhibition> getExhibitions() {
        return exhibitions;
    }

    @OneToMany(mappedBy = "museum")
    public List<Beacon> getBeacons() {
        return beacons;
    }

    //TODO: somethings wrong here
    @OneToMany(mappedBy = "museum", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Ambientaudio> getAmbientaudios() {
        return ambientaudios;
    }

    @OneToOne(mappedBy = "museum")
    public Museumadditional getAdditionalInfo() {
        return additionalInfo;
    }

    public String getInstitution() {
        return institution;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "idinstitution", referencedColumnName = "idinstitution")
    public Institution getInstitutionObj() {
        return institutionObj;
    }

    @OneToMany(mappedBy = "museum", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemimage> getImages() {
        return images;
    }

    @OneToMany(mappedBy = "museum", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemaudio> getAudios() {
        return audios;
    }

    @OneToMany(mappedBy = "museum",  cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Itemvideo> getVideos() {
        return videos;
    }

    @OneToMany(mappedBy = "museum")
    public List<Itemaudiolanguages> getItemaudiolanguages() {
        return itemaudiolanguages;
    }

    @OneToMany(mappedBy = "museum")
    public List<Itemvideolanguages> getItemvideolanguages() {
        return itemvideolanguages;
    }

    public void setIdmuseum(int idmuseum) {
        this.idmuseum = idmuseum;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    public void setTexturllocal(String texturllocal) {
        this.texturllocal = texturllocal;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public void setImageurllocal(String imageurllocal) {
        this.imageurllocal = imageurllocal;
    }

    public void setAudiourl(String audiourl) {
        this.audiourl = audiourl;
    }

    public void setAudiourllocal(String audiourllocal) {
        this.audiourllocal = audiourllocal;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    public void setVideourllocal(String videourllocal) {
        this.videourllocal = videourllocal;
    }

    public void setXcoordinate(Double xcoordinate) {
        this.xcoordinate = xcoordinate;
    }

    public void setYcoordinate(Double ycoordinate) {
        this.ycoordinate = ycoordinate;
    }

    public void setZcoordinate(Double zcoordinate) {
        this.zcoordinate = zcoordinate;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void setFloormaps(List<Floormap> floormaps) {
        this.floormaps = floormaps;
    }

    public void setMuseummaps(List<Museummap> museummaps) {
        this.museummaps = museummaps;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public void setExhibitions(List<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }

    public void setBeacons(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    public void setAmbientaudios(List<Ambientaudio> ambientaudios) {
        this.ambientaudios = ambientaudios;
    }

    public void setAdditionalInfo(Museumadditional additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public void setInstitutionObj(Institution institutionObj) {
        this.institutionObj = institutionObj;
    }

    public void setImages(final List<Itemimage> images) {
        this.images = images;
    }

    public void setAudios(final List<Itemaudio> audios) {
        this.audios = audios;
    }

    public void setVideos(final List<Itemvideo> videos) {
        this.videos = videos;
    }

    public void setItemaudiolanguages(final List<Itemaudiolanguages> itemaudiolanguages) {
        this.itemaudiolanguages = itemaudiolanguages;
    }

    public void setItemvideolanguages(final List<Itemvideolanguages> itemvideolanguages) {
        this.itemvideolanguages = itemvideolanguages;
    }
}
