package com.etf.master.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Exhibitionlanguages {
    private int idexhibitionlanguages;
    private String name;
    private String author;
    private String text;
    private String texturl;
    private Languages language;
    private Exhibition exhibition;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "idexhibitionlanguages")
    public int getIdexhibitionlanguages() {
        return idexhibitionlanguages;
    }

    public void setIdexhibitionlanguages(int idexhibitionlanguages) {
        this.idexhibitionlanguages = idexhibitionlanguages;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "texturl")
    public String getTexturl() {
        return texturl;
    }

    public void setTexturl(String texturl) {
        this.texturl = texturl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Exhibitionlanguages that = (Exhibitionlanguages) o;
        return idexhibitionlanguages == that.idexhibitionlanguages &&
                Objects.equals(name, that.name) &&
                Objects.equals(author, that.author) &&
                Objects.equals(text, that.text) &&
                Objects.equals(texturl, that.texturl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idexhibitionlanguages, name, author, text, texturl);
    }

    @OneToOne
    @JoinColumn(name = "idlanguage", referencedColumnName = "idlanguages")
    public Languages getLanguage() {
        return language;
    }

    public void setLanguage(Languages language) {
        this.language = language;
    }

    @OneToOne
    @JoinColumn(name = "idexhibition", referencedColumnName = "idexhibition")
    public Exhibition getExhibition() {
        return exhibition;
    }

    public void setExhibition(Exhibition exhibition) {
        this.exhibition = exhibition;
    }
}
