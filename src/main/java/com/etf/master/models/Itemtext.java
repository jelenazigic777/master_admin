package com.etf.master.models;

import java.util.List;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Itemtext {
    private int iditemtext;
    private Byte islocal;
    private Item item;
    private String text;
    private List<Exhibition> exhibitions;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "iditemtext")
    public int getIditemtext() {
        return iditemtext;
    }

    public void setIditemtext(int iditemtext) {
        this.iditemtext = iditemtext;
    }

    @Basic
    @Column(name = "islocal")
    public Byte getIslocal() {
        return islocal;
    }

    public void setIslocal(Byte islocal) {
        this.islocal = islocal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Itemtext that = (Itemtext) o;
        return iditemtext == that.iditemtext &&
               Objects.equals(text, that.text) &&
               Objects.equals(islocal, that.islocal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iditemtext, text, islocal);
    }

    @ManyToOne
    @JoinColumn(name = "iditem", referencedColumnName = "iditem")
    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @ManyToMany(mappedBy = "texts", cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    public List<Exhibition> getExhibitions() {
        return exhibitions;
    }

    public void setExhibitions(List<Exhibition> exhibitions) {
        this.exhibitions = exhibitions;
    }
}
