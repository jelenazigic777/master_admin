package com.etf.master.filesmanipulation;

import java.util.Map;

public interface FilesManipulator {

    Map upload(Object file, Map options);

    void delete(String id);

}
