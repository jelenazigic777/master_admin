package com.etf.master.filesmanipulation.impl;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.cloudinary.Cloudinary;
import com.cloudinary.Singleton;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;
import com.etf.master.filesmanipulation.FilesManipulator;

@Service
public class CloudinaryImpl implements FilesManipulator {

    private Cloudinary cloudinary;
    @Autowired
    public CloudinaryImpl(@Value("${cloudinary.apikey}") String key,
                          @Value("${cloudinary.apisecret}") String secret,
                          @Value("${cloudinary.cloudname}") String cloud){
        cloudinary = Singleton.getCloudinary();
        cloudinary.config.cloudName=cloud;
        cloudinary.config.apiSecret=secret;
        cloudinary.config.apiKey=key;
    }

    public Map upload(Object file, Map options){
        try{
            return cloudinary.uploader().upload(file, options);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String createUrl(String name, int width, int height, String action){
        return cloudinary.url()
                .transformation(new Transformation().width(width).height(height)
                        .border("2px_solid_black").crop(action))
                .imageTag(name);
    }

    public void delete(String id) {
        try {
            cloudinary.uploader().destroy(id, ObjectUtils.emptyMap());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
