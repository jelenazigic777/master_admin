package com.etf.master.constants;

public class PageConstants {

        public static final String LOGIN_PAGE = "login";
        public static final String REGISTRATION_PAGE = "registration";
        public static final String ADMIN_PAGE = "admin";
        public static final String HOME_PAGE = "home";
        public static final String MUSEUMS_PAGE = "museums";
        public static final String MUSEUM_FORM_PAGE = "museumform";
        public static final String INSTITUTION_FORM_PAGE = "institutionform";
        public static final String MUSEUM_ADDITIONAL_FORM_PAGE = "museumadditionalform";
        public static final String DETAILS_INSTITUTION_PAGE = "institutiondetails";
        public static final String DETAILS_MUSEUM_PAGE = "museumformdetails";
        public static final String ITEM_FORM_PAGE = "itemform";
        public static final String EXHIBITION_FORM_PAGE = "exhibitionform";
        public static final String ITEMS_MUSEUM = "items";
        public static final String EXHIBITION_MUSEUM = "exhibitions";
        public static final String FLOORS_PAGE = "floors";
        public static final String FLOORS_ADDITIONAL = "flooradditional";
        public static final String MAPS_PAGE = "maps";
        public static final String MAP_FORM_PAGE = "mapform";
        public static final String FLOOR_FORM_PAGE = "floorform";
        public static final String EXHIBITION_ADDITIONAL = "exhibitionadditional";
        public static final String MAP_ADDITIONAL = "mapadditional";
        public static final String ITEM_ADDITIONAL = "itemadditional";
        public static final String BEACONS_PAGE = "beacons";
        public static final String BEACON_FORM = "beaconform";
        public static final String ITEM_PAGE_FOR_EXHIBITION = "itemforexhibition";
        public static final String LANGUAGES_PAGE = "languages";
        public static final String MUSEUM_LANGUAGES_PAGE = "museumlanguages";
        public static final String ITEM_LANGUAGES_PAGE = "itemlanguages";
        public static final String EXHIBITION_LANGUAGES_PAGE = "exhibitionlanguages";
        public static final String MAP_LANGUAGES_PAGE = "museummaplanguages";
}
