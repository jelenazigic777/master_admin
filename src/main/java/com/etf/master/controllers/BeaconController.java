package com.etf.master.controllers;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.etf.master.constants.PageConstants;
import com.etf.master.models.Beacon;
import com.etf.master.models.Floormap;
import com.etf.master.models.Museummap;
import com.etf.master.services.BeaconService;
import com.etf.master.services.MainService;
import com.etf.master.services.MapService;
import com.etf.master.validators.BeaconValidator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
@RequestMapping("/beacons")
public class BeaconController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private BeaconValidator beaconValidator;

    @Autowired
    private MapService mapService;

    @Autowired
    private MainService mainService;

    @Autowired
    BeaconService beaconService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String beaconsPage(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model) {

        List<Beacon> beacons = beaconService.findAllFromMuseum(idmuseum);
        model.addAttribute("beacons", beacons);
        model.addAttribute("idmuseum", idmuseum);
        return PageConstants.BEACONS_PAGE;
    }

    @RequestMapping(value = "/beaconform", method = RequestMethod.GET)
    public String beaconForm(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model, Locale locale) {
        Beacon beacon = new Beacon();
        model.addAttribute("idbeacon", beacon.getIdbeacon());
        model.addAttribute("beacon", beacon);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("floormaps", mapService.findAllActiveFloormapsFromMuseum(idmuseum));
        model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
        return PageConstants.BEACON_FORM;
    }

    @RequestMapping(value = "/beaconform", method = RequestMethod.POST)
    public String beaconForm(@ModelAttribute("beacon") Beacon beacon, @RequestParam(value = "idmuseum") Integer idmuseum, BindingResult bindingResult, final Model model, Locale locale, RedirectAttributes redirectAttributes) {

        Beacon foundedBeacon = beaconService.findBeacon(beacon.getIdbeacon());
        beaconValidator.validate(beacon, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("floormaps", mapService.findAllActiveFloormapsFromMuseum(idmuseum));
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            return PageConstants.BEACON_FORM;
        }
        beacon.setMuseum(mainService.findMuseum(idmuseum));
        beacon.setMuseummap(foundedBeacon.getMuseummap());
        beaconService.saveBeacon(beacon);

        if (foundedBeacon != null) {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.beacon.success", new Object[]{}, locale));
        } else {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("add.beacon.success", new Object[]{}, locale));
        }
        return "redirect:/beacons/beaconform/update?idmuseum=" + idmuseum + "&idbeacon=" + beacon.getIdbeacon();
    }

    @RequestMapping(value = "/beacon/delete", method = RequestMethod.GET)
    public String deleteBeacon(@RequestParam(value = "idbeacon") Integer idbeacon, @RequestParam(value = "idmuseum") Integer idmuseum, Model model) {
        beaconService.deleteBeacon(idbeacon);
        return "redirect:/beacons?idmuseum=" + idmuseum;
    }

    @RequestMapping(value = "/beaconform/update", method = RequestMethod.GET)
    public String beaconFormUpdate(@RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "idbeacon") Integer idBeacon, final Model model, Locale locale, @ModelAttribute("successMessage") String successMessage) {
        Beacon beacon = beaconService.findBeacon(idBeacon);
        if (beacon.getMuseummap() != null) {
            List<Beacon> beacons = beaconService.findAllFromMuseummap(beacon.getMuseummap().getIdmuseummap());
            beacons.remove(beacon);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            model.addAttribute("otherBeacons", gson.toJson(beaconService.convertBeacons(beacons)));
        }
        model.addAttribute("beacon", beacon);
        model.addAttribute("floormap", mapService.findFloorByRoomFloorNumber(idmuseum, beacon.getMuseummap().getFloormap().getFloornumber()));
        model.addAttribute("museummap", beacon.getMuseummap());
        model.addAttribute("floormaps", mapService.findAllActiveFloormapsFromMuseum(idmuseum));
        model.addAttribute("idmuseum", beacon.getMuseum().getIdmuseum());
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        model.addAttribute("successMessage", successMessage);
        return PageConstants.BEACON_FORM;
    }

    @Transactional
    @RequestMapping(value = "/set/map", method = RequestMethod.POST)
    public String setActiveMap(@ModelAttribute("beacon") Beacon beacon, @RequestParam(value = "idmuseum") int idmuseum, HttpServletRequest request) {
        String selectedFloormapId = request.getParameter("selectedFloormap");

        Floormap floormap = mapService.findFloormap(Integer.valueOf(selectedFloormapId));
        Museummap museummap = floormap.getMuseummap();
        if (beacon.getIdbeacon() != 0) {
            beacon = beaconService.findBeacon(beacon.getIdbeacon());
        }
        beacon.setMuseum(mainService.findMuseum(idmuseum));
        beacon.setMuseummap(museummap);
        //beacon.setZcoordinate(museummap.getZcoordinate());
        beacon = beaconService.saveBeacon(beacon);
        int idbeacon = beacon.getIdbeacon();

        return "redirect:/beacons/beaconform/update?idmuseum=" + idmuseum + "&idbeacon=" + idbeacon;
    }
}
