package com.etf.master.controllers;

import java.util.Arrays;
import java.util.Locale;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.etf.master.constants.PageConstants;
import com.etf.master.models.User;
import com.etf.master.services.RoleService;
import com.etf.master.services.UserService;
import com.etf.master.validators.UserValidator;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    AuthenticationManager authenticationManager;

    //returns registration page, if user wants to go to that page
    @RequestMapping(value="/registration", method = RequestMethod.GET)
    public String registration(final Model model, Locale locale){
        model.addAttribute("user", new User());
        return PageConstants.REGISTRATION_PAGE;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String createNewUser(@AuthenticationPrincipal @Valid @ModelAttribute("user") User user, BindingResult bindingResult, final Model model, Locale locale) {
        userValidator.validate(user, bindingResult);
        if (bindingResult.hasErrors()) {
            return PageConstants.REGISTRATION_PAGE;
        }
        model.addAttribute("user", user);
        user.setActive(false);
        userService.saveUser(user);
        user.setRoles(Arrays.asList(roleService.findRoleByName("ROLE_USER")));
        model.addAttribute("successMessage", messageSource.getMessage("user.registration.success", new Object[] {}, locale));
        return PageConstants.REGISTRATION_PAGE;
    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String redirectToHome() {
        return "redirect:/home";
    }

    @RequestMapping(value="/admin", method = RequestMethod.GET)
    public String adminPage(Model model) {
        model.addAttribute("users", userService.findAllUsersNotActive());
        return PageConstants.ADMIN_PAGE;
    }

    @RequestMapping(value = "/admin/user/setActive", method = RequestMethod.GET)
    public String approveRegistrationForUser(@RequestParam(value = "iduser") Integer iduser) {
        User user = userService.findUserById(iduser);
        user.setActive(true);
        userService.saveUser(user);
       return "redirect:/admin";
    }

    @RequestMapping(value = "/admin/user/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam(value = "iduser") Integer iduser) {
        userService.deleteUserById(iduser);
        return "redirect:/admin";
    }

    @RequestMapping(value="/login/error", method = RequestMethod.GET)
    public String redirectError(final Model model, Locale locale) {
        model.addAttribute("errorMessage", messageSource.getMessage("wrong.credentials.login", new Object[] {}, locale));
        return PageConstants.LOGIN_PAGE;
    }

}
