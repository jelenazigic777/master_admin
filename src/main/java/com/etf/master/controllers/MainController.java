package com.etf.master.controllers;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.etf.master.constants.PageConstants;
import com.etf.master.models.Institution;
import com.etf.master.models.Museum;
import com.etf.master.models.Museumadditional;
import com.etf.master.services.AmbientaudioService;
import com.etf.master.services.MainService;
import com.etf.master.validators.InstitutionValidator;
import com.etf.master.validators.MuseumValidator;
import com.etf.master.validators.MuseumadditionalValidator;

@Controller
public class MainController {
    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MuseumValidator museumValidator;

    @Autowired
    private InstitutionValidator institutionValidator;

    @Autowired
    private MuseumadditionalValidator museumadditionalValidator;

    @Autowired
    private MainService mainService;

    @Autowired
    private AmbientaudioService ambientaudioService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String homePage(final Model model, @ModelAttribute("errorMessage") String errormessage) {

        List<Institution> institutions = mainService.findAllInstitutions();
        model.addAttribute("institutions", institutions);
        model.addAttribute("errorMessage", errormessage);
        return PageConstants.HOME_PAGE;
    }

    @RequestMapping(value = "/museums", method = RequestMethod.GET)
    public String museumsPage(@RequestParam(value = "idinstitution") int idinstitution, final Model model, @ModelAttribute("errorMessage") String errormessage) {
        List<Museum> museums = mainService.findAllMuseumsFromInstitution(idinstitution);
        model.addAttribute("museums", museums);
        model.addAttribute("idinstitution", idinstitution);
        model.addAttribute("errorMessage", errormessage);
        return PageConstants.MUSEUMS_PAGE;
    }

    @RequestMapping(value = "/museums/museum/additionalform", method = RequestMethod.GET)
    public String museumAdditionalForm(@ModelAttribute("museum") Museum museum, @RequestParam("idinstitution") int idinstitution, @RequestParam(value="idmuseum") int idmuseum, final Model model, Locale locale, BindingResult bindingResult) {
        if (idmuseum == 0) {
            museumValidator.validate(museum, bindingResult);
            if (bindingResult.hasErrors()) {
                model.addAttribute("idinstitution", idinstitution);
                model.addAttribute("idmuseum", idmuseum);
                model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
                return PageConstants.MUSEUM_FORM_PAGE;
            }
            museum.setLanguage(locale.getLanguage());
            museum.setInstitutionObj(mainService.findInstitution(idinstitution));
            Museum newMuseum = mainService.saveMuseum(museum);
            model.addAttribute(newMuseum.getIdmuseum());
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
        } else {
            if (mainService.findMuseumAdditionalForMuseum(idmuseum) != null) {
                model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
            } else {
                model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            }
            model.addAttribute("idmuseum", museum.getIdmuseum());
        }
        Museumadditional museumadditional = mainService.findMuseumAdditionalForMuseum(idmuseum);
        if (museumadditional == null) {
            museumadditional = new Museumadditional();
        }
        model.addAttribute("museumadditional", museumadditional);
        model.addAttribute("idinstitution", idinstitution);
        model.addAttribute("title", messageSource.getMessage("museum.additional.title", new Object[]{museum.getName()}, locale));
        return PageConstants.MUSEUM_ADDITIONAL_FORM_PAGE;
    }

    //@RequestMapping(value = "museums/museumform/error", method = RequestMethod.GET)
    //public String museumErrorPage(Model model, @RequestParam("idinstitution") int idinstitution) {
    //    model.addAttribute("idinstitution", idinstitution);
    //    return PageConstants.MUSEUM_FORM_PAGE;
    //}

    @RequestMapping(value = "/museums/museum/additionalform", method = RequestMethod.POST)
    public String museumForm(@ModelAttribute("museumadditional") Museumadditional museumadditional, @RequestParam("idinstitution") int idinstitution, @RequestParam("idmuseum") int idmuseum, BindingResult bindingResult, final Model model, Locale locale) {

        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("idinstitution", idinstitution);
        Museum museum = mainService.findMuseum(idmuseum);
        model.addAttribute("title", messageSource.getMessage("museum.additional.title", new Object[]{museum.getName()}, locale));
        museumadditionalValidator.validate(museumadditional, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("idinstitution", idinstitution);
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            return PageConstants.MUSEUM_ADDITIONAL_FORM_PAGE;
        }
        museumadditional.setMuseum(museum);
        mainService.saveAdditionalMuseumInfo(museumadditional);
        model.addAttribute("museumadditional", museumadditional);

        model.addAttribute("successMessage", messageSource.getMessage("add.museumadditional.success", new Object[]{}, locale));
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        return PageConstants.MUSEUM_ADDITIONAL_FORM_PAGE;
    }

    @RequestMapping(value = "/museums/museumform", method = RequestMethod.GET)
    public String museumForm(final Model model, Locale locale, @RequestParam("idinstitution") int idinstitution) {
        Museum museum = new Museum();
        museum.setInstitution(mainService.findInstitution(idinstitution).getName());
        model.addAttribute("museum", museum);
        model.addAttribute("idinstitution", idinstitution);
        model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));

        return PageConstants.MUSEUM_FORM_PAGE;
    }

    @RequestMapping(value = "/museums/museumform", method = RequestMethod.POST)
    public String museumForm(@ModelAttribute("museum") Museum museum, @RequestParam("idinstitution") int idinstitution, @RequestParam(value = "ambientaudio", required = false) MultipartFile ambientaudio, BindingResult bindingResult,
                             @RequestParam(name="pickedimages", required = false) List<MultipartFile> images, @RequestParam(name = "pickedaudios", required = false) List<MultipartFile> audios, @RequestParam(name = "pickedvideos", required = false) List<MultipartFile> videos,
                             @RequestParam(name="pickedPrimaryImage", required = false) MultipartFile primaryImage,
                             @RequestParam(name="pickedSecondaryImage", required = false) MultipartFile secondaryImage,
                             final Model model, Locale locale, RedirectAttributes redirectAttributes) {

        Museum foundedMuseum = mainService.findMuseum(museum.getIdmuseum());
        museumValidator.validate(museum, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("idinstitution", idinstitution);
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            return PageConstants.MUSEUM_FORM_PAGE;
        }
        museum.setLanguage(locale.getLanguage());
        museum.setInstitutionObj(mainService.findInstitution(idinstitution));
        museum = mainService.saveMuseum(museum);
        museum = mainService.saveClientImages(primaryImage, secondaryImage, museum);
        museum = mainService.saveMultipartFiles(images, audios, videos, museum);
        museum = mainService.saveAmbientAudioOnMuseum(ambientaudio, museum);
        if (foundedMuseum == null) {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("add.museum.success", new Object[]{}, locale));
        } else {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.museum.success", new Object[]{}, locale));
        }
        return "redirect:/museums/museumform/update?idmuseum=" + museum.getIdmuseum() + "&idinstitution=" + idinstitution;
    }

    @RequestMapping(value = "/museums/museumform/update", method = RequestMethod.GET)
    public String museumFormUpdate(@RequestParam(value = "idmuseum") Integer id, @RequestParam(value = "idinstitution") int idinstitution, final Model model, Locale locale, @ModelAttribute("successMessage") String successMessage, HttpServletResponse response) {
        Museum museum = mainService.findMuseum(id);
        model.addAttribute("museum", museum);
        model.addAttribute("primaryClientImage", mainService.findPrimaryClientImageForMuseumId(museum.getIdmuseum()));
        model.addAttribute("secondaryClientImage", mainService.findSecondaryClientImageForMuseumId(museum.getIdmuseum()));
        model.addAttribute("idinstitution", idinstitution);
        model.addAttribute("successMessage", successMessage);
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));

        return PageConstants.DETAILS_MUSEUM_PAGE;
    }

    @RequestMapping(value = "/museums/museum/delete", method = RequestMethod.GET)
    public String deleteMuseum(@RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "idinstitution") Integer idinstitution, Model model, RedirectAttributes redirectAttributes, Locale locale) {
        if (museumValidator.canBeErased(idmuseum)) {
            mainService.deleteMuseum(idmuseum);
        } else {
            redirectAttributes.addAttribute("errorMessage", messageSource.getMessage("delete.museum.failure", new Object[]{}, locale));
        }
        return "redirect:/museums?idinstitution=" + idinstitution;
    }

    @RequestMapping(value = "/institutions/institutionform", method = RequestMethod.GET)
    public String institutionForm(final Model model, Locale locale) {
        Institution institution = new Institution();
        model.addAttribute("institution", institution);
        model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
        return PageConstants.INSTITUTION_FORM_PAGE;
    }

    @RequestMapping(value = "/institutions/institutionform", method = RequestMethod.POST)
    public String institutionForm(@ModelAttribute("institution") Institution institution, BindingResult bindingResult, final Model model, Locale locale, RedirectAttributes redirectAttributes) {
        Institution foundedInstitution = mainService.findInstitution(institution.getIdinstitution());
        institutionValidator.validate(institution, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            return PageConstants.INSTITUTION_FORM_PAGE;
        }
        mainService.saveInstitution(institution);
        if (foundedInstitution == null) {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("add.institution.success", new Object[]{}, locale));
        } else {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.institution.success", new Object[]{}, locale));
        }
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        return "redirect:/institutions/institutionform/update?idinstitution=" + institution.getIdinstitution();
    }

    @RequestMapping(value = "/institutions/institutionform/update", method = RequestMethod.GET)
    public String institutionFormUpdate(@RequestParam(value = "idinstitution") Integer id, final Model model, Locale locale, @ModelAttribute("successMessage") String successMessage) {
        Institution institution = mainService.findInstitution(id);
        model.addAttribute("institution", institution);
        Date foundationDate = institution.getFoundationdate();
        if (foundationDate != null) {
            model.addAttribute("foundationdate", mainService.transformDateFormat(foundationDate));
        }
        Date startDate = institution.getStartdate();
        if (startDate != null) {
            model.addAttribute("startdate", mainService.transformDateFormat(startDate));
        }
        model.addAttribute("successMessage", successMessage);
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        return PageConstants.DETAILS_INSTITUTION_PAGE;
    }

    @RequestMapping(value = "/institutions/institution/delete", method = RequestMethod.GET)
    public String deleteInstitution(@RequestParam(value = "idinstitution") Integer idinstitution, Locale locale, final RedirectAttributes redirectAttributes) {
        if (!institutionValidator.haveMuseums(idinstitution)) {
            mainService.deleteInstitution(idinstitution);
        } else {
            redirectAttributes.addAttribute("errorMessage", messageSource.getMessage("delete.institution.failure", new Object[]{}, locale));
        }
        return "redirect:/home";
    }

    @RequestMapping(value = "/museums/ambientaudio/delete/{idambientaudio}", method = RequestMethod.GET)
    public String deleteAmbientaudio(@PathVariable(value = "idambientaudio") int idambientaudio, @RequestParam(value = "idmuseum") int idmuseum, Locale locale, @RequestParam(value = "idinstitution") int idinstitution, RedirectAttributes redirectAttributes) {
        ambientaudioService.deleteAmbientAudio(idambientaudio);
        redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.museum.success", new Object[]{}, locale));
        return "redirect:/museums/museumform/update?idmuseum=" + idmuseum + "&idinstitution=" + idinstitution;
    }

    @RequestMapping(value = "/museums/image/delete/{iditemimage}", method = RequestMethod.POST)
    public String deleteMuseumImage(@PathVariable(value = "iditemimage") int iditemimage, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idinstitution") int idinstitution, Locale locale, RedirectAttributes redirectAttributes) {
        mainService.deleteMuseumImage(iditemimage);
        redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.museum.success", new Object[]{}, locale));
        return "redirect:/museums/museumform/update?idmuseum=" + idmuseum + "&idinstitution=" + idinstitution;
    }

    @RequestMapping(value = "/museums/audio/delete/{iditemaudio}", method = RequestMethod.GET)
    public String deleteMuseumAudio(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idinstitution") int idinstitution, Locale locale, RedirectAttributes redirectAttributes) {
        mainService.deleteMuseumAudio(iditemaudio);
        redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.museum.success", new Object[]{}, locale));
        return "redirect:/museums/museumform/update?idmuseum=" + idmuseum + "&idinstitution=" + idinstitution;
    }

    @RequestMapping(value = "/museums/video/delete/{iditemvideo}", method = RequestMethod.GET)
    public String deleteMuseumVideo(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idinstitution") int idinstitution, Locale locale, RedirectAttributes redirectAttributes) {
        mainService.deleteMuseumVideo(iditemvideo);
        redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.museum.success", new Object[]{}, locale));
        return "redirect:/museums/museumform/update?idmuseum=" + idmuseum + "&idinstitution=" + idinstitution;
    }
}
