package com.etf.master.controllers;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.etf.master.constants.PageConstants;
import com.etf.master.models.Floormap;
import com.etf.master.models.Item;
import com.etf.master.models.Museum;
import com.etf.master.models.Museummap;
import com.etf.master.models.Room;
import com.etf.master.services.AmbientaudioService;
import com.etf.master.services.MainService;
import com.etf.master.services.MapService;
import com.etf.master.services.ItemService;
import com.etf.master.validators.MapValidator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class MapController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MapValidator mapValidator;

    @Autowired
    private MapService mapService;

    @Autowired
    private MainService mainService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private AmbientaudioService ambientaudioService;

    @RequestMapping(value = "/floors", method = RequestMethod.GET)
    public String floorsPage(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model, @ModelAttribute("errorMessage") String errormessage) {
        List<Museummap> floors = mapService.findAllFloorsFromMuseum(idmuseum);
        model.addAttribute("floors", floors);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("errorMessage", errormessage);
        return PageConstants.FLOORS_PAGE;
    }

    @RequestMapping(value = "/floors/floorform", method = RequestMethod.GET)
    public String floorForm(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model, Locale locale) {
        Museummap museummap = new Museummap();
        Floormap floormap = new Floormap();
        museummap.setFloormap(floormap);
        model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
        model.addAttribute("museummap", museummap);
        model.addAttribute("idmuseum", idmuseum);
        return PageConstants.FLOOR_FORM_PAGE;
    }

    @RequestMapping(value = "/floors/floorform", method = RequestMethod.POST)
    public String floorForm(@ModelAttribute("museummap") Museummap museummap, @RequestParam(value = "idmuseum") Integer idmuseum, BindingResult bindingResult, final Model model, Locale locale, RedirectAttributes redirectAttributes) {
        Museummap foundedmuseummap = mapService.findMuseummap(museummap.getIdmuseummap());
        mapValidator.validate(museummap, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            model.addAttribute("idmuseum", idmuseum);
            return PageConstants.FLOOR_FORM_PAGE;
        }
        Museum museum = mainService.findMuseum(idmuseum);
        boolean isold = (foundedmuseummap != null);
        mapService.setAndSave(museummap, null, museum, locale, isold, true);

        if (foundedmuseummap == null) {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("add.floor.success", new Object[]{}, locale));
        } else {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.floor.success", new Object[]{}, locale));
        }
        return "redirect:/floors/floorform/update?idmuseum=" + idmuseum + "&idmuseummap=" + museummap.getIdmuseummap();
    }

    @RequestMapping(value = "/floors/floorform/update", method = RequestMethod.GET)
    public String floorFormUpdate(@RequestParam(value = "idmuseummap") Integer id, final Model model, Locale locale, @ModelAttribute("successMessage") String successMessage) {
        Museummap museummap = mapService.findMuseummap(id);
        model.addAttribute("museummap", museummap);

        int idmuseum = museummap.getMuseum().getIdmuseum();
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        model.addAttribute("successMessage", successMessage);
        return PageConstants.FLOOR_FORM_PAGE;
    }

    //todo :change url and rename
    @RequestMapping(value = "/floors/floor/set/additional", method = RequestMethod.POST)
    public String goToAdditionalPageFloor(@ModelAttribute(value = "museummap") Museummap museummap, @RequestParam(value = "idmuseummap") Integer idmuseummap, BindingResult bindingResult, @RequestParam("idmuseum") int idmuseum, Model model, Locale locale) {
        mapValidator.validate(museummap, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            model.addAttribute("museummap", museummap);
            model.addAttribute("idmuseum", idmuseum);
            model.addAttribute("idmuseummap", idmuseummap);
            return PageConstants.FLOOR_FORM_PAGE;
        }
        if (idmuseummap != 0) {
            museummap = mapService.findMuseummap(museummap.getIdmuseummap());
        }
        Museum museum = mainService.findMuseum(idmuseum);
        mapService.setAndSave(museummap, null, museum, locale, idmuseummap != 0, true);

        return "redirect:/floors/floor/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + museummap.getIdmuseummap();
    }

    @RequestMapping(value = "/floors/floor/set", method = RequestMethod.POST)
    public String setAdditionalFilesFloor(@RequestParam("idmuseummap") Integer idmuseummap, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam("images") MultipartFile[] images, @RequestParam("audios") MultipartFile[] audios, @RequestParam("videos") MultipartFile[] videos, HttpServletRequest request, @RequestParam(name = "ambientaudio", required = false) MultipartFile ambientaudio) {
        String[] additionalVideoUrls = request.getParameterValues("urlvideo");
        Museummap floor = mapService.findMuseummap(idmuseummap);
        if (ambientaudio != null) {
            if (!ambientaudio.isEmpty()) {
                mapService.saveAmbientAudioOnFloor(ambientaudio, idmuseum, idmuseummap);
            }
        }
        floor = mapService.saveMultipartFiles(images, audios, videos, floor, additionalVideoUrls);
        mapService.saveMuseummap(floor);
        return "redirect:/floors/floor/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap;
    }

    @RequestMapping(value = "/floors/floor/set/additional", method = RequestMethod.GET)
    public String goToAdditionalPageFloor(@ModelAttribute(value = "museummap") Museummap museummap, @RequestParam(value = "idmuseum") Integer idmuseum, Model model, Locale locale) {
        Museummap museummapFound = mapService.findMuseummap(museummap.getIdmuseummap());
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("idmuseummap", museummapFound.getIdmuseummap());
        model.addAttribute("museummap", museummapFound);
        model.addAttribute("mapimages", mapService.findallImagesFromMuseummap(museummap.getIdmuseummap()));
        model.addAttribute("mapaudios", mapService.findallAudiosFromMuseummap(museummap.getIdmuseummap()));
        model.addAttribute("mapvideos", mapService.findVideosWithoutOnlyUrlByMuseummapId(museummap.getIdmuseummap()));
        model.addAttribute("videoUrls", mapService.findVideosWithOnlyUrlByMuseummapId(museummap.getIdmuseummap()));
        model.addAttribute("successMessage", messageSource.getMessage("update.floor.success", new Object[]{}, locale));
        return PageConstants.FLOORS_ADDITIONAL;
    }

    @RequestMapping(value = "/maps/image/delete/{iditemimage}", method = RequestMethod.GET)
    public String deleteImageOnMap(@PathVariable(value = "iditemimage") int iditemimage, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(name = "idroom", required = false) Integer idroom) {
        Museummap map = mapService.findMuseummap(idmuseummap);
        mapService.deleteImage(iditemimage);
        if (map.getIsfloor()) {
            return "redirect:/floors/floor/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap;
        } else {
            return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;
        }
    }

    @RequestMapping(value = "/maps/audio/delete/{iditemaudio}", method = RequestMethod.GET)
    public String deleteAudioOnMap(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(name = "idroom", required = false) Integer idroom) {

        Museummap map = mapService.findMuseummap(idmuseummap);
        mapService.deleteAudio(iditemaudio);
        if (map.getIsfloor()) {
            return "redirect:/floors/floor/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap;
        } else {
            return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;
        }
    }

    @RequestMapping(value = "/maps/video/delete/{iditemvideo}", method = RequestMethod.GET)
    public String deleteVideoOnMap(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(name = "idroom", required = false) Integer idroom) {
        Museummap map = mapService.findMuseummap(idmuseummap);
        mapService.deleteVideo(iditemvideo);
        if (map.getIsfloor()) {
            return "redirect:/floors/floor/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap;
        } else {
            return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;
        }
    }

    @RequestMapping(value = "/maps", method = RequestMethod.GET)
    public String mapsPage(@ModelAttribute(value = "museummap") Museummap floor, @RequestParam(value = "idmuseum") Integer idmuseum, final Model model, Locale locale) {

        if (floor.getIdmuseummap() == 0) {
            Museum museum = mainService.findMuseum(idmuseum);
            mapService.setAndSave(floor, null, museum, locale, false, true);
        }
        List<Museummap> museummaps = mapService.findAllFromFloor(idmuseum, floor.getFloormap().getFloornumber());
        model.addAttribute("museummaps", museummaps);
        model.addAttribute("idmuseum", idmuseum);
        int floorNumber = floor.getFloormap().getFloornumber();
        model.addAttribute("parentId", floor.getIdmuseummap());
        model.addAttribute("floorNumber", floorNumber);
        model.addAttribute("title", messageSource.getMessage("maps.list", new Object[]{floorNumber}, locale));
        return PageConstants.MAPS_PAGE;
    }

    @RequestMapping(value = "/maps/mapform", method = RequestMethod.GET)
    public String mapForm(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model, Locale locale, HttpServletRequest httpServletRequest) {
        String[] floorNumbers = httpServletRequest.getParameterValues("floorNumber");
        int floorNumber = Integer.parseInt(floorNumbers[0]);

        String parentIdString = httpServletRequest.getParameter("parentId");
        int parentId = Integer.parseInt(parentIdString);

        Museummap museummap = new Museummap();
        Floormap floormap = new Floormap();
        Museummap parent = mapService.findMuseummap(parentId);
        floormap.setZcoordinate(parent.getFloormap().getZcoordinate());

        floormap.setFloornumber(floorNumber);
        Room room = new Room();
        museummap.setFloormap(floormap);
        model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
        model.addAttribute("museummap", museummap);
        model.addAttribute("room", room);
        model.addAttribute("idmuseum", idmuseum);

        return PageConstants.MAP_FORM_PAGE;
    }

    @RequestMapping(value = "/maps/mapform", method = RequestMethod.POST)
    public String mapForm(@ModelAttribute("museummap") Museummap museummap, @ModelAttribute("room") Room room, @RequestParam(value = "idmuseum") Integer idmuseum, BindingResult bindingResult, final Model model, Locale locale, RedirectAttributes redirectAttributes) {

        Museummap foundedmuseummap = mapService.findMuseummap(museummap.getIdmuseummap());
        mapValidator.validate(museummap, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            model.addAttribute("idmuseum", idmuseum);
            return PageConstants.MAP_FORM_PAGE;
        }
        Museum museum = mainService.findMuseum(idmuseum);
        boolean isold = (foundedmuseummap != null);
        mapService.setAndSave(museummap, room, museum, locale, isold, false);
        if (foundedmuseummap == null) {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("add.room.success", new Object[]{}, locale));
        } else {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.room.success", new Object[]{}, locale));
        }
        return "redirect:/maps/mapform/update?idmuseum=" + idmuseum + "&idmuseummap=" + museummap.getIdmuseummap();
    }

    @RequestMapping(value = "/maps/mapform/update", method = RequestMethod.GET)
    public String mapFormUpdate(@RequestParam(value = "idmuseummap") Integer id, final Model model, Locale locale, @ModelAttribute("successMessage") String successmessage) {
        Museummap museummap = mapService.findMuseummap(id);
        Floormap floormap = museummap.getFloormap();
        Room room = floormap.getRoom();
        model.addAttribute("room", room);
        model.addAttribute("museummap", museummap);

        int idmuseum = museummap.getMuseum().getIdmuseum();
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        model.addAttribute("successMessage", successmessage);

        return PageConstants.MAP_FORM_PAGE;
    }

    @RequestMapping(value = "/maps/map/delete", method = RequestMethod.GET)
    public String deleteMap(@RequestParam(value = "idmuseummap") Integer idmuseummap, @RequestParam(value = "idmuseum") Integer idmuseum, final RedirectAttributes redirectAttributes, Locale locale, Model model) {
        Museummap museummap = mapService.findMuseummap(idmuseummap);
        Boolean isfloor = museummap.getIsfloor();
        if (isfloor) {
            List<Museummap> roomsmap = mapService.findAllFromFloor(idmuseum, museummap.getFloormap().getFloornumber());
            if (!roomsmap.isEmpty()) {
                redirectAttributes.addAttribute("errorMessage", messageSource.getMessage("cant.erase.floor", new Object[]{}, locale));
            } else {
                Floormap floormap = museummap.getFloormap();
                if (floormap != null) {
                    Room room = floormap.getRoom();
                    if (room != null) {
                        mapService.deleteRoom(room.getIdroom());
                    }
                    mapService.deleteFloormap(floormap.getIdfloormap());
                }

                mapService.deleteMuseummap(idmuseummap);
            }
            return "redirect:/floors?idmuseum=" + idmuseum;
        }
        Floormap floormap = museummap.getFloormap();
        Museummap floor = mapService.findFloorByRoomFloorNumber(idmuseum, floormap.getFloornumber());
        if (floormap != null) {
            int idroom = floormap.getRoom().getIdroom();
            mapService.deleteRoom(idroom);
            mapService.deleteFloormap(floormap.getIdfloormap());
        }
        mapService.deleteMuseummap(idmuseummap);

        List<Museummap> museummaps = mapService.findAllFromFloor(idmuseum, floor.getFloormap().getFloornumber());
        model.addAttribute("museummaps", museummaps);
        model.addAttribute("idmuseum", idmuseum);
        int floorNumber = floor.getFloormap().getFloornumber();
        model.addAttribute("floorNumber", floorNumber);
        model.addAttribute("title", messageSource.getMessage("maps.list", new Object[]{floorNumber}, locale));
        return PageConstants.MAPS_PAGE;
    }

    @RequestMapping(value = "/maps/roomsfromanothersource", method = RequestMethod.GET)
    public String itemFromExhibition(@RequestParam(value = "idmuseum") Integer idmuseum) {
        return "redirect:/maps/mapform?idmuseum=" + idmuseum;
    }

    @RequestMapping(value = "/maps/map/set", method = RequestMethod.GET)
    public String goToAdditionalPage(@ModelAttribute(value = "museummap") Museummap museummap, BindingResult bindingResult, @ModelAttribute("room") Room room, @RequestParam(value = "idmuseummap") Integer idmuseummap, @RequestParam(value = "idroom") int idroom, @RequestParam("idmuseum") int idmuseum, Model model, Locale locale) {
        mapValidator.validate(museummap, bindingResult);
        if (bindingResult.hasErrors()) {
            Floormap floormap = new Floormap();
            room = new Room();
            museummap.setFloormap(floormap);
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            model.addAttribute("museummap", museummap);
            model.addAttribute("room", room);
            model.addAttribute("idmuseum", idmuseum);
            model.addAttribute("idmuseummap", idmuseummap);
            return PageConstants.MAP_FORM_PAGE;
        }
        if (idmuseummap != 0) {
            museummap = mapService.findMuseummap(museummap.getIdmuseummap());
        }
        Museum museum = mainService.findMuseum(idmuseum);
        //check of need to go through this if its old one
        mapService.setAndSave(museummap, room, museum, locale, idmuseummap != 0, false);
        return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + museummap.getIdmuseummap() + "&idroom=" + room.getIdroom();
    }

    @RequestMapping(value = "/maps/map/set/additional", method = RequestMethod.GET)
    public String setAdditionalOnMap(@RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "idroom") Integer idroom, @RequestParam(value = "idmuseum") Integer idmuseum, Model model, Locale locale) {

        List<Item> mapItems = mapService.findItemsByMuseummapId(idmuseummap);
        model.addAttribute("mapitems", mapItems);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        model.addAttribute("jsonItems", gson.toJson(itemService.convertMapItems(mapItems)));

        List<Room> mapRooms = mapService.findConnectedRooms(idroom);
        model.addAttribute("maprooms", mapRooms);
        List<Room> rooms = mapService.findAllRoomsFromMuseum(idmuseum);
        rooms.remove(mapService.findRoom(idroom));
        model.addAttribute("roomstoset", rooms);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("idmuseummap", idmuseummap);
        model.addAttribute("idroom", idroom);
        model.addAttribute("room", mapService.findRoom(idroom));
        Museummap map = mapService.findMuseummap(idmuseummap);
        model.addAttribute("museummap", map);
        model.addAttribute("floormap", mapService.findFloorByRoomFloorNumber(idmuseum, map.getFloormap().getFloornumber()));
        model.addAttribute("mapimages", mapService.findallImagesFromMuseummap(idmuseummap));
        model.addAttribute("mapaudios", mapService.findallAudiosFromMuseummap(idmuseummap));
        model.addAttribute("mapvideos", mapService.findVideosWithoutOnlyUrlByMuseummapId(idmuseummap));
        model.addAttribute("videoUrls", mapService.findVideosWithOnlyUrlByMuseummapId(idmuseummap));
        model.addAttribute("successMessage", messageSource.getMessage("update.room.success", new Object[]{}, locale));

        return PageConstants.MAP_ADDITIONAL;
    }

    @RequestMapping(value = "/maps/map/set", method = RequestMethod.POST)
    public String setAdditionalFilesMap(@RequestParam("idmuseummap") Integer idmuseummap, @RequestParam(value = "idroom") int idroom, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam("images") MultipartFile[] images, @RequestParam("audios") MultipartFile[] audios, @RequestParam("videos") MultipartFile[] videos, @RequestParam(name = "ambientaudio", required = false) MultipartFile ambientaudio, HttpServletRequest request) {
        String[] additionalVideoUrls = request.getParameterValues("urlvideo");
        Museummap map = mapService.findMuseummap(idmuseummap);
        map = mapService.saveMultipartFiles(images, audios, videos, map, additionalVideoUrls);
        if (ambientaudio != null) {
            if (!ambientaudio.isEmpty()) {
                mapService.saveAmbientAudio(ambientaudio, idmuseum, idroom);
                mapService.saveMuseummap(map);
            }
        }
        return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;
    }

    @Transactional
    @RequestMapping(value = "/maps/map/set/additional", method = RequestMethod.POST)
    public String setAdditionalOnMap(@RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "idroom") int idroom, @RequestParam(value = "idmuseum") Integer idmuseum, HttpServletRequest request) {
        Room room = mapService.findRoom(idroom);
        Museummap museummap = mapService.findMuseummap(idmuseummap);
        String[] selectedItemids = request.getParameterValues("selectedItems");
        String[] selectedRoomsids = request.getParameterValues("selectedRooms");
        mapService.setItems(selectedItemids, museummap, room);
        mapService.setRooms(selectedRoomsids, room);

        return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;
    }

    @Transactional
    @RequestMapping(value = "/maps/map/set/active", method = RequestMethod.POST)
    public String setActiveMap(@RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "idroom") int idroom, @RequestParam(value = "idmuseum") Integer idmuseum, HttpServletRequest request) {

        List<Item> itemsOnMap = mapService.findItemsByMuseummapId(idmuseummap);
        for (Item p : itemsOnMap) {
            mapService.removeItemMapRelation(idmuseummap, p.getIditem());
        }

        //maybe remove beacons
        //List<Beacon> beaconsOnMap = mapService.findAllBeaconsByMuseummapId(idmuseummap);
        //for (Beacon b : beaconsOnMap) {
        //    mapService.removeBeaconMapRelation(idmuseummap, b.getIdbeacon());
        //}
        String selectedImageId = request.getParameter("selectedMap");
        Museummap museummap = mapService.findMuseummap(idmuseummap);
        Floormap floormap = museummap.getFloormap();
        floormap.setImageurl(itemService.findImage(Integer.parseInt(selectedImageId)).getImageurl());
        museummap.setFloormap(floormap);
        mapService.saveFloorMap(floormap);
        mapService.saveMuseummap(museummap);

        return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;
    }

    @RequestMapping(value = "/maps/delete/item", method = RequestMethod.GET)
    public String deleteItemFromMap(@RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "idroom") Integer idroom, @RequestParam(value = "iditem") Integer iditem) {
        mapService.removeItemMapRelation(idmuseummap, iditem);
        return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;
    }

    @RequestMapping(value = "/maps/delete/room", method = RequestMethod.GET)
    public String deleteRoomFromRoom(@RequestParam(value = "idroom1") int idroom1, @RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "idroom2") Integer idroom2) {
        mapService.removeRoomRelation(idroom1, idroom2);
        return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom1;
    }

    @RequestMapping(value = "/maps/goto/mapform", method = RequestMethod.GET)
    public String goToExhibitionForm(@RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "idmuseum") int idmuseum, final Model model) {
        model.addAttribute("idexhibition", idmuseummap);
        model.addAttribute("idmuseum", idmuseum);
        return "redirect:/maps/mapform/update?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap;
    }

    @RequestMapping(value = "/maps/set/ambientaudio", method = RequestMethod.POST)
    public String setAmbientaudio(@RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idroom") int idroom, @RequestParam(value = "idmuseummap") int idmuseummap, @RequestParam(value = "ambientaudio") MultipartFile ambientaudio) {
        //here set and save ambient audio, also provide it to page
        mapService.saveAmbientAudio(ambientaudio, idmuseum, idroom);
        return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;

    }

    @RequestMapping(value = "/maps/ambientaudio/delete/{idambientaudio}", method = RequestMethod.GET)
    public String deleteAmbientaudioOnMap(@PathVariable(value = "idambientaudio") int idambientaudio, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idroom") int idroom, @RequestParam(value = "idmuseummap") int idmuseummap) {
        ambientaudioService.deleteAmbientAudio(idambientaudio);
        return "redirect:/maps/map/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap + "&idroom=" + idroom;
    }

    @RequestMapping(value = "/floors/ambientaudio/delete/{idambientaudio}", method = RequestMethod.GET)
    public String deleteAmbientaudioOnFloor(@PathVariable(value = "idambientaudio") int idambientaudio, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idmuseummap") int idmuseummap) {
        ambientaudioService.deleteAmbientAudio(idambientaudio);
        return "redirect:/floors/floor/set/additional?idmuseum=" + idmuseum + "&idmuseummap=" + idmuseummap;
    }
}
