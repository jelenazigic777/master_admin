package com.etf.master.controllers;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.etf.master.constants.PageConstants;
import com.etf.master.models.Floormap;
import com.etf.master.models.Item;
import com.etf.master.models.Museummap;
import com.etf.master.services.MapService;
import com.etf.master.services.ItemService;
import com.etf.master.validators.ItemValidator;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
@RequestMapping("/items")
public class ItemController {
    @Autowired
    private ItemService itemService;

    @Autowired
    private MapService mapService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ItemValidator itemValidator;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String itemsPage(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model, Locale locale) {

        List<Item> items = itemService.findAllActiveItemsFromMuseum(idmuseum);
        model.addAttribute("items", items);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("isActive", true);
        model.addAttribute("activeInactiveButtonText", messageSource.getMessage("go.to.inactive.items", new Object[]{}, locale));
        return PageConstants.ITEMS_MUSEUM;
    }

    @RequestMapping(value = "/inactive", method = RequestMethod.GET)
    public String itemsPageInactive(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model, Locale locale) {

        List<Item> items = itemService.findAllInactiveItemsFromMuseum(idmuseum);
        model.addAttribute("items", items);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("isActive", false);
        model.addAttribute("activeInactiveButtonText", messageSource.getMessage("go.to.active.items", new Object[]{}, locale));
        return PageConstants.ITEMS_MUSEUM;
    }

    @RequestMapping(value = "/itemfromanothersource", method = RequestMethod.GET)
    public String itemFromExhibition(@RequestParam(value = "idmuseum") Integer idmuseum) {
        return "redirect:/items/itemform?idmuseum=" + idmuseum;
    }

    @RequestMapping(value = "/itemform", method = RequestMethod.GET)
    public String itemForm(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model, Locale locale) {

        Item item = new Item();
        model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
        model.addAttribute("item", item);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("floormaps", mapService.findAllActiveFloormapsFromMuseum(idmuseum));
        return PageConstants.ITEM_FORM_PAGE;
    }

    @RequestMapping(value = "/itemform", method = RequestMethod.POST)
    public String itemForm(@ModelAttribute("item") Item item, @RequestParam(value = "idmuseum") Integer idmuseum, BindingResult bindingResult, RedirectAttributes redirectAttributes, Locale locale, Model model) {

        Item foundeditem = itemService.findItem(item.getIditem());
        itemValidator.validate(item, bindingResult);
        if (bindingResult.hasErrors()) {
            if (foundeditem == null) {
                model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            } else {
                model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
            }
            model.addAttribute("floormaps", mapService.findAllActiveFloormapsFromMuseum(idmuseum));
            model.addAttribute("idmuseum", idmuseum);
            return PageConstants.ITEM_FORM_PAGE;
        }
        if (foundeditem != null) {
            item.setMuseummap(foundeditem.getMuseummap());
        }
        itemService.basicSave(item, idmuseum, locale);

        if (foundeditem == null) {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("add.item.success", new Object[]{}, locale));
        } else {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.item.success", new Object[]{}, locale));
        }
        return "redirect:/items/itemform/update?idmuseum=" + idmuseum + "&iditem=" + item.getIditem();
    }

    @RequestMapping(value = "/itemform/update", method = RequestMethod.GET)
    public String itemFormUpdate(@RequestParam(value = "iditem") Integer id, final Model model, Locale locale, @ModelAttribute("successMessage") String successMessage) {
        Item item = itemService.findItem(id);
        model.addAttribute("item", item);
        model.addAttribute("idmuseum", item.getMuseum().getIdmuseum());
        model.addAttribute("successMessage", successMessage);
        model.addAttribute("museummap", item.getMuseummap());
        if (item.getMuseummap() != null) {
            model.addAttribute("floormap",
                               mapService.findFloorByRoomFloorNumber(item.getMuseum().getIdmuseum(), item.getMuseummap().getFloormap().getFloornumber()));
        }
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        model.addAttribute("floormaps", mapService.findAllActiveFloormapsFromMuseum(item.getMuseum().getIdmuseum()));

        if (item.getMuseummap() != null) {
            List<Item> items = itemService.findAllFromMuseummap(item.getMuseummap().getIdmuseummap());
            items.remove(item);
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            model.addAttribute("otherItems", gson.toJson(itemService.convertItems(items)));
        }

        return PageConstants.ITEM_FORM_PAGE;
    }

    @RequestMapping(value = "/item/delete", method = RequestMethod.GET)
    public String deleteItem(@RequestParam(value = "iditem") Integer iditem, @RequestParam(value = "idmuseum") Integer idmuseum, Model model) {
        itemService.deleteItem(iditem);
        return "redirect:/items?idmuseum=" + idmuseum;
    }

    @RequestMapping(value = "/item/set/additional", method = RequestMethod.GET)
    public String goToAdditionalPage(@ModelAttribute("item") Item item, @RequestParam(value = "idmuseum") Integer idmuseum, final Model model) {
        Item itemFound = itemService.findItem(item.getIditem());
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("iditem", item.getIditem());
        model.addAttribute("item", itemFound);
        model.addAttribute("itemimages", itemFound.getImages());
        model.addAttribute("itemaudios", itemFound.getAudios());
        model.addAttribute("itemvideos", itemService.findVideosWithoutOnlyUrlByItemId(itemFound.getIditem()));
        model.addAttribute("itemtexts", itemFound.getTexts());
        model.addAttribute("barcode", itemFound.getBarcodeimage());
        model.addAttribute("videoUrls", itemService.findVideosWithOnlyUrlByItemId(item.getIditem()));
        return PageConstants.ITEM_ADDITIONAL;
    }

    @RequestMapping(value = "/item/set/additional", method = RequestMethod.POST)
    public String goToAdditionalPage(@ModelAttribute("item") Item item, @RequestParam(value = "idmuseum") Integer idmuseum, BindingResult bindingResult, final Model model, Locale locale) {

        itemValidator.validate(item, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("idmuseum", idmuseum);
            model.addAttribute("floormaps", mapService.findAllActiveFloormapsFromMuseum(idmuseum));
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            return PageConstants.ITEM_FORM_PAGE;
        }
        if (item.getIditem() == 0) {
            itemService.basicSave(item, idmuseum, locale);
        } else {
            itemService.updateExistingItem(item);
        }
        return "redirect:/items/item/set/additional?idmuseum=" + idmuseum + "&iditem=" + item.getIditem();
    }

    @RequestMapping(value = "/item/set", method = RequestMethod.POST)
    public String setAdditional(@RequestParam("iditem") Integer iditem, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "images", required = false) MultipartFile[] images, @RequestParam(value = "barcode", required = false) MultipartFile barcode, @RequestParam(value = "audios", required = false) MultipartFile[] audios, @RequestParam(value = "videos", required = false) MultipartFile[] videos, HttpServletRequest request) {
        String[] additionalVideoUrls = request.getParameterValues("urlvideo");
        String[] texts = request.getParameterValues("text");
        Item item = itemService.findItem(iditem);

        item = itemService.saveMultipartFiles(images, audios, videos, texts, item, additionalVideoUrls, barcode);
        itemService.saveItem(item);
        return "redirect:/items/item/set/additional?idmuseum=" + idmuseum + "&iditem=" + iditem;
    }

    @RequestMapping(value = "/image/delete/{iditemimage}", method = RequestMethod.GET)
    public String deleteImageOnItem(@PathVariable(value = "iditemimage") int iditemimage, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") Integer idmuseum) {

        itemService.deleteImage(iditemimage);
        return "redirect:/items/item/set/additional?idmuseum=" + idmuseum + "&iditem=" + iditem;
    }

    @RequestMapping(value = "/audio/delete/{iditemaudio}", method = RequestMethod.GET)
    public String deleteAudioOnItem(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") Integer idmuseum) {

        itemService.deleteAudio(iditemaudio);
        return "redirect:/items/item/set/additional?idmuseum=" + idmuseum + "&iditem=" + iditem;
    }

    @RequestMapping(value = "/video/delete/{iditemvideo}", method = RequestMethod.GET)
    public String deleteVideoOnItem(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") Integer idmuseum) {

        itemService.deleteVideo(iditemvideo);
        return "redirect:/items/item/set/additional?idmuseum=" + idmuseum + "&iditem=" + iditem;
    }

    @RequestMapping(value = "/text/delete/{iditemtext}", method = RequestMethod.GET)
    public String deleteTextOnItem(@PathVariable(value = "iditemtext") int iditemtext, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") Integer idmuseum) {

        itemService.deleteText(iditemtext);
        return "redirect:/items/item/set/additional?idmuseum=" + idmuseum + "&iditem=" + iditem;
    }

    @RequestMapping(value = "/barcode/delete/{idbarcode}", method = RequestMethod.GET)
    public String deleteBarcodeOnItem(@PathVariable(value = "idbarcode") int idbarcode, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") Integer idmuseum) {

        itemService.deleteBarcode(idbarcode, iditem);
        return "redirect:/items/item/set/additional?idmuseum=" + idmuseum + "&iditem=" + iditem;
    }

    @Transactional
    @RequestMapping(value = "/set/map", method = RequestMethod.POST)
    public String setActiveMap(@RequestParam(value = "iditem") int iditem, @ModelAttribute("item") Item item, @RequestParam(value = "idmuseum") int idmuseum, HttpServletRequest request, Locale locale) {
        String selectedFloormapId = request.getParameter("selectedFloormap");

        Floormap floormap = mapService.findFloormap(Integer.valueOf(selectedFloormapId));
        Museummap museummap = floormap.getMuseummap();
        item.setMuseummap(museummap);
        item.setZcoordinate(museummap.getZcoordinate());
        item = itemService.basicSave(item, idmuseum, locale);
        iditem = item.getIditem();
        return "redirect:/items/itemform/update?idmuseum=" + idmuseum + "&iditem=" + iditem;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchItems(@RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "name") String name, @RequestParam(value = "author") String author, @RequestParam(value = "code") String code,
                                 @RequestParam(value = "exhibitionName") String exhibitionName,
                                 @RequestParam(value = "roomName") String roomName,
                                 @RequestParam(value = "xCoordinate") Double xCoordinate,
                                 @RequestParam(value = "yCoordinate") Double yCoordinate,
                                 @RequestParam(value = "zCoordinate") Double zCoordinate,
                                 @RequestParam(value = "deviation") Double deviation,
                                 RedirectAttributes redirectAttributes, Model model, Locale locale) {

        List<Item> items = itemService.findItems(name, author, code, exhibitionName, xCoordinate, yCoordinate, zCoordinate, deviation, roomName);
        model.addAttribute("items", items);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("isActive", true);
        model.addAttribute("activeInactiveButtonText", messageSource.getMessage("go.to.inactive.items", new Object[]{}, locale));
        return PageConstants.ITEMS_MUSEUM;
    }
}
