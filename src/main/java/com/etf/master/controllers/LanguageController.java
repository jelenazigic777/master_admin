package com.etf.master.controllers;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.etf.master.constants.PageConstants;
import com.etf.master.dtos.ExhibitionlanguagesResponseDTO;
import com.etf.master.dtos.ItemlanguagesResponseDTO;
import com.etf.master.dtos.MuseumlanguagesResponseDTO;
import com.etf.master.dtos.MuseummaplanguagesResponseDTO;
import com.etf.master.models.Exhibitionlanguages;
import com.etf.master.models.Itemlanguages;
import com.etf.master.models.Languages;
import com.etf.master.models.Museumlanguages;
import com.etf.master.models.Museummaplanguages;
import com.etf.master.services.ExhibitionService;
import com.etf.master.services.ItemService;
import com.etf.master.services.LanguageService;
import com.etf.master.services.MainService;
import com.etf.master.services.MapService;
import com.etf.master.validators.MuseumValidator;

@Controller
@RequestMapping("/languages")
public class LanguageController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    LanguageService languageService;

    @Autowired
    MainService mainService;

    @Autowired
    ItemService itemService;

    @Autowired
    ExhibitionService exhibitionService;

    @Autowired
    MapService mapService;

    @Autowired
    MuseumValidator museumValidator;

    @RequestMapping(value = "/editor", method = RequestMethod.GET)
    public String getLanguagePage(Model model) {
        model.addAttribute("languages", languageService.loadAllLanguges());
        return PageConstants.LANGUAGES_PAGE;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addNewLanguage(@RequestParam("language") String languageName, @RequestParam("languageAbbreviation") String languageAbbreviation, Model model) {
        languageService.saveLanguage(languageName, languageAbbreviation);
        return "redirect:/languages/editor";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteLanguage(@RequestParam("idlanguage") Integer idlanguage) {
        languageService.deleteLanguage(idlanguage);
        return "redirect:/languages/editor";
    }

    @RequestMapping(value = "/museum", method = RequestMethod.GET)
    public String openMuseumLanguagePage(@RequestParam("idmuseum") int idmuseum, Model model, Locale locale) {
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("languages", languageService.loadAllLanguges());
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        Languages language = languageService.loadAllLanguges().get(0);
        Museumlanguages museumlanguages = languageService.findMuseumLanguageByLanguageAndMuseum(language.getIdlanguages(), idmuseum);
        if (museumlanguages == null) {
            museumlanguages = new Museumlanguages();
        }
        model.addAttribute("museumlanguages", museumlanguages);
        model.addAttribute("museum", mainService.findMuseum(idmuseum));
        return PageConstants.MUSEUM_LANGUAGES_PAGE;
    }

    @RequestMapping(value = "/museum", method = RequestMethod.POST)
    public String openMuseumLanguagePage(@ModelAttribute("museumlanguages") Museumlanguages museumlanguages, BindingResult bindingResult, @RequestParam("idmuseum") int idmuseum, Model model, Locale locale, HttpServletRequest request, @RequestParam(name = "pickedaudios", required = false) List<MultipartFile> audios, @RequestParam(name = "pickedvideos", required = false) List<MultipartFile> videos) {
        museumValidator.validateLanguage(museumlanguages, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("idmuseum", idmuseum);
            model.addAttribute("languages", languageService.loadAllLanguges());
            model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
            return PageConstants.MUSEUM_LANGUAGES_PAGE;
        }
        String[] selectedLanguages = request.getParameterValues("selectedLanguage");
        if (!selectedLanguages[0].equals("0")) {
            languageService.saveMuseumLanguage(museumlanguages, idmuseum, Integer.parseInt(selectedLanguages[0]), audios, videos);
        }
        return "redirect:/languages/museum?idmuseum=" + idmuseum;
    }

    @RequestMapping(value = "/museum/language/{idlanguage}/{idmuseum}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public MuseumlanguagesResponseDTO getMuseumLanguageData(@PathVariable(value = "idlanguage") int idlanguage, @PathVariable(value = "idmuseum") int idmuseum) {
        Museumlanguages museumlanguages = languageService.findMuseumLanguageByLanguageAndMuseum(idlanguage, idmuseum);
        if (museumlanguages == null) {
            museumlanguages = new Museumlanguages();
        }
        MuseumlanguagesResponseDTO responseDTO = new MuseumlanguagesResponseDTO();
        responseDTO.setIdmuseumlanguages(museumlanguages.getIdmuseumlanguages());
        responseDTO.setName(museumlanguages.getName());
        responseDTO.setText(museumlanguages.getText());
        responseDTO.setTexturl(museumlanguages.getTexturl());
        return responseDTO;
    }

    @RequestMapping(value = "/museum/audio/delete/{iditemaudio}", method = RequestMethod.GET)
    public String deleteMuseumAudio(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam("idmuseum") int idmuseum, @RequestParam("idmuseumlanguages") int idmuseumlanguages, Model model, Locale locale) {
        languageService.deleteAudioLanguage(iditemaudio);
        return "redirect:/languages/museum?idmuseum=" + idmuseum;
    }

    @RequestMapping(value = "/museum/video/delete/{iditemvideo}", method = RequestMethod.GET)
    public String deleteMuseumVideo(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam("idmuseum") int idmuseum, @RequestParam("idmuseumlanguages") int idmuseumlanguages, Model model, Locale locale) {
        languageService.deleteVideoLanguage(iditemvideo);
        return "redirect:/languages/museum?idmuseum=" + idmuseum;
    }

    @RequestMapping(value = "/item", method = RequestMethod.GET)
    public String openItemLanguagePage(@RequestParam("iditem") int iditem, Model model, Locale locale) {
        model.addAttribute("iditem", iditem);
        model.addAttribute("languages", languageService.loadAllLanguges());
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        Languages language = languageService.loadAllLanguges().get(0);
        Itemlanguages itemlanguages = languageService.findItemLanguageByLanguageAndItem(language.getIdlanguages(), iditem);

        if (itemlanguages == null) {
            itemlanguages = new Itemlanguages();
        }
        model.addAttribute("itemlanguages", itemlanguages);
        model.addAttribute("item", itemService.findItem(iditem));
        return PageConstants.ITEM_LANGUAGES_PAGE;
    }

    @RequestMapping(value = "/item", method = RequestMethod.POST)
    public String openItemLanguagePage(@ModelAttribute("itemlanguages") Itemlanguages itemlanguages, BindingResult bindingResult,
                                          @RequestParam("iditem")
                                            int iditem, Model model, Locale locale, HttpServletRequest request, @RequestParam(name = "pickedaudios", required = false) List<MultipartFile> audios, @RequestParam(name = "pickedvideos", required = false) List<MultipartFile> videos) {
        museumValidator.validateLanguage(itemlanguages, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("iditem", iditem);
            model.addAttribute("languages", languageService.loadAllLanguges());
            model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
            return PageConstants.ITEM_LANGUAGES_PAGE;
        }
        String[] selectedLanguages = request.getParameterValues("selectedLanguage");
        if (!selectedLanguages[0].equals("0")) {
            languageService.saveItemLanguage(itemlanguages, iditem, Integer.parseInt(selectedLanguages[0]), audios, videos);
        }
        return "redirect:/languages/item?iditem=" + iditem;
    }

    @RequestMapping(value = "/item/audio/delete/{iditemaudio}", method = RequestMethod.GET)
    public String deleteItemAudio(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam("iditem") int iditem, @RequestParam("iditemlanguages") int iditemlanguages, Model model, Locale locale) {
        languageService.deleteAudioLanguage(iditemaudio);
        return "redirect:/languages/item?iditem=" + iditem;
    }

    @RequestMapping(value = "/item/video/delete/{iditemvideo}", method = RequestMethod.GET)
    public String deleteItemVideo(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam("iditem") int iditem, @RequestParam("iditemlanguages") int iditemlanguages, Model model, Locale locale) {
        languageService.deleteVideoLanguage(iditemvideo);
        return "redirect:/languages/item?iditem=" + iditem;
    }

    @RequestMapping(value = "/item/language/{idlanguage}/{iditem}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ItemlanguagesResponseDTO getItemLanguageData(
      @PathVariable(value = "idlanguage")
        int idlanguage,
      @PathVariable(value = "iditem")
        int iditem) {
        Itemlanguages itemlanguages = languageService.findItemLanguageByLanguageAndItem(idlanguage, iditem);
        if (itemlanguages == null) {
            itemlanguages = new Itemlanguages();
        }
        ItemlanguagesResponseDTO responseDTO = new ItemlanguagesResponseDTO();
        responseDTO.setIditemlanguages(itemlanguages.getIditemlanguages());
        responseDTO.setName(itemlanguages.getName());
        responseDTO.setText(itemlanguages.getText());
        responseDTO.setAuthor(itemlanguages.getAuthor());
        responseDTO.setTexturl(itemlanguages.getTexturl());
        return responseDTO;
    }

    //exhibitionmuseum
    @RequestMapping(value = "/exhibition", method = RequestMethod.GET)
    public String openExhibitionLanguagePage(
      @RequestParam("idexhibition")
        int idexhibition, Model model, Locale locale) {
        model.addAttribute("idexhibition", idexhibition);
        model.addAttribute("languages", languageService.loadAllLanguges());
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        Languages language = languageService.loadAllLanguges().get(0);
        Exhibitionlanguages exhibitionlanguages = languageService.findExhibitionLanguageByLanguageAndExhibition(language.getIdlanguages(), idexhibition);
        if (exhibitionlanguages == null) {
            exhibitionlanguages = new Exhibitionlanguages();
        }
        model.addAttribute("exhibitionlanguages", exhibitionlanguages);
        model.addAttribute("exhibition", exhibitionService.findExhibition(idexhibition));
        return PageConstants.EXHIBITION_LANGUAGES_PAGE;
    }

    @RequestMapping(value = "/exhibition", method = RequestMethod.POST)
    public String openExhibitionLanguagePage(@ModelAttribute("exhibitionlanguages") Exhibitionlanguages exhibitionlanguages, BindingResult bindingResult,
                                          @RequestParam("idexhibition")
                                            int idexhibition, Model model, Locale locale, HttpServletRequest request, @RequestParam(name = "pickedaudios", required = false) List<MultipartFile> audios, @RequestParam(name = "pickedvideos", required = false) List<MultipartFile> videos) {
        museumValidator.validateLanguage(exhibitionlanguages, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("idexhibition", idexhibition);
            model.addAttribute("languages", languageService.loadAllLanguges());
            model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
            return PageConstants.EXHIBITION_LANGUAGES_PAGE;
        }
        String[] selectedLanguages = request.getParameterValues("selectedLanguage");
        if (!selectedLanguages[0].equals("0")) {
            languageService.saveExhibitionLanguage(exhibitionlanguages, idexhibition, Integer.parseInt(selectedLanguages[0]), audios, videos);
        }
        return "redirect:/languages/exhibition?idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/exhibition/audio/delete/{iditemaudio}", method = RequestMethod.GET)
    public String deleteExhibitionAudio(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam("idexhibition") int idexhibition, @RequestParam("idexhibitionlanguages") int idexhibitionlanguages, Model model, Locale locale) {
        languageService.deleteAudioLanguage(iditemaudio);
        return "redirect:/languages/exhibition?idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/exhibition/video/delete/{iditemvideo}", method = RequestMethod.GET)
    public String deleteExhibitionVideo(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam("idexhibition") int idexhibition, @RequestParam("idexhibitionlanguages") int idexhibitionlanguages, Model model, Locale locale) {
        languageService.deleteVideoLanguage(iditemvideo);
        return "redirect:/languages/exhibition?idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/exhibition/language/{idlanguage}/{idexhibition}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ExhibitionlanguagesResponseDTO getExhibitionLanguageData(
      @PathVariable(value = "idlanguage") int idlanguage,
      @PathVariable(value = "idexhibition") int idexhibition) {
        Exhibitionlanguages exhibitionlanguages = languageService.findExhibitionLanguageByLanguageAndExhibition(idlanguage, idexhibition);
        if (exhibitionlanguages == null) {
            exhibitionlanguages = new Exhibitionlanguages();
        }
        ExhibitionlanguagesResponseDTO responseDTO = new ExhibitionlanguagesResponseDTO();
        responseDTO.setIdexhibitionlanguages(exhibitionlanguages.getIdexhibitionlanguages());
        responseDTO.setName(exhibitionlanguages.getName());
        responseDTO.setText(exhibitionlanguages.getText());
        responseDTO.setAuthor(exhibitionlanguages.getAuthor());
        responseDTO.setTexturl(exhibitionlanguages.getTexturl());
        return responseDTO;
    }

    //museummap
    @RequestMapping(value = "/museummap", method = RequestMethod.GET)
    public String openMuseummapLanguagePage(
      @RequestParam("idmuseummap") int idmuseummap, Model model, Locale locale) {
        model.addAttribute("idmuseummap", idmuseummap);
        model.addAttribute("languages", languageService.loadAllLanguges());
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        Languages language = languageService.loadAllLanguges().get(0);
        Museummaplanguages museummaplanguages = languageService.findMuseummapLanguageByLanguageAndMuseummap(language.getIdlanguages(), idmuseummap);
        if (museummaplanguages == null) {
            museummaplanguages = new Museummaplanguages();
        }
        model.addAttribute("museummaplanguages", museummaplanguages);
        model.addAttribute("map", mapService.findMuseummap(idmuseummap));
        return PageConstants.MAP_LANGUAGES_PAGE;
    }

    @RequestMapping(value = "/museummap", method = RequestMethod.POST)
    public String openMuseummapLanguagePage(@ModelAttribute("museummaplanguages") Museummaplanguages museummaplanguages, BindingResult bindingResult,
                                            @RequestParam("idmuseummap")
                                              int idmuseummap, Model model, Locale locale, HttpServletRequest request, @RequestParam(name = "pickedaudios", required = false) List<MultipartFile> audios, @RequestParam(name = "pickedvideos", required = false) List<MultipartFile> videos) {
        museumValidator.validateLanguage(museummaplanguages, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("idmuseummap", idmuseummap);
            model.addAttribute("languages", languageService.loadAllLanguges());
            model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
            return PageConstants.MAP_LANGUAGES_PAGE;
        }
        String[] selectedLanguages = request.getParameterValues("selectedLanguage");
        if (!selectedLanguages[0].equals("0")) {
            languageService.saveMuseummapLanguage(museummaplanguages, idmuseummap, Integer.parseInt(selectedLanguages[0]), audios, videos);
        }
        return "redirect:/languages/exhibition?idmuseummap=" + idmuseummap;
    }

    @RequestMapping(value = "/map/audio/delete/{iditemaudio}", method = RequestMethod.GET)
    public String deleteMapAudio(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam("idmuseummap") int idmuseummap, @RequestParam("idmuseummaplanguages") int idmuseummaplanguages, Model model, Locale locale) {
        languageService.deleteAudioLanguage(iditemaudio);
        return "redirect:/languages/exhibition?idmuseummap=" + idmuseummap;
    }

    @RequestMapping(value = "/map/video/delete/{iditemvideo}", method = RequestMethod.GET)
    public String deleteMapVideo(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam("idmuseummap") int idmuseummap, @RequestParam("idmuseummaplanguages") int idmuseummaplanguages, Model model, Locale locale) {
        languageService.deleteVideoLanguage(iditemvideo);
        return "redirect:/languages/exhibition?idmuseummap=" + idmuseummap;
    }

    @RequestMapping(value = "/museummap/language/{idlanguage}/{idmuseum}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public MuseummaplanguagesResponseDTO getMuseummapLanguageData(
      @PathVariable(value = "idlanguage")
        int idlanguage,
      @PathVariable(value = "idmuseummap")
        int idmuseummap) {
        Museummaplanguages museummaplanguages = languageService.findMuseummapLanguageByLanguageAndMuseummap(idlanguage, idmuseummap);
        if (museummaplanguages == null) {
            museummaplanguages = new Museummaplanguages();
        }
        MuseummaplanguagesResponseDTO responseDTO = new MuseummaplanguagesResponseDTO();
        responseDTO.setIdmuseummaplanguages(museummaplanguages.getIdmuseummaplanguages());
        responseDTO.setName(museummaplanguages.getName());
        responseDTO.setText(museummaplanguages.getText());
        responseDTO.setTexturl(museummaplanguages.getTexturl());
        return responseDTO;
    }

}
