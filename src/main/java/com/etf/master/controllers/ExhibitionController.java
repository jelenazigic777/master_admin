package com.etf.master.controllers;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.etf.master.constants.PageConstants;
import com.etf.master.dtos.MapResponseDTO;
import com.etf.master.models.Exhibition;
import com.etf.master.models.Item;
import com.etf.master.models.Itemaudio;
import com.etf.master.models.Itemimage;
import com.etf.master.models.Itemtext;
import com.etf.master.models.Itemvideo;
import com.etf.master.services.AmbientaudioService;
import com.etf.master.services.ExhibitionService;
import com.etf.master.services.ItemService;
import com.etf.master.validators.ExhibitionValidator;

@Controller
@RequestMapping("/exhibitions")
public class ExhibitionController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ExhibitionValidator exhibitionValidator;

    @Autowired
    private ExhibitionService exhibitionService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private AmbientaudioService ambientaudioService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String exhibitionPage(@RequestParam(value = "idmuseum") int idmuseum, final Model model, Locale locale) {

        List<Exhibition> exhibitions = exhibitionService.findAllActiveFromMuseum(idmuseum);
        model.addAttribute("exhibitions", exhibitions);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("isActive", true);
        model.addAttribute("activeInactiveButtonText", messageSource.getMessage("go.to.inactive.exhibitions", new Object[]{}, locale));
        return PageConstants.EXHIBITION_MUSEUM;
    }

    @RequestMapping(value = "/inactive", method = RequestMethod.GET)
    public String itemsPageInactive(@RequestParam(value = "idmuseum") Integer idmuseum, final Model model, Locale locale) {

        List<Exhibition> exhibitions = exhibitionService.findAllInactiveItemsFromMuseum(idmuseum);
        model.addAttribute("exhibitions", exhibitions);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("isActive", false);
        model.addAttribute("activeInactiveButtonText", messageSource.getMessage("go.to.active.exhibitions", new Object[]{}, locale));
        return PageConstants.EXHIBITION_MUSEUM;
    }

    @RequestMapping(value = "/exhibitionform", method = RequestMethod.GET)
    public String exhibitionForm(@RequestParam(value = "idmuseum") int idmuseum, final Model model, Locale locale) {

        Exhibition exhibition = new Exhibition();
        exhibition.setActive(false);
        model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
        model.addAttribute("exhibition", exhibition);
        model.addAttribute("idmuseum", idmuseum);

        return PageConstants.EXHIBITION_FORM_PAGE;
    }

    @RequestMapping(value = "/exhibitionform", method = RequestMethod.POST)
    public String exhibitionForm(@ModelAttribute("exhibition") Exhibition exhibition, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(name = "pickedimage", required = false) MultipartFile image, @RequestParam(name = "pickedaudio", required = false) MultipartFile audio, @RequestParam(name = "pickedvideo", required = false) MultipartFile video, BindingResult bindingResult, final Model model, Locale locale, RedirectAttributes redirectAttributes) {

        Exhibition foundedExhibition = exhibitionService.findExhibition(exhibition.getIdexhibition());
        exhibitionValidator.validate(exhibition, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            model.addAttribute("idmuseum", idmuseum);
            return PageConstants.EXHIBITION_FORM_PAGE;
        }
        if (exhibition.getIdexhibition() == 0) {
            exhibitionService.basicSave(exhibition, idmuseum, locale);
            exhibition = exhibitionService.saveMultipartFiles(image, audio, video, exhibition);
        }
        if (foundedExhibition != null) {
            foundedExhibition.setName(exhibition.getName());
            foundedExhibition.setAuthor(exhibition.getAuthor());
            foundedExhibition.setActive(exhibition.getActive());
            foundedExhibition.setText(exhibition.getText());
            exhibition = exhibitionService.saveMultipartFiles(image, audio, video, foundedExhibition);
        }

        if (foundedExhibition == null) {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("add.exhibition.success", new Object[]{}, locale));
        } else {
            redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.exhibition.success", new Object[]{}, locale));
        }
        redirectAttributes.addAttribute("showCopyButton", false);
        return "redirect:/exhibitions/exhibitionform/update?idmuseum=" + idmuseum + "&idexhibition=" + exhibition.getIdexhibition();
    }

    @RequestMapping(value = "/exhibitionform/update", method = RequestMethod.GET)
    public String exhibitionFormUpdate(@RequestParam(value = "idexhibition") Integer id, final Model model, Locale locale, @ModelAttribute("successMessage") String successMessage, @ModelAttribute("showCopyButton") String showCopyButton) {
        Exhibition exhibition = exhibitionService.findExhibition(id);
        int idmuseum = exhibition.getMuseum().getIdmuseum();
        model.addAttribute("exhibition", exhibition);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("buttontext", messageSource.getMessage("update.to.db", new Object[]{}, locale));
        model.addAttribute("successMessage", successMessage);

        model.addAttribute("authors", exhibitionService.findAuthorsFromExhibitionItems(exhibition.getIdexhibition()));
        if (showCopyButton != null) {
            model.addAttribute("showCopyButton", true);
        } else {
            model.addAttribute("showCopyButton", showCopyButton);
        }
        return PageConstants.EXHIBITION_FORM_PAGE;
    }

    @RequestMapping(value = "/exhibition/delete", method = RequestMethod.GET)
    public String deleteExhibition(@RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "idmuseum") Integer idmuseum, Model model) {
        exhibitionService.deleteExhibition(idexhibition);
        return "redirect:/exhibitions?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/exhibition/set", method = RequestMethod.POST)
    public String goToAdditionalPage(@ModelAttribute(value = "exhibition") Exhibition exhibition, BindingResult bindingResult, @RequestParam(value = "idexhibition") int idexhibition, @RequestParam("idmuseum") int idmuseum, Model model, Locale locale) {

        exhibitionValidator.validate(exhibition, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("idmuseum", idmuseum);
            model.addAttribute("buttontext", messageSource.getMessage("add.to.db", new Object[]{}, locale));
            return PageConstants.EXHIBITION_FORM_PAGE;
        }
        if (idexhibition != 0) {
            exhibition = exhibitionService.findExhibition(exhibition.getIdexhibition());
        } else {
            exhibitionService.basicSave(exhibition, idmuseum, locale);
        }
        return "redirect:/exhibitions/exhibition/set/additional?idmuseum=" + idmuseum + "&idexhibition=" + exhibition.getIdexhibition();
    }

    @RequestMapping(value = "/exhibition/set/additional", method = RequestMethod.GET)
    public String setAdditionalOnExhibition(@RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "idmuseum") Integer idmuseum, Model model, Locale locale) {

        List<Item> exhibitionitems = itemService.findItemsByExhibitionId(idexhibition);
        List<MapResponseDTO> responseList = exhibitionService.findMapResponseListByExhibitionId(idexhibition);
        model.addAttribute("responseList", responseList);
        model.addAttribute("responseListSize", responseList.size());
        model.addAttribute("exhibitionitems", exhibitionitems);
        //only active can be set on exhibition
        model.addAttribute("itemstoselect", itemService.findAllActiveItemsFromMuseum(idmuseum));
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("idexhibition", idexhibition);
        model.addAttribute("exhibition", exhibitionService.findExhibition(idexhibition));

        return PageConstants.EXHIBITION_ADDITIONAL;
    }

    @RequestMapping(value = "/exhibition/set/additional", method = RequestMethod.POST)
    public String setAdditionalOnExhibition(@RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "idmuseum") Integer idmuseum, HttpServletRequest request) {

        Exhibition exhibition = exhibitionService.findExhibition(idexhibition);
        String[] selectedItemids = request.getParameterValues("selectedItems");
        exhibition = exhibitionService.setItems(selectedItemids, exhibition);
        exhibitionService.saveExhibition(exhibition);
        return "redirect:/exhibitions/exhibition/set/additional?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/delete/item", method = RequestMethod.GET)
    public String deleteItemFromExhibition(@RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "iditem") Integer iditem) {

        exhibitionService.removeItemExhibitionRelation(idexhibition, iditem);
        return "redirect:/exhibitions/exhibition/set/additional?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/delete/museummap", method = RequestMethod.GET)
    public String deleteMuseummapFromExhibition(@RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "idmuseummap") Integer idmuseummap) {
        exhibitionService.removeMuseummapExhibitionRelation(idexhibition, idmuseummap);
        return "redirect:/exhibitions/exhibition/set/additional?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    //TODO MOVE ALL LOGIC TO BUSINESS LAYER AND TEST
    @RequestMapping(value = "/exhibition/copy", method = RequestMethod.GET)
    public String cloneExhibition(@RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "idmuseum") Integer idmuseum) {
        Exhibition exhibition = exhibitionService.findExhibition(idexhibition);
        Exhibition saved = exhibitionService.cloneExhibitionAndReturnSaved(exhibition);
        return "redirect:/exhibitions/exhibitionform/update?idmuseum=" + idmuseum + "&idexhibition=" + saved.getIdexhibition();
    }

    @RequestMapping(value = "/goto/exhibitionform/additional", method = RequestMethod.POST)
    public String goToExhibitionForm(@RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "idmuseum") int idmuseum, final Model model, @RequestParam(name = "ambientaudio", required = false) MultipartFile ambientaudio) {
        exhibitionService.setAmbientAudioOnExhibition(ambientaudio, idmuseum, idexhibition);
        return "redirect:/exhibitions/exhibition/set/additional?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    //TODO MOVE TO BUSINESS
    @RequestMapping(value = "/set/ambientaudio", method = RequestMethod.POST)
    public String setAmbientaudio(@RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idexhibition") int idexhibition, @RequestParam(name = "ambientaudio", required = false) MultipartFile ambientaudio) {
        exhibitionService.setAmbientAudioOnExhibition(ambientaudio, idmuseum, idexhibition);
        return "redirect:/exhibitions/exhibition/set/additional?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/ambientaudio/delete/{idambientaudio}", method = RequestMethod.GET)
    public String deleteAmbientaudio(@PathVariable(value = "idambientaudio") int idambientaudio, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idexhibition") int idexhibition) {
        ambientaudioService.deleteAmbientAudio(idambientaudio);
        return "redirect:/exhibitions/exhibition/set/additional?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/set/item/additional", method = RequestMethod.GET)
    public String setAdditionalForSelectedItem(@RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "iditem") Integer iditem, Model model, HttpServletRequest request) {
        //go to new page for item and selecting
        Item item = itemService.findItem(iditem);
        Integer itemimageint = exhibitionService.findExhibitionImage(idexhibition, iditem);
        Itemimage itemimage = null;
        if (itemimageint != null) {
            itemimage = itemService.findImage(itemimageint);
        }

        Integer itemvideoint = exhibitionService.findExhibitionVideo(idexhibition, iditem);
        Itemvideo itemvideo = null;
        if (itemvideoint != null) {
            itemvideo = itemService.findVideo(itemvideoint);
        }

        Integer itemaudioint = exhibitionService.findExhibitionAudio(idexhibition, iditem);
        Itemaudio itemaudio = null;
        if (itemaudioint != null) {
            itemaudio = itemService.findAudio(itemaudioint);
        }

        Integer itemtextint = exhibitionService.findExhibitionText(idexhibition, iditem);
        Itemtext itemtext = null;
        if (itemtextint != null) {
            itemtext = itemService.findText(itemtextint);
        }

        model.addAttribute("idexhibition", idexhibition);
        model.addAttribute("item", item);
        model.addAttribute("iditem", iditem);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("image", itemimage);
        model.addAttribute("video", itemvideo);
        model.addAttribute("audio", itemaudio);
        model.addAttribute("text", itemtext);
        return PageConstants.ITEM_PAGE_FOR_EXHIBITION;
    }

    @RequestMapping(value = "/set/item/additional", method = RequestMethod.POST)
    public String setAdditionalImage(@RequestParam("idexhibition") int idexhibition, @RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "iditem") Integer iditem, Model model, HttpServletRequest request) {
        //go to new page for item and selecting

        model.addAttribute("image", null);
        model.addAttribute("text", null);
        model.addAttribute("video", null);
        model.addAttribute("audio", null);
        Exhibition exhibition = exhibitionService.findExhibition(idexhibition);

        String[] selectedImageId = request.getParameterValues("itemimage");
        String[] selectedTextId = request.getParameterValues("itemtext");
        String[] selectedVideoId = request.getParameterValues("itemvideo");
        String[] selectedAudioId = request.getParameterValues("itemaudio");
        Item item = itemService.findItem(iditem);
        //if not null!
        if (!selectedImageId[0].equals("0")) {
            Itemimage itemimage = itemService.findImage(Integer.parseInt(selectedImageId[0]));
            //old must be set to null
            List<Itemimage> exhibitionimages = exhibition.getImages();

            exhibitionService.removeImageExhibitionRelation(idexhibition, iditem);

            exhibitionimages.add(itemimage);
            exhibition.setImages(exhibitionimages);
            model.addAttribute("image", itemimage);
        }
        if (!selectedTextId[0].equals("0")) {
            Itemtext itemtext = itemService.findText(Integer.parseInt(selectedTextId[0]));
            //old must be set to null
            List<Itemtext> exhibitiontexts = exhibition.getTexts();
            exhibitionService.removeTextExhibitionRelation(idexhibition, iditem);
            exhibitiontexts.add(itemtext);
            exhibition.setTexts(exhibitiontexts);
            model.addAttribute("text", itemtext);
        }
        if (!selectedVideoId[0].equals("0")) {
            Itemvideo itemvideo = itemService.findVideo(Integer.parseInt(selectedVideoId[0]));
            //old must be set to null
            List<Itemvideo> exhibitionvideos = exhibition.getVideos();
            exhibitionService.removeVideoExhibitionRelation(idexhibition, iditem);
            exhibitionvideos.add(itemvideo);
            exhibition.setVideos(exhibitionvideos);
            model.addAttribute("video", itemvideo);
        }
        if (!selectedAudioId[0].equals("0")) {
            Itemaudio itemaudio = itemService.findAudio(Integer.parseInt(selectedAudioId[0]));
            //old must be set to null
            List<Itemaudio> exhibitionaudios = exhibition.getAudios();
            exhibitionService.removeAudioExhibitionRelation(idexhibition, iditem);
            exhibitionaudios.add(itemaudio);
            exhibition.setAudios(exhibitionaudios);
            model.addAttribute("audio", itemaudio);
        }
        exhibitionService.saveExhibition(exhibition);

        return "redirect:/exhibitions/set/item/additional?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition + "&iditem=" + iditem;
    }

    @RequestMapping(value = "/image/remove/{iditemimage}", method = RequestMethod.GET)
    public String deleteImageOnExhibition(@PathVariable(value = "iditemimage") int iditemimage, @RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") int idmuseum, Model model, HttpServletRequest request) {

        Item item = itemService.findItem(iditem);
        exhibitionService.removeImageExhibitionRelation(idexhibition, iditem);

        model.addAttribute("idexhibition", idexhibition);
        model.addAttribute("item", item);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("iditem", iditem);
        model.addAttribute("image", null);

        Integer itemvideoint = exhibitionService.findExhibitionVideo(idexhibition, iditem);
        Itemvideo itemvideo = null;
        if (itemvideoint != null) {
            itemvideo = itemService.findVideo(itemvideoint);
        }
        Integer itemaudioint = exhibitionService.findExhibitionAudio(idexhibition, iditem);
        Itemaudio itemaudio = null;
        if (itemaudioint != null) {
            itemaudio = itemService.findAudio(itemaudioint);
        }
        Integer itemtextint = exhibitionService.findExhibitionText(idexhibition, iditem);
        Itemtext itemtext = null;
        if (itemtextint != null) {
            itemtext = itemService.findText(itemtextint);
        }
        model.addAttribute("video", itemvideo);
        model.addAttribute("audio", itemaudio);
        model.addAttribute("text", itemtext);
        return PageConstants.ITEM_PAGE_FOR_EXHIBITION;
    }

    @RequestMapping(value = "/audio/remove/{iditemaudio}", method = RequestMethod.GET)
    public String deleteAudioOnExhibition(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") int idmuseum, Model model, HttpServletRequest request) {

        Item item = itemService.findItem(iditem);
        exhibitionService.removeAudioExhibitionRelation(idexhibition, iditem);
        model.addAttribute("idexhibition", idexhibition);
        model.addAttribute("item", item);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("iditem", iditem);

        Integer itemvideoint = exhibitionService.findExhibitionVideo(idexhibition, iditem);
        Itemvideo itemvideo = null;
        if (itemvideoint != null) {
            itemvideo = itemService.findVideo(itemvideoint);
        }
        Integer itemimageint = exhibitionService.findExhibitionImage(idexhibition, iditem);
        Itemimage itemimage = null;
        if (itemimageint != null) {
            itemimage = itemService.findImage(itemimageint);
        }

        Integer itemtextint = exhibitionService.findExhibitionText(idexhibition, iditem);
        Itemtext itemtext = null;
        if (itemtextint != null) {
            itemtext = itemService.findText(itemtextint);
        }

        model.addAttribute("video", itemvideo);
        model.addAttribute("image", itemimage);
        model.addAttribute("text", itemtext);
        model.addAttribute("audio", null);
        return PageConstants.ITEM_PAGE_FOR_EXHIBITION;
    }

    @RequestMapping(value = "/video/remove/{iditemvideo}", method = RequestMethod.GET)
    public String deleteVideoOnExhibition(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") int idmuseum, Model model, HttpServletRequest request) {
        Item item = itemService.findItem(iditem);
        exhibitionService.removeVideoExhibitionRelation(idexhibition, iditem);

        model.addAttribute("idexhibition", idexhibition);
        model.addAttribute("item", item);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("iditem", iditem);
        model.addAttribute("video", null);

        Integer itemimageint = exhibitionService.findExhibitionImage(idexhibition, iditem);
        Itemimage itemimage = null;
        if (itemimageint != null) {
            itemimage = itemService.findImage(itemimageint);
        }
        Integer itemaudioint = exhibitionService.findExhibitionAudio(idexhibition, iditem);
        Itemaudio itemaudio = null;
        if (itemaudioint != null) {
            itemaudio = itemService.findAudio(itemaudioint);
        }
        Integer itemtextint = exhibitionService.findExhibitionText(idexhibition, iditem);
        Itemtext itemtext = null;
        if (itemtextint != null) {
            itemtext = itemService.findText(itemtextint);
        }

        model.addAttribute("image", itemimage);
        model.addAttribute("audio", itemaudio);
        model.addAttribute("text", itemtext);
        return PageConstants.ITEM_PAGE_FOR_EXHIBITION;
    }

    @RequestMapping(value = "/text/remove/{iditemtext}", method = RequestMethod.GET)
    public String deleteTextOnExhibition(@PathVariable(value = "iditemtext") int iditemtext, @RequestParam(value = "idexhibition") int idexhibition, @RequestParam(value = "iditem") int iditem, @RequestParam(value = "idmuseum") int idmuseum, Model model, HttpServletRequest request) {

        Item item = itemService.findItem(iditem);
        exhibitionService.removeTextExhibitionRelation(idexhibition, iditem);

        model.addAttribute("idexhibition", idexhibition);
        model.addAttribute("item", item);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("iditem", iditem);
        model.addAttribute("text", null);

        Integer itemvideoint = exhibitionService.findExhibitionVideo(idexhibition, iditem);
        Itemvideo itemvideo = null;
        if (itemvideoint != null) {
            itemvideo = itemService.findVideo(itemvideoint);
        }
        Integer itemaudioint = exhibitionService.findExhibitionAudio(idexhibition, iditem);
        Itemaudio itemaudio = null;
        if (itemaudioint != null) {
            itemaudio = itemService.findAudio(itemaudioint);
        }
        Integer itemimageint = exhibitionService.findExhibitionImage(idexhibition, iditem);
        Itemimage itemimage = null;
        if (itemimageint != null) {
            itemimage = itemService.findImage(itemimageint);
        }

        model.addAttribute("video", itemvideo);
        model.addAttribute("audio", itemaudio);
        model.addAttribute("image", itemimage);
        return PageConstants.ITEM_PAGE_FOR_EXHIBITION;
    }

    @RequestMapping(value = "/image/delete/{iditemimage}", method = RequestMethod.POST)
    public String deleteExhibitionImage(@PathVariable(value = "iditemimage") int iditemimage, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idexhibition") int idexhibition, Model model, Locale locale, RedirectAttributes redirectAttributes) {
        itemService.deleteImage(iditemimage);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("exhibition", exhibitionService.findExhibition(idexhibition));
        redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.exhibition.success", new Object[]{}, locale));
        return "redirect:/exhibitions/exhibitionform/update?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/audio/delete/{iditemaudio}", method = RequestMethod.POST)
    public String deleteExhibitionAudio(@PathVariable(value = "iditemaudio") int iditemaudio, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idexhibition") int idexhibition, Model model, Locale locale, RedirectAttributes redirectAttributes) {
        itemService.deleteAudio(iditemaudio);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("exhibition", exhibitionService.findExhibition(idexhibition));
        redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.exhibition.success", new Object[]{}, locale));
        return "redirect:/exhibitions/exhibitionform/update?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/video/delete/{iditemvideo}", method = RequestMethod.POST)
    public String deleteExhibitionVideo(@PathVariable(value = "iditemvideo") int iditemvideo, @RequestParam(value = "idmuseum") int idmuseum, @RequestParam(value = "idexhibition") int idexhibition, Model model, Locale locale, RedirectAttributes redirectAttributes) {
        itemService.deleteVideo(iditemvideo);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("exhibition", exhibitionService.findExhibition(idexhibition));
        redirectAttributes.addAttribute("successMessage", messageSource.getMessage("update.exhibition.success", new Object[]{}, locale));
        return "redirect:/exhibitions/exhibitionform/update?idmuseum=" + idmuseum + "&idexhibition=" + idexhibition;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchExhibition(@RequestParam(value = "idmuseum") Integer idmuseum, @RequestParam(value = "name") String name, @RequestParam(value = "author") String author, @RequestParam(value = "itemName") String itemName, @RequestParam(value = "itemCode") String itemCode, @RequestParam(value = "roomName") String roomName, Model model, Locale locale) {
        List<Exhibition> exhibitions = exhibitionService.findExhibitions(name, author, itemName, itemCode, roomName);
        model.addAttribute("exhibitions", exhibitions);
        model.addAttribute("idmuseum", idmuseum);
        model.addAttribute("isActive", true);
        model.addAttribute("activeInactiveButtonText", messageSource.getMessage("go.to.inactive.exhibitions", new Object[]{}, locale));
        return PageConstants.EXHIBITION_MUSEUM;
    }
}
