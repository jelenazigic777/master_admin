-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: master_app
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'jeja','jejacar','jeja','zigic'),(2,'jejazigic','jejajeja','jeja2','zigic'),(3,'jejazigic1','jejajejajeja','jelenaa','zigiccc'),(4,'milan93','perazdera','milan','rajic'),(5,'peraaa','perazdera','pera','peric'),(6,'anazigic','anaanaana','ana','zigic'),(7,'jejamilan','jejamilan','jejamilan','jelan'),(8,'milannn','milanmilan','milan','milan'),(9,'milja9333','milja9333','milja','milenkovic'),(10,'blaaaaaa','blaaaaaa','blaa','njknjk'),(11,'blaaaaaaaa','blaaaaaaaa','blaaaaaaaa','blaaaaaaaa'),(12,'peraaaaAA','peraaaaAA','peraaaa','peraaaa'),(13,'redirect','redirect','redirect','redirect'),(14,'redirectredirect','redirectredirect','redirectredirect','hbhj'),(15,'aaaaaaaaaaaa','aaaaaaaaaaaa','njknkjn','nknkn'),(16,'aaaaaaaaaaaaaaaaaaaaaaaa','aaaaaaaaaaaa','axsaanj','nknknk'),(17,'wdewfddwe','wdewfddwe','wdewfddwe','wdewfddwe'),(18,'asaasaasa','asaasaasa','asa','asa'),(19,'jijijiijiiiji','jijijiijiiiji','jijijij','jijiji'),(20,'qqqqqqqqqqqqq','qqqqqqqqqqqqq','qqqqqqqqqqqqq','qqqqqqqqqqqqq'),(21,'eeeeeeeeeeee','eeeeeeeeeeee','eeeeeeeeeeee','eeeeeeeeeeee'),(22,'ancxo32','laki3216','Ana','Zigic'),(23,'ancxo3216','laki3216','Ana','Zigic'),(24,'jkhkjh','hkjhkhkjhk','hkhkhkjhkj','hkhkhkj');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-31 21:26:04
