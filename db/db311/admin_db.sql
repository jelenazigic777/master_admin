-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: master_app
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beacon`
--

DROP TABLE IF EXISTS `beacon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `beacon` (
  `idbeacon` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `power` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`idbeacon`),
  KEY `fk_beacon_museum_idx` (`idmuseum`),
  KEY `fk_beacon_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_beacon_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_beacon_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beacon`
--

LOCK TABLES `beacon` WRITE;
/*!40000 ALTER TABLE `beacon` DISABLE KEYS */;
/*!40000 ALTER TABLE `beacon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibition`
--

DROP TABLE IF EXISTS `exhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibition` (
  `idexhibition` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `room` varchar(45) DEFAULT NULL,
  `additionalimagesurl` mediumtext,
  `additionalimagesurllocal` mediumtext,
  `idmuseum` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idexhibition`),
  KEY `fk_exibition_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_exibition_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibition`
--

LOCK TABLES `exhibition` WRITE;
/*!40000 ALTER TABLE `exhibition` DISABLE KEYS */;
INSERT INTO `exhibition` VALUES (17,'m','en','njbjknk','nknklnk','kjnkl','','','','','','',NULL,NULL,NULL,NULL,NULL,'',NULL,12,0);
/*!40000 ALTER TABLE `exhibition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitiontomuseummap`
--

DROP TABLE IF EXISTS `exhibitiontomuseummap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitiontomuseummap` (
  `idexhibitiontomuseummap` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseummap` int(11) NOT NULL,
  `idexhibition` int(11) NOT NULL,
  PRIMARY KEY (`idexhibitiontomuseummap`),
  KEY `fk_exibition_idx` (`idexhibition`),
  KEY `fk_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_museummap` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_exhibition` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitiontomuseummap`
--

LOCK TABLES `exhibitiontomuseummap` WRITE;
/*!40000 ALTER TABLE `exhibitiontomuseummap` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitiontomuseummap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `floormap`
--

DROP TABLE IF EXISTS `floormap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `floormap` (
  `idfloormap` int(11) NOT NULL AUTO_INCREMENT,
  `floornumber` varchar(45) NOT NULL,
  `xstart` double DEFAULT NULL,
  `ystart` double DEFAULT NULL,
  `zstart` double DEFAULT NULL,
  `xend` double DEFAULT NULL,
  `yend` double DEFAULT NULL,
  `zend` double DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `idmuseummap` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `radius` double DEFAULT NULL,
  PRIMARY KEY (`idfloormap`),
  KEY `idmuseum_idx` (`idmuseum`),
  KEY `fk_floormap_museummap_idx` (`idmuseummap`),
  KEY `fk_floormap_room_idx` (`idroom`),
  CONSTRAINT `fk_floormap_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_floormap_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_floormap_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `floormap`
--

LOCK TABLES `floormap` WRITE;
/*!40000 ALTER TABLE `floormap` DISABLE KEYS */;
INSERT INTO `floormap` VALUES (17,'6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'lll','8888888','88888888888888','888888','888','',NULL,NULL,71,51,12,'sr',NULL),(18,'8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'jkjlkjlk','','','','','',NULL,NULL,72,52,12,'sr',NULL),(19,'8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'jkjlkjlk','','','','','',NULL,NULL,73,53,12,'sr',NULL);
/*!40000 ALTER TABLE `floormap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museum`
--

DROP TABLE IF EXISTS `museum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museum` (
  `idmuseum` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `audiourl` tinytext,
  `audiourllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `qr` tinytext,
  PRIMARY KEY (`idmuseum`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museum`
--

LOCK TABLES `museum` WRITE;
/*!40000 ALTER TABLE `museum` DISABLE KEYS */;
INSERT INTO `museum` VALUES (12,'mmm','m','sr','mmmm','','','','',NULL,NULL,'','',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `museum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museummap`
--

DROP TABLE IF EXISTS `museummap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museummap` (
  `idmuseummap` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `room` varchar(45) DEFAULT NULL,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  PRIMARY KEY (`idmuseummap`),
  KEY `fk_museummap_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_museummap_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museummap`
--

LOCK TABLES `museummap` WRITE;
/*!40000 ALTER TABLE `museummap` DISABLE KEYS */;
INSERT INTO `museummap` VALUES (71,'m','sr','lll','8888888','88888888888888','888888',NULL,NULL,NULL,NULL,NULL,12,'888',''),(72,'m','sr','jkjlkjlk','','','',NULL,NULL,NULL,NULL,NULL,12,'',''),(73,'m','sr','jkjlkjlk','','','',NULL,NULL,NULL,NULL,NULL,12,'','');
/*!40000 ALTER TABLE `museummap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item` (
  `iditem` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `author` varchar(100) NOT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `audiourl` tinytext,
  `audiourllocal` tinytext,
  `audiotype` int(11) DEFAULT NULL,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `qr` varchar(45) DEFAULT NULL,
  `additionalimagesurl` mediumtext,
  `additionalimagesurllocal` mediumtext,
  `idmuseum` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  PRIMARY KEY (`iditem`),
  UNIQUE KEY `iditem_UNIQUE` (`iditem`),
  KEY `idmuseum_idx` (`idmuseum`),
  KEY `fk_picture_room_idx` (`idroom`),
  KEY `fk_picture_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_picture_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (3,'bla','sr','van gogh','van gogh','kjnjknkjn','','',NULL,NULL,NULL,'','',300,200,0,4,NULL,NULL,NULL,NULL,NULL,NULL,'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg/300px-Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg','https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg/300px-Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg'),(4,'m1','sr','jnjknkjn','jnkjnkjn','nkjnknk','','',NULL,NULL,NULL,'','',20,200,0,5,NULL,NULL,NULL,NULL,NULL,NULL,'njknkjnk',''),(6,'muzej','sr','jkhkjhkj','hkhkjkh','khkhkjhkj','','',NULL,NULL,NULL,'','',20,200,0,5,NULL,NULL,NULL,NULL,NULL,NULL,'khkhkjhkj',''),(16,'m','sr','slika1','van gogh','nkl','','',NULL,NULL,NULL,'','',20,200,0,5,NULL,NULL,NULL,12,NULL,NULL,'lnkkknlk',''),(17,'m','sr','slika2','van gogh','klmklmlmlk','','',NULL,NULL,NULL,'','',20,200,0,5,NULL,NULL,NULL,12,NULL,NULL,'mkmkmk','');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `picturetoexhibition`
--

DROP TABLE IF EXISTS `picturetoexhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `picturetoexhibition` (
  `idpicturetoexhibition` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) NOT NULL,
  `idexhibition` int(11) NOT NULL,
  PRIMARY KEY (`idpicturetoexhibition`),
  KEY `fk_exebition_picture_idx` (`idexhibition`),
  KEY `fk_picture_exhibition_idx` (`iditem`),
  CONSTRAINT `fk_exhibition_picture` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_exhibition` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `picturetoexhibition`
--

LOCK TABLES `picturetoexhibition` WRITE;
/*!40000 ALTER TABLE `picturetoexhibition` DISABLE KEYS */;
INSERT INTO `picturetoexhibition` VALUES (57,16,17);
/*!40000 ALTER TABLE `picturetoexhibition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `room` (
  `idroom` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  PRIMARY KEY (`idroom`),
  KEY `fk_room_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_room_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (51,'m','sr','lll','8888888','88888888888888','888888','888','',NULL,NULL,NULL,NULL,NULL,NULL,12),(52,'m','sr','jkjlkjlk','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,12),(53,'m','sr','jkjlkjlk','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,12);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomtoroom`
--

DROP TABLE IF EXISTS `roomtoroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roomtoroom` (
  `idroomtoroom` int(11) NOT NULL AUTO_INCREMENT,
  `idroom1` int(11) NOT NULL,
  `idroom2` int(11) NOT NULL,
  PRIMARY KEY (`idroomtoroom`),
  KEY `fk_room1_room2_idx` (`idroom1`),
  KEY `fk_room2_room1_idx` (`idroom2`),
  CONSTRAINT `fk_room1_room2` FOREIGN KEY (`idroom1`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_room2_room1` FOREIGN KEY (`idroom2`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomtoroom`
--

LOCK TABLES `roomtoroom` WRITE;
/*!40000 ALTER TABLE `roomtoroom` DISABLE KEYS */;
INSERT INTO `roomtoroom` VALUES (8,51,51),(9,52,51),(10,53,51);
/*!40000 ALTER TABLE `roomtoroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'jeja','jejacar','jeja','zigic'),(2,'jejazigic','jejajeja','jeja2','zigic'),(3,'jejazigic1','jejajejajeja','jelenaa','zigiccc'),(4,'milan93','perazdera','milan','rajic'),(5,'peraaa','perazdera','pera','peric'),(6,'anazigic','anaanaana','ana','zigic'),(7,'jejamilan','jejamilan','jejamilan','jelan'),(8,'milannn','milanmilan','milan','milan'),(9,'milja9333','milja9333','milja','milenkovic'),(10,'blaaaaaa','blaaaaaa','blaa','njknjk'),(11,'blaaaaaaaa','blaaaaaaaa','blaaaaaaaa','blaaaaaaaa'),(12,'peraaaaAA','peraaaaAA','peraaaa','peraaaa'),(13,'redirect','redirect','redirect','redirect'),(14,'redirectredirect','redirectredirect','redirectredirect','hbhj'),(15,'aaaaaaaaaaaa','aaaaaaaaaaaa','njknkjn','nknkn'),(16,'aaaaaaaaaaaaaaaaaaaaaaaa','aaaaaaaaaaaa','axsaanj','nknknk'),(17,'wdewfddwe','wdewfddwe','wdewfddwe','wdewfddwe'),(18,'asaasaasa','asaasaasa','asa','asa'),(19,'jijijiijiiiji','jijijiijiiiji','jijijij','jijiji'),(20,'qqqqqqqqqqqqq','qqqqqqqqqqqqq','qqqqqqqqqqqqq','qqqqqqqqqqqqq'),(21,'eeeeeeeeeeee','eeeeeeeeeeee','eeeeeeeeeeee','eeeeeeeeeeee'),(22,'ancxo32','laki3216','Ana','Zigic'),(23,'ancxo3216','laki3216','Ana','Zigic'),(24,'jkhkjh','hkjhkhkjhk','hkhkhkjhkj','hkhkhkj');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-22 10:35:37
