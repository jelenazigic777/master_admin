-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: master_app
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `itemaudio`
--

DROP TABLE IF EXISTS `itemaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemaudio` (
  `idpictureaudio` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `audiourl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `audiotype` int(11) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpictureaudio`),
  KEY `fk_picture_audio_idx` (`iditem`),
  KEY `fk_exhibition_audio_idx` (`idexhibition`),
  CONSTRAINT `fk_exhibition_audio` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_audio` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemaudio`
--

LOCK TABLES `itemaudio` WRITE;
/*!40000 ALTER TABLE `itemaudio` DISABLE KEYS */;
INSERT INTO `itemaudio` VALUES (1,43,'http://res.cloudinary.com/dktuxe18z/video/upload/v1548248323/cwiz9gd63pmawdcftl2d.mp3',NULL,NULL,'cwiz9gd63pmawdcftl2d','LjubaPopovic.mp3',NULL),(4,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1548694589/soowfmxsnfbdn1b41sf1.mp3',NULL,NULL,'soowfmxsnfbdn1b41sf1','LjubaPopovic.mp3',NULL),(5,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1548695056/bzf5ecvnliobatitij1w.mp3',NULL,NULL,'bzf5ecvnliobatitij1w','LjubaPopovic.mp3',NULL);
/*!40000 ALTER TABLE `itemaudio` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-31 21:26:06
