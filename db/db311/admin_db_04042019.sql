-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: master_app
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ambientaudio`
--

DROP TABLE IF EXISTS `ambientaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ambientaudio` (
  `idambientaudio` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `ambientaudiourl` longtext,
  `idroom` int(11) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `idfloor` int(11) DEFAULT NULL,
  PRIMARY KEY (`idambientaudio`),
  UNIQUE KEY `UK_qwg6u8eabd5qc6j3ni02fabhc` (`idexhibition`),
  KEY `fk_ambientaudio_museum_idx` (`idmuseum`),
  KEY `fk_ambientaudio_room_idx` (`idroom`),
  KEY `fk_ambientaudio_exhibition_idx` (`idexhibition`),
  KEY `fk_ambientaudio_floor_idx` (`idfloor`),
  CONSTRAINT `fk_ambientaudio_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_floor` FOREIGN KEY (`idfloor`) REFERENCES `museummap` (`idmuseummap`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ambientaudio`
--

LOCK TABLES `ambientaudio` WRITE;
/*!40000 ALTER TABLE `ambientaudio` DISABLE KEYS */;
INSERT INTO `ambientaudio` VALUES (21,45,'http://res.cloudinary.com/dktuxe18z/video/upload/v1554402359/wexcwjcyxdgybaqhbet0.mp3',NULL,NULL,'wexcwjcyxdgybaqhbet0',NULL);
/*!40000 ALTER TABLE `ambientaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beacon`
--

DROP TABLE IF EXISTS `beacon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `beacon` (
  `idbeacon` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `power` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`idbeacon`),
  KEY `fk_beacon_museum_idx` (`idmuseum`),
  KEY `fk_beacon_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_beacon_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_beacon_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beacon`
--

LOCK TABLES `beacon` WRITE;
/*!40000 ALTER TABLE `beacon` DISABLE KEYS */;
/*!40000 ALTER TABLE `beacon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibition`
--

DROP TABLE IF EXISTS `exhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibition` (
  `idexhibition` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `room` varchar(45) DEFAULT NULL,
  `additionalimagesurl` mediumtext,
  `additionalimagesurllocal` mediumtext,
  `idmuseum` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idexhibition`),
  KEY `fk_exibition_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_exibition_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibition`
--

LOCK TABLES `exhibition` WRITE;
/*!40000 ALTER TABLE `exhibition` DISABLE KEYS */;
INSERT INTO `exhibition` VALUES (106,'jnkjnkj','sr','izlozba 1','njknkjn','knkjnjk',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,45,0);
/*!40000 ALTER TABLE `exhibition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionaudio`
--

DROP TABLE IF EXISTS `exhibitionaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionaudio` (
  `idexhibitionaudio` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `idpictureaudio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionaudio`),
  KEY `fk_pictureaudio_exhibition_idx` (`idexhibition`),
  KEY `fk_exhibition_pictureaudio_idx` (`idpictureaudio`),
  CONSTRAINT `fk_exhibition_pictureaudio` FOREIGN KEY (`idpictureaudio`) REFERENCES `itemaudio` (`idpictureaudio`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pictureaudio_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionaudio`
--

LOCK TABLES `exhibitionaudio` WRITE;
/*!40000 ALTER TABLE `exhibitionaudio` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionimage`
--

DROP TABLE IF EXISTS `exhibitionimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionimage` (
  `idexhibitionimage` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `idpictureimage` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionimage`),
  KEY `fk_exhibition_pictureimage_idx` (`idpictureimage`),
  KEY `fk_pictureimage_exhibition_idx` (`idexhibition`),
  CONSTRAINT `fk_exhibition_pictureimage` FOREIGN KEY (`idpictureimage`) REFERENCES `itemimage` (`idpictureimage`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_pictureimage_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionimage`
--

LOCK TABLES `exhibitionimage` WRITE;
/*!40000 ALTER TABLE `exhibitionimage` DISABLE KEYS */;
INSERT INTO `exhibitionimage` VALUES (13,106,64);
/*!40000 ALTER TABLE `exhibitionimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionlanguages`
--

DROP TABLE IF EXISTS `exhibitionlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionlanguages` (
  `idexhibitionlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(300) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionlanguages`),
  KEY `fk_exhibition_exhibitionlanguages_idx` (`idexhibition`),
  KEY `fk_language_exhibitionlanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_exhibition_exhibitionlanguages` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_language_exhibitionlanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionlanguages`
--

LOCK TABLES `exhibitionlanguages` WRITE;
/*!40000 ALTER TABLE `exhibitionlanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitiontext`
--

DROP TABLE IF EXISTS `exhibitiontext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitiontext` (
  `idexhibitiontext` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `idpicturetext` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitiontext`),
  KEY `fk_exhibition_picturetext_idx` (`idexhibition`),
  KEY `fk_picturetext_exhibition_idx` (`idpicturetext`),
  CONSTRAINT `fk_exhibition_picturetext` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picturetext_exhibition` FOREIGN KEY (`idpicturetext`) REFERENCES `itemtext` (`idpicturetext`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitiontext`
--

LOCK TABLES `exhibitiontext` WRITE;
/*!40000 ALTER TABLE `exhibitiontext` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitiontext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitiontomuseummap`
--

DROP TABLE IF EXISTS `exhibitiontomuseummap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitiontomuseummap` (
  `idexhibitiontomuseummap` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseummap` int(11) NOT NULL,
  `idexhibition` int(11) NOT NULL,
  PRIMARY KEY (`idexhibitiontomuseummap`),
  KEY `fk_exibition_idx` (`idexhibition`),
  KEY `fk_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_museummap` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_exhibition` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitiontomuseummap`
--

LOCK TABLES `exhibitiontomuseummap` WRITE;
/*!40000 ALTER TABLE `exhibitiontomuseummap` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitiontomuseummap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionvideo`
--

DROP TABLE IF EXISTS `exhibitionvideo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionvideo` (
  `idexhibitionvideo` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `idpicturevideo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionvideo`),
  KEY `fk_exhibition_picturevideo_idx` (`idexhibition`),
  KEY `fk_picturevideo_exhibition_idx` (`idpicturevideo`),
  CONSTRAINT `fk_exhibition_picturevideo` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picturevideo_exhibition` FOREIGN KEY (`idpicturevideo`) REFERENCES `itemvideo` (`idpicturevideo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionvideo`
--

LOCK TABLES `exhibitionvideo` WRITE;
/*!40000 ALTER TABLE `exhibitionvideo` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionvideo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `floormap`
--

DROP TABLE IF EXISTS `floormap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `floormap` (
  `idfloormap` int(11) NOT NULL AUTO_INCREMENT,
  `xstart` double DEFAULT NULL,
  `ystart` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `xend` double DEFAULT NULL,
  `yend` double DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `idmuseummap` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `floornumber` int(11) NOT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  PRIMARY KEY (`idfloormap`),
  KEY `idmuseum_idx` (`idmuseum`),
  KEY `fk_floormap_museummap_idx` (`idmuseummap`),
  KEY `fk_floormap_room_idx` (`idroom`),
  CONSTRAINT `fk_floormap_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_floormap_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_floormap_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `floormap`
--

LOCK TABLES `floormap` WRITE;
/*!40000 ALTER TABLE `floormap` DISABLE KEYS */;
INSERT INTO `floormap` VALUES (25,NULL,NULL,NULL,NULL,NULL,NULL,'sprat1','','',NULL,NULL,NULL,NULL,NULL,149,NULL,33,'sr',NULL,1,NULL,NULL),(27,NULL,NULL,NULL,NULL,NULL,NULL,'mapa1','','','','','',NULL,NULL,151,101,33,'sr',NULL,1,NULL,NULL),(29,1,2,1,2,2,NULL,'sprat1','','ytyvftvtftttttttttttt',NULL,NULL,NULL,NULL,NULL,153,NULL,45,'sr',NULL,1,NULL,NULL),(36,1,1,1,2,2,NULL,'stepeniste1','','','',NULL,NULL,NULL,NULL,160,108,45,'sr',NULL,1,NULL,NULL),(40,3,4,1,3,4,NULL,'prostorija2','njknjknjkn','','',NULL,NULL,NULL,NULL,163,111,45,'sr',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `floormap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institution`
--

DROP TABLE IF EXISTS `institution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `institution` (
  `idinstitution` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `township` varchar(45) DEFAULT NULL,
  `town` varchar(45) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `founder` varchar(45) DEFAULT NULL,
  `foundationdate` datetime DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `idnumber` int(11) DEFAULT NULL,
  `registrationnumber` int(11) DEFAULT NULL,
  `taxid` int(11) DEFAULT NULL,
  `contactphones` mediumtext,
  `emails` mediumtext,
  `webaddresses` mediumtext,
  PRIMARY KEY (`idinstitution`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institution`
--

LOCK TABLES `institution` WRITE;
/*!40000 ALTER TABLE `institution` DISABLE KEYS */;
INSERT INTO `institution` VALUES (2,'Institucija','adresa15765789','','','','osnivac1','2019-03-14 00:00:00','2019-03-21 00:00:00',NULL,NULL,NULL,'','',''),(6,'test','adresa1','pirokrompo','kokodaka','bugumala','osnivac1','2019-03-14 00:00:00','2019-03-27 00:00:00',NULL,NULL,NULL,'c\\favbxb','bfbsfnsnsnf','ksfnalknfa');
/*!40000 ALTER TABLE `institution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `languages` (
  `idlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `languagename` varchar(45) DEFAULT NULL,
  `languageabbreviation` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idlanguages`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (3,'spanski','es'),(4,'nemacki','de'),(5,'engleski','en');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museum`
--

DROP TABLE IF EXISTS `museum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museum` (
  `idmuseum` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `audiourl` tinytext,
  `audiourllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `qr` tinytext,
  `idinstitution` int(11) DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idmuseum`),
  KEY `fk_museum_institution_idx` (`idinstitution`),
  CONSTRAINT `fk_museum_institution` FOREIGN KEY (`idinstitution`) REFERENCES `institution` (`idinstitution`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museum`
--

LOCK TABLES `museum` WRITE;
/*!40000 ALTER TABLE `museum` DISABLE KEYS */;
INSERT INTO `museum` VALUES (33,'Muzej1','sr','jnkj','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(34,'blaa','sr','kmonjo','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'m1'),(35,'Muzej1','sr','gfdgfd','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'m1'),(36,'Muzej2','sr','nknk','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'bkjbjk'),(45,'muzej123','sr','kjnkjnjknkjnkjn6757576','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'jnkjnkj'),(46,'Mikicin vrt','sr','hobiti ','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,6,'test'),(48,'muzej2',NULL,'njnjknkjn','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Institucija2'),(57,'huihiuhiu','sr','uihiuhuihuihiuh','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,2,'Institucija222');
/*!40000 ALTER TABLE `museum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museumadditional`
--

DROP TABLE IF EXISTS `museumadditional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museumadditional` (
  `idmuseumadditional` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `town` varchar(45) DEFAULT NULL,
  `township` varchar(45) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `buildyear` int(11) DEFAULT NULL,
  `openyear` int(11) DEFAULT NULL,
  `aims` mediumtext,
  `contactphones` mediumtext,
  PRIMARY KEY (`idmuseumadditional`),
  KEY `fk_museum_additional_idx` (`idmuseum`),
  CONSTRAINT `fk_museum_additional` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museumadditional`
--

LOCK TABLES `museumadditional` WRITE;
/*!40000 ALTER TABLE `museumadditional` DISABLE KEYS */;
INSERT INTO `museumadditional` VALUES (2,45,'tip1',NULL,NULL,NULL,NULL,'','','','','',NULL,NULL,'','');
/*!40000 ALTER TABLE `museumadditional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museumlanguages`
--

DROP TABLE IF EXISTS `museumlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museumlanguages` (
  `idmuseumlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmuseumlanguages`),
  KEY `fk_museum_museumlanguages_idx` (`idmuseum`),
  KEY `fk_language_museumlanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_language_museumlanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_museumlanguages` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museumlanguages`
--

LOCK TABLES `museumlanguages` WRITE;
/*!40000 ALTER TABLE `museumlanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `museumlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museummap`
--

DROP TABLE IF EXISTS `museummap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museummap` (
  `idmuseummap` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `room` varchar(45) DEFAULT NULL,
  `xstart` double DEFAULT NULL,
  `ystart` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `isfloor` tinyint(4) DEFAULT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `isstaircase` tinyint(4) DEFAULT NULL,
  `xend` double DEFAULT NULL,
  `yend` double DEFAULT NULL,
  PRIMARY KEY (`idmuseummap`),
  KEY `fk_museummap_museum_idx` (`idmuseum`),
  CONSTRAINT `FKhu2t65f6debhyfmh1nr3tdb1g` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museummap`
--

LOCK TABLES `museummap` WRITE;
/*!40000 ALTER TABLE `museummap` DISABLE KEYS */;
INSERT INTO `museummap` VALUES (149,'m1n','sr','sprat1','','',NULL,NULL,NULL,NULL,NULL,NULL,33,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(151,'m1n','sr','mapa1','','','',NULL,NULL,NULL,NULL,NULL,33,'','',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(153,'jnkjnkj','sr','sprat1','','ytyvftvtftttttttttttt',NULL,NULL,1,2,1,NULL,45,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,2,2),(160,'jnkjnkj','sr','stepeniste1','','','',NULL,1,1,1,NULL,45,NULL,NULL,0,NULL,NULL,NULL,NULL,0,2,2),(163,'jnkjnkj','sr','prostorija2','njknjknjkn','','',NULL,3,4,1,NULL,45,NULL,NULL,0,NULL,NULL,NULL,NULL,0,3,4);
/*!40000 ALTER TABLE `museummap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museummaplanguages`
--

DROP TABLE IF EXISTS `museummaplanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museummaplanguages` (
  `idmuseummaplanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseummap` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idmuseummaplanguages`),
  KEY `fk_museummap_museummaplanguages_idx` (`idmuseummap`),
  KEY `fk_language_museummaplanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_language_museummaplanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_museummaplanguages` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museummaplanguages`
--

LOCK TABLES `museummaplanguages` WRITE;
/*!40000 ALTER TABLE `museummaplanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `museummaplanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item` (
  `iditem` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `author` varchar(100) NOT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `audiourl` tinytext,
  `audiourllocal` tinytext,
  `audiotype` int(11) DEFAULT NULL,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `qr` varchar(45) DEFAULT NULL,
  `additionalimagesurl` mediumtext,
  `additionalimagesurllocal` mediumtext,
  `idmuseum` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  PRIMARY KEY (`iditem`),
  UNIQUE KEY `iditem_UNIQUE` (`iditem`),
  KEY `idmuseum_idx` (`idmuseum`),
  KEY `fk_picture_room_idx` (`idroom`),
  KEY `fk_picture_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_picture_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (3,'bla','sr','van gogh','van gogh','kjnjknkjn','','',NULL,NULL,NULL,'','',300,200,0,4,NULL,NULL,NULL,NULL,NULL,NULL,'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg/300px-Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg','https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg/300px-Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg'),(4,'m1','sr','jnjknkjn','jnkjnkjn','nkjnknk','','',NULL,NULL,NULL,'','',20,200,0,5,NULL,NULL,NULL,NULL,NULL,NULL,'njknkjnk',''),(6,'muzej','sr','jkhkjhkj','hkhkjkh','khkhkjhkj','','',NULL,NULL,NULL,'','',20,200,0,5,NULL,NULL,NULL,NULL,NULL,NULL,'khkhkjhkj',''),(49,'jnkjnkj','sr','nknkjnhjk','jknknkjn','kjnkjnjknkjn',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,45,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemaudio`
--

DROP TABLE IF EXISTS `itemaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemaudio` (
  `idpictureaudio` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `audiourl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `audiotype` int(11) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpictureaudio`),
  KEY `fk_picture_audio_idx` (`iditem`),
  KEY `fk_exhibition_audio_idx` (`idexhibition`),
  KEY `fk_museum_audio_idx` (`idmuseum`),
  KEY `fk_museummap_audio_idx` (`idmuseummap`),
  CONSTRAINT `FKl5awlmicq49sh8ab6u399pw9j` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_audio` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_audio` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_audio` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_audio` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemaudio`
--

LOCK TABLES `itemaudio` WRITE;
/*!40000 ALTER TABLE `itemaudio` DISABLE KEYS */;
INSERT INTO `itemaudio` VALUES (4,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1548694589/soowfmxsnfbdn1b41sf1.mp3',NULL,NULL,'soowfmxsnfbdn1b41sf1','LjubaPopovic.mp3',NULL,NULL,NULL);
/*!40000 ALTER TABLE `itemaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemimage`
--

DROP TABLE IF EXISTS `itemimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemimage` (
  `idpictureimage` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `imageurl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpictureimage`),
  KEY `fk_picture_image_idx` (`iditem`),
  KEY `fk_exhibition_image_idx` (`idexhibition`),
  KEY `fk_museum_image_idx` (`idmuseum`),
  KEY `fk_museummap_image_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_image` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_image` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_image` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_image` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemimage`
--

LOCK TABLES `itemimage` WRITE;
/*!40000 ALTER TABLE `itemimage` DISABLE KEYS */;
INSERT INTO `itemimage` VALUES (48,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1552508413/g2qoezkjt0stth8kzz2t.jpg',NULL,'g2qoezkjt0stth8kzz2t',NULL,NULL,153),(49,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1552509376/rczzpupijpgw5kcrzowv.jpg',NULL,'rczzpupijpgw5kcrzowv',NULL,NULL,153),(64,49,'http://res.cloudinary.com/dktuxe18z/image/upload/v1553266134/tu4rlot1nqckrbcvw9nt.jpg',NULL,'tu4rlot1nqckrbcvw9nt',NULL,NULL,NULL),(69,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1553287386/ly00y8rw3sjghr4x6mzb.jpg',NULL,'ly00y8rw3sjghr4x6mzb',NULL,46,NULL),(84,49,'http://res.cloudinary.com/dktuxe18z/image/upload/v1553973690/xsjd80pmpfrx7s83vjap.jpg',NULL,'xsjd80pmpfrx7s83vjap',NULL,NULL,NULL),(85,49,'http://res.cloudinary.com/dktuxe18z/image/upload/v1553974184/v7l9akjxbnusvugjo3w3.jpg',NULL,'v7l9akjxbnusvugjo3w3',NULL,NULL,NULL),(86,49,'http://res.cloudinary.com/dktuxe18z/image/upload/v1553974217/mqqleayxj9cviren6os2.jpg',NULL,'mqqleayxj9cviren6os2',NULL,NULL,NULL),(87,49,'http://res.cloudinary.com/dktuxe18z/image/upload/v1553974224/s9hs2qc1wsxdeoj24tcm.jpg',NULL,'s9hs2qc1wsxdeoj24tcm',NULL,NULL,NULL),(90,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1554035300/hd8fry8nbfhu03szqcuk.jpg',NULL,'hd8fry8nbfhu03szqcuk',NULL,NULL,160),(91,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1554216174/tudvvfvdgnsc1nwqe7vf.jpg',NULL,'tudvvfvdgnsc1nwqe7vf',NULL,NULL,160),(92,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1554401931/klge3pdojjf5ligwrd3j.jpg',NULL,'klge3pdojjf5ligwrd3j',106,NULL,NULL),(93,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1554402349/pcazkyy1xpi86phhfgpz.jpg',NULL,'pcazkyy1xpi86phhfgpz',NULL,45,NULL);
/*!40000 ALTER TABLE `itemimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemlanguages`
--

DROP TABLE IF EXISTS `itemlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemlanguages` (
  `idpicturelanguages` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idpicturelanguages`),
  KEY `fk_picture_picturelanguages_idx` (`iditem`),
  KEY `fk_language_picturelanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_language_picturelanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_picturelanguages` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemlanguages`
--

LOCK TABLES `itemlanguages` WRITE;
/*!40000 ALTER TABLE `itemlanguages` DISABLE KEYS */;
INSERT INTO `itemlanguages` VALUES (2,49,3,'cfgcgfcgcc','gfcgcgfc','cgfcgfc','gfcgcgfc');
/*!40000 ALTER TABLE `itemlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemtext`
--

DROP TABLE IF EXISTS `itemtext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemtext` (
  `idpicturetext` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `text` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idpicturetext`),
  KEY `fk_picture_idx` (`iditem`),
  CONSTRAINT `fk_picture_text` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemtext`
--

LOCK TABLES `itemtext` WRITE;
/*!40000 ALTER TABLE `itemtext` DISABLE KEYS */;
INSERT INTO `itemtext` VALUES (23,49,'Petar Zika Mika',NULL),(24,49,'xcxvcx',NULL),(25,49,'vcxvcxvc',NULL),(26,49,'xvcxvcx',NULL);
/*!40000 ALTER TABLE `itemtext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `picturetoexhibition`
--

DROP TABLE IF EXISTS `picturetoexhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `picturetoexhibition` (
  `idpicturetoexhibition` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) NOT NULL,
  `idexhibition` int(11) NOT NULL,
  PRIMARY KEY (`idpicturetoexhibition`),
  KEY `fk_exebition_picture_idx` (`idexhibition`),
  KEY `fk_picture_exhibition_idx` (`iditem`),
  CONSTRAINT `fk_exhibition_picture` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_exhibition` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `picturetoexhibition`
--

LOCK TABLES `picturetoexhibition` WRITE;
/*!40000 ALTER TABLE `picturetoexhibition` DISABLE KEYS */;
INSERT INTO `picturetoexhibition` VALUES (22,49,106);
/*!40000 ALTER TABLE `picturetoexhibition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemvideo`
--

DROP TABLE IF EXISTS `itemvideo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemvideo` (
  `idpicturevideo` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `videourl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpicturevideo`),
  KEY `fk_picture_video_idx` (`iditem`),
  KEY `fk_exhibition_video_idx` (`idexhibition`),
  KEY `fk_museum_video_idx` (`idmuseum`),
  KEY `fk_museummap_video_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_video` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_video` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_video` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_video` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemvideo`
--

LOCK TABLES `itemvideo` WRITE;
/*!40000 ALTER TABLE `itemvideo` DISABLE KEYS */;
INSERT INTO `itemvideo` VALUES (4,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1548695062/fx9ofrzxxzs8hrythqnp.mp4',NULL,'fx9ofrzxxzs8hrythqnp','SampleVideo_1280x720_2mb.mp4',NULL,NULL,NULL),(5,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1549043779/wjec6ikaszfxoxy9bhtb.mp4',NULL,'wjec6ikaszfxoxy9bhtb','SampleVideo_1280x720_2mb.mp4',NULL,NULL,NULL),(6,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1549044075/gjeglhfhjlaj2gc1lvl6.mp4',NULL,'gjeglhfhjlaj2gc1lvl6','SampleVideo_1280x720_2mb.mp4',NULL,NULL,NULL),(9,49,'http://res.cloudinary.com/dktuxe18z/video/upload/v1553200720/wqbrnym9ollhj1evghmn.mp4',NULL,'wqbrnym9ollhj1evghmn','SampleVideo_360x240_1mb.mp4',NULL,NULL,NULL),(14,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1554037606/ckcnuh2bj1e8tw4slvgo.mp4',NULL,'ckcnuh2bj1e8tw4slvgo','SampleVideo_360x240_1mb.mp4',NULL,NULL,160),(16,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1554401937/wzmgsmatj7nlw4exfyxj.mp4',NULL,'wzmgsmatj7nlw4exfyxj','SampleVideo_360x240_1mb.mp4',106,NULL,NULL),(17,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1554402357/kfybicdi5xhjbp9nc2c.mp4',NULL,'kfybicdi5xhjbp9nc2c','SampleVideo_360x240_1mb.mp4',NULL,45,NULL);
/*!40000 ALTER TABLE `itemvideo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `room` (
  `idroom` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  PRIMARY KEY (`idroom`),
  KEY `fk_room_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_room_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (101,'m1n','sr','mapa1','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,33,NULL,NULL),(108,'jnkjnkj','sr','stepeniste1','','','',NULL,NULL,NULL,NULL,1,1,1,NULL,45,NULL,NULL),(111,'jnkjnkj','sr','prostorija2','njknjknjkn','','',NULL,NULL,NULL,NULL,3,4,1,NULL,45,NULL,NULL);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomtoroom`
--

DROP TABLE IF EXISTS `roomtoroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roomtoroom` (
  `idroomtoroom` int(11) NOT NULL AUTO_INCREMENT,
  `idroom1` int(11) NOT NULL,
  `idroom2` int(11) NOT NULL,
  PRIMARY KEY (`idroomtoroom`),
  KEY `fk_room1_room2_idx` (`idroom1`),
  KEY `fk_room2_room1_idx` (`idroom2`),
  CONSTRAINT `fk_room1_room2` FOREIGN KEY (`idroom1`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_room2_room1` FOREIGN KEY (`idroom2`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomtoroom`
--

LOCK TABLES `roomtoroom` WRITE;
/*!40000 ALTER TABLE `roomtoroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `roomtoroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'jeja','jejacar','jeja','zigic'),(2,'jejazigic','jejajeja','jeja2','zigic'),(3,'jejazigic1','jejajejajeja','jelenaa','zigiccc'),(4,'milan93','perazdera','milan','rajic'),(5,'peraaa','perazdera','pera','peric'),(6,'anazigic','anaanaana','ana','zigic'),(7,'jejamilan','jejamilan','jejamilan','jelan'),(8,'milannn','milanmilan','milan','milan'),(9,'milja9333','milja9333','milja','milenkovic'),(10,'blaaaaaa','blaaaaaa','blaa','njknjk'),(11,'blaaaaaaaa','blaaaaaaaa','blaaaaaaaa','blaaaaaaaa'),(12,'peraaaaAA','peraaaaAA','peraaaa','peraaaa'),(13,'redirect','redirect','redirect','redirect'),(14,'redirectredirect','redirectredirect','redirectredirect','hbhj'),(15,'aaaaaaaaaaaa','aaaaaaaaaaaa','njknkjn','nknkn'),(16,'aaaaaaaaaaaaaaaaaaaaaaaa','aaaaaaaaaaaa','axsaanj','nknknk'),(17,'wdewfddwe','wdewfddwe','wdewfddwe','wdewfddwe'),(18,'asaasaasa','asaasaasa','asa','asa'),(19,'jijijiijiiiji','jijijiijiiiji','jijijij','jijiji'),(20,'qqqqqqqqqqqqq','qqqqqqqqqqqqq','qqqqqqqqqqqqq','qqqqqqqqqqqqq'),(21,'eeeeeeeeeeee','eeeeeeeeeeee','eeeeeeeeeeee','eeeeeeeeeeee'),(22,'ancxo32','laki3216','Ana','Zigic'),(23,'ancxo3216','laki3216','Ana','Zigic'),(24,'jkhkjh','hkjhkhkjhk','hkhkhkjhkj','hkhkhkj');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'master_app'
--

--
-- Dumping routines for database 'master_app'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-04 22:24:59
