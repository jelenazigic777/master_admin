-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: master_app
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ambientaudio`
--

DROP TABLE IF EXISTS `ambientaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ambientaudio` (
  `idambientaudio` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `ambientaudiourl` longtext,
  `idroom` int(11) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `idfloor` int(11) DEFAULT NULL,
  PRIMARY KEY (`idambientaudio`),
  UNIQUE KEY `UK_qwg6u8eabd5qc6j3ni02fabhc` (`idexhibition`),
  KEY `fk_ambientaudio_museum_idx` (`idmuseum`),
  KEY `fk_ambientaudio_room_idx` (`idroom`),
  KEY `fk_ambientaudio_exhibition_idx` (`idexhibition`),
  KEY `fk_ambientaudio_floor_idx` (`idfloor`),
  CONSTRAINT `fk_ambientaudio_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_floor` FOREIGN KEY (`idfloor`) REFERENCES `museummap` (`idmuseummap`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ambientaudio`
--

LOCK TABLES `ambientaudio` WRITE;
/*!40000 ALTER TABLE `ambientaudio` DISABLE KEYS */;
/*!40000 ALTER TABLE `ambientaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barcodeimage`
--

DROP TABLE IF EXISTS `barcodeimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `barcodeimage` (
  `idbarcodeimage` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `imageurl` longtext,
  `serverid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idbarcodeimage`),
  KEY `fk_barcode_picture_idx` (`iditem`),
  CONSTRAINT `fk_barcode_item` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barcodeimage`
--

LOCK TABLES `barcodeimage` WRITE;
/*!40000 ALTER TABLE `barcodeimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `barcodeimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beacon`
--

DROP TABLE IF EXISTS `beacon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `beacon` (
  `idbeacon` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `power` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`idbeacon`),
  KEY `fk_beacon_museum_idx` (`idmuseum`),
  KEY `fk_beacon_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_beacon_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_beacon_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beacon`
--

LOCK TABLES `beacon` WRITE;
/*!40000 ALTER TABLE `beacon` DISABLE KEYS */;
/*!40000 ALTER TABLE `beacon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibition`
--

DROP TABLE IF EXISTS `exhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibition` (
  `idexhibition` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `room` varchar(45) DEFAULT NULL,
  `additionalimagesurl` mediumtext,
  `additionalimagesurllocal` mediumtext,
  `idmuseum` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idexhibition`),
  KEY `fk_exibition_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_exibition_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibition`
--

LOCK TABLES `exhibition` WRITE;
/*!40000 ALTER TABLE `exhibition` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionaudio`
--

DROP TABLE IF EXISTS `exhibitionaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionaudio` (
  `idexhibitionaudio` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `iditemaudio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionaudio`),
  KEY `fk_pictureaudio_exhibition_idx` (`idexhibition`),
  KEY `fk_exhibition_pictureaudio_idx` (`iditemaudio`),
  CONSTRAINT `fk_exhibition_itemaudio` FOREIGN KEY (`iditemaudio`) REFERENCES `itemaudio` (`iditemaudio`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemaudio_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionaudio`
--

LOCK TABLES `exhibitionaudio` WRITE;
/*!40000 ALTER TABLE `exhibitionaudio` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionimage`
--

DROP TABLE IF EXISTS `exhibitionimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionimage` (
  `idexhibitionimage` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `iditemimage` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionimage`),
  KEY `fk_exhibition_pictureimage_idx` (`iditemimage`),
  KEY `fk_pictureimage_exhibition_idx` (`idexhibition`),
  CONSTRAINT `fk_exhibition_itemimage` FOREIGN KEY (`iditemimage`) REFERENCES `itemimage` (`iditemimage`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemimage_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionimage`
--

LOCK TABLES `exhibitionimage` WRITE;
/*!40000 ALTER TABLE `exhibitionimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionlanguages`
--

DROP TABLE IF EXISTS `exhibitionlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionlanguages` (
  `idexhibitionlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(300) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionlanguages`),
  KEY `fk_exhibition_exhibitionlanguages_idx` (`idexhibition`),
  KEY `fk_language_exhibitionlanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_exhibition_exhibitionlanguages` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_language_exhibitionlanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionlanguages`
--

LOCK TABLES `exhibitionlanguages` WRITE;
/*!40000 ALTER TABLE `exhibitionlanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitiontext`
--

DROP TABLE IF EXISTS `exhibitiontext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitiontext` (
  `idexhibitiontext` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `iditemtext` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitiontext`),
  KEY `fk_exhibition_picturetext_idx` (`idexhibition`),
  KEY `fk_picturetext_exhibition_idx` (`iditemtext`),
  CONSTRAINT `fk_exhibition_itemtext` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemtext_exhibition` FOREIGN KEY (`iditemtext`) REFERENCES `itemtext` (`iditemtext`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitiontext`
--

LOCK TABLES `exhibitiontext` WRITE;
/*!40000 ALTER TABLE `exhibitiontext` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitiontext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitiontomuseummap`
--

DROP TABLE IF EXISTS `exhibitiontomuseummap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitiontomuseummap` (
  `idexhibitiontomuseummap` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseummap` int(11) NOT NULL,
  `idexhibition` int(11) NOT NULL,
  PRIMARY KEY (`idexhibitiontomuseummap`),
  KEY `fk_exibition_idx` (`idexhibition`),
  KEY `fk_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_museummap` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_exhibition` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitiontomuseummap`
--

LOCK TABLES `exhibitiontomuseummap` WRITE;
/*!40000 ALTER TABLE `exhibitiontomuseummap` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitiontomuseummap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionvideo`
--

DROP TABLE IF EXISTS `exhibitionvideo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionvideo` (
  `idexhibitionvideo` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `iditemvideo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionvideo`),
  KEY `fk_exhibition_picturevideo_idx` (`idexhibition`),
  KEY `fk_picturevideo_exhibition_idx` (`iditemvideo`),
  CONSTRAINT `fk_exhibition_itemvideo` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemvideo_exhibition` FOREIGN KEY (`iditemvideo`) REFERENCES `itemvideo` (`iditemvideo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionvideo`
--

LOCK TABLES `exhibitionvideo` WRITE;
/*!40000 ALTER TABLE `exhibitionvideo` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionvideo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `floormap`
--

DROP TABLE IF EXISTS `floormap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `floormap` (
  `idfloormap` int(11) NOT NULL AUTO_INCREMENT,
  `xstart` double DEFAULT NULL,
  `ystart` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `xend` double DEFAULT NULL,
  `yend` double DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `idmuseummap` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `floornumber` int(11) NOT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  PRIMARY KEY (`idfloormap`),
  KEY `idmuseum_idx` (`idmuseum`),
  KEY `fk_floormap_museummap_idx` (`idmuseummap`),
  KEY `fk_floormap_room_idx` (`idroom`),
  CONSTRAINT `fk_floormap_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_floormap_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_floormap_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `floormap`
--

LOCK TABLES `floormap` WRITE;
/*!40000 ALTER TABLE `floormap` DISABLE KEYS */;
/*!40000 ALTER TABLE `floormap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institution`
--

DROP TABLE IF EXISTS `institution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `institution` (
  `idinstitution` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `township` varchar(45) DEFAULT NULL,
  `town` varchar(45) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `founder` varchar(45) DEFAULT NULL,
  `foundationdate` datetime DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `idnumber` int(11) DEFAULT NULL,
  `registrationnumber` int(11) DEFAULT NULL,
  `taxid` int(11) DEFAULT NULL,
  `contactphones` mediumtext,
  `emails` mediumtext,
  `webaddresses` mediumtext,
  PRIMARY KEY (`idinstitution`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institution`
--

LOCK TABLES `institution` WRITE;
/*!40000 ALTER TABLE `institution` DISABLE KEYS */;
/*!40000 ALTER TABLE `institution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item` (
  `iditem` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `author` varchar(100) NOT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `audiourl` tinytext,
  `audiourllocal` tinytext,
  `audiotype` int(11) DEFAULT NULL,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `qr` varchar(45) DEFAULT NULL,
  `additionalimagesurl` mediumtext,
  `additionalimagesurllocal` mediumtext,
  `idmuseum` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `identificationcode` varchar(100) DEFAULT NULL,
  `idbarcodepicture` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditem`),
  UNIQUE KEY `iditem_UNIQUE` (`iditem`),
  KEY `idmuseum_idx` (`idmuseum`),
  KEY `fk_picture_room_idx` (`idroom`),
  KEY `fk_picture_museummap_idx` (`idmuseummap`),
  KEY `fk_picture_barcode_idx` (`idbarcodepicture`),
  CONSTRAINT `fk_item_barcode` FOREIGN KEY (`idbarcodepicture`) REFERENCES `itemimage` (`iditemimage`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_item_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemaudio`
--

DROP TABLE IF EXISTS `itemaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemaudio` (
  `iditemaudio` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `audiourl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `audiotype` int(11) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditemaudio`),
  KEY `fk_picture_audio_idx` (`iditem`),
  KEY `fk_exhibition_audio_idx` (`idexhibition`),
  KEY `fk_museum_audio_idx` (`idmuseum`),
  KEY `fk_museummap_audio_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_audio` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_audio` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_audio` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_audio` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemaudio`
--

LOCK TABLES `itemaudio` WRITE;
/*!40000 ALTER TABLE `itemaudio` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemimage`
--

DROP TABLE IF EXISTS `itemimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemimage` (
  `iditemimage` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `imageurl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditemimage`),
  KEY `fk_picture_image_idx` (`iditem`),
  KEY `fk_exhibition_image_idx` (`idexhibition`),
  KEY `fk_museum_image_idx` (`idmuseum`),
  KEY `fk_museummap_image_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_image` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_image` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_image` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_image` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemimage`
--

LOCK TABLES `itemimage` WRITE;
/*!40000 ALTER TABLE `itemimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemlanguages`
--

DROP TABLE IF EXISTS `itemlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemlanguages` (
  `iditemlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`iditemlanguages`),
  KEY `fk_picture_picturelanguages_idx` (`iditem`),
  KEY `fk_language_picturelanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_itemlanguage_language` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_language_itemlanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemlanguages`
--

LOCK TABLES `itemlanguages` WRITE;
/*!40000 ALTER TABLE `itemlanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemtext`
--

DROP TABLE IF EXISTS `itemtext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemtext` (
  `iditemtext` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `text` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`iditemtext`),
  KEY `fk_picture_idx` (`iditem`),
  CONSTRAINT `fk_item_text` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemtext`
--

LOCK TABLES `itemtext` WRITE;
/*!40000 ALTER TABLE `itemtext` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemtext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemtoexhibition`
--

DROP TABLE IF EXISTS `itemtoexhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemtoexhibition` (
  `iditemtoexhibition` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) NOT NULL,
  `idexhibition` int(11) NOT NULL,
  PRIMARY KEY (`iditemtoexhibition`),
  KEY `fk_exebition_picture_idx` (`idexhibition`),
  KEY `fk_picture_exhibition_idx` (`iditem`),
  CONSTRAINT `fk_exhibition_item` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_exhibition` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemtoexhibition`
--

LOCK TABLES `itemtoexhibition` WRITE;
/*!40000 ALTER TABLE `itemtoexhibition` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemtoexhibition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemvideo`
--

DROP TABLE IF EXISTS `itemvideo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemvideo` (
  `iditemvideo` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `videourl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditemvideo`),
  KEY `fk_picture_video_idx` (`iditem`),
  KEY `fk_exhibition_video_idx` (`idexhibition`),
  KEY `fk_museum_video_idx` (`idmuseum`),
  KEY `fk_museummap_video_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_video` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_video` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_video` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_video` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemvideo`
--

LOCK TABLES `itemvideo` WRITE;
/*!40000 ALTER TABLE `itemvideo` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemvideo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `languages` (
  `idlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `languagename` varchar(45) DEFAULT NULL,
  `languageabbreviation` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idlanguages`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (3,'spanski','es'),(4,'nemacki','de'),(5,'engleski','en');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museum`
--

DROP TABLE IF EXISTS `museum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museum` (
  `idmuseum` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `audiourl` tinytext,
  `audiourllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `qr` tinytext,
  `idinstitution` int(11) DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idmuseum`),
  KEY `fk_museum_institution_idx` (`idinstitution`),
  CONSTRAINT `fk_museum_institution` FOREIGN KEY (`idinstitution`) REFERENCES `institution` (`idinstitution`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museum`
--

LOCK TABLES `museum` WRITE;
/*!40000 ALTER TABLE `museum` DISABLE KEYS */;
/*!40000 ALTER TABLE `museum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museumadditional`
--

DROP TABLE IF EXISTS `museumadditional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museumadditional` (
  `idmuseumadditional` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `town` varchar(45) DEFAULT NULL,
  `township` varchar(45) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `buildyear` int(11) DEFAULT NULL,
  `openyear` int(11) DEFAULT NULL,
  `aims` mediumtext,
  `contactphones` mediumtext,
  PRIMARY KEY (`idmuseumadditional`),
  KEY `fk_museum_additional_idx` (`idmuseum`),
  CONSTRAINT `fk_museum_additional` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museumadditional`
--

LOCK TABLES `museumadditional` WRITE;
/*!40000 ALTER TABLE `museumadditional` DISABLE KEYS */;
/*!40000 ALTER TABLE `museumadditional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museumlanguages`
--

DROP TABLE IF EXISTS `museumlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museumlanguages` (
  `idmuseumlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmuseumlanguages`),
  KEY `fk_museum_museumlanguages_idx` (`idmuseum`),
  KEY `fk_language_museumlanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_language_museumlanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_museumlanguages` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museumlanguages`
--

LOCK TABLES `museumlanguages` WRITE;
/*!40000 ALTER TABLE `museumlanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `museumlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museummap`
--

DROP TABLE IF EXISTS `museummap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museummap` (
  `idmuseummap` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `room` varchar(45) DEFAULT NULL,
  `xstart` double DEFAULT NULL,
  `ystart` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `isfloor` tinyint(4) DEFAULT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `isstaircase` tinyint(4) DEFAULT NULL,
  `xend` double DEFAULT NULL,
  `yend` double DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idmuseummap`),
  KEY `fk_museummap_museum_idx` (`idmuseum`),
  CONSTRAINT `FKhu2t65f6debhyfmh1nr3tdb1g` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museummap`
--

LOCK TABLES `museummap` WRITE;
/*!40000 ALTER TABLE `museummap` DISABLE KEYS */;
/*!40000 ALTER TABLE `museummap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museummaplanguages`
--

DROP TABLE IF EXISTS `museummaplanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museummaplanguages` (
  `idmuseummaplanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseummap` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idmuseummaplanguages`),
  KEY `fk_museummap_museummaplanguages_idx` (`idmuseummap`),
  KEY `fk_language_museummaplanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_language_museummaplanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_museummaplanguages` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museummaplanguages`
--

LOCK TABLES `museummaplanguages` WRITE;
/*!40000 ALTER TABLE `museummaplanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `museummaplanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `idrole` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `room` (
  `idroom` int(11) NOT NULL AUTO_INCREMENT,
  `institutuion` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  PRIMARY KEY (`idroom`),
  KEY `fk_room_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_room_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomtoroom`
--

DROP TABLE IF EXISTS `roomtoroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roomtoroom` (
  `idroomtoroom` int(11) NOT NULL AUTO_INCREMENT,
  `idroom1` int(11) NOT NULL,
  `idroom2` int(11) NOT NULL,
  PRIMARY KEY (`idroomtoroom`),
  KEY `fk_room1_room2_idx` (`idroom1`),
  KEY `fk_room2_room1_idx` (`idroom2`),
  CONSTRAINT `fk_room1_room2` FOREIGN KEY (`idroom1`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_room2_room1` FOREIGN KEY (`idroom2`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomtoroom`
--

LOCK TABLES `roomtoroom` WRITE;
/*!40000 ALTER TABLE `roomtoroom` DISABLE KEYS */;
/*!40000 ALTER TABLE `roomtoroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'jeja','jejacar','jeja','zigic',1),(25,'pera123@gmail.com','pera1234','Pera','Peric',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `userrole` (
  `iduserrole` int(11) NOT NULL AUTO_INCREMENT,
  `idrole` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduserrole`),
  KEY `fk_roles_users_idx` (`idrole`),
  KEY `fk_users_roles_idx` (`iduser`),
  CONSTRAINT `fk_roles_users` FOREIGN KEY (`idrole`) REFERENCES `role` (`idrole`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_roles` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrole`
--

LOCK TABLES `userrole` WRITE;
/*!40000 ALTER TABLE `userrole` DISABLE KEYS */;
INSERT INTO `userrole` VALUES (1,1,1);
/*!40000 ALTER TABLE `userrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'master_app'
--

--
-- Dumping routines for database 'master_app'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-11 21:07:42
