-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: master_app
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `itemvideo`
--

DROP TABLE IF EXISTS `itemvideo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemvideo` (
  `idpicturevideo` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `videourl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpicturevideo`),
  KEY `fk_picture_video_idx` (`iditem`),
  KEY `fk_exhibition_video_idx` (`idexhibition`),
  KEY `fk_museum_video_idx` (`idmuseum`),
  CONSTRAINT `fk_exhibition_video` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_video` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_picture_video` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemvideo`
--

LOCK TABLES `itemvideo` WRITE;
/*!40000 ALTER TABLE `itemvideo` DISABLE KEYS */;
INSERT INTO `itemvideo` VALUES (1,43,'http://res.cloudinary.com/dktuxe18z/video/upload/v1548248331/ccd1bikopcb8l7az1iwu.mp4',NULL,'ccd1bikopcb8l7az1iwu','SampleVideo_1280x720_2mb.mp4',NULL,NULL),(4,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1548695062/fx9ofrzxxzs8hrythqnp.mp4',NULL,'fx9ofrzxxzs8hrythqnp','SampleVideo_1280x720_2mb.mp4',NULL,NULL),(5,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1549043779/wjec6ikaszfxoxy9bhtb.mp4',NULL,'wjec6ikaszfxoxy9bhtb','SampleVideo_1280x720_2mb.mp4',NULL,NULL),(6,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1549044075/gjeglhfhjlaj2gc1lvl6.mp4',NULL,'gjeglhfhjlaj2gc1lvl6','SampleVideo_1280x720_2mb.mp4',NULL,NULL);
/*!40000 ALTER TABLE `itemvideo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-12 21:11:06
