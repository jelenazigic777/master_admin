-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: master_app
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ambientaudio`
--

DROP TABLE IF EXISTS `ambientaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ambientaudio` (
  `idambientaudio` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `ambientaudiourl` longtext,
  `idroom` int(11) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `idfloor` int(11) DEFAULT NULL,
  PRIMARY KEY (`idambientaudio`),
  UNIQUE KEY `UK_qwg6u8eabd5qc6j3ni02fabhc` (`idexhibition`),
  KEY `fk_ambientaudio_museum_idx` (`idmuseum`),
  KEY `fk_ambientaudio_room_idx` (`idroom`),
  KEY `fk_ambientaudio_exhibition_idx` (`idexhibition`),
  KEY `fk_ambientaudio_floor_idx` (`idfloor`),
  CONSTRAINT `fk_ambientaudio_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_floor` FOREIGN KEY (`idfloor`) REFERENCES `museummap` (`idmuseummap`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ambientaudio_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ambientaudio`
--

LOCK TABLES `ambientaudio` WRITE;
/*!40000 ALTER TABLE `ambientaudio` DISABLE KEYS */;
/*!40000 ALTER TABLE `ambientaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barcodeimage`
--

DROP TABLE IF EXISTS `barcodeimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `barcodeimage` (
  `idbarcodeimage` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `imageurl` longtext,
  `serverid` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idbarcodeimage`),
  KEY `fk_barcode_picture_idx` (`iditem`),
  CONSTRAINT `fk_barcode_item` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barcodeimage`
--

LOCK TABLES `barcodeimage` WRITE;
/*!40000 ALTER TABLE `barcodeimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `barcodeimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beacon`
--

DROP TABLE IF EXISTS `beacon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `beacon` (
  `idbeacon` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `major` int(11) DEFAULT NULL,
  `minor` int(11) DEFAULT NULL,
  `power` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`idbeacon`),
  KEY `fk_beacon_museum_idx` (`idmuseum`),
  KEY `fk_beacon_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_beacon_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_beacon_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beacon`
--

LOCK TABLES `beacon` WRITE;
/*!40000 ALTER TABLE `beacon` DISABLE KEYS */;
INSERT INTO `beacon` VALUES (17,'1',5.41,0.56,0,3,70,20,50,85,198),(18,'2',7.03,5.03,0,3,70,20,50,85,198),(19,'3',11.66,5.03,0,3,70,20,50,85,198),(20,'4',10.94,0.75,0,3,70,20,50,85,198),(21,'5',2.84,11.94,0,3,70,20,50,85,199),(22,'6',4.81,6.59,0,3,70,20,50,85,199),(23,'7',7.28,11.34,0,3,70,20,50,85,199),(24,'8',12.5,11.69,0,3,70,20,50,85,200),(25,'9',13.03,6.94,0,3,70,20,50,85,200),(26,'10',16.75,11.53,0,3,70,20,50,85,200),(27,'11',4.34,6.66,0,3,70,20,50,85,202),(28,'12',5.47,11.84,0,3,70,20,50,85,202),(29,'13',9.41,9.41,0,3,70,20,50,85,202);
/*!40000 ALTER TABLE `beacon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibition`
--

DROP TABLE IF EXISTS `exhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibition` (
  `idexhibition` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `room` varchar(45) DEFAULT NULL,
  `additionalimagesurl` mediumtext,
  `additionalimagesurllocal` mediumtext,
  `idmuseum` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idexhibition`),
  KEY `fk_exibition_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_exibition_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibition`
--

LOCK TABLES `exhibition` WRITE;
/*!40000 ALTER TABLE `exhibition` DISABLE KEYS */;
INSERT INTO `exhibition` VALUES (154,'Народни музеј Пожаревац',NULL,'Праисторија','непознат','Браничево представља једну од главних геостратегијских тачака југоисточне Европе. Природне комуникације, плодно земљиште и рудама богате заледе, условили су богату културно-историјску прошлост овог простора. Први трагови човека потичу из времена старијег неолита, око 6000. године п.н.е. Крајем неолита, око 4500. године п.н.е. долази до формирања винчанске цивилизације, која је међу првима дошла до открића металургије бакра, која је потврђена на локалитету Беловоде код Петровца. Време развијеног бакарног доба, познато је у науци под називом костолачка култура, која је име добила по налазишту које је смештено у селу Костолцу. \r\n\r\nТоком бронзаног доба, на простору Браничева егзистира ватинска култура, коју смењује дубовачко-жутобрдска, названа по локалитету Жуто Брдо код Голупца. Откривена гробља у Подунављу, раскошним керамичким материјалом говоре о високом степену духовности. О великом значају овог простора крајем II миленијума старе ере, сведоче бројне оставе металних предмета. Оне сведоче о високом степену духовности и технолошко-металуршком знању становника овог простора. \r\n\r\nТоком старијег гвозденог доба у овом делу Србије живе Трибали, једно од најзначајнијих старобалканских племена. Њихови гробови, откривени у Костолцу, садрже бројне предмете који осветљавају овај период. Крајем IV века старе ере у српско Подунавље стижу Келти. \r\n\r\nОни на овом простору формирају базно подручје за експанзију на Балкан. Најзначајније налазиште је некропола Пећине у селу Костолцу.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,85,1),(155,'Народни музеј Пожаревац',NULL,'Антика','непознат','Област српског Подунавља улази у интересну сферу Римског царства крајем I века п.н.е. Геостратешки разлози доводе до изградње прве утврђене границе на Дунаву - Лимеса. На простору од ушћа Мораве до доњомезијске границе лежали су легијски логори и градска насеља: Margum (Дубравица), Viminacium (Костолац), као и већи број мањих утврђења и насеља: Lederata (Рам), Pincum (Велико Градисте), Cuppae (Голубац), а јужно се налазио Mansio Municipium (Калиште) и Iovis Pagus (Велико Лаоле). Најзначајнији војно-политички и економски пункт, највеће градско насеље и центар провинције Горње Мезије (Moesia Superior), био је Виминацијум.\r\n\r\nВиминацијум (Viminacium) је настао средином I века н.е. као логор легије VII Claudiae. Виминацијум је све до краја Царства имао, често, пресудну улогу у догађајима значајним по његов опстанак. Такав положај је и озваничен додељивањем статуса муниципијума, под Хадријаном 117. године, и колоније, 239. године под Гордијаном III, када добија и право ковања свог локалног новца. За време династије Севера, Виминацијум је доживео свој највећи економски и културни просперитет. У другој половини III века, Виминацијум је био центар војно-политичких збивања у Царству, која обележавају претендентске борбе, где битну улогу игра легија VII Claudiae.\r\nМаргум (Margum) је римски град који се налазио на десној обали Велике Мораве, у близини њеног ушћа у Дунав. Налази се у атару села Дубравица. Крајем I века овде је подигнут војни логор у коме се неко време налазила легија IV Flavia. Од насеља око логора, највероватније је у доба Марка Аурелија настао муниципијум, чији је назив гласио Municipium Aurelium Augustum Margum. У граду су се налазили делови домаће милиције и делови дунавске флоте. 441. године град су освојили и разорили Хуни. Од налаза се издвајају: мермерна глава императора Каринуса, римски скиптар, бронзана скулптура дечака са лампом и др.\r\n\r\nЛедерата (Lederata) је римско утврђење настало на платоу изнад десне обале Дунава код Рама. Утврђење је настало крајем 1. или почетком 2. века, у току припрема за Трајанов први рат са Дачанима. На том месту је био постављен понтонски мост, који је приказан и на трајановом стубу, преко кога је Трајан са својом војском прешао Дунав 101. године. У утврђењу је био стациониран део легије VII Claudie. Од налаза требало би издвојити мермерну скулптуру богиње Викторије и натпис уклесан у стену посвећен богу Јупитеру.\r\nПинкум (Pincum) је римско насеље које је лежало на ушћу Пека у Дунав, на месту данашњег Великог Градишта. У утврђењу , подигнутом у II веку, био је стациониран део легије VII Claudiae. Утврђење је страдало у најезди Хуна 441. године, а обновљено је у време Јустинијана.\r\n\r\nКупе (Cupae) су римска станица на дунавском путу, која се налазила на високој дунавској тераси код Голупца. На узвишењу изнад Голупца видљиви су остаци већег кастела. Касније су овде стационирани делови легије IV Flaviae. Налажене су опеке са жигом LEG(io) IIII F(lavia) F(elix) CUPP(is).\r\nУ оквиру античке поставке изложени су предмети који потичу са горе наведених античких локалитета у Браничевском округу. Многи предмети су европски и светски раритети : мермерни саркофаг са гирландама, грб Виминацијума, каламус (писаљка од кости), портрет императора Каринуса и др. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,85,1),(159,'Народни музеј Пожаревац',NULL,'Фреске гробнице ','непознат','Фреске из ове гробнице пружају идеју како се паганство преображавало у Хришћанство. Гробница је имала облик куће и била је цела осликана фрексама. На западном зиду приказан је Христов монограм. На источном зиду налази се представа два пауна између којих је посуда са водом - кантарос. На бочним зидовима приказан је живот покојника, и као паганина и као Хришћанина. На северном зиду приказан је коњаник на белом коњу, а супротно од њега надреална сцена лова.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,85,1),(161,'Народни музеј Пожаревац','sr','Средњи век','непознат','Почетком VII века Византија је била потиснута са Дунава, када Авари и Словени прелазе Дунав и насељавају Балканско полуострво. У IX веку долази до јачања бугарске државе, а бугарска административна власт представља својеврсни отпор Византији. Крајем IX века долази до покрштавања становништва ове области. Из тог времена познат је епископ моравски Агатон. Средином X века спомиње се кнежевина Морава која се налазила у сливу река Велике Мораве, Млаве и Пека.\r\n\r\nУ XII веку Браничево је под влашћу Византије и често се спомиње као узрок византијско-угарских сукоба. Захваљујући свом геостратешком положају, Браничево у XII и првим деценијама XIII века доживљава процват и постаје важан трговачки центар. Деведесетих година XIII века, након победе краља Драгутина и Милутина над татарским вазалима Дрманом и Куделином 1291. године, Браничево улази у састав српске државе. Након смрти цара Душана, Браничевом су 1361. године завладали Растислалићи. \r\n\r\nГодине 1379. кнез Лазар је победио Растислалиће и подигао манастир Горњак (1379-1380). Тада је настао и читав низ других манастира у овом крају. Налазећи се у непосредној близини Смедерева, Браничево има значајну улогу за време Деспотовине. Падом Смедерева 1459. године и ова област потпада под турску власт. Вредна археолошка збирка оруђа, оружја, накита, новца и посуда, сведочи нам о бурним историјским догађајима током овог периода. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,85,1);
/*!40000 ALTER TABLE `exhibition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionaudio`
--

DROP TABLE IF EXISTS `exhibitionaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionaudio` (
  `idexhibitionaudio` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `iditemaudio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionaudio`),
  KEY `fk_pictureaudio_exhibition_idx` (`idexhibition`),
  KEY `fk_exhibition_pictureaudio_idx` (`iditemaudio`),
  CONSTRAINT `fk_exhibition_itemaudio` FOREIGN KEY (`iditemaudio`) REFERENCES `itemaudio` (`iditemaudio`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemaudio_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionaudio`
--

LOCK TABLES `exhibitionaudio` WRITE;
/*!40000 ALTER TABLE `exhibitionaudio` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionimage`
--

DROP TABLE IF EXISTS `exhibitionimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionimage` (
  `idexhibitionimage` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `iditemimage` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionimage`),
  KEY `fk_exhibition_pictureimage_idx` (`iditemimage`),
  KEY `fk_pictureimage_exhibition_idx` (`idexhibition`),
  CONSTRAINT `fk_exhibition_itemimage` FOREIGN KEY (`iditemimage`) REFERENCES `itemimage` (`iditemimage`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemimage_exhibition` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionimage`
--

LOCK TABLES `exhibitionimage` WRITE;
/*!40000 ALTER TABLE `exhibitionimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionlanguages`
--

DROP TABLE IF EXISTS `exhibitionlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionlanguages` (
  `idexhibitionlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(300) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionlanguages`),
  KEY `fk_exhibition_exhibitionlanguages_idx` (`idexhibition`),
  KEY `fk_language_exhibitionlanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_exhibition_exhibitionlanguages` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_language_exhibitionlanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionlanguages`
--

LOCK TABLES `exhibitionlanguages` WRITE;
/*!40000 ALTER TABLE `exhibitionlanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitiontext`
--

DROP TABLE IF EXISTS `exhibitiontext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitiontext` (
  `idexhibitiontext` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `iditemtext` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitiontext`),
  KEY `fk_exhibition_picturetext_idx` (`idexhibition`),
  KEY `fk_picturetext_exhibition_idx` (`iditemtext`),
  CONSTRAINT `fk_exhibition_itemtext` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemtext_exhibition` FOREIGN KEY (`iditemtext`) REFERENCES `itemtext` (`iditemtext`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitiontext`
--

LOCK TABLES `exhibitiontext` WRITE;
/*!40000 ALTER TABLE `exhibitiontext` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitiontext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitiontomuseummap`
--

DROP TABLE IF EXISTS `exhibitiontomuseummap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitiontomuseummap` (
  `idexhibitiontomuseummap` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseummap` int(11) NOT NULL,
  `idexhibition` int(11) NOT NULL,
  PRIMARY KEY (`idexhibitiontomuseummap`),
  KEY `fk_exibition_idx` (`idexhibition`),
  KEY `fk_museummap_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_museummap` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_exhibition` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitiontomuseummap`
--

LOCK TABLES `exhibitiontomuseummap` WRITE;
/*!40000 ALTER TABLE `exhibitiontomuseummap` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitiontomuseummap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exhibitionvideo`
--

DROP TABLE IF EXISTS `exhibitionvideo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exhibitionvideo` (
  `idexhibitionvideo` int(11) NOT NULL AUTO_INCREMENT,
  `idexhibition` int(11) DEFAULT NULL,
  `iditemvideo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idexhibitionvideo`),
  KEY `fk_exhibition_picturevideo_idx` (`idexhibition`),
  KEY `fk_picturevideo_exhibition_idx` (`iditemvideo`),
  CONSTRAINT `fk_exhibition_itemvideo` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_itemvideo_exhibition` FOREIGN KEY (`iditemvideo`) REFERENCES `itemvideo` (`iditemvideo`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exhibitionvideo`
--

LOCK TABLES `exhibitionvideo` WRITE;
/*!40000 ALTER TABLE `exhibitionvideo` DISABLE KEYS */;
/*!40000 ALTER TABLE `exhibitionvideo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `floormap`
--

DROP TABLE IF EXISTS `floormap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `floormap` (
  `idfloormap` int(11) NOT NULL AUTO_INCREMENT,
  `xstart` double DEFAULT NULL,
  `ystart` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `xend` double DEFAULT NULL,
  `yend` double DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `idmuseummap` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `floornumber` int(11) NOT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  PRIMARY KEY (`idfloormap`),
  KEY `idmuseum_idx` (`idmuseum`),
  KEY `fk_floormap_museummap_idx` (`idmuseummap`),
  KEY `fk_floormap_room_idx` (`idroom`),
  CONSTRAINT `fk_floormap_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_floormap_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_floormap_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `floormap`
--

LOCK TABLES `floormap` WRITE;
/*!40000 ALTER TABLE `floormap` DISABLE KEYS */;
INSERT INTO `floormap` VALUES (76,0,0,0,18,12.5,NULL,'Приземље','','',NULL,NULL,NULL,NULL,NULL,197,NULL,85,'sr',NULL,0,NULL,NULL),(77,2,0,0,15,6,NULL,'Централна изложбена сала','Садржај ове просторије чине експонати који припадају раздобљу праисторије и антике, а пронађени су на територијама градова браничевског округа.','','','https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%98%D0%B01_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_2.png',NULL,NULL,NULL,198,125,85,'sr',NULL,0,NULL,NULL),(78,0,6,0,10,12.5,NULL,'Изложбена сала Виминацијум','Садржи експонате пронађене на територији некадашњег града Виминацијум, на територији данашњег Костолца.','','','https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%98%D0%B02_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_2.png',NULL,NULL,NULL,199,126,85,'sr',NULL,0,NULL,NULL),(79,10,6,0,18,12.5,NULL,'Изложбена сала Виминацијум 2','','','','https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%98%D0%B03_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_3.png',NULL,NULL,NULL,200,127,85,'sr',NULL,0,NULL,NULL),(80,0,0,5,18,12.5,NULL,'Први спрат','','',NULL,NULL,NULL,NULL,NULL,201,NULL,85,'sr',NULL,1,NULL,NULL),(81,6,0,5,10,12.5,NULL,'изложбена сала фреске Виминацијума','Ова просторија садржи експонате фрески које су пронађене на просторима некадашњег римског града Виминацијум.','','','https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D1%81%D0%BF%D1%80%D0%B0%D1%822_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_2.png',NULL,NULL,NULL,202,128,85,'sr',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `floormap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institution`
--

DROP TABLE IF EXISTS `institution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `institution` (
  `idinstitution` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `township` varchar(45) DEFAULT NULL,
  `town` varchar(45) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `founder` varchar(45) DEFAULT NULL,
  `foundationdate` datetime DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `idnumber` int(11) DEFAULT NULL,
  `registrationnumber` int(11) DEFAULT NULL,
  `taxid` int(11) DEFAULT NULL,
  `contactphones` mediumtext,
  `emails` mediumtext,
  `webaddresses` mediumtext,
  PRIMARY KEY (`idinstitution`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institution`
--

LOCK TABLES `institution` WRITE;
/*!40000 ALTER TABLE `institution` DISABLE KEYS */;
INSERT INTO `institution` VALUES (13,'Народни музеј Пожаревац','Др. Воје Дулића 10-12','Пожаревац','Пожаревац','Браничевски','Гордан Бојковић','1895-08-08 00:00:00','2003-01-01 00:00:00',6961304,102027650,NULL,'012/223-597','muzejpo@ptt.rs','http://www.muzejpozarevac.rs/index.php/sr-rs/\r\nhttp://topozarevac.rs/');
/*!40000 ALTER TABLE `institution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `item` (
  `iditem` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `author` varchar(100) NOT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `audiourl` tinytext,
  `audiourllocal` tinytext,
  `audiotype` int(11) DEFAULT NULL,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `qr` varchar(45) DEFAULT NULL,
  `additionalimagesurl` mediumtext,
  `additionalimagesurllocal` mediumtext,
  `idmuseum` int(11) DEFAULT NULL,
  `idroom` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `identificationcode` varchar(100) DEFAULT NULL,
  `idbarcodepicture` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditem`),
  UNIQUE KEY `iditem_UNIQUE` (`iditem`),
  KEY `idmuseum_idx` (`idmuseum`),
  KEY `fk_picture_room_idx` (`idroom`),
  KEY `fk_picture_museummap_idx` (`idmuseummap`),
  KEY `fk_picture_barcode_idx` (`idbarcodepicture`),
  CONSTRAINT `fk_item_barcode` FOREIGN KEY (`idbarcodepicture`) REFERENCES `itemimage` (`iditemimage`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_museummap` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_item_room` FOREIGN KEY (`idroom`) REFERENCES `room` (`idroom`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (125,'Народни музеј Пожаревац','sr','Праисторијски предмети','непознат','Први трагови човека на подручју Браничева потичу из времена старијег неолита, око 6000 година п.н.е. Крајем неолита долази до формирања Винчанске цивилизације. Време развијеног бакарног доба карактерише Костолачка култура, названа по налазишту у месту Костолац. Током бронзаног доба, на овом простору егзистира Ватиканска култура, а затим Дубровачко-жутобрдска. Током старијег гвозденог доба овај део Србије насељава старобалканско племе Трибали. Крајем четвртог века п.н.е на ово подручје долазе Келти..',NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,0.69,0,0.5,NULL,NULL,NULL,85,NULL,198,NULL,NULL,'1',NULL),(126,'Народни музеј Пожаревац','sr','Беловоде','непознат','Крајем неолита oкo 4500. године п.н.е на овом подручју формира се Винчанска цивилизација која је међу првима дошла до открића металургије бакра, што је и потврђено на локалитету Беловоде код Петровца. Овде је откривен читав технолошки поступак вађења руде из рудника малахита и азурита, чијим се топљењем добија бакар.Експонат представља. У музеју је изложен низ предмета са овог локалитета као што су велике камене плоче на којима је дробљена руда, посуде за топљење руде, рударски батови и тучкови.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6.38,0.75,0,0.4,NULL,NULL,NULL,85,NULL,198,NULL,NULL,'2',NULL),(127,'Народни музеј Пожаревац','sr','Праисторијска бургија','напознат','Ово софистицирано оруђе потиче из епохе бакарног доба и служило је за бушење различитих врста материјала. Чине га лук са тетивом од животињске коже око кога се обмотава шупља цев. Повлачењем гудала цев, у коју се сипа вода и кварцни песак, окреће се на једну и другу страну. Разарење врши песак који пада под доњи део цеви. Оваква бургија правила је око 2000 обртаја у минути.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,7.84,0.88,0,0.8,NULL,NULL,NULL,85,NULL,198,NULL,NULL,'3',NULL),(128,'Народни музеј Пожаревац','sr','Кличевачки идол','непознат','Глинена фигура паганском божанства нађена у селу Кличевац 1881. године. Представља женску фигуру, највероватније богиње плодности. Потиче из бронзаног доба и припада Дубровачко-жутобрдској кутурној групи. Нађена је у једном гробу поред урне са пепелом покојника. Оригинални примерак је уништен током бомбардовања у време Првог светског рата, када је разорена зграда Народног музеја у Београду, где је примерак чуван. Као први и најзначајнији праисторијски предмет са територије Браничевског округа, Кличевачки идол краси грб града Пожаревца.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,11.47,0.81,0,0.2,NULL,NULL,NULL,85,NULL,198,NULL,NULL,'4',NULL),(129,'Народни музеј Пожаревац','sr','Урне','нема аутора','Око 1600. године п.н.е. се формира Дубровачко - жутобрдска култура, названа по локалитетима Дубовац у Банату и Жуто брдо код Голупца. Ову културу одликују раскошно украшене керамичке посуде. Спаљивање покојника представљало је једини начин сахрањивања у овој култури. Спаљене кости су полагане у ове урне које су често стављане на керамичка постоља. Урне су израђене од печене земље, украшене геометријским орнаментима у белој боји и различитих су облика и димензија.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,13,0.66,0,0.5,NULL,NULL,NULL,85,NULL,198,NULL,NULL,'5',NULL),(130,'Народни музеј Пожаревац','sr','Келти','непознат','На ово подручје крајем четвртог века п.н.е. долазе Келти. Келти су били познати као веште занатлије и врхунски металурзи који израђују квалитетно гвоздено оружје. У пољопривреду уводе употребу гвоздених алатки, штоомогућава брзи развој пољопривреде. За израду керамичких посуда Келти користе ножно витло које омогућава серијску израду. Једна од највећих новина коју Келти доносе на ове просторе је увођење робно-новчане привреде, користећи се у почетку грчко-македонским новцем, а касније и сами кују свој новац.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,14.31,5.31,0,1,NULL,NULL,NULL,85,NULL,198,NULL,NULL,'7',NULL),(131,'Народни музеј Пожаревац','sr','Котао из доба Халштата','непознат','Међи најстарије налазе старијег гвозденог доба код нас спадају два бронзана котла из Дубравице и Старог Костолца. Имају по две дршке које су за реципијент причвршћене носачима у облику људске фиигуре. Котао из Старог Костолца, богато украшен урезаним меандром и троугловима, слично постољу за урну из Кличевца, датује се у период од око 1000 година п.н.е.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,14.25,1.44,0,0.3,NULL,NULL,NULL,85,NULL,198,NULL,NULL,'6',NULL),(132,'Народни музеј Пожаревац','sr','Експонати из античког доба','непознат','Територија данашње Србије потпала је под власт Римљана почетком првог века нове ере. Из геостратешких разлога Римљани на подручју данашњег браничевског округа граде прву утврђену границу на Дунаву - Лимес у оквиру кога су се налазили бројни легијски логори и градска насеља. Највеће градско насеље и центар ове римске провинције, позната као Горња Мезија, био је Виминацијум  на подручју недалеко од данашњег Костолца. Виминацијум је све до краја римског царства имао често пресудну улогу у догађајима значајним по његов опсанак.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,9.66,5.34,0,1,NULL,NULL,NULL,85,NULL,198,NULL,NULL,'8',NULL),(133,'Народни музеј Пожаревац','sr','Клепсидра','непознат','Клепсидра представља антички предмет за мерење кратких временских интервала. Код Римљана је служила за мерење пулса болесника, а била је и у свакодневној кућној употреби. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6.66,6.41,0,1,NULL,NULL,NULL,85,NULL,199,NULL,NULL,'9',NULL),(134,'Народни музеј Пожаревац','sr','Златна минђуша са камејом','непознат','Златна минђуша са камејом на којој је дата представља римског императора Елагабала важан је део културне баштине Србије.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,3.19,6.31,0,0.6,NULL,NULL,NULL,85,NULL,199,NULL,NULL,'10',NULL),(135,'Народни музеј Пожаревац','sr','Античка лампа','непознат','Античка лампа представља предмет са масивном дршком која се завршава са приказом маске за коју се претпоставља да представља римског бога Диониса. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,6.03,11.84,0,0.7,NULL,NULL,NULL,85,NULL,199,NULL,NULL,'11',NULL),(136,'Народни музеј Пожаревац','sr','Фреска гробнице Христов монограм','непознат','Гробница је имала облик куће и била је цела осликана фрескама. Један део гробнице садржи приказ Христовог монограма, знак који се у сну према предању јавио римском цару Константину.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.06,10.56,5,0.7,NULL,NULL,NULL,85,NULL,202,NULL,NULL,'12',NULL),(137,'Народни музеј Пожаревац','sr','Фреска гробнице Два пауна','непознат','Између представе два пауна на једном од делова гробнице, налази се посуда са водом - кантарос.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.06,7.81,5,0.7,NULL,NULL,NULL,85,NULL,202,NULL,NULL,'13',NULL),(138,'Народни музеј Пожаревац','sr','Фреска живот покојника као паганина и као Хришћанина','непознат','Један од бочних делова гробнице садржи фреску на којој је приказан живот покојника као паганина и као Хришћанина.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,9.63,10.72,5,0.4,NULL,NULL,NULL,85,NULL,202,NULL,NULL,'14',NULL),(139,'Народни музеј Пожаревац','sr','Фреска портрет жене из Виминацијума','непознат','Портрет представља младу жену, покојницу, са наглашеним индивидуалним карактеристикама, у достојанственом, стојећем ставу. Покојница је одевена у скупоцену одору, проткану златним нитима. У шаци држи балсамаријум од стакла, антички предмет који је служио за чување мирисних уља и често је проналажен у гробовима.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,9.5,7.88,5,0.5,NULL,NULL,NULL,85,NULL,202,NULL,NULL,'15',NULL),(140,'Народни музеј Пожаревац','sr','Фреска гробнице коњаник','непознат','Приказ слике коњаника на белом коњу.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.91,6.19,5,0.7,NULL,NULL,NULL,85,NULL,202,NULL,NULL,'17',NULL),(141,'Народни музеј Пожаревац','sr','Фреска приказ лова','непознат','Надреални приказ лова.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2.19,12.22,5,0.7,NULL,NULL,NULL,85,NULL,202,NULL,NULL,'18',NULL),(142,'Народни музеј Пожаревац','sr','Средњовековни мач и оклоп','нема аутора','Почетком VII века Византија је била потиснута са Дунава, када Авари и Словени прелазе Дунав и насељавају Балканско полуострво. У IX веку долази до јачања бугарске државе, а бугарска административна власт представља својеврсни отпор Византији. Крајем IX века долази до покрштавања становништва ове области. Из тог времена познат је епископ моравски Агатон. Средином X века спомиње се кнежевина Морава која се налазила у сливу река Велике Мораве, Млаве и Пека.\r\n\r\nУ XII веку Браничево је под влашћу Византије и често се спомиње као узрок византијско-угарских сукоба. Захваљујући свом геостратешком положају, Браничево у XII и првим деценијама XIII века доживљава процват и постаје важан трговачки центар. Деведесетих година XIII века, након победе краља Драгутина и Милутина над татарским вазалима Дрманом и Куделином 1291. године, Браничево улази у састав српске државе. Након смрти цара Душана, Браничевом су 1361. године завладали Растислалићи. \r\n\r\nГодине 1379. кнез Лазар је победио Растислалиће и подигао манастир Горњак (1379-1380). Тада је настао и читав низ других манастира у овом крају. Налазећи се у непосредној близини Смедерева, Браничево има значајну улогу за време Деспотовине. Падом Смедерева 1459. године и ова област потпада под турску власт. Вредна археолошка збирка оруђа, оружја, накита, новца и посуда, сведочи нам о бурним историјским догађајима током овог периода. ',NULL,NULL,NULL,NULL,NULL,NULL,NULL,8.97,11.81,5,0.4,NULL,NULL,NULL,85,NULL,202,NULL,NULL,'19',NULL),(143,'Народни музеј Пожаревац','sr','Круна из средњег века','непознат','Музеј поседује вредну археолошку збирку оруђа, оружја и посуђа, која сведочи о истирији овог периода. Почевице - врста круне коју су носиле властелинке у средњем веку, израђене су од танког лима, позлаћене и украшене приказима грифона и птица.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,17.66,9.09,0,1,NULL,NULL,NULL,85,NULL,200,NULL,NULL,'20',NULL),(144,'Народни музеј Пожаревац','sr','Средњовековни властелински накит','непознат','Минђуше - властелински накит изађен од сребра, коришћен у 13. и 14. веку, осликава моду тадашњег времена. Столовато прстење - масивно прстење, израђено од сребра и бронзе, ношено је приликом обедовања и означавало је припадност одређеним сталежима.',NULL,NULL,NULL,NULL,NULL,NULL,NULL,13.25,6.22,0,0.9,NULL,NULL,NULL,85,NULL,200,NULL,NULL,'21',NULL);
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemaudio`
--

DROP TABLE IF EXISTS `itemaudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemaudio` (
  `iditemaudio` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `audiourl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `audiotype` int(11) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditemaudio`),
  KEY `fk_picture_audio_idx` (`iditem`),
  KEY `fk_exhibition_audio_idx` (`idexhibition`),
  KEY `fk_museum_audio_idx` (`idmuseum`),
  KEY `fk_museummap_audio_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_audio` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_audio` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_audio` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_audio` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemaudio`
--

LOCK TABLES `itemaudio` WRITE;
/*!40000 ALTER TABLE `itemaudio` DISABLE KEYS */;
INSERT INTO `itemaudio` VALUES (37,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566059194/mzy3dkll6scuyw1kr9xq.mp3',NULL,NULL,'mzy3dkll6scuyw1kr9xq','1.1.mp3',NULL,85,NULL),(38,125,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566128858/rytppflrdeq0shqyxmrc.mp3',NULL,NULL,'rytppflrdeq0shqyxmrc','3.1.mp3',NULL,NULL,NULL),(39,126,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566129478/p8twqettbfb1jqlspbjw.mp3',NULL,NULL,'p8twqettbfb1jqlspbjw','4.1.mp3',NULL,NULL,NULL),(40,128,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566129531/ojt7zhpyg7yfkjigpymj.mp3',NULL,NULL,'ojt7zhpyg7yfkjigpymj','6.1.mp3',NULL,NULL,NULL),(41,130,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566129646/l5mnes7jtn9pe6jpdy1n.mp3',NULL,NULL,'l5mnes7jtn9pe6jpdy1n','8.1.mp3',NULL,NULL,NULL),(42,132,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566129808/mlb5qec9e6xqvzgcgrqj.mp3',NULL,NULL,'mlb5qec9e6xqvzgcgrqj','9.1.mp3',NULL,NULL,NULL),(43,133,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566129978/zkmzqoeaxtxgt80rabtj.mp3',NULL,NULL,'zkmzqoeaxtxgt80rabtj','10.1.mp3',NULL,NULL,NULL),(44,134,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566130010/gre77obnvzzaktyf6rsj.mp3',NULL,NULL,'gre77obnvzzaktyf6rsj','10.2.mp3',NULL,NULL,NULL),(45,135,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566130147/efx5yu97sfgcb3yoh1qp.mp3',NULL,NULL,'efx5yu97sfgcb3yoh1qp','10.1.mp3',NULL,NULL,NULL),(46,139,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566130259/hfyjneam8g6ejybqzomk.mp3',NULL,NULL,'hfyjneam8g6ejybqzomk','11.1.mp3',NULL,NULL,NULL),(47,136,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566130441/w9vtasmxrf6v2v0lsiij.mp3',NULL,NULL,'w9vtasmxrf6v2v0lsiij','11.2.mp3',NULL,NULL,NULL),(48,140,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566130586/ptiigwofokbuts54n4mi.mp3',NULL,NULL,'ptiigwofokbuts54n4mi','11.2.mp3',NULL,NULL,NULL),(49,142,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566130688/esuxecwcugfsotxujhkz.mp3',NULL,NULL,'esuxecwcugfsotxujhkz','13.1.mp3',NULL,NULL,NULL);
/*!40000 ALTER TABLE `itemaudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemimage`
--

DROP TABLE IF EXISTS `itemimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemimage` (
  `iditemimage` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `imageurl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  `clientImagePriority` int(11) DEFAULT NULL,
  `client_image_priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditemimage`),
  KEY `fk_picture_image_idx` (`iditem`),
  KEY `fk_exhibition_image_idx` (`idexhibition`),
  KEY `fk_museum_image_idx` (`idmuseum`),
  KEY `fk_museummap_image_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_image` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_image` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_image` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_image` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemimage`
--

LOCK TABLES `itemimage` WRITE;
/*!40000 ALTER TABLE `itemimage` DISABLE KEYS */;
INSERT INTO `itemimage` VALUES (169,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565956847/zlhhafx70ovfs2t1efne.jpg',NULL,'zlhhafx70ovfs2t1efne',NULL,85,NULL,NULL,NULL),(170,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565956930/lbz0z4fgabpaonh4rs6a.jpg',NULL,'lbz0z4fgabpaonh4rs6a',NULL,85,NULL,NULL,NULL),(171,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565957141/qxwol5rqty9iywl3vhrp.jpg',NULL,'qxwol5rqty9iywl3vhrp',NULL,85,NULL,NULL,NULL),(172,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565957196/n2warzoajfkeb5mieqnb.jpg',NULL,'n2warzoajfkeb5mieqnb',NULL,85,NULL,NULL,NULL),(178,NULL,'https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D1%81%D0%BF%D1%80%D0%B0%D1%821_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_2.png',NULL,'i8q3ubizwedljlx9fdjj',NULL,NULL,197,NULL,NULL),(179,NULL,'https://res.cloudinary.com/dktuxe18z/image/upload/v1566940847/%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%98%D0%B04_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_2.png',NULL,'vn8xxatgegalukdkxlex',NULL,NULL,201,NULL,NULL),(180,NULL,'https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%98%D0%B01_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_2.png',NULL,'vzqquka45csankzglsvf',NULL,NULL,198,NULL,NULL),(181,NULL,'https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%98%D0%B02_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_2.png',NULL,'fwip3l1rcyxmvzmvf9cj',NULL,NULL,199,NULL,NULL),(182,NULL,'https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D0%BF%D1%80%D0%BE%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%98%D0%B03_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_3.png',NULL,'jqirbzp4ncvzo5ieamnl',NULL,NULL,200,NULL,NULL),(183,NULL,'https://res.cloudinary.com/dktuxe18z/image/upload/v1566939064/%D1%81%D0%BF%D1%80%D0%B0%D1%822_%D1%82%D0%BB%D0%BE%D1%86%D1%80%D1%82_2.png',NULL,'l3xm8g7aivifx5y8h4f3',NULL,NULL,202,NULL,NULL),(184,125,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565975559/mpcgxprs1kb7okqfb9ma.jpg',NULL,'mpcgxprs1kb7okqfb9ma',NULL,NULL,NULL,NULL,NULL),(185,125,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565975562/kfkqkoeaaj3cgnhnokig.jpg',NULL,'kfkqkoeaaj3cgnhnokig',NULL,NULL,NULL,NULL,NULL),(186,126,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565976220/g2qpnt5ibr3osikjgyvm.png',NULL,'g2qpnt5ibr3osikjgyvm',NULL,NULL,NULL,NULL,NULL),(187,126,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565976222/tarnxwlm5v4kyhvtex4s.png',NULL,'tarnxwlm5v4kyhvtex4s',NULL,NULL,NULL,NULL,NULL),(188,127,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565976655/eaf9cwatywufebdeinmc.jpg',NULL,'eaf9cwatywufebdeinmc',NULL,NULL,NULL,NULL,NULL),(189,128,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565977185/qszpmka6iibwykfvqsid.jpg',NULL,'qszpmka6iibwykfvqsid',NULL,NULL,NULL,NULL,NULL),(190,129,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565978425/krzq74ghspf7ts5w5o06.jpg',NULL,'krzq74ghspf7ts5w5o06',NULL,NULL,NULL,NULL,NULL),(191,129,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565978426/lxiuqxbdxfudxxlweafr.jpg',NULL,'lxiuqxbdxfudxxlweafr',NULL,NULL,NULL,NULL,NULL),(192,129,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565978427/qbvbckxldj4tqe9ismcu.jpg',NULL,'qbvbckxldj4tqe9ismcu',NULL,NULL,NULL,NULL,NULL),(193,130,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565978882/l0ufneusu0eley7hszw0.jpg',NULL,'l0ufneusu0eley7hszw0',NULL,NULL,NULL,NULL,NULL),(194,131,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565979311/atvrtozlb9lhwbbbvwef.jpg',NULL,'atvrtozlb9lhwbbbvwef',NULL,NULL,NULL,NULL,NULL),(197,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565979818/epqh3u8ngjo8smsdgwkr.jpg',NULL,'epqh3u8ngjo8smsdgwkr',154,NULL,NULL,NULL,NULL),(198,132,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565980788/xjjw5fht0ka536savmmj.jpg',NULL,'xjjw5fht0ka536savmmj',NULL,NULL,NULL,NULL,NULL),(199,132,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565980790/gkbjd5odh2jyehyqqze7.jpg',NULL,'gkbjd5odh2jyehyqqze7',NULL,NULL,NULL,NULL,NULL),(200,132,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565980792/b4ntmebovjv297mjogjz.jpg',NULL,'b4ntmebovjv297mjogjz',NULL,NULL,NULL,NULL,NULL),(203,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565980889/user28vffel4pqvtae5y.jpg',NULL,'user28vffel4pqvtae5y',155,NULL,NULL,NULL,NULL),(206,133,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565981623/rrz3cokhtjjfoyjzu4ys.jpg',NULL,'rrz3cokhtjjfoyjzu4ys',NULL,NULL,NULL,NULL,NULL),(207,134,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565981858/b7xze6iny7o2qs65yibe.jpg',NULL,'b7xze6iny7o2qs65yibe',NULL,NULL,NULL,NULL,NULL),(208,135,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565981993/om0esvk7idhbn5jakshk.jpg',NULL,'om0esvk7idhbn5jakshk',NULL,NULL,NULL,NULL,NULL),(209,136,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565982680/i78pxzbsuxc72qg97him.jpg',NULL,'i78pxzbsuxc72qg97him',NULL,NULL,NULL,NULL,NULL),(210,137,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565982847/cqo3lpmopwg0nnyuqlnl.png',NULL,'cqo3lpmopwg0nnyuqlnl',NULL,NULL,NULL,NULL,NULL),(212,138,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565983214/tc2h05cn0qomrwkuqrge.png',NULL,'tc2h05cn0qomrwkuqrge',NULL,NULL,NULL,NULL,NULL),(213,139,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565983367/c06pghwszmipezvprmmz.png',NULL,'c06pghwszmipezvprmmz',NULL,NULL,NULL,NULL,NULL),(214,140,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565983541/cqludy4vb2etd4nbcdij.jpg',NULL,'cqludy4vb2etd4nbcdij',NULL,NULL,NULL,NULL,NULL),(215,141,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565983671/wnt7sgl2pfta4o5g8vtq.png',NULL,'wnt7sgl2pfta4o5g8vtq',NULL,NULL,NULL,NULL,NULL),(216,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1565983982/v99wkxz2olta3d55sgnr.png',NULL,'v99wkxz2olta3d55sgnr',159,NULL,NULL,NULL,NULL),(217,142,'http://res.cloudinary.com/dktuxe18z/image/upload/v1566039295/ioro8dit3zxk9tsongto.jpg',NULL,'ioro8dit3zxk9tsongto',NULL,NULL,NULL,NULL,NULL),(218,143,'http://res.cloudinary.com/dktuxe18z/image/upload/v1566040701/g6imhwtn0gw5urrfmaue.jpg',NULL,'g6imhwtn0gw5urrfmaue',NULL,NULL,NULL,NULL,NULL),(219,143,'http://res.cloudinary.com/dktuxe18z/image/upload/v1566040706/bfdxoqd5etiugbatclsd.jpg',NULL,'bfdxoqd5etiugbatclsd',NULL,NULL,NULL,NULL,NULL),(220,144,'http://res.cloudinary.com/dktuxe18z/image/upload/v1566041683/w4zv94wru2bt2wbzcikx.jpg',NULL,'w4zv94wru2bt2wbzcikx',NULL,NULL,NULL,NULL,NULL),(221,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1566042276/ohbkxeb1e2ihmczufbhy.jpg',NULL,'ohbkxeb1e2ihmczufbhy',161,NULL,NULL,NULL,NULL),(226,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1567274717/ouzrgvepweq1owukrir6.jpg',NULL,'ouzrgvepweq1owukrir6',NULL,85,NULL,NULL,2),(227,NULL,'http://res.cloudinary.com/dktuxe18z/image/upload/v1567275752/kkudmcx2puxxjclgidhv.jpg',NULL,'kkudmcx2puxxjclgidhv',NULL,85,NULL,NULL,1);
/*!40000 ALTER TABLE `itemimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemlanguages`
--

DROP TABLE IF EXISTS `itemlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemlanguages` (
  `iditemlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`iditemlanguages`),
  KEY `fk_picture_picturelanguages_idx` (`iditem`),
  KEY `fk_language_picturelanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_itemlanguage_language` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_language_itemlanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemlanguages`
--

LOCK TABLES `itemlanguages` WRITE;
/*!40000 ALTER TABLE `itemlanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemtext`
--

DROP TABLE IF EXISTS `itemtext`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemtext` (
  `iditemtext` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `text` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`iditemtext`),
  KEY `fk_picture_idx` (`iditem`),
  CONSTRAINT `fk_item_text` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemtext`
--

LOCK TABLES `itemtext` WRITE;
/*!40000 ALTER TABLE `itemtext` DISABLE KEYS */;
INSERT INTO `itemtext` VALUES (40,125,'Први трагови човека на подручју Браничева потичу из времена старијег неолита, око 6000 година п.н.е. Крајем неолита долази до формирања Винчанске цивилизације. Време развијеног бакарног доба карактерише Костолачка култура, названа по налазишту у месту Костолац. Током бронзаног доба, на овом простору егзистира Ватиканска култура, а затим Дубровачко-жутобрдска. Током старијег гвозденог доба овај део Србије насељава старобалканско племе Трибали. Крајем четвртог века п.н.е на ово подручје долазе Келти.',NULL),(41,126,'Пећ за топљење руда и добијање бакра.',NULL),(42,127,'Ово софистицирано оруђе потиче из епохе бакарног доба и служило је за бушење различитих врста материјала. Чине га лук са тетивом од животињске коже око кога се обмотава шупља цев. Повлачењем гудала цев, у коју се сипа вода и кварцни песак, окреће се на једну и другу страну. Разарење врши песак који пада под доњи део цеви. Оваква бургија правила је око 2000 обртаја у минути.',NULL),(43,128,'Глинена фигура паганском божанства нађена у селу Кличевац 1881. године. Представља женску фигуру, највероватније богиње плодности. Потиче из бронзаног доба и припада Дубровачко-жутобрдској кутурној групи. Нађена је у једном гробу поред урне са пепелом покојника. Оригинални примерак је уништен током бомбардовања у време Првог светског рата, када је разорена зграда Народног музеја у Београду, где је примерак чуван. Као први и најзначајнији праисторијски предмет са територије Браничевског округа, Кличевачки идол краси грб града Пожаревца.',NULL),(44,129,'Око 1600. године п.н.е. се формира Дубровачко - жутобрдска култура, названа по локалитетима Дубовац у Банату и Жуто брдо код Голупца. Ову културу одликују раскошно украшене керамичке посуде. Спаљивање покојника представљало је једини начин сахрањивања у овој култури. Спаљене кости су полагане у ове урне које су често стављане на керамичка постоља. Урне су израђене од печене земље, украшене геометријским орнаментима у белој боји и различитих су облика и димензија.',NULL),(45,130,'На ово подручје крајем четвртог века п.н.е. долазе Келти. Келти су били познати као веште занатлије и врхунски металурзи који израђују квалитетно гвоздено оружје. У пољопривреду уводе употребу гвоздених алатки, штоомогућава брзи развој пољопривреде. За израду керамичких посуда Келти користе ножно витло које омогућава серијску израду. Једна од највећих новина коју Келти доносе на ове просторе је увођење робно-новчане привреде, користећи се у почетку грчко-македонским новцем, а касније и сами кују свој новац.',NULL),(46,131,'Међи најстарије налазе старијег гвозденог доба код нас спадају два бронзана котла из Дубравице и Старог Костолца. Имају по две дршке које су за реципијент причвршћене носачима у облику људске фиигуре. Котао из Старог Костолца, богато украшен урезаним меандром и троугловима, слично постољу за урну из Кличевца, датује се у период од око 1000 година п.н.е.',NULL),(47,132,'Територија данашње Србије потпала је под власт Римљана почетком првог века нове ере. Из геостратешких разлога Римљани на подручју данашњег браничевског округа граде прву утврђену границу на Дунаву - Лимес у оквиру кога су се налазили бројни легијски логори и градска насеља. Највеће градско насеље и центар ове римске провинције, позната као Горња Мезија, био је Виминацијум  на подручју недалеко од данашњег Костолца. Виминацијум је све до краја римског царства имао често пресудну улогу у догађајима значајним по његов опсанак.',NULL),(48,133,'Клепсидра представља антички предмет за мерење кратких временских интервала. Код Римљана је служила за мерење пулса болесника, а била је и у свакодневној кућној употреби. ',NULL),(49,134,'Златна минђуша са камејом на којој је дата представља римског императора Елагабала важан је део културне баштине Србије.',NULL),(50,135,'Античка лампа представља предмет са масивном дршком која се завршава са приказом маске за коју се претпоставља да представља римског бога Диониса. ',NULL),(51,136,'Гробница је имала облик куће и била је цела осликана фрескама. Један део гробнице садржи приказ Христовог монограма, знак који се у сну према предању јавио римском цару Константину.',NULL),(52,137,'Између представе два пауна на једном од делова гробнице, налази се посуда са водом - кантарос.',NULL),(55,140,'Приказ слике коњаника на белом коњу.',NULL),(57,141,'Надреални приказ лова.',NULL),(60,142,'Почетком VII века Византија је била потиснута са Дунава, када Авари и Словени прелазе Дунав и насељавају Балканско полуострво. У IX веку долази до јачања бугарске државе, а бугарска административна власт представља својеврсни отпор Византији. Крајем IX века долази до покрштавања становништва ове области. Из тог времена познат је епископ моравски Агатон. Средином X века спомиње се кнежевина Морава која се налазила у сливу река Велике Мораве, Млаве и Пека.\r\n\r\nУ XII веку Браничево је под влашћу Византије и често се спомиње као узрок византијско-угарских сукоба. Захваљујући свом геостратешком положају, Браничево у XII и првим деценијама XIII века доживљава процват и постаје важан трговачки центар. Деведесетих година XIII века, након победе краља Драгутина и Милутина над татарским вазалима Дрманом и Куделином 1291. године, Браничево улази у састав српске државе. Након смрти цара Душана, Браничевом су 1361. године завладали Растислалићи. \r\n\r\nГодине 1379. кнез Лазар је победио Растислалиће и подигао манастир Горњак (1379-1380). Тада је настао и читав низ других манастира у овом крају. Налазећи се у непосредној близини Смедерева, Браничево има значајну улогу за време Деспотовине. Падом Смедерева 1459. године и ова област потпада под турску власт. Вредна археолошка збирка оруђа, оружја, накита, новца и посуда, сведочи нам о бурним историјским догађајима током овог периода. ',NULL),(61,143,'Музеј поседује вредну археолошку збирку оруђа, оружја и посуђа, која сведочи о истирији овог периода. Почевице - врста круне коју су носиле властелинке у средњем веку, израђене су од танког лима, позлаћене и украшене приказима грифона и птица.',NULL),(62,138,'Један од бочних делова гробнице садржи фреску на којој је приказан живот покојника као паганина и као Хришћанина.',NULL),(63,139,'Портрет представља младу жену, покојницу, са наглашеним индивидуалним карактеристикама, у достојанственом, стојећем ставу. Покојница је одевена у скупоцену одору, проткану златним нитима. У шаци држи балсамаријум од стакла, антички предмет који је служио за чување мирисних уља и често је проналажен у гробовима.',NULL),(64,144,'Минђуше - властелински накит изађен од сребра, коришћен у 13. и 14. веку, осликава моду тадашњег времена. Столовато прстење - масивно прстење, израђено од сребра и бронзе, ношено је приликом обедовања и означавало је припадност одређеним сталежима.',NULL);
/*!40000 ALTER TABLE `itemtext` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemtoexhibition`
--

DROP TABLE IF EXISTS `itemtoexhibition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemtoexhibition` (
  `iditemtoexhibition` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) NOT NULL,
  `idexhibition` int(11) NOT NULL,
  PRIMARY KEY (`iditemtoexhibition`),
  KEY `fk_exebition_picture_idx` (`idexhibition`),
  KEY `fk_picture_exhibition_idx` (`iditem`),
  CONSTRAINT `fk_exhibition_item` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_exhibition` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemtoexhibition`
--

LOCK TABLES `itemtoexhibition` WRITE;
/*!40000 ALTER TABLE `itemtoexhibition` DISABLE KEYS */;
INSERT INTO `itemtoexhibition` VALUES (131,141,159),(132,136,159),(133,137,159),(134,138,159),(135,139,159),(136,140,159),(166,125,154),(167,126,154),(168,127,154),(169,128,154),(170,129,154),(171,130,154),(172,131,154),(173,136,155),(174,137,155),(175,138,155),(176,139,155),(177,140,155),(178,141,155),(179,132,155),(180,133,155),(181,134,155),(182,135,155),(183,142,161),(184,143,161),(185,144,161);
/*!40000 ALTER TABLE `itemtoexhibition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemvideo`
--

DROP TABLE IF EXISTS `itemvideo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `itemvideo` (
  `iditemvideo` int(11) NOT NULL AUTO_INCREMENT,
  `iditem` int(11) DEFAULT NULL,
  `videourl` longtext,
  `islocal` tinyint(4) DEFAULT NULL,
  `serverid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `idexhibition` int(11) DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `idmuseummap` int(11) DEFAULT NULL,
  PRIMARY KEY (`iditemvideo`),
  KEY `fk_picture_video_idx` (`iditem`),
  KEY `fk_exhibition_video_idx` (`idexhibition`),
  KEY `fk_museum_video_idx` (`idmuseum`),
  KEY `fk_museummap_video_idx` (`idmuseummap`),
  CONSTRAINT `fk_exhibition_video` FOREIGN KEY (`idexhibition`) REFERENCES `exhibition` (`idexhibition`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_video` FOREIGN KEY (`iditem`) REFERENCES `item` (`iditem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_video` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_video` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemvideo`
--

LOCK TABLES `itemvideo` WRITE;
/*!40000 ALTER TABLE `itemvideo` DISABLE KEYS */;
INSERT INTO `itemvideo` VALUES (43,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566126858/ezhekj9aez8oeaxtdxwu.mp4',NULL,'ezhekj9aez8oeaxtdxwu','y2mate.com - narodni',NULL,85,NULL),(44,NULL,'https://www.youtube.com/watch?v=gtodHdfruzQ',NULL,NULL,NULL,NULL,NULL,197),(45,NULL,'http://res.cloudinary.com/dktuxe18z/video/upload/v1566129309/l2nhspy8whitqdnwnpsi.mp4',NULL,'l2nhspy8whitqdnwnpsi','y2mate.com - narodni',NULL,NULL,198);
/*!40000 ALTER TABLE `itemvideo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `languages` (
  `idlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `languagename` varchar(45) DEFAULT NULL,
  `languageabbreviation` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idlanguages`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (3,'шпански','es'),(4,'немачки','de'),(5,'енглески','en');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museum`
--

DROP TABLE IF EXISTS `museum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museum` (
  `idmuseum` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` tinytext,
  `imageurllocal` tinytext,
  `audiourl` tinytext,
  `audiourllocal` tinytext,
  `videourl` tinytext,
  `videourllocal` tinytext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `qr` tinytext,
  `idinstitution` int(11) DEFAULT NULL,
  `institution` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idmuseum`),
  KEY `fk_museum_institution_idx` (`idinstitution`),
  CONSTRAINT `fk_museum_institution` FOREIGN KEY (`idinstitution`) REFERENCES `institution` (`idinstitution`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museum`
--

LOCK TABLES `museum` WRITE;
/*!40000 ALTER TABLE `museum` DISABLE KEYS */;
INSERT INTO `museum` VALUES (85,'Народни музеј Пожаревац - главна зграда','sr','Установе попут музеја које се претежно баве прошлошћу, по природи ствари имају и своју сопствену историју. Сакупљање старина са подручја Браничева датира још из прве половине 19. века, с обзиром да је читав пожаревачки крај био богат многим остацима старина. После вишегодишњих сакупљања старина, првенствено археолошких предмета (нумизматике) из Виминацијума, при Пожаревачкој гимназији је 1895. установљена музејска збирка. Правила Музеја донета су 22. јануара 1896. године. У Србији је до тада постојао само Народни музеј у Београду, основан 1844. године. Оснивање музеја у Србији образложило је Попечитељство просвештенија у свом оснивачком акту 10. маја 1844. године потребом обавезног сакупљања старина и то упутило свим окружним начелницима. Установе попут музеја које се претежно баве прошлошћу, по природи ствари имају и своју сопствену историју. Сакупљање старина са подручја Браничева датира још из прве половине 19. века, с обзиром да је читав пожаревачки крај био богат многим остацима старина. После вишегодишњих сакупљања старина, првенствено археолошких предмета (нумизматике) из Виминацијума, при Пожаревачкој гимназији је 1895. установљена музејска збирка. Правила Музеја донета су 22. јануара 1896. године.\r\nНекадашњи власник куће која је данас главна зграда Нардоног музеја у Пожаревцу је адвокат Крста Д. Николајевић.','https://pozarevac.rs/narodni-muzej-pozarevac/',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,13,'Народни музеј Пожаревац');
/*!40000 ALTER TABLE `museum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museumadditional`
--

DROP TABLE IF EXISTS `museumadditional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museumadditional` (
  `idmuseumadditional` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `distance` double DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `town` varchar(45) DEFAULT NULL,
  `township` varchar(45) DEFAULT NULL,
  `district` varchar(45) DEFAULT NULL,
  `buildyear` int(11) DEFAULT NULL,
  `openyear` int(11) DEFAULT NULL,
  `aims` mediumtext,
  `contactphones` mediumtext,
  PRIMARY KEY (`idmuseumadditional`),
  KEY `fk_museum_additional_idx` (`idmuseum`),
  CONSTRAINT `fk_museum_additional` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museumadditional`
--

LOCK TABLES `museumadditional` WRITE;
/*!40000 ALTER TABLE `museumadditional` DISABLE KEYS */;
INSERT INTO `museumadditional` VALUES (12,85,'музеј',44.6215304,21.1721642,NULL,0,'Др. Воје Дулића 10-12','12000','Пожаревац','Пожаревац','Браничевски',1850,1895,'делатност музеја, галерија и збирки','012/223-597');
/*!40000 ALTER TABLE `museumadditional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museumlanguages`
--

DROP TABLE IF EXISTS `museumlanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museumlanguages` (
  `idmuseumlanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseum` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmuseumlanguages`),
  KEY `fk_museum_museumlanguages_idx` (`idmuseum`),
  KEY `fk_language_museumlanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_language_museumlanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museum_museumlanguages` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museumlanguages`
--

LOCK TABLES `museumlanguages` WRITE;
/*!40000 ALTER TABLE `museumlanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `museumlanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museummap`
--

DROP TABLE IF EXISTS `museummap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museummap` (
  `idmuseummap` int(11) NOT NULL AUTO_INCREMENT,
  `institution` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `room` varchar(45) DEFAULT NULL,
  `xstart` double DEFAULT NULL,
  `ystart` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `isfloor` tinyint(4) DEFAULT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `isstaircase` tinyint(4) DEFAULT NULL,
  `xend` double DEFAULT NULL,
  `yend` double DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idmuseummap`),
  KEY `fk_museummap_museum_idx` (`idmuseum`),
  CONSTRAINT `FKhu2t65f6debhyfmh1nr3tdb1g` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museummap`
--

LOCK TABLES `museummap` WRITE;
/*!40000 ALTER TABLE `museummap` DISABLE KEYS */;
INSERT INTO `museummap` VALUES (197,'Народни музеј Пожаревац','sr','Приземље','','',NULL,NULL,0,0,0,NULL,85,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,18,12.5,NULL),(198,'Народни музеј Пожаревац','sr','Централна изложбена сала','Садржај ове просторије чине експонати који припадају раздобљу праисторије и антике, а пронађени су на територијама градова браничевског округа.','','',NULL,2,0,0,NULL,85,NULL,NULL,0,NULL,NULL,NULL,NULL,0,15,6,NULL),(199,'Народни музеј Пожаревац','sr','Изложбена сала Виминацијум 1','Садржи експонате пронађене на територији некадашњег града Виминацијум, на територији данашњег Костолца.','','',NULL,0,6,0,NULL,85,NULL,NULL,0,NULL,NULL,NULL,NULL,0,10,12.5,NULL),(200,'Народни музеј Пожаревац','sr','Изложбена сала Виминацијум 2','Садржи експонате пронађене на територији некадашњег града Виминацијум, на територији данашњег Костолца.','','',NULL,10,6,0,NULL,85,NULL,NULL,0,NULL,NULL,NULL,NULL,0,18,12.5,NULL),(201,'Народни музеј Пожаревац','sr','Први спрат','','',NULL,NULL,0,0,5,NULL,85,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,18,12.5,NULL),(202,'Народни музеј Пожаревац','sr','изложбена сала фреске Виминацијума','Ова просторија садржи експонате фрески које су пронађене на просторима некадашњег римског града Виминацијум.','','',NULL,6,0,5,NULL,85,NULL,NULL,0,NULL,NULL,NULL,NULL,0,10,12.5,NULL);
/*!40000 ALTER TABLE `museummap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `museummaplanguages`
--

DROP TABLE IF EXISTS `museummaplanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `museummaplanguages` (
  `idmuseummaplanguages` int(11) NOT NULL AUTO_INCREMENT,
  `idmuseummap` int(11) DEFAULT NULL,
  `idlanguage` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idmuseummaplanguages`),
  KEY `fk_museummap_museummaplanguages_idx` (`idmuseummap`),
  KEY `fk_language_museummaplanguages_idx` (`idlanguage`),
  CONSTRAINT `fk_language_museummaplanguages` FOREIGN KEY (`idlanguage`) REFERENCES `languages` (`idlanguages`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_museummap_museummaplanguages` FOREIGN KEY (`idmuseummap`) REFERENCES `museummap` (`idmuseummap`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `museummaplanguages`
--

LOCK TABLES `museummaplanguages` WRITE;
/*!40000 ALTER TABLE `museummaplanguages` DISABLE KEYS */;
/*!40000 ALTER TABLE `museummaplanguages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role` (
  `idrole` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `room` (
  `idroom` int(11) NOT NULL AUTO_INCREMENT,
  `institutuion` varchar(45) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` longtext,
  `texturl` tinytext,
  `texturllocal` tinytext,
  `imageurl` mediumtext,
  `imageurllocal` mediumtext,
  `videourl` mediumtext,
  `videourllocal` mediumtext,
  `xcoordinate` double DEFAULT NULL,
  `ycoordinate` double DEFAULT NULL,
  `zcoordinate` double DEFAULT NULL,
  `radius` double DEFAULT NULL,
  `idmuseum` int(11) DEFAULT NULL,
  `audiourl` mediumtext,
  `audiourllocal` mediumtext,
  PRIMARY KEY (`idroom`),
  KEY `fk_room_museum_idx` (`idmuseum`),
  CONSTRAINT `fk_room_museum` FOREIGN KEY (`idmuseum`) REFERENCES `museum` (`idmuseum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (125,'Народни музеј Пожаревац','sr','Централна изложбена сала','Садржај ове просторије чине експонати који припадају раздобљу праисторије и антике, а пронађени су на територијама градова браничевског округа.','','',NULL,NULL,NULL,NULL,2,0,0,NULL,85,NULL,NULL),(126,'Народни музеј Пожаревац','sr','Изложбена сала Виминацијум 1','Садржи експонате пронађене на територији некадашњег града Виминацијум, на територији данашњег Костолца.','','',NULL,NULL,NULL,NULL,0,6,0,NULL,85,NULL,NULL),(127,'Народни музеј Пожаревац','sr','Изложбена сала Виминацијум 2','Садржи експонате пронађене на територији некадашњег града Виминацијум, на територији данашњег Костолца.','','',NULL,NULL,NULL,NULL,10,6,0,NULL,85,NULL,NULL),(128,'Народни музеј Пожаревац','sr','изложбена сала фреске Виминацијума','Ова просторија садржи експонате фрески које су пронађене на просторима некадашњег римског града Виминацијум.','','',NULL,NULL,NULL,NULL,6,0,5,NULL,85,NULL,NULL);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomtoroom`
--

DROP TABLE IF EXISTS `roomtoroom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roomtoroom` (
  `idroomtoroom` int(11) NOT NULL AUTO_INCREMENT,
  `idroom1` int(11) NOT NULL,
  `idroom2` int(11) NOT NULL,
  PRIMARY KEY (`idroomtoroom`),
  KEY `fk_room1_room2_idx` (`idroom1`),
  KEY `fk_room2_room1_idx` (`idroom2`),
  CONSTRAINT `fk_room1_room2` FOREIGN KEY (`idroom1`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_room2_room1` FOREIGN KEY (`idroom2`) REFERENCES `room` (`idroom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomtoroom`
--

LOCK TABLES `roomtoroom` WRITE;
/*!40000 ALTER TABLE `roomtoroom` DISABLE KEYS */;
INSERT INTO `roomtoroom` VALUES (122,127,126),(124,128,126),(129,126,128),(133,125,126);
/*!40000 ALTER TABLE `roomtoroom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin','admin','admin',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrole`
--

DROP TABLE IF EXISTS `userrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `userrole` (
  `iduserrole` int(11) NOT NULL AUTO_INCREMENT,
  `idrole` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduserrole`),
  KEY `fk_roles_users_idx` (`idrole`),
  KEY `fk_users_roles_idx` (`iduser`),
  CONSTRAINT `fk_roles_users` FOREIGN KEY (`idrole`) REFERENCES `role` (`idrole`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_roles` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrole`
--

LOCK TABLES `userrole` WRITE;
/*!40000 ALTER TABLE `userrole` DISABLE KEYS */;
INSERT INTO `userrole` VALUES (1,1,1);
/*!40000 ALTER TABLE `userrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'master_app'
--

--
-- Dumping routines for database 'master_app'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-31 20:52:49
